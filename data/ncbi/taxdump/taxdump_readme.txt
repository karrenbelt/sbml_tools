This directory contains the following NCBI Taxonomy database dump files:

   new_taxdump.zip
   new_taxdump.tar.Z
   new_taxdump.tar.gz

All these files containes exactly the same information and are arranged so
for the convenience of unpacking them on various operating environments.
In addition there are files:

   new_taxdump.zip.md5
   new_taxdump.tar.Z.md5
   new_taxdump.tar.gz.md5

which contain MD5 sums for the corresponding archive files. These files
might be used to check correctness of the download of corresponding 
archive file.

new_taxdump.zip
---------------

Is intended for zip-capable utilities such as pkunzip, unzip, and WinZip.
These utilities are widely available in almost all operating environments.
To unpack it command-line pkunzip and unzip:

        pkunzip new_taxdump.zip
or
        unzip new_taxdump.zip

Note: pkunzip and/or unzip executables must be in the executable search path
and new_taxdump.zip must be in the current directory. Files will be unzipped into
current directory. For desired dump files placement and more please refer to
the manual and/or option descriptions of pkunzip and unzip utilities.

new_taxdump.tar.Z
-----------------

This file is to be unpacked by uncompress utility and subsequent tar 
archiver. These utilities are usually used in UNIX-like environment. 
Unpacking instructions follows:

           uncompress -c new_taxdump.tar.Z | tar xf - 

new_taxdump.tar.gz
------------------

This file is to be unpacked by GNU unzip utility and subsequent tar 
archiver. These utilities are usually used in UNIX-like environment. 
Unpacking instructions follows:

           gunzip -c new_taxdump.tar.gz | tar xf - 

The content of the archive
--------------------------

It may look like this:

citations.dmp
delnodes.dmp
division.dmp
gencode.dmp
typeoftype.dmp
merged.dmp
names.dmp
nodes.dmp
host.dmp
typematerial.dmp
rankedlineage.dmp
fullnamelineage.dmp
taxidlineage.dmp
readme.txt

The readme.txt file gives a brief description of *.dmp files. These files
contain taxonomic information and are briefly described below. Each of the
files store one record in the single line that are delimited by "\t|\n"
(tab, vertical bar, and newline) characters. Each record consists of one 
or more fields delimited by "\t|\t" (tab, vertical bar, and tab) characters.
The brief description of field position and meaning for each file follows.

nodes.dmp
---------
This file represents taxonomy nodes. The description for each node includes 
the following fields:

	tax_id					-- node id in GenBank taxonomy database
 	parent tax_id				-- parent node id in GenBank taxonomy database
 	rank					-- rank of this node (superkingdom, kingdom, ...) 
 	embl code				-- locus-name prefix; not unique
 	division id				-- see division.dmp file
 	inherited div flag  (1 or 0)		-- 1 if node inherits division from parent
 	genetic code id				-- see gencode.dmp file
 	inherited GC  flag  (1 or 0)		-- 1 if node inherits genetic code from parent
 	mitochondrial genetic code id		-- see gencode.dmp file
 	inherited MGC flag  (1 or 0)		-- 1 if node inherits mitochondrial gencode from parent
 	GenBank hidden flag (1 or 0)            -- 1 if name is suppressed in GenBank entry lineage
 	hidden subtree root flag (1 or 0)       -- 1 if this subtree has no sequence data yet
 	comments				-- free-text comments and citations
        plastid genetic code id                 -- see gencode.dmp file
        inherited PGC flag  (1 or 0)            -- 1 if node inherits plastid gencode from parent
	specified_species			-- 1 if species in the node's lineage has formal name
        hydrogenosome genetic code id           -- see gencode.dmp file
        inherited HGC flag  (1 or 0)            -- 1 if node inherits hydrogenosome gencode from parent

names.dmp
---------
Taxonomy names file has these fields:

	tax_id					-- the id of node associated with this name
	name_txt				-- name itself
	unique name				-- the unique variant of this name if name not unique
	name class				-- (synonym, common name, ...)

division.dmp
------------
Divisions file has these fields:
	division id				-- taxonomy database division id
	division cde				-- GenBank division code (three characters)
	division name				-- e.g. BCT, PLN, VRT, MAM, PRI...
	comments

gencode.dmp
-----------
Genetic codes file:

	genetic code id				-- GenBank genetic code id
	abbreviation				-- genetic code name abbreviation
	name					-- genetic code name
	cde					-- translation table for this genetic code
	starts					-- start codons for this genetic code

delnodes.dmp
------------
Deleted nodes (nodes that existed but were deleted) file field:

	tax_id					-- deleted node id

merged.dmp
----------
Merged nodes file fields:

	old_tax_id                              -- id of nodes which has been merged
	new_tax_id                              -- id of nodes which is result of merging

citations.dmp
-------------
Citations file fields:

	cit_id					-- the unique id of citation
	cit_key					-- citation key
        medline_id                              -- unique id in MedLine database (0 if not in MedLine)
	pubmed_id				-- unique id in PubMed database (0 if not in PubMed)
	url					-- URL associated with citation
	text					-- any text (usually article name and authors)
						-- The following characters are escaped in this text by a backslash:
						-- newline (appear as "\n"),
						-- tab character ("\t"),
						-- double quotes ('\"'),
						-- backslash character ("\\").
	taxid_list				-- list of node ids separated by a single space

typeoftype.dmp
--------------
Type material types file fields:

	type_name				-- name of type material type
	synonyms				-- alternative names for type material type
	nomenclature				-- Taxonomic Code of Nomenclature coded by a single letter:
						-- B - International Code for algae, fungi and plants (ICN), previously Botanical Code,
						-- P - International Code of Nomenclature of Prokaryotes (ICNP),
						-- Z - International Code of Zoological Nomenclature (ICZN),
						-- V - International Committee on Taxonomy of Viruses (ICTV) virus classification.
	description				-- descriptive text

host.dmp
--------
Theoretical host for organism file fields:

	tax_id					-- node id
	potential_hosts				-- theoretical host list separated by comma ','

typematerial.dmp
----------------
Type material information file fields:

	tax_id					-- node id
	tax_name				-- organism name type material is assigned to
	type					-- type material type (see typeoftype.dmp)
	identifier				-- identifier in type material collection

rankedlineage.dmp
-----------------
Select ancestor names for well-established taxonomic ranks (species, genus, family, order, class, phylum, kingdom, superkingdom) file fields:

        tax_id                                  -- node id
        tax_name                                -- scientific name of the organism
        species                                 -- name of a species (coincide with organism name for species-level nodes)
	genus					-- genus name when available
	family					-- family name when available
	order					-- order name when available
	class					-- class name when available
	phylum					-- phylum name when available
	kingdom					-- kingdom name when available
	superkingdom				-- superkingdom (domain) name when available

fullnamelineage.dmp
----------------
Full name lineage file fields:

        tax_id                                  -- node id
        tax_name                                -- scientific name of the organism
        lineage                                 -- sequence of sncestor names separated by semicolon ';' denoting nodes' ancestors starting from the most distant one and ending with the immediate one

taxidlineage.dmp
----------------
Taxonomy id lineage file fields:

        tax_id                                  -- node id
        lineage                                 -- sequence of node ids separated by space denoting nodes' ancestors starting from the most distant one and ending with the immediate one
