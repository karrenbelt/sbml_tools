﻿GENERAL INFORMATION
Due to an infrastructure grant from the German Federal Ministry of Education and Research (BMBF) the contents of BRENDA can now be made available for free
download. The information is in form of a text file the structure
of which is similar but not identical to the Swissprot/Trembl/Uniprot text file format so that parsers written
for that file could be easily modified. Information on the
structure of the file is given at the end of this README.

LICENSE
The full BRENDA contents are copyright-protected. For details see
www.brenda-enzymes.org/copy.php.


INFORMATION ON CONTENTS
The file is organised in an EC-number specific format. The
information on each EC-number is given in a very short and compact
way in a part of the file. The contents of each line are described
by a two/three letter acronym at the beginning of the line and
start after a TAB. Empty spaces at the beginning of a line
indicate a continuation line.

The contents are organised in 40 information fields as given
below. Protein information is included in '#'...#', literature
citations are in '<...>', commentaries in '(...)' and field-
special information in '{...}'.

Protein information is given as the combination organism/Uniprot
accession number where available. When this information is not
given in the original paper only the organism is given.

///	indicates the end of an EC-number specific part. 
AC	activating compound
AP	application
CF	cofactor
CL	cloned
CR	crystallization
EN	engineering
EXP	expression
GI	general information on enzyme
GS	general stability
IC50	IC-50 Value
ID	EC-class
IN	inhibitors
KKM	Kcat/KM-Value substrate in {...}
KI	Ki-value	inhibitor in {...}
KM	KM-value	substrate in {...}
LO	localization
ME	metals/ions
MW	molecular weight
NSP	natural substrates/products	reversibilty information in {...}
OS	oxygen stability
OSS	organic solvent stability
PHO	pH-optimum
PHR	pH-range
PHS	pH stability
PI	isoelectric point
PM	posttranslation modification
PR	protein
PU	purification
RE	reaction catalyzed
RF	references
REN	renatured
RN	accepted name (IUPAC) 
RT	reaction type
SA	specific activity
SN	synonyms
SP	substrates/products	reversibilty information in {...}
SS	storage stability
ST	source/tissue
SU	subunits
SY	systematic name 
TN	turnover number	substrate in {...}
TO	temperature optimum
TR	temperature range
TS	temperature	stability

