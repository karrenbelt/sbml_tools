
import os
import sys
import inspect
import platform
from setuptools import setup

"""NOTE: install using: pip install -e ."""

VERSION = '0.1.1'

CURRENT_PYTHON = sys.version_info[:2]
REQUIRED_PYTHON = (3, 7)
if CURRENT_PYTHON != REQUIRED_PYTHON:
    # must be 3.6 or 3.7, libroadrunner is not compatible with version 3.8 yet
    sys.stderr.write("unsupported python version: {}.{} should be {}.{}".format(*CURRENT_PYTHON + REQUIRED_PYTHON))

# glpk was installed using brew, pip install failed on macos

# we need to manually install owlready2 from the local folder because of the encoding utf-8 not being declared
# SCRIPT_DIR     = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
# BASE_DIR       = os.path.join(*os.path.split(SCRIPT_DIR)[0:-1])
# fp = os.path.join(BASE_DIR, 'Owlready2-0.20')
# os.system(f'pip install -e {fp}')

install_requires = [
    'bidict==0.19.0',
    'boolean.py==3.8',
    'cached-property==1.5.1',  # functools.cached-property for python3.8 and beyond
    'cobra==0.18.1',
    'coverage==5.1',
    'iminuit==1.4.5',
    'ipython==7.16.1',
    'libroadrunner==1.6.0',
    'lmfit==1.0.1',
    'numpy==1.19.0',
    'lxml==4.5.1',
    'networkx==2.4',
    'matplotlib==3.2.2',
    'optlang==1.4.4',
    'owlready2==0.24',
    'pandas==1.0.5',
    'pbalancing==2.1.3',
    'pint==0.13',
    'pytest==5.4.3',
    'python-libsbml-experimental==5.18.1',
    'sbtab==0.9.73',
    'scipy==1.5.0',
    'seaborn==0.10.1',
    'statsmodels==0.11.1',
    'stopit==1.1.2',
    'sympy==1.6',
    'tabulate==0.8.7',
    'tqdm==4.47.0',
    'uncertainties==3.1.4',
    'xmltodict==0.12.0',  # want to get rid of this dependency
    ]

setup(name='sbml_tools',
      version=VERSION,
      description='',
      python_requires='>={}.{}'.format(*REQUIRED_PYTHON),
      author='M.A.P. Karrenbelt',
      author_email='karrenbelt@imsb.biol.ethz.ch',
      packages=[],
      install_requires=install_requires,
      dependency_links=[
    #      'https://github.com/sbmlteam/sbml-test-suite.git', # not sure if this works like this: see .gitlab-ci.yml
      ]
     )


# because otherwise libsmbl.ArrayExtension and DistribExtension are not found from the experimental version
# might be due to cobra complaining: cobra 0.16.0 has requirement python-libsbml-experimental==5.18.0, but you'll have python-libsbml-experimental 5.18.1 which is incompatible.
os.system(f'pip uninstall python-libsbml-experimental -y')
os.system(f'pip install python-libsbml-experimental==5.18.1')


# needed on ubuntu 19.10 for libroadrunner 1.6.0:
# sudo apt-get install libncurses5-dev libncursesw5-dev
if platform.system() == 'Linux':
    print('need to: sudo apt-get install libncurses5')
    # os.system(f'sudo apt-get install libncurses5')
