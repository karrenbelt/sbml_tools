

def get_math_swapping_objects():
    import platform
    import libsbml
    import string

    print('platform:', platform.platform())
    print('python:', platform.python_version())
    print('libsbml:', libsbml.__version__)

    def unique(sequence):
        seen = set()
        return [x for x in sequence if not (x in seen or seen.add(x))]

    def simplify(li):
        d = dict(zip(unique(li), string.ascii_uppercase))
        return [d[el] for el in li]

    level = 3
    version = 2
    f_def = libsbml.FunctionDefinition(level, version)
    i_ass = libsbml.InitialAssignment(level, version)
    k_law = libsbml.KineticLaw(level, version)

    # same for libsbml.parseL3Formula('x'), thus not because it is empty or type is unknown
    a = libsbml.ASTNode()
    assert a is not None and a.isWellFormedASTNode()

    ast_node_ids = []
    for obj in [f_def, i_ass, k_law]:
        ids = []
        ids.append(id(a))  # 0. 'A'
        assert obj.setMath(a) == 0
        ids.append(id(a))  # 1. 'A'
        ids.append(id(obj.getMath()))  # 2. 'B'
        ids.append(id(obj.getMath()))  # 3. 'B'
        ids.append(id(obj.getMath()))  # 4. 'B'

        # assigning to variables
        b = obj.getMath()
        ids.append(id(b))  # 5. 'B'
        ids.append(id(obj.getMath()))  # 6. 'C'
        c = obj.getMath()
        ids.append(id(b))  # 7. 'B'
        ids.append(id(obj.getMath()))  # 8. 'D'

        ## assigning to same variable as before: c
        c = obj.getMath()
        ids.append(id(obj.getMath()))  # 9. 'C'

        ast_node_ids.append(ids)

    # generate simpler representation
    simpler = list(map(simplify, ast_node_ids))

    for li in simpler:
        print(li)

    # output:
    #
    # platform: Linux-5.3.0-62-generic-x86_64-with-Ubuntu-19.10-eoan
    # python: 3.7.5
    # libsbml: 5.18.1
    #
    # ['A', 'A', 'B', 'B', 'B', 'C', 'D', 'C', 'D', 'E']
    # ['A', 'A', 'B', 'B', 'B', 'C', 'D', 'C', 'E', 'D']
    # ['A', 'A', 'B', 'B', 'B', 'C', 'D', 'C', 'E', 'E']
    #
    # expected:
    #
    # ['A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A', 'A']


def astnode_time_to_mathml():
    import platform
    import libsbml

    print('platform:', platform.platform())
    print('python:', platform.python_version())
    print('libsbml:', libsbml.__version__)

    node = libsbml.ASTNode()
    node.setType(libsbml.AST_NAME_TIME)
    libsbml.writeMathMLToString(node)

    # platform: Linux-5.3.0-62-generic-x86_64-with-Ubuntu-19.10-eoan
    # python: 3.7.5
    # libsbml: 5.18.1
    # terminate called after throwing an instance of 'std::logic_error'
    #   what():  basic_string::_S_construct null not valid
    # Process finished with exit code 134 (interrupted by signal 6: SIGABRT)
