# Tools to make SBML files easier to explore and manipulate <br>
**Version 0.1.0**

---
## SBML model definition
- list of function definitions (optional)
- list of unit definitions (optional)
- list of compartments (optional)
- list of species (optional)
- list of parameters (optional)
- list of initial assignments (optional)
- list of rules (optional)
- list of constraints (optional)
- list of reactions (optional)
- list of events (optional)
see documentation folder for the official documentation and details

---
### TO DO:
- kinetic laws
- unit checking of equations
- inline function definitions / rules / events (networkx)
- state space representation
- verify rate laws
- symengine (jitcode) and roadrunner cannot be used together 

---

## Contributor

- Michiel A.P. Karrenbelt  <karrenbelt@imsb.biol.ethz.ch>
  
---
  
## License & copyright

© Michiel A.P. Karrenbelt, ETH Zurich

Licensed under the [MIT License](LICENSE).