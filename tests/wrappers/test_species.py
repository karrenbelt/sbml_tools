

import pytest
import libsbml
import cmath
from sbml_tools.wrappers.species import Species


# fixtures
@pytest.fixture()
def incomplete_species():
    species = libsbml.Species(3, 2)
    incomplete_species = Species(species)
    return incomplete_species


@pytest.fixture()
def minimal_species():
    species = libsbml.Species(3, 2)
    species.setId('species_id')
    species.setCompartment('c')
    species.setHasOnlySubstanceUnits(True)
    species.setBoundaryCondition(False)
    species.setConstant(False)
    minimal_species = Species(species)
    return minimal_species


@pytest.fixture()
def complete_species():
    species = libsbml.Species(3, 2)
    species.setId('species_id')
    species.setName('species_name')
    species.setMetaId('species_meta_id')
    species.setSBOTerm(236)
    # species.setNotes()
    # species.setAnnotation()
    species.setCompartment('c')
    species.setHasOnlySubstanceUnits(True)
    species.setBoundaryCondition(False)
    species.setConstant(False)
    species.setInitialAmount(1.0)
    # species.setInitialConcentration(1.0)  # cannot set both amount and concentration
    species.setSubstanceUnits('mole')
    species.setConversionFactor('SIdRef')
    complete_species = Species(species)
    return complete_species


# tests
def test_default_attributes(incomplete_species):
    assert incomplete_species.compartment is None
    assert incomplete_species.initial_amount is None
    assert incomplete_species.initial_concentration is None
    assert incomplete_species.substance_units is None
    assert incomplete_species.has_only_substance_units is None
    assert incomplete_species.boundary_condition is None
    assert incomplete_species.constant is None
    assert incomplete_species.conversion_factor is None


def test_required_attributes(incomplete_species, minimal_species):
    assert incomplete_species.required == list(incomplete_species.difference(minimal_species))
    assert not incomplete_species.has_required_attributes
    assert minimal_species.has_required_attributes


def test_setting_proper_initial_amount(incomplete_species):
    incomplete_species.initial_amount = 1.0
    assert incomplete_species.initial_amount == 1.0
    assert incomplete_species.has_only_substance_units is True


def test_setting_improper_initial_amount(incomplete_species):
    incomplete_species.has_only_substance_units = True
    with pytest.raises(Exception):
        incomplete_species.initial_amount = -1.0

    incomplete_species.has_only_substance_units = False
    with pytest.raises(Exception):
        incomplete_species.initial_amount = 1.0


def test_setting_proper_initial_concentration(incomplete_species):
    incomplete_species.initial_concentration = 1.0
    assert incomplete_species.initial_concentration == 1.0
    assert incomplete_species.has_only_substance_units is False


def test_setting_improper_initial_concentration(incomplete_species):
    incomplete_species.has_only_substance_units = False
    with pytest.raises(Exception):
        incomplete_species.initial_concentration = -1.0

    incomplete_species.has_only_substance_units = True
    with pytest.raises(Exception):
        incomplete_species.initial_concentration = 1.0


def test_setting_unsetting_initial_amount_and_changing_has_only_substance_units(incomplete_species):
    incomplete_species.initial_amount = 1.0
    assert incomplete_species.has_only_substance_units is True
    with pytest.raises(Exception):
        incomplete_species.has_only_substance_units = False
    incomplete_species.unset_initial_amount()
    incomplete_species.has_only_substance_units = False
    assert incomplete_species.has_only_substance_units is False


def test_setting_unsetting_initial_concentration_and_changing_has_only_substance_units(incomplete_species):
    incomplete_species.initial_concentration = 1.0
    assert incomplete_species.has_only_substance_units is False
    with pytest.raises(Exception):
        incomplete_species.has_only_substance_units = True
    incomplete_species.unset_initial_concentration()
    incomplete_species.has_only_substance_units = True
    assert incomplete_species.has_only_substance_units is True


def test_init_from_kwargs(complete_species):
    species = Species(id='species_id', name='species_name',
                      meta_id='species_meta_id', sbo_term=236,
                      compartment='c', has_only_substance_units=True, boundary_condition=False, constant=False,
                      initial_amount=1.0, conversion_factor='SIdRef', substance_units='mole')
    assert complete_species == species


def test_copy(complete_species):
    assert complete_species is not complete_species.copy()
    assert complete_species == complete_species.copy()


def test_setting_proper_units(incomplete_species):
    for substance_units in ('mole', 'item', 'dimensionless', 'kilogram', 'gram', 'avogadro'):
        incomplete_species.substance_units = substance_units
        assert incomplete_species.substance_units == substance_units


def test_setting_improper_units(incomplete_species):
    for substance_units in ('henry', 'katal', 'ohm'):
        with pytest.raises(Exception):
            incomplete_species.substance_units = substance_units


def test_setting_conversion_factor(incomplete_species):
    incomplete_species.conversion_factor = 'SidRef'
    assert incomplete_species.conversion_factor == 'SidRef'


def test_logic_table(incomplete_species):
    # somehow False is False yields an error here, thus using ==
    incomplete_species.constant = True
    incomplete_species.boundary_condition = True
    assert incomplete_species.can_have_assignment_or_rate_rule == False
    assert incomplete_species.can_be_reactant_or_product == True
    assert incomplete_species.what_can_change_species_amount == '(never changes)'

    incomplete_species.constant = True
    incomplete_species.boundary_condition = False
    assert incomplete_species.can_have_assignment_or_rate_rule == False
    assert incomplete_species.can_be_reactant_or_product == False
    assert incomplete_species.what_can_change_species_amount == '(never changes)'

    incomplete_species.constant = False
    incomplete_species.boundary_condition = True
    assert incomplete_species.can_have_assignment_or_rate_rule == True
    assert incomplete_species.can_be_reactant_or_product == True
    assert incomplete_species.what_can_change_species_amount == 'rules and events'

    incomplete_species.constant = False
    incomplete_species.boundary_condition = False
    assert incomplete_species.can_have_assignment_or_rate_rule == True
    assert incomplete_species.can_be_reactant_or_product == True
    assert incomplete_species.what_can_change_species_amount == 'reactions OR rules, and events'


__all__ = ['incomplete_species', 'minimal_species', 'complete_species']
