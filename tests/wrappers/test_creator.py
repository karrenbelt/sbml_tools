
import pytest
import libsbml
from sbml_tools.wrappers import Creator


@pytest.fixture()
def john_doe():
    creator = libsbml.ModelCreator()
    creator.setName('John')
    creator.setFamilyName('Doe')
    creator.setEmail('john@doe.com')
    creator.setOrganization('organization')
    john_doe = Creator(creator)
    return john_doe


def test_setting_required_attributes():
    creator = Creator()
    assert not creator.has_required_attributes
    creator.given_name = 'John'
    creator.family_name = 'Doe'
    assert creator.has_required_attributes


def test_setting_proper_email():
    creator = Creator()
    for address in ('a@a.com', 'name@provider.ch'):
        creator.email = address
        assert creator.email == address


def test_setting_improper_email():
    creator = Creator()
    for address in ('a@a.c', 'a@a.comm',  'name_of_provider.ch'):
        with pytest.raises(Exception):
            creator.email = address


def test_init_from_kwargs(john_doe):
    assert john_doe == Creator(given_name='John', family_name='Doe',
                               email='john@doe.com', organization='organization')


def test_copy(john_doe):
    creator = john_doe
    assert creator is not creator.copy()
    assert creator == creator.copy()
