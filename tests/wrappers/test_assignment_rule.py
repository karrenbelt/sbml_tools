
import pytest
import libsbml
from sbml_tools.wrappers import ASTNode, AssignmentRule


# fixtures
@pytest.fixture()
def incomplete_assignment_rule():
    assignment_rule = libsbml.AssignmentRule(3, 2)
    incomplete_assignment_rule = AssignmentRule(assignment_rule)
    return incomplete_assignment_rule


@pytest.fixture()
def minimal_assignment_rule():
    assignment_rule = libsbml.AssignmentRule(3, 2)
    assignment_rule.setVariable('x')
    minimal_assignment_rule = AssignmentRule(assignment_rule)
    return minimal_assignment_rule


@pytest.fixture()
def complete_assignment_rule():
    assignment_rule = libsbml.AssignmentRule(3, 2)
    assignment_rule.setIdAttribute('assignment_rule_id')
    assignment_rule.setName('assignment_rule_name')
    assignment_rule.setMetaId('assignment_rule_meta_id')
    assignment_rule.setSBOTerm(64)
    # assignment_rule.setNotes()
    # assignment_rule.setAnnotation()
    assignment_rule.setVariable('x')  # variable sets .id
    assignment_rule.setMath(libsbml.parseL3Formula('2 mole'))
    # assignment_rule.setUnits('mole')  - does not work: returns -2
    complete_assignment_rule = AssignmentRule(assignment_rule)
    return complete_assignment_rule


# tests
def test_defaults(incomplete_assignment_rule):
    assert incomplete_assignment_rule.variable is ''
    assert incomplete_assignment_rule.math is None


def test_required_attributes(incomplete_assignment_rule, minimal_assignment_rule):
    assert incomplete_assignment_rule.required == list(incomplete_assignment_rule.difference(minimal_assignment_rule))
    assert not incomplete_assignment_rule.has_required_attributes
    assert minimal_assignment_rule.has_required_attributes


def test_setting_math(incomplete_assignment_rule):
    incomplete_assignment_rule.math = 'x * x'
    assert incomplete_assignment_rule.math == ASTNode.from_string('x * x')


def test_init_from_kwargs(complete_assignment_rule):
    assignment_rule = AssignmentRule(id_attribute='assignment_rule_id', name='assignment_rule_name',
                                     meta_id='assignment_rule_meta_id', sbo_term=64,
                                     variable='x', math='2 mole')
    assert complete_assignment_rule == assignment_rule


def test_copy(complete_assignment_rule):
    assert complete_assignment_rule is not complete_assignment_rule.copy()
    assert complete_assignment_rule == complete_assignment_rule.copy()


__all__ = ['incomplete_assignment_rule', 'minimal_assignment_rule', 'complete_assignment_rule']
