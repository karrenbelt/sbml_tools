
import pytest
import libsbml
from sbml_tools.wrappers import ASTNode, AlgebraicRule


# fixtures
@pytest.fixture()
def minimal_algebraic_rule():
    algebraic_rule = libsbml.AlgebraicRule(3, 2)
    minimal_algebraic_rule = AlgebraicRule(algebraic_rule)
    return minimal_algebraic_rule


@pytest.fixture()
def complete_algebraic_rule():
    algebraic_rule = libsbml.AlgebraicRule(3, 2)
    algebraic_rule.setIdAttribute('algebraic_rule_id')
    algebraic_rule.setName('algebraic_rule_name')
    algebraic_rule.setMetaId('algebraic_rule_meta_id')
    algebraic_rule.setSBOTerm(359)
    # algebraic_rule.setNotes(libsbml.XMLNode_convertStringToXMLNode('<notes> some notes </notes>'))
    # algebraic_rule.setAnnotation(libsbml.XMLNode_convertStringToXMLNode('<annotation> some annotation </annotation>'))
    algebraic_rule.setMath(libsbml.parseL3Formula('x - y'))
    complete_algebraic_rule = AlgebraicRule(algebraic_rule)
    return complete_algebraic_rule


# tests
def test_defaults(minimal_algebraic_rule):
    assert minimal_algebraic_rule.math is None


def test_required_attributes(minimal_algebraic_rule):
    assert minimal_algebraic_rule.has_required_attributes


def test_setting_math(minimal_algebraic_rule):
    minimal_algebraic_rule.math = 'x * x'
    assert minimal_algebraic_rule.math == ASTNode.from_string('x * x')


def test_init_from_kwargs(complete_algebraic_rule):
    algebraic_rule = AlgebraicRule(id_attribute='algebraic_rule_id', name='algebraic_rule_name',
                                   meta_id='algebraic_rule_meta_id', sbo_term=359,
                                   math='x - y')
    assert complete_algebraic_rule == algebraic_rule


def test_copy(complete_algebraic_rule):
    assert complete_algebraic_rule is not complete_algebraic_rule.copy()
    assert complete_algebraic_rule == complete_algebraic_rule.copy()


__all__ = ['minimal_algebraic_rule', 'complete_algebraic_rule']
