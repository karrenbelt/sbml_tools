
# Wrapper tests

These are fairly simple and minimal tests to check whether wrappers behave as expected. 

needs revision: xml_base, notes, message, annotation
TODO: ListOf of all 10 lists
TODO: add sbase attributes to complete_fixtures: sbo_term, notes, annotation

## TODO:
- document
- list_of

## DONE
- algebraic_rule
- annotation
- assignment_rule
- ast_node
- compartment
- constraint
- creator
- cv_term
- date
- delay
- event
- event_assignment
- function_definition
- history
- initial_assignment
- kinetic_law
- local_parameter
- model
- modifier_reference
- notes
- parameter
- priority
- rate_rule
- reaction
- species
- species_reference
- trigger
- unit
- unit_definition
