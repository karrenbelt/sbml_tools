
import pytest
import libsbml
from datetime import datetime
from sbml_tools.wrappers.date import Date, get_current_date_string


@pytest.fixture()
def default_libsbml_date():
    default_libsbml_date = Date(libsbml.Date())
    return default_libsbml_date


def test_default_libsbml_date(default_libsbml_date):
    date = default_libsbml_date
    assert date.validate() is None
    assert date.year == 2000
    assert date.month == 1
    assert date.day == 1
    assert date.hour == 0
    assert date.minute == 0
    assert date.second == 0
    assert date.hours_offset == 0
    assert date.minutes_offset == 0


def test_date_from_current_time_valid():
    date = Date.from_current_time()
    assert date.validate() is None


def test_setting_to_current(default_libsbml_date):
    date = Date()
    default_libsbml_date.year = date.year
    default_libsbml_date.month = date.month
    default_libsbml_date.day = date.day
    default_libsbml_date.hour = date.hour
    default_libsbml_date.minute = date.minute
    default_libsbml_date.second = date.second
    default_libsbml_date.hours_offset = date.hours_offset
    default_libsbml_date.minutes_offset = date.minutes_offset
    assert date == default_libsbml_date


def test_date_string():
    date = Date()
    date_string = get_current_date_string()
    date.date_string = date_string
    assert date == Date.from_date_string(date_string)


def test_date_string_from_current():
    date = Date.from_current_time()
    assert date.validate() is None


def test_copy():
    date = Date()
    assert date is not date.copy()
    assert date == date.copy()


def test_arithmetic(default_libsbml_date):
    date = Date()
    assert isinstance(date.as_datetime, datetime)
    assert date > default_libsbml_date
    assert date >= default_libsbml_date
    assert (date < default_libsbml_date) is False
    assert (date <= default_libsbml_date) is False
    assert default_libsbml_date < date
    assert default_libsbml_date <= date
    assert (default_libsbml_date > date) is False
    assert (default_libsbml_date >= date) is False
