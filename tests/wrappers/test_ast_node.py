
import pytest
import libsbml
import fractions
from sbml_tools.wrappers.ast_node import ASTNode, ALL_AST_TYPES

from tests.simple_ast_nodes import (
    ARITHMETIC, CONSTANT, CSYMBOL, CSYMBOL_FUNCTION, DISTRIB_FUNCTION, LINEAR_ALGEBRA, LOGICAL, NUMERICAL, RELATIONAL,
    SERIES, STATISTICS, TRIGONOMETRIC_FUNCTION, OTHER)
from tests.simple_ast_nodes import ALL_SIMPLE_NODES


# fixtures
@pytest.fixture()
def ast_node():
    ast_node = ASTNode()
    return ast_node


@pytest.fixture()
def x_times_x():
    x_times_x = ASTNode(libsbml.parseL3Formula('x*x'))
    return x_times_x


@pytest.fixture()
def five_point_two_ml():
    five_point_two_ml = ASTNode(libsbml.parseL3Formula('5.2ml'))
    return five_point_two_ml


@pytest.fixture()
def nested():
    nested = ASTNode(libsbml.parseL3Formula('f(x, y) + g(h(x), y) * 2'))
    return nested


def test_all_ast_types_covered():
    simple_types = [node.getType() for node in ALL_SIMPLE_NODES.values()]
    missing = [v for k, v in ALL_AST_TYPES.items() if k not in simple_types]
    assert missing == ['AST_END_OF_CORE']


def test_ast_end_of_core_error():
    with pytest.raises(Exception):
        ASTNode(type=libsbml.AST_END_OF_CORE)


def test_simple_ast_nodes_well_formed():

    for ast_group in (ARITHMETIC, CONSTANT, CSYMBOL, CSYMBOL_FUNCTION, DISTRIB_FUNCTION, LINEAR_ALGEBRA, LOGICAL,
                      NUMERICAL, RELATIONAL, SERIES, STATISTICS, TRIGONOMETRIC_FUNCTION, OTHER):
        for name, ast_node in {k: v for k, v in vars(ast_group).items() if not k.startswith('__')}.items():
            assert ast_node.isWellFormedASTNode()


def test_ast_node_string_setter(ast_node, x_times_x):
    # for more extensive testing in SymbolicASTNode (toolbox -> math -> _test_nodes.py)
    ast_node.string = 'x*x'
    assert ast_node.string == 'x * x'
    assert ast_node == x_times_x == ASTNode.from_string('x*x')


def test_copy(x_times_x):
    assert x_times_x is not x_times_x.copy()
    assert x_times_x == x_times_x


def test_mathml_string(x_times_x):
    assert x_times_x == ASTNode.from_mathml_string(x_times_x.mathml_string)


def test_numerical_value():
    assert ASTNode(NUMERICAL.INTEGER).value == 1
    assert ASTNode(NUMERICAL.RATIONAL).value == fractions.Fraction(1, 3)
    assert ASTNode(NUMERICAL.REAL).value == 1.0
    assert ASTNode(NUMERICAL.REAL_E).value == 2e3 == 2000.0


def test_setting_value(ast_node):
    assert ast_node.type == libsbml.AST_UNKNOWN
    ast_node.value = 1
    assert ast_node.type == libsbml.AST_INTEGER
    ast_node.value = 1.0
    assert ast_node.type == libsbml.AST_REAL
    ast_node.value = 1, 1
    assert ast_node.type == libsbml.AST_RATIONAL
    ast_node.value = 1.0, 1
    assert ast_node.type == libsbml.AST_REAL_E
    ast_node.value = float('nan')
    assert ast_node.is_nan


def test_units(five_point_two_ml):
    assert five_point_two_ml.units == 'ml'
    five_point_two_ml.units = 'litre'
    assert five_point_two_ml.units == 'litre'


# def test_arithmetic_operations_other_ast_node():
#
#     assert + ASTNode.from_string('-x') == ASTNode.from_string('plus(-x)')
#     assert --- ASTNode.from_string('-x') == ASTNode.from_string('----x')
#     assert abs(ASTNode.from_string('x')) == ASTNode.from_string('abs(x)')
#
#     assert ASTNode.from_string('x * x') + ASTNode.from_string('x * x') == ASTNode.from_string('x * x + x * x')
#     assert ASTNode.from_string('x * x') - ASTNode.from_string('x * x') == ASTNode.from_string('x * x - x * x')
#     assert ASTNode.from_string('x + x') * ASTNode.from_string('x + x') == ASTNode.from_string('(x + x) * (x + x)')
#     assert ASTNode.from_string('x + x') / ASTNode.from_string('x + x') == ASTNode.from_string('(x + x) / (x + x)')
#     assert ASTNode.from_string('x + x') ** ASTNode.from_string('x + x') == ASTNode.from_string('(x + x) ^ (x + x)')


# def test_arithmetic_operations_with_integers():
#     # move to math tests
#     assert ASTNode.from_string('x - x') + 2 == ASTNode.from_string('(x - x) + 2')
#     assert 2 + ASTNode.from_string('x - x') == ASTNode.from_string('2 + (x - x)')
#
#     assert ASTNode.from_string('x - x') - 2 == ASTNode.from_string('(x - x) - 2')
#     assert 2 - ASTNode.from_string('x - x') == ASTNode.from_string('2 - (x - x)')
#
#     assert ASTNode.from_string('x - x') * 2 == ASTNode.from_string('(x - x) * 2')
#     assert 2 * ASTNode.from_string('x - x') == ASTNode.from_string('2 * (x - x)')
#
#     assert ASTNode.from_string('x - x') / 2 == ASTNode.from_string('(x - x) / 2')
#     assert 2 / ASTNode.from_string('x - x') == ASTNode.from_string('2 / (x - x)')
#
#     assert ASTNode.from_string('x - x') ** 2 == ASTNode.from_string('(x - x) ^ 2')
#     assert 2 ** ASTNode.from_string('x - x') == ASTNode.from_string('2 ^ (x - x)')
#
#     # sum works differently than np.sum
#     # assert sum([ASTNode.from_string('x - x'), ASTNode.from_string('x - x')])
#     # assert np.sum([ASTNode.from_string('x - x'), ASTNode.from_string('x - x')])


def test_children(nested):
    assert list(nested.children) == [ASTNode.from_string('f(x,y)'), ASTNode.from_string('g(h(x), y) * 2')]


def test_iter(nested):
    expected = [
        ('f(x, y) + g(h(x), y) * 2', libsbml.AST_PLUS),
        ('f(x, y)', libsbml.AST_FUNCTION),
        ('x', libsbml.AST_NAME),
        ('y', libsbml.AST_NAME),
        ('g(h(x), y) * 2', libsbml.AST_TIMES),
        ('g(h(x), y)', libsbml.AST_FUNCTION),
        ('h(x)', libsbml.AST_FUNCTION),
        ('x', libsbml.AST_NAME),
        ('y', libsbml.AST_NAME),
        ('2', libsbml.AST_INTEGER),
        ]

    for i, x in enumerate(nested):
        assert (x.string, x.type) == expected[i]


def test_reduce_to_binary():
    ast_node = ASTNode.from_string('and(x, y, z)')
    ast_node.reduce_to_binary()
    assert ast_node == ASTNode.from_string('and(and(x, y), z)')


def test_adding_single_child():
    # important test, because ASTNodes are iterable
    child_with_children = ASTNode.from_string('divide(plus(x, y, z), 3)')
    node = ASTNode(type=libsbml.AST_TIMES, children=child_with_children)
    assert len(list(node.children)) == 1
