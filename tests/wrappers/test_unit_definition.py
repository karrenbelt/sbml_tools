
import pytest
import libsbml
from sbml_tools.wrappers import UnitDefinition, Unit
from sbml_tools import config


# fixtures
@pytest.fixture()
def incomplete_unit_definition():
    unit_definition = libsbml.UnitDefinition(3, 2)
    incomplete_unit_definition = UnitDefinition(unit_definition)
    return incomplete_unit_definition


@pytest.fixture()
def minimal_unit_definition():
    unit_definition = libsbml.UnitDefinition(3, 2)
    unit_definition.setId('unit_definition_id')
    minimal_unit_definition = UnitDefinition(unit_definition)
    return minimal_unit_definition


@pytest.fixture()
def complete_unit_definition():
    unit_definition = libsbml.UnitDefinition(3, 2)
    unit_definition.setId('unit_definition_id')
    unit_definition.setName('unit_definition_name')
    unit_definition.setMetaId('unit_definition_meta_id')
    # unit_definition.setSBOTerm()
    # unit_definition.setNotes()
    # unit_definition.setAnnotation()

    milli_mole = libsbml.Unit(3, 2)
    milli_mole.setKind(libsbml.UNIT_KIND_MOLE)
    milli_mole.setExponent(1)
    milli_mole.setScale(-3)
    milli_mole.setMultiplier(1.0)

    per_gram = libsbml.Unit(3, 2)
    per_gram.setKind(libsbml.UNIT_KIND_GRAM)
    per_gram.setExponent(-1)
    per_gram.setScale(0)
    per_gram.setMultiplier(1.0)

    per_hour = libsbml.Unit(3, 2)
    per_hour.setKind(libsbml.UNIT_KIND_SECOND)
    per_hour.setExponent(-1)
    per_hour.setScale(0)
    per_hour.setMultiplier(3600.0)

    unit_definition.addUnit(milli_mole)
    unit_definition.addUnit(per_gram)
    unit_definition.addUnit(per_hour)

    complete_unit_definition = UnitDefinition(unit_definition)
    return complete_unit_definition


# tests
def test_default_values(incomplete_unit_definition):
    assert not incomplete_unit_definition.units


def test_has_required_attributes(incomplete_unit_definition, minimal_unit_definition):
    assert incomplete_unit_definition.required == list(incomplete_unit_definition.difference(minimal_unit_definition))
    assert not incomplete_unit_definition.has_required_attributes
    assert minimal_unit_definition.has_required_attributes


def test_setters(complete_unit_definition):
    mmole = Unit(kind='mole', exponent=1, scale=-3, multiplier=1.0)
    per_gram = Unit(kind='gram', exponent=-1, scale=0, multiplier=1.0)
    per_hour = Unit(kind='second', exponent=-1, scale=0, multiplier=3600.0)
    unit_definition = UnitDefinition()
    unit_definition.id = 'unit_definition_id'
    unit_definition.name = 'unit_definition_name'
    unit_definition.meta_id = 'unit_definition_meta_id'
    unit_definition.units = [mmole, per_gram, per_hour]
    assert unit_definition == complete_unit_definition


def test_init_from_kwargs(complete_unit_definition):
    mmole = Unit(kind='mole', exponent=1, scale=-3, multiplier=1.0)
    per_gram = Unit(kind='gram', exponent=-1, scale=0, multiplier=1.0)
    per_hour = Unit(kind='second', exponent=-1, scale=0, multiplier=3600.0)
    unit_definition = UnitDefinition(id='unit_definition_id', name='unit_definition_name',
                                     meta_id='unit_definition_meta_id', units=[mmole, per_gram, per_hour])
    assert unit_definition == complete_unit_definition


def test_copy(complete_unit_definition):
    assert complete_unit_definition is not complete_unit_definition.copy()
    assert complete_unit_definition == complete_unit_definition.copy()


def test_equivalence():
    kilogram = UnitDefinition(id='kilogram', units=[Unit(kind='kilogram', exponent=1, scale=0, multiplier=1.0)])
    thousand_grams = UnitDefinition(id='cubic_meter', units=[Unit(kind='gram', exponent=1, scale=0, multiplier=1000)])
    cubic_gram = UnitDefinition(id='cubic_meter', units=[Unit(kind='gram', exponent=1, scale=3, multiplier=1.0)])
    assert kilogram.equals(thousand_grams) and kilogram.equals(cubic_gram)

    kilo_litre = UnitDefinition(id='kilo_litre', units=[Unit(kind='liter', exponent=1, scale=0, multiplier=1000.0)])
    cubic_meter = UnitDefinition(id='cubic_meter', units=[Unit(kind='meter', exponent=3, scale=0, multiplier=1.0)])
    assert kilo_litre.equals(cubic_meter)


def test_as_quantity_from_quantity(complete_unit_definition):
    quantity = complete_unit_definition.as_quantity
    assert quantity.to('mmole / gram / hour').magnitude == 0.9999999999999999  # floating point arithmetic
    unit_definition = UnitDefinition.from_quantity(quantity, identifier='unit_definition_id')
    assert not unit_definition == complete_unit_definition
    assert unit_definition.equals(complete_unit_definition)


def test_equality_when_numerically_close():
    gram = UnitDefinition(id='gram', units=[Unit(kind='gram', exponent=1, scale=0, multiplier=1.0)])
    multiplier = gram.units[0].multiplier - gram.units[0].multiplier * config.IS_CLOSE_RELATIVE_TOLERANCE
    almost_gram = UnitDefinition(id='gram', units=[Unit(kind='gram', exponent=1, scale=0, multiplier=multiplier)])
    assert gram == almost_gram


def test_simplify(minimal_unit_definition):
    minimal_unit_definition.units = [Unit(kind='hertz', exponent=1, scale=0, multiplier=1.0),
                                     Unit(kind='hertz', exponent=1, scale=0, multiplier=1.0)]
    minimal_unit_definition.simplify()
    assert list(minimal_unit_definition.units) == [Unit(kind='hertz', exponent=2, scale=0, multiplier=1.0)]


def test_convert_gram_to_SI(minimal_unit_definition):
    minimal_unit_definition.units = [Unit(kind='gram', exponent=1, scale=0, multiplier=1.0)]
    minimal_unit_definition.convert_to_SI()
    assert list(minimal_unit_definition.units) == [Unit(kind='kilogram', exponent=1, scale=0, multiplier=0.001)]


def test_convert_hertz_to_SI(minimal_unit_definition):
    minimal_unit_definition.units = [Unit(kind='hertz', exponent=1, scale=0, multiplier=1.0),
                                     Unit(kind='hertz', exponent=1, scale=0, multiplier=1.0)]
    minimal_unit_definition.convert_to_SI()
    assert list(minimal_unit_definition.units) == [Unit(kind='second', exponent=-2, scale=0, multiplier=1.0)]


__all__ = ['incomplete_unit_definition', 'minimal_unit_definition', 'complete_unit_definition']
