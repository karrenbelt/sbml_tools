
import pytest
import libsbml
from sbml_tools.wrappers import Priority, ASTNode


#fixtures
@pytest.fixture()
def minimal_priority():
    priority = libsbml.Priority(3, 2)
    minimal_priority = Priority(priority)
    return minimal_priority


@pytest.fixture()
def complete_priority():
    priority = libsbml.Priority(3, 2)
    priority.setIdAttribute('priority_id')
    priority.setName('priority_name')
    priority.setMetaId('priority_meta_id')
    # priority.setSBOTerm()
    # priority.setNotes()
    # priority.setAnnotation()
    priority.setMath(libsbml.parseL3Formula('1'))
    complete_priority = Priority(priority)
    return complete_priority


# tests
def test_defaults(minimal_priority):
    assert minimal_priority.math is None


def test_required_attributes(minimal_priority):
    assert minimal_priority.has_required_attributes


def test_setting_math(minimal_priority):
    minimal_priority.math = 'x*x'
    assert minimal_priority.math == ASTNode.from_string('x*x')


def test_delay_init_from_kwargs(complete_priority):
    priority = Priority(id='priority_id', name='priority_name', meta_id='priority_meta_id', math='1')
    assert priority == complete_priority


def test_copy(complete_priority):
    assert complete_priority is not complete_priority.copy()
    assert complete_priority == complete_priority.copy()


__all__ = ['minimal_priority', 'complete_priority']
