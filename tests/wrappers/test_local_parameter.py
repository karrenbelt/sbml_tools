
import pytest
import libsbml
from sbml_tools.wrappers import LocalParameter, ListOfLocalParameters


# fixtures
@pytest.fixture()
def incomplete_local_parameter():
    local_parameter = libsbml.LocalParameter(3, 2)
    incomplete_local_parameter = LocalParameter(local_parameter)
    return incomplete_local_parameter


@pytest.fixture()
def minimal_local_parameter():
    local_parameter = libsbml.LocalParameter(3, 2)
    local_parameter.setId('local_parameter_id')
    minimal_local_parameter = LocalParameter(local_parameter)
    return minimal_local_parameter


@pytest.fixture()
def complete_local_parameter():
    # fixture is also used in test_kinetic_law.py
    local_parameter = libsbml.LocalParameter(3, 2)
    local_parameter.setId('local_parameter_id')
    local_parameter.setName('local_parameter_name')
    local_parameter.setMetaId('local_parameter_meta_id')
    local_parameter.setSBOTerm(545)
    # local_parameter.setNotes()
    # local_parameter.setAnnotation()
    local_parameter.setValue(-1)
    local_parameter.setUnits('watt')
    complete_local_parameter = LocalParameter(local_parameter)
    return complete_local_parameter


# tests
def test_defaults(incomplete_local_parameter):
    assert incomplete_local_parameter.id is None
    assert incomplete_local_parameter.value is None
    assert incomplete_local_parameter.units is None


def test_required_attributes(incomplete_local_parameter, minimal_local_parameter):
    assert incomplete_local_parameter.required == list(incomplete_local_parameter.difference(minimal_local_parameter))
    assert not incomplete_local_parameter.has_required_attributes
    assert minimal_local_parameter.has_required_attributes


def test_init_from_kwargs(complete_local_parameter):
    local_parameter = LocalParameter(id='local_parameter_id', name='local_parameter_name',
                                     meta_id='local_parameter_meta_id', sbo_term=545,
                                     value=-1, units='watt')
    assert complete_local_parameter == local_parameter


def test_copy(complete_local_parameter):
    assert complete_local_parameter is not complete_local_parameter.copy()
    assert complete_local_parameter == complete_local_parameter.copy()


__all__ = ['incomplete_local_parameter', 'minimal_local_parameter', 'complete_local_parameter']
