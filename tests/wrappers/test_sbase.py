
import pytest
import libsbml
from sbml_tools.wrappers import SBase  # , Notes, CVTerm, Date, Creator, History, SBOTerm, Annotation


# fixtures
from tests.wrappers.test_cv_term import nested_biological_cv_term
from tests.wrappers.test_history import history


@pytest.fixture()
def sbase_species():
    # libsbml.SBase is an abstract base class that cannot be initialized,
    # the wrapped version can; but we must supply a wrapped class (not instance)
    sbase_species = SBase(libsbml.Species)  # initializes the species instance in wrapper.py
    return sbase_species


# tests
def test_defaults(sbase_species):
    assert sbase_species.id is None
    assert sbase_species.name is None
    assert sbase_species.meta_id is None
    assert sbase_species.sbo_term is None
    assert sbase_species.notes is None
    assert sbase_species.annotation is None
    assert sbase_species.history is None
    assert sbase_species.cv_terms is None


def test_set_proper_sbase_class_instance(sbase_species):
    sbase_species.wrapped = libsbml.Species(3, 2)


def test_set_improper_sbase_class_instance(sbase_species):
    with pytest.raises(Exception):
        sbase_species.wrapped = libsbml.Unit(3,2)


def test_set_proper_id(sbase_species):
    for identifier in ('id', '_1'):
        sbase_species.id = identifier
        assert sbase_species.id == identifier


def test_set_improper_id(sbase_species):
    for identifier in ('', ' id', 'id ', '1id', 'i d'):
        with pytest.raises(Exception):
            sbase_species.id = identifier


def test_set_proper_name(sbase_species):
    for name in ('1', 'some name', '%'):
        sbase_species.name = name
        assert sbase_species.name == name


def test_set_improper_name(sbase_species):
    for name in (1, None):
        with pytest.raises(Exception):
            sbase_species.name = name


def test_set_proper_meta_id(sbase_species):
    for identifier in ('id', '_1'):
        sbase_species.meta_id = identifier
        assert sbase_species.meta_id == identifier


def test_set_improper_meta_id(sbase_species):
    for identifier in (' id', 'id ', '1id', 'i d'):
        with pytest.raises(Exception):
            sbase_species.meta_id = identifier


def test_set_proper_sbo_term(sbase_species):
    for sbo_term in (14, 236, 328, 297, 406, 649):  # some random yet correct terms
        sbase_species.sbo_term = sbo_term
        assert sbase_species.sbo_term == sbo_term


def test_set_improper_sbo_term(sbase_species):
    for sbo_term in (9, 11, 64, 111, 222, 555):  # some random yet incorrect terms
        with pytest.raises(Exception):
            sbase_species.sbo_term = sbo_term


# def test_set_proper_notes(sbase_species):
#     notes = Notes()
#     sbase_species.notes = notes
#     assert sbase_species.notes == notes
#
#     notes = Notes()
#     paragraph = 'Some known additional roles of this species not described by the model:'
#     ordered_list = ['1. member of signalling cascade X',
#                     '2. actively degraded under condition Y',
#                     '3. suggested to play a role in process Z']
#     citation = Citation('Adams, A. et al.', 'Big discoveries', 2004, 'scientific journal', 7, (101, 103))
#     table = pd.DataFrame(np.random.randint(0, 100, size=(5, 4)), columns=list('ABCD'), index=list('VWXYZ'))
#     notes.add(paragraph, ordered_list, citation, table)
#     sbase.notes = notes
#
#
# def test_set_improper_notes(sbase_species):
#     notes = Notes()
#     with pytest.raises(Exception):
#         sbase_species.notes = notes


# def test_set_proper_annotation(sbase_species):
#     annotation = Annotation()
#     sbase_species.annotation = annotation
#     assert sbase_species.annotation == annotation


# def test_set_improper_annotation(sbase_species):
#     annotation = Annotation()
#     with pytest.raises(Exception):
#         sbase_species.annotation = annotation


def test_set_history(sbase_species, history):
    sbase_species.history = history
    assert sbase_species.history is not history
    assert sbase_species.history == history


def test_set_cv_term(sbase_species, nested_biological_cv_term):
    # this is important, if __eq__ doesn't hold then we might have iterated the nested terms instead
    sbase_species.cv_terms = nested_biological_cv_term
    assert sbase_species.cv_terms is not nested_biological_cv_term
    assert sbase_species.cv_terms == [nested_biological_cv_term]


def test_set_cv_terms(sbase_species, nested_biological_cv_term):
    sbase_species.cv_terms = [nested_biological_cv_term]
    assert sbase_species.cv_terms is not nested_biological_cv_term
    assert sbase_species.cv_terms == [nested_biological_cv_term]
