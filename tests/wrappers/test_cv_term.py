import pytest
import libsbml
from sbml_tools.wrappers.cv_term import CVTerm, BQB, BQM


@pytest.fixture()
def empty_cv_term():
    cv_term = libsbml.CVTerm()
    cv_term = CVTerm(cv_term)
    return cv_term


@pytest.fixture()
def simple_cv_term():
    cv_term = libsbml.CVTerm()
    cv_term.setQualifierType(1)
    cv_term.setQualifierType(libsbml.BIOLOGICAL_QUALIFIER)
    cv_term.setBiologicalQualifierType(libsbml.BQB_IS)
    cv_term.addResource('http://identifiers.org/taxonomy/1')
    cv_term = CVTerm(cv_term)
    return cv_term


@pytest.fixture()
def nested_biological_cv_term():
    def add_biological_cv_term(cv_term, qualifier_type, resource):
        nested_cv_term = libsbml.CVTerm()
        nested_cv_term.setQualifierType(cv_term.getQualifierType())
        nested_cv_term.setBiologicalQualifierType(qualifier_type)
        nested_cv_term.addResource(resource)
        cv_term.addNestedCVTerm(nested_cv_term)

    cv_term = libsbml.CVTerm()
    cv_term.setQualifierType(1)
    cv_term.setQualifierType(libsbml.BIOLOGICAL_QUALIFIER)
    cv_term.setBiologicalQualifierType(libsbml.BQB_IS)
    cv_term.addResource('http://identifiers.org/taxonomy/1')

    add_biological_cv_term(cv_term, libsbml.BQB_IS, 'http://identifiers.org/taxonomy/2')
    add_biological_cv_term(cv_term, libsbml.BQB_IS, 'http://identifiers.org/taxonomy/3')
    child2 = cv_term.getNestedCVTerm(0)
    add_biological_cv_term(child2, libsbml.BQB_IS, 'http://identifiers.org/taxonomy/4')

    nested_biological_cv_term = CVTerm(cv_term)
    return nested_biological_cv_term


def test_qualifier_proper_type_setter():
    cv = CVTerm()

    for qualifier_type in (0, 'model'):
        cv.qualifier_type = qualifier_type
        assert cv.qualifier_type == 'model'

    for qualifier_type in (1, 'biological'):
        cv.qualifier_type = qualifier_type
        assert cv.qualifier_type == 'biological'


def test_qualifier_improper_type_setter():
    cv = CVTerm()
    for qualifier_type in (-1, 2, 'unknown'):
        with pytest.raises(Exception):
            cv.qualifier_type = qualifier_type


def test_model_qualifier_proper_type_setter():
    cv = CVTerm()
    cv.qualifier_type = 0
    for name, number in BQM.items():
        if name == 'UNKNOWN':
            continue
        cv.model_qualifier = name
        assert cv.model_qualifier == name.lower()


def test_model_qualifier_improper_type_setter():
    cv = CVTerm()
    cv.qualifier_type = 1
    for name, number in BQB.items():
        with pytest.raises(Exception):
            cv.model_qualifier = name


def test_biological_qualifier_proper_type_setter():
    cv = CVTerm()
    cv.qualifier_type = 1
    for name, number in BQB.items():
        if name == 'UNKNOWN':
            continue
        cv.biological_qualifier = name
        assert cv.biological_qualifier == name.lower()


def test_biological_qualifier_improper_type_setter():
    cv = CVTerm()
    cv.qualifier_type = 0
    for name, number in BQM.items():
        with pytest.raises(Exception):
            cv.biological_qualifier = name


def test_add_proper_resources():
    cv = CVTerm()
    cv.add_resource('http://identifiers.org/taxonomy/9606')
    assert cv.resources == ['http://identifiers.org/taxonomy/9606']


def test_add_improper_resources():
    cv = CVTerm()
    with pytest.raises(Exception):
        cv.add_resource('http://identifiers.org/taxonomy/9606a')


def test_init_from_kwargs():
    qualifier_type = 'model'
    model_qualifier = 'is'
    resources = ['http://identifiers.org/taxonomy/1']
    cv_term = CVTerm(qualifier_type=qualifier_type, model_qualifier=model_qualifier, resources=resources)
    assert cv_term.qualifier_type == qualifier_type
    assert cv_term.model_qualifier == model_qualifier
    assert cv_term.resources == resources


def test_add_proper_cv_term(empty_cv_term):
    nested_cv_term = CVTerm(qualifier_type='biological', biological_qualifier='has_part',
                            resources=['http://identifiers.org/taxonomy/1'])
    empty_cv_term.add_nested_cv_terms(nested_cv_term)
    assert empty_cv_term.nested_cv_terms == [nested_cv_term]


def test_iterate_cv_term(nested_biological_cv_term):
    expected = [
        ['http://identifiers.org/taxonomy/1'],
        ['http://identifiers.org/taxonomy/2'],
        ['http://identifiers.org/taxonomy/4'],
        ['http://identifiers.org/taxonomy/3'],
        ]
    for cv_term in nested_biological_cv_term:
        assert cv_term.resources == expected.pop(0)
    assert not expected


def test_nested_cv_term_valid(nested_biological_cv_term):
    assert not nested_biological_cv_term.validate()


def test_cv_term_invalid_nested_resource(nested_biological_cv_term):
    cv_term = nested_biological_cv_term.nested_cv_terms[0].nested_cv_terms[0]
    cv_term.wrapped.addResource('not a proper resource')
    with pytest.raises(Exception):
        nested_biological_cv_term.validate()


def test_cv_term_invalid_nested_qualifier_type(nested_biological_cv_term):
    cv_term = nested_biological_cv_term.nested_cv_terms[0].nested_cv_terms[0]
    cv_term.qualifier_type = 'model'
    with pytest.raises(Exception):
        nested_biological_cv_term.validate()


def test_copy(nested_biological_cv_term):
    cv_term = nested_biological_cv_term
    assert cv_term is not cv_term.copy()
    assert cv_term == cv_term.copy()
