
import pytest
import libsbml
from sbml_tools.wrappers import Trigger, ASTNode


# fixtures
@pytest.fixture()
def incomplete_trigger():
    trigger = libsbml.Trigger(3, 2)
    incomplete_trigger = Trigger(trigger)
    return incomplete_trigger


@pytest.fixture()
def minimal_trigger():
    trigger = libsbml.Trigger(3, 2)
    trigger.setInitialValue(True)
    trigger.setPersistent(True)
    minimal_trigger = Trigger(trigger)
    return minimal_trigger


@pytest.fixture()
def complete_trigger():
    trigger = libsbml.Trigger(3, 2)
    trigger.setIdAttribute('trigger_id')
    trigger.setName('trigger_name')
    trigger.setMetaId('trigger_meta_id')
    # trigger.setSBOTerm()
    # trigger.setNotes()
    # trigger.setAnnotation()
    trigger.setInitialValue(True)
    trigger.setPersistent(True)
    trigger.setMath(libsbml.parseL3Formula('time >= 10'))
    complete_trigger = Trigger(trigger)
    return complete_trigger


# tests
def test_defaults(incomplete_trigger):
    assert incomplete_trigger.initial_value is None
    assert incomplete_trigger.persistent is None
    assert incomplete_trigger.math is None


def test_required_attributes(incomplete_trigger, minimal_trigger):
    assert incomplete_trigger.required == list(incomplete_trigger.difference(minimal_trigger))
    assert not incomplete_trigger.has_required_attributes
    assert minimal_trigger.has_required_attributes


def test_setting_math(incomplete_trigger):
    incomplete_trigger.math = 'time >= 10'
    assert incomplete_trigger.math == ASTNode.from_string('time >= 10')


def test_delay_init_from_kwargs(complete_trigger):
    trigger = Trigger(id='trigger_id', name='trigger_name', meta_id='trigger_meta_id',
                      initial_value=True, persistent=True, math='time >= 10')
    assert complete_trigger == trigger


def test_copy(complete_trigger):
    assert complete_trigger is not complete_trigger.copy()
    assert complete_trigger == complete_trigger.copy()


__all__ = ['incomplete_trigger', 'minimal_trigger', 'complete_trigger']
