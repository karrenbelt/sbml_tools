
import pytest
import libsbml
from sbml_tools.wrappers import InitialAssignment, ASTNode


# fixtures
@pytest.fixture()
def incomplete_initial_assignment():
    initial_assignment = libsbml.InitialAssignment(3, 2)
    incomplete_initial_assignment = InitialAssignment(initial_assignment)
    return incomplete_initial_assignment


@pytest.fixture()
def minimal_initial_assignment():
    initial_assignment = libsbml.InitialAssignment(3, 2)
    initial_assignment.setSymbol('x')  # also sets .id attribute
    minimal_initial_assignment = InitialAssignment(initial_assignment)
    return minimal_initial_assignment


@pytest.fixture()
def complete_initial_assignment():
    initial_assignment = libsbml.InitialAssignment(3, 2)
    initial_assignment.setIdAttribute('initial_assignment_id')  # symbol overwrites id
    initial_assignment.setName('initial_assignment_name')
    initial_assignment.setMetaId('initial_assignment_meta_id')
    initial_assignment.setSBOTerm(64)
    # initial_assignment.setNotes()
    # initial_assignment.setAnnotation()
    initial_assignment.setSymbol('x')
    initial_assignment.setMath(libsbml.parseL3Formula('2 millimole'))
    complete_initial_assignment = InitialAssignment(initial_assignment)
    return complete_initial_assignment


# tests
def test_set_symbol_sets_id_attribute(incomplete_initial_assignment):
    # known exception that setting symbol also sets the id attribute
    assert incomplete_initial_assignment.id is None
    incomplete_initial_assignment.symbol = 'x'
    assert incomplete_initial_assignment.id == 'x'


def test_defaults(incomplete_initial_assignment):
    assert incomplete_initial_assignment.symbol is None
    assert incomplete_initial_assignment.math is None


def test_required_attributes(incomplete_initial_assignment, minimal_initial_assignment):
    assert (incomplete_initial_assignment.required
            == list(incomplete_initial_assignment.difference(minimal_initial_assignment)))
    assert not incomplete_initial_assignment.has_required_attributes
    assert minimal_initial_assignment.has_required_attributes


def test_setting_math(incomplete_initial_assignment):
    incomplete_initial_assignment.math = 'x*x'
    assert incomplete_initial_assignment.math == ASTNode.from_string('x*x')


def test_init_from_kwargs(complete_initial_assignment):
    initial_assignment = InitialAssignment(id_attribute='initial_assignment_id', name='initial_assignment_name',
                                            meta_id='initial_assignment_meta_id', sbo_term=64,
                                           symbol='x', math='2 millimole')
    assert complete_initial_assignment == initial_assignment


def test_copy(complete_initial_assignment):
    assert complete_initial_assignment is not complete_initial_assignment.copy()
    assert complete_initial_assignment == complete_initial_assignment.copy()


__all__ = ['incomplete_initial_assignment', 'minimal_initial_assignment', 'complete_initial_assignment']
