
import pytest
import libsbml
from sbml_tools.wrappers import KineticLaw, LocalParameter, ASTNode
from tests.wrappers import complete_local_parameter

# fixtures
@pytest.fixture()
def minimal_kinetic_law():
    kinetic_law = libsbml.KineticLaw(3, 2)
    minimal_kinetic_law = KineticLaw(kinetic_law)
    return minimal_kinetic_law


@pytest.fixture()
def complete_kinetic_law(complete_local_parameter):
    kinetic_law = libsbml.KineticLaw(3, 2)
    kinetic_law.setId('kinetic_law_id')
    kinetic_law.setName('kinetic_law_name')
    kinetic_law.setMetaId('kinetic_law_meta_id')
    kinetic_law.setSBOTerm(64)
    # kinetic_law.setNotes()
    # kinetic_law.setAnnotation()
    kinetic_law.setMath(libsbml.parseL3Formula('kf * s1 * s2 - kr * s3 ^ 2'))
    kinetic_law.addLocalParameter(complete_local_parameter.wrapped)
    complete_kinetic_law = KineticLaw(kinetic_law)
    return complete_kinetic_law


# tests
def test_defaults(minimal_kinetic_law):
    assert minimal_kinetic_law.math is None
    assert not minimal_kinetic_law.local_parameters


def test_required_attributes(minimal_kinetic_law):
    assert minimal_kinetic_law.has_required_attributes


def test_setting_math(minimal_kinetic_law):
    minimal_kinetic_law.math = 'x*x'
    assert minimal_kinetic_law.math == ASTNode.from_string('x*x')


def test_init_from_kwargs(complete_kinetic_law, complete_local_parameter):
    local_parameters = [complete_local_parameter]
    kinetic_law = KineticLaw(id='kinetic_law_id', name='kinetic_law_name',
                             meta_id='kinetic_law_meta_id', sbo_term=64,
                             math='kf * s1 * s2 - kr * s3 ^ 2', local_parameters=local_parameters)
    assert complete_kinetic_law == kinetic_law


def test_local_parameters_from_strings(minimal_kinetic_law):
    minimal_kinetic_law.local_parameters = 'kf'
    assert list(minimal_kinetic_law.local_parameters) == [LocalParameter(id='kf')]
    minimal_kinetic_law.local_parameters = ['kf']
    assert list(minimal_kinetic_law.local_parameters) == [LocalParameter(id='kf')]
    minimal_kinetic_law.local_parameters = ['kf = 3.3']
    assert list(minimal_kinetic_law.local_parameters) == [LocalParameter(id='kf', value=3.3)]
    minimal_kinetic_law.local_parameters = ['kf = 1.0 per_mole_per_second']
    expected = [LocalParameter(id='kf', value=1.0, units='per_mole_per_second')]
    assert list(minimal_kinetic_law.local_parameters) == expected
    minimal_kinetic_law.local_parameters = ['kf = 1.0 per_mole_per_second']
    # multiple
    expected = [LocalParameter(id='kf', value=2.0, units='per_mole_per_second'),
                LocalParameter(id='kr', value=1.0, units='per_mole_per_second'), ]
    minimal_kinetic_law.local_parameters = ['kf = 2.0 per_mole_per_second', 'kr = 1.0 per_mole_per_second']
    assert list(minimal_kinetic_law.local_parameters) == expected
    minimal_kinetic_law.local_parameters = 'kf = 2.0 per_mole_per_second; kr = 1.0 per_mole_per_second'
    assert list(minimal_kinetic_law.local_parameters) == expected



def test_copy(complete_kinetic_law):
    assert complete_kinetic_law is not complete_kinetic_law.copy()
    assert complete_kinetic_law == complete_kinetic_law.copy()


__all__ = ['minimal_kinetic_law', 'complete_kinetic_law']
