
import pytest
import libsbml
from sbml_tools.wrappers import Delay, ASTNode


# fixtures
@pytest.fixture()
def minimal_delay():
    delay = libsbml.Delay(3, 2)
    minimal_delay = Delay(delay)
    return minimal_delay


@pytest.fixture()
def complete_delay():
    delay = libsbml.Delay(3, 2)
    delay.setIdAttribute('delay_id')
    delay.setName('delay_name')
    delay.setMetaId('delay_meta_id')
    delay.setSBOTerm(64)
    # delay.setNotes()
    # delay.setAnnotation()
    delay.setMath(libsbml.parseL3Formula('1'))
    complete_delay = Delay(delay)
    return complete_delay


# tests
def test_defaults(minimal_delay):
    assert minimal_delay.math is None


def test_required_attributes(minimal_delay):
    assert minimal_delay.has_required_attributes


def test_setting_math(minimal_delay):
    minimal_delay.math = 'x*x'
    assert minimal_delay.math == ASTNode.from_string('x*x')


def test_delay_init_from_kwargs(complete_delay):
    delay = Delay(id='delay_id', name='delay_name',
                  meta_id='delay_meta_id', sbo_term=64,
                  # notes='', annotation='',
                  math='1')
    assert delay == complete_delay


def test_copy(complete_delay):
    assert complete_delay is not complete_delay.copy()
    assert complete_delay == complete_delay.copy()


__all__ = ['minimal_delay', 'complete_delay']
