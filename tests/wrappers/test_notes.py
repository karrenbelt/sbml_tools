
import pytest
import libsbml

import numpy as np
import pandas as pd
from sbml_tools.wrappers import Notes, Species
# from sbml_tools.wrappers.xml_base import XHTML_NAMESPACE


# NOTE: because scope='module', the order of tests matters
# fixtures
@pytest.fixture(scope='module')  # scope is module: we test overwriting and removing
def notes():
    notes = Notes()
    return notes


# @pytest.fixture()
# def notes_with_parent():
#     species = Species()
#     xhtml = "<html:body xmlns:html='http://www.w3.org/1999/xhtml'> some notes </html:body>"
#     species.wrapped.setNotes(libsbml.XMLNode_convertStringToXMLNode(xhtml))
#     # species = SBase(libsbml.Species)
#     notes = species.notes
#     return notes


# tests
def test_defaults(notes):
    # these are xml_base attributes
    assert notes.name_space_map == {}
    assert notes.all_tags == ['notes']
    assert notes.all_texts == []
    assert notes.all_attributes == [{}]
    assert notes.all_comments == []
    assert notes.all_entities == []
    assert notes.as_string == '<notes/>\n'
    assert notes.max_depth == 0
    assert notes.doctype == ''
    assert notes.has_doctype_declaration is False
    assert notes.xml_version == '1.0'
    assert notes.has_xml_declaration is False

    assert notes.paragraphs == []
    assert notes.ordered_lists == []
    assert notes.tables == []


def test_set_empty_notes(notes):
    species = Species()
    assert not species.notes
    species.notes = notes
    assert species.notes.as_string == '<notes/>\n'


# paragraph
def test_add_paragraph(notes):
    paragraph = 'some text'
    notes.add_paragraph(paragraph)
    assert notes.paragraphs == ['some text']


def test_set_paragraph(notes):
    paragraphs = ['some text', 'more text']
    notes.paragraphs = paragraphs
    assert notes.paragraphs == paragraphs


def test_remove_paragraph(notes):
    assert not notes.paragraphs == []
    notes.remove_paragraphs()
    assert notes.paragraphs == []


# ordered lists
def test_add_ordered_list(notes):
    ordered_list = ['A. first', 'B. second']
    notes.add_ordered_list(ordered_list)
    assert notes.ordered_lists == [ordered_list]


def test_set_ordered_lists(notes):
    ordered_lists = [['1.1 a', '1.2 b', '1.3 c'], ['2.1 d', '2.2 e', '2.3 f']]
    notes.ordered_lists = ordered_lists
    assert notes.ordered_lists == ordered_lists


def test_remove_ordered_lists(notes):
    assert not notes.ordered_lists == []
    notes.remove_ordered_lists()
    assert notes.ordered_lists == []


# unordered lists
def test_add_unordered_list(notes):
    unordered_list = set('abcde')
    notes.add_unordered_list(unordered_list)
    assert notes.unordered_lists == [unordered_list]


def test_set_unordered_lists(notes):
    unordered_lists = [set('abcd'), set('ef')]
    notes.unordered_lists = unordered_lists
    assert notes.unordered_lists == unordered_lists


def test_remove_unordered_list(notes):
    notes.remove_unordered_lists()
    assert notes.unordered_lists == []


# tables
def test_tables(notes):
    tables = [pd.DataFrame([[1, 2], [3, 4], [5, 6]], columns=list('AB'), index=list('XYZ')),
              pd.DataFrame([['a', 'b'], ['c', 'd']], columns=[1, 2], index=[3, 4])]
    notes.tables = tables
    assert notes.tables[0].equals(tables[0])
    table1 = notes.tables[1]
    table1.index = pd.to_numeric(table1.index)
    table1.columns = pd.to_numeric(table1.columns)
    assert table1.equals(tables[1])
    notes.remove_tables()
    assert notes.tables == []


# test setting full notes on parent object
def test_generic_add(notes):
    paragraph = 'Some known additional roles of this species not described by the model:'
    ordered_list = ['1. member of signalling cascade X',
                    '2. actively degraded under condition Y',
                    '3. suggested to play a role in process Z']
    unordered_lists = [set('abcd'), set('ef')]

    tables = [pd.DataFrame([[1, 2], [3, 4], [5, 6]], columns=list('AB'), index=list('XYZ')),
              pd.DataFrame([['a', 'b'], ['c', 'd']], columns=[1, 2], index=[3, 4])]

    notes.add(paragraph, ordered_list, *unordered_lists, *tables)
    assert ''.join(notes.paragraphs) == paragraph
    assert notes.ordered_lists[0] == ordered_list
    assert notes.unordered_lists == unordered_lists
    assert len(notes.tables) == 2


def test_set_full_notes_on_parent(notes):
    parent = Species()
    parent.notes = notes
    assert parent.notes.paragraphs == ['Some known additional roles of this species not described by the model:']
    assert parent.notes.ordered_lists == [['1. member of signalling cascade X',
                                           '2. actively degraded under condition Y',
                                           '3. suggested to play a role in process Z']]
    assert parent.notes.unordered_lists == [{'a', 'b', 'c', 'd'}, {'e', 'f'}]
    assert len(parent.notes.tables) == 2

