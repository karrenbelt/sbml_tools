
import pytest
import libsbml
from sbml_tools.wrappers import Reaction, ASTNode
from sbml_tools.wrappers import SpeciesReference, ModifierSpeciesReference, KineticLaw, LocalParameter
from sbml_tools.wrappers.utils import string_to_species_reference


# fixtures
@pytest.fixture()
def incomplete_reaction():
    reaction = libsbml.Reaction(3, 2)
    incomplete_reaction = Reaction(reaction)
    return incomplete_reaction


@pytest.fixture()
def minimal_reaction():
    reaction = libsbml.Reaction(3, 2)
    reaction.setId('reaction_id')
    reaction.setReversible(True)
    minimal_reaction = Reaction(reaction)
    return minimal_reaction


@pytest.fixture()
def complete_reaction():

    def setup_species_reference(species_reference, species, stoichiometry, constant):
        # one cannot add libsbml.SimpleSpeciesReferences, only libsbml.Species
        # that's why we create libsbml.SpeciesReferences from the parent object instead
        species_reference.setSpecies(species)
        species_reference.setStoichiometry(stoichiometry)
        species_reference.setConstant(constant)

    def setup_local_parameter(local_parameter, identifier, value, units):
        local_parameter.setId(identifier)
        local_parameter.setValue(value)
        local_parameter.setUnits(units)

    reaction = libsbml.Reaction(3, 2)
    reaction.setId('reaction_id')
    reaction.setName('reaction_name')
    reaction.setMetaId('reaction_meta_id')
    reaction.setSBOTerm(231)
    # reaction.setNotes()
    # reaction.setAnnotation()
    reaction.setReversible(True)
    reaction.setCompartment('c')
    # reactants and products
    setup_species_reference(reaction.createReactant(), 'a', 1.0, True)
    setup_species_reference(reaction.createReactant(), 'b', 1.0, True)
    setup_species_reference(reaction.createProduct(), 'c', 2.0, True)
    # modifier
    modifier = reaction.createModifier()
    modifier.setSpecies('m')
    # kinetic law
    kinetic_law = reaction.createKineticLaw()
    kinetic_law.setMath(libsbml.parseL3Formula('kf * s1 * s2 - kr * s3 ^ 2'))
    setup_local_parameter(kinetic_law.createLocalParameter(), 'kf', 2.0, 'per_mole_per_second')
    setup_local_parameter(kinetic_law.createLocalParameter(), 'kr', 1.0, 'per_mole_per_second')

    complete_reaction = Reaction(reaction)
    return complete_reaction


# tests
def test_defaults(incomplete_reaction):
    assert incomplete_reaction.id is None
    assert incomplete_reaction.reversible is None
    assert not incomplete_reaction.reactants
    assert not incomplete_reaction.products
    assert not incomplete_reaction.modifiers


def test_required_attributes(incomplete_reaction, minimal_reaction):
    assert incomplete_reaction.required == list(incomplete_reaction.difference(minimal_reaction))
    assert not incomplete_reaction.has_required_attributes
    assert minimal_reaction.has_required_attributes


def test_init_from_kwargs(complete_reaction):
    reactants = [SpeciesReference(species='a', stoichiometry=1.0, constant=True),
                 SpeciesReference(species='b', stoichiometry=1.0, constant=True)]
    products = [SpeciesReference(species='c', stoichiometry=2.0, constant=True)]
    modifiers = [ModifierSpeciesReference(species='m')]

    local_parameters = [LocalParameter(id='kf', value=2.0, units='per_mole_per_second'),
                        LocalParameter(id='kr', value=1.0, units='per_mole_per_second')]
    kinetic_law = KineticLaw(math='kf * s1 * s2 - kr * s3 ^ 2', local_parameters=local_parameters)

    reaction = Reaction(id='reaction_id', name='reaction_name',
                        meta_id='reaction_meta_id', sbo_term=231,
                        reversible=True, compartment='c',
                        reactants=reactants, products=products, modifiers=modifiers,
                        kinetic_law=kinetic_law)
    assert complete_reaction == reaction


def test_init_modifier_from_string(incomplete_reaction):
    incomplete_reaction.modifiers = ['m']
    assert incomplete_reaction.modifiers[0].species == 'm'


def test_init_kinetic_law_from_string(incomplete_reaction):
    incomplete_reaction.kinetic_law = 'kf * s1 * s2 - kr * s3 ^ 2'
    assert incomplete_reaction.kinetic_law.math == ASTNode.from_string('kf * s1 * s2 - kr * s3 ^ 2')


# def test_reaction_formula(complete_reaction):  # TODO: now requires model
#     assert complete_reaction.formula == 'a + b <=> 2 c'


def test_set_local_parameters(incomplete_reaction):
    local_parameters = [LocalParameter(id='p'), LocalParameter(id='q')]
    with pytest.raises(Exception):
        incomplete_reaction.local_parameters = local_parameters
    incomplete_reaction.kinetic_law = '1'
    incomplete_reaction.local_parameters = local_parameters
    assert incomplete_reaction.local_parameters[1].id == 'q'


def test_init_with_local_parameters():
    local_parameters = [LocalParameter(id='p'), LocalParameter(id='q')]
    with pytest.raises(Exception):
        Reaction(id='reaction_id', reversible=True, local_parameters=local_parameters)
    reaction = Reaction(id='reaction_id', reversible=True, kinetic_law='1', local_parameters=local_parameters)
    assert list(reaction.local_parameters) == local_parameters


def test_string_to_single_species_references():

    for s, expected in [
            ('x', SpeciesReference(species='x', stoichiometry=1.0, constant=True)),
            ('X', SpeciesReference(species='X', stoichiometry=1.0, constant=True)),
            ('x1', SpeciesReference(species='x1', stoichiometry=1.0, constant=True)),
            ('_1', SpeciesReference(species='_1', stoichiometry=1.0, constant=True)),
            ('2 x', SpeciesReference(species='x', stoichiometry=2.0, constant=True)),
            ('2x', SpeciesReference(species='x', stoichiometry=2.0, constant=True)),
            ('2.0 x', SpeciesReference(species='x', stoichiometry=2.0, constant=True)),
            ('1.33x', SpeciesReference(species='x', stoichiometry=1.33, constant=True)),
            ('.33x', SpeciesReference(species='x', stoichiometry=0.33, constant=True)),
            ('1e10 x3', SpeciesReference(species='x3', stoichiometry=1e10, constant=True)),
            ('1e-10 x3', SpeciesReference(species='x3', stoichiometry=1e-10, constant=True)), ]:
        s_ref = string_to_species_reference(s)
        assert s_ref == expected


def test_string_to_multiple_species_references(incomplete_reaction):
    for s, expected in [
            ('x+2y', [SpeciesReference(species='x', stoichiometry=1.0, constant=True),
                      SpeciesReference(species='y', stoichiometry=2.0, constant=True)]),
            ('2x+2y', [SpeciesReference(species='x', stoichiometry=2.0, constant=True),
                       SpeciesReference(species='y', stoichiometry=2.0, constant=True)]),
            ('x2+y', [SpeciesReference(species='x2', stoichiometry=1.0, constant=True),
                      SpeciesReference(species='y', stoichiometry=1.0, constant=True)]),
            ('x+.3y+10z', [SpeciesReference(species='x', stoichiometry=1.0, constant=True),
                           SpeciesReference(species='y', stoichiometry=0.3, constant=True),
                           SpeciesReference(species='z', stoichiometry=10.0, constant=True)]),
            ]:
        incomplete_reaction.reactants = s
        assert list(incomplete_reaction.reactants) == expected


def test_improper_string_to_species_references():
    for s in ('1', 'x.', '-x', 'x 1', '1.1. x', 'x - y', 'e3 x', 'x + 2 2 y'):
        with pytest.raises(Exception):
            string_to_species_reference(s)


# def test_reactants_and_products_from_string(incomplete_reaction):  # TODO: now requires model
#     # generates the species references, parses them again to product the formula
#     incomplete_reaction.reactants = 'a + 2b + 1.1c + 11d2'
#     incomplete_reaction.products = '2e-3 e + 4e2 f'
#     assert incomplete_reaction.formula == 'a + 2 b + 1.1 c + 11 d2 <=> 0.002 e + 400 f'


def test_local_parameters_from_strings(incomplete_reaction):
    expected = [LocalParameter(id='kf', value=2.0, units='per_mole_per_second'),
                LocalParameter(id='kr', value=1.0, units='per_mole_per_second'),]
    incomplete_reaction.kinetic_law = ''  # initializes empty kinetic law required to set local parameter on
    incomplete_reaction.local_parameters = ['kf = 2.0 per_mole_per_second', 'kr = 1.0 per_mole_per_second']
    assert list(incomplete_reaction.local_parameters) == expected


def test_init_from_kwarg_strings(complete_reaction):
    # have to assume a default value for species_reference 'constant', which in this case defaults to True
    reaction = Reaction(id='reaction_id', name='reaction_name',
                        meta_id='reaction_meta_id', sbo_term='occurring entity representation',
                        reversible=True, compartment='c',
                        reactants='a + b', products='2 c', modifiers='m',
                        kinetic_law='kf * s1 * s2 - kr * s3 ^ 2',
                        local_parameters='kf = 2.0 per_mole_per_second; kr = 1.0 per_mole_per_second')
    assert complete_reaction == reaction


def test_copy(complete_reaction):
    assert complete_reaction is not complete_reaction.copy()
    assert complete_reaction == complete_reaction.copy()


__all__ = ['incomplete_reaction', 'minimal_reaction', 'complete_reaction']
