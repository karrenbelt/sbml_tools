
import pytest
import libsbml
from sbml_tools.wrappers import Compartment


# fixtures
@pytest.fixture()
def incomplete_compartment():
    compartment = libsbml.Compartment(3, 2)
    incomplete_compartment = Compartment(compartment)
    return incomplete_compartment


@pytest.fixture()
def minimal_compartment():
    compartment = libsbml.Compartment(3, 2)
    compartment.setId('compartment_id')
    compartment.setConstant(False)
    minimal_compartment = Compartment(compartment)
    return minimal_compartment


@pytest.fixture()
def complete_compartment():
    compartment = libsbml.Compartment(3, 2)
    compartment.setId('compartment_id')
    compartment.setName('compartment_name')
    compartment.setMetaId('compartment_meta_id')
    compartment.setSBOTerm(290)
    # compartment.setNotes()
    # compartment.setAnnotation()
    compartment.setConstant(False)
    compartment.setSpatialDimensions(3)
    compartment.setSize(1.0)
    compartment.setUnits('litre')
    complete_compartment = Compartment(compartment)
    return complete_compartment


# tests
def test_defaults(incomplete_compartment):
    assert incomplete_compartment.spatial_dimensions is None
    assert incomplete_compartment.size is None
    assert incomplete_compartment.constant is None
    assert incomplete_compartment.units is None


def test_set_required_attributes(incomplete_compartment, minimal_compartment):
    assert incomplete_compartment.required == list(incomplete_compartment.difference(minimal_compartment))
    assert not incomplete_compartment.has_required_attributes
    assert minimal_compartment.has_required_attributes


def test_init_from_kwargs(complete_compartment):
    compartment = Compartment(id='compartment_id', name='compartment_name',
                              meta_id='compartment_meta_id', sbo_term=290,
                              # notes='', annotation='',
                              constant=False, spatial_dimensions=3, size=1.0, units='litre')
    assert complete_compartment == compartment


def test_copy(complete_compartment):
    assert complete_compartment is not complete_compartment.copy()
    assert complete_compartment == complete_compartment.copy()


def test_setter_constant_proper_boolean(incomplete_compartment):
    for boolean in (True, False):
        incomplete_compartment.constant = boolean
        assert incomplete_compartment.constant is boolean


def test_setter_constant_improper_value_type(incomplete_compartment):
    for value in (0, 1, 'True'):
        with pytest.raises(TypeError):
            incomplete_compartment.constant = value


def test_setter_size_proper_value(incomplete_compartment):  # can also be set to float('nan')
    for value in (0.0, 1.0, 333.33, 2e33, 2e-33):
        incomplete_compartment.size = value
        assert incomplete_compartment.size == value


def test_setter_size_improper_value(incomplete_compartment):
    with pytest.raises(Exception):
        incomplete_compartment.size = - 1.0


def test_setter_spatial_dimensions_proper_value(incomplete_compartment):
    for value in (0, 1, 2, 3):
        incomplete_compartment.spatial_dimensions = value
        assert incomplete_compartment.spatial_dimensions == value


def test_setter_spatial_dimensions_improper_value(incomplete_compartment):
    for value in (-1, 0.5, 20):
        with pytest.raises(Exception):
            incomplete_compartment.spatial_dimensions = value


def test_setter_units_proper_values(incomplete_compartment):
    incomplete_compartment.spatial_dimensions = 1
    for units in ('metre', 'dimensionless', 'nanometre'):
        incomplete_compartment.units = units  # some valid sid to non-existing unit
        assert incomplete_compartment.units == units
    incomplete_compartment.spatial_dimensions = 2
    for units in ('squared_metre', 'dimensionless'):
        incomplete_compartment.units = units  # some valid sid to non-existing unit
        assert incomplete_compartment.units == units
    incomplete_compartment.spatial_dimensions = 3
    for units in ('litre', 'metre3', 'dimensionless'):
        incomplete_compartment.units = units  # some valid sid to non-existing unit
        assert incomplete_compartment.units == units


def test_setter_units_spatial_dimensions_not_set(incomplete_compartment):
    for units in ('metre', ):
        with pytest.raises(Exception):
            incomplete_compartment.units = units


def test_setter_units_improper_values(incomplete_compartment):
    incomplete_compartment.spatial_dimensions = 1
    for units in ('kelvin', 'joule', 'volt'):
        with pytest.raises(Exception):
            incomplete_compartment.units = units


def test_compartment_from_proper_kwargs():
    for kwargs in (
            dict(id='c', constant=True),
            dict(id='c', constant=False, spatial_dimensions=3, size=2.0),
            dict(id='c', name='cytosol', constant=False, spatial_dimensions=0, units='dimensionless'),
            dict(id='c', name='cytosol', constant=False, spatial_dimensions=1, units='metre'),
            dict(id='c', name='cytosol', constant=False, spatial_dimensions=2, units='squared_metre'),
            dict(id='c', name='cytosol', constant=False, spatial_dimensions=3, units='cubic_metre'),
            ):
        assert Compartment(**kwargs)


def test_compartment_from_improper_kwargs():
    for kwargs in (
            dict(id='c'),
            dict(id='c', constant=True, units='litre'),
            dict(id='c', name='cytosol', constant=False, spatial_dimensions=1, units='litre'),
            ):
        with pytest.raises(Exception):
            Compartment(**kwargs)


__all__ = ['incomplete_compartment', 'minimal_compartment', 'complete_compartment']
