
import pytest
import libsbml
from sbml_tools.wrappers import ASTNode, Trigger, Priority, Delay, EventAssignment
from sbml_tools.wrappers import Event

from tests.wrappers import complete_trigger, complete_priority, complete_delay, complete_event_assignment


# fixtures
@pytest.fixture()
def incomplete_event():
    event = libsbml.Event(3, 2)
    incomplete_event = Event(event)
    return incomplete_event


@pytest.fixture()
def minimal_event():
    event = libsbml.Event(3, 2)
    event.setUseValuesFromTriggerTime(True)
    minimal_event = Event(event)
    return minimal_event


@pytest.fixture()
def complete_event(complete_trigger, complete_priority, complete_delay, complete_event_assignment):
    event = libsbml.Event(3, 2)
    event.setIdAttribute('event_id')
    event.setName('event_name')
    event.setMetaId('event_meta_id')
    event.setSBOTerm(231)
    # event.setNotes()
    # event.setAnnotation()
    event.setUseValuesFromTriggerTime(True)
    event.setTrigger(complete_trigger.wrapped)
    event.setPriority(complete_priority.wrapped)
    event.setDelay(complete_delay.wrapped)
    event.addEventAssignment(complete_event_assignment.wrapped)
    complete_event = Event(event)
    return complete_event


# tests
def test_defaults(incomplete_event):
    assert incomplete_event.use_values_from_trigger_time is None
    assert incomplete_event.trigger is None
    assert incomplete_event.priority is None
    assert incomplete_event.delay is None


def test_required_attributes(incomplete_event, minimal_event):
    assert incomplete_event.required == list(incomplete_event.difference(minimal_event))
    assert not incomplete_event.has_required_attributes
    assert minimal_event.has_required_attributes


def test_setting_trigger(incomplete_event):
    incomplete_event.trigger = Trigger(initial_value=True, persistent=False)
    assert incomplete_event.trigger.initial_value is True
    assert incomplete_event.trigger.persistent is False


def test_setting_delay(incomplete_event):
    incomplete_event.delay = Delay()
    assert incomplete_event.delay
    incomplete_event.delay = '5 seconds'
    assert incomplete_event.delay.math == ASTNode.from_string('5 seconds')


def test_setting_priority(incomplete_event):
    incomplete_event.priority = Priority()
    assert incomplete_event.priority
    incomplete_event.priority = 'sin(time)'
    assert incomplete_event.priority.math == ASTNode.from_string('sin(time)')


def test_setting_event_assignment(incomplete_event):
    incomplete_event.event_assignments = EventAssignment(variable='x')
    assert incomplete_event.event_assignments[0].variable == 'x'
    incomplete_event.event_assignments = [EventAssignment(variable='y')]
    assert incomplete_event.event_assignments[0].variable == 'y'


def test_setting_event_assignment_from_string(incomplete_event):
    incomplete_event.add_event_assignment('x = cos(time)')
    assert list(incomplete_event.event_assignments) == [EventAssignment(variable='x', math='cos(time)')]
    incomplete_event.event_assignments = ['x = 3 mole', 'y = 3*(z-1)']
    assert list(incomplete_event.event_assignments) == [EventAssignment(variable='x', math='3 mole'),
                                                        EventAssignment(variable='y', math='3 * (z - 1)')]


def test_improper_setting_event_assignments(incomplete_event):
    for event_assignments in ('1', '1 = 3 = 4', ['sin(x)']):
        with pytest.raises(Exception):
            incomplete_event.event_assignments = event_assignments


def test_init_from_kwargs(complete_event, complete_trigger, complete_priority,
                          complete_delay, complete_event_assignment):
    event = Event(id='event_id', name='event_name',
                  meta_id='event_meta_id', sbo_term=231,
                  # notes='', annotation='',
                  use_values_from_trigger_time=True,
                  trigger=complete_trigger,
                  delay=complete_delay, priority=complete_priority, event_assignments=[complete_event_assignment],
                  )
    assert event == complete_event


__all__ = ['incomplete_event', 'minimal_event', 'complete_event']
