
import pytest
import cmath
import libsbml
from sbml_tools.wrappers import SpeciesReference


# fixtures
@pytest.fixture()
def incomplete_species_reference():
    species_reference = libsbml.SpeciesReference(3, 2)
    incomplete_species_reference = SpeciesReference(species_reference)
    return incomplete_species_reference


@pytest.fixture()
def minimal_species_reference():
    species_reference = libsbml.SpeciesReference(3, 2)
    species_reference.setSpecies('x')
    species_reference.setConstant(True)
    minimal_species_reference = SpeciesReference(species_reference)
    return minimal_species_reference


@pytest.fixture()
def complete_species_reference():
    species_reference = libsbml.SpeciesReference(3, 2)
    species_reference.setId('species_reference_id')
    species_reference.setName('species_reference_name')
    species_reference.setMetaId('species_reference_meta_id')
    # species_reference.setSBOTerm()
    # species_reference.setNotes()
    # species_reference.setAnnotation()
    species_reference.setSpecies('x')
    species_reference.setStoichiometry(2.0)
    species_reference.setConstant(True)
    complete_species_reference = SpeciesReference(species_reference)
    return complete_species_reference


# tests
def test_defaults(incomplete_species_reference):
    assert incomplete_species_reference.species is ''
    assert incomplete_species_reference.stoichiometry is None
    assert incomplete_species_reference.constant is None


def test_has_required_attributes(incomplete_species_reference, minimal_species_reference):
    assert (incomplete_species_reference.required
            == list(incomplete_species_reference.difference(minimal_species_reference)))
    assert not incomplete_species_reference.has_required_attributes
    assert minimal_species_reference.has_required_attributes


def test_setting_proper_stoichiometry(incomplete_species_reference):
    for stoichiometry in (1e-10, 3.3, 10, 2e99):
        incomplete_species_reference.stoichiometry = stoichiometry
        assert incomplete_species_reference.stoichiometry == stoichiometry
    incomplete_species_reference.stoichiometry = float('nan')
    assert cmath.isnan(incomplete_species_reference.stoichiometry)


def test_setting_improper_stoichiometry(incomplete_species_reference):
    with pytest.raises(Exception):
        incomplete_species_reference.stoichiometry = -1.0


def test_init_from_kwargs(complete_species_reference):
    species_reference = SpeciesReference(id='species_reference_id', name='species_reference_name',
                                         meta_id='species_reference_meta_id',
                                         species='x', stoichiometry=2.0, constant=True)
    assert complete_species_reference == species_reference


def test_copy(complete_species_reference):
    assert complete_species_reference is not complete_species_reference.copy()
    assert complete_species_reference == complete_species_reference.copy()


__all__ = ['incomplete_species_reference', 'minimal_species_reference', 'complete_species_reference']
