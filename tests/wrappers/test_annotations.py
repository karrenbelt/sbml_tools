
import pytest
import libsbml
from sbml_tools.wrappers import Annotation, SBase
from tests.wrappers.test_cv_term import simple_cv_term
from tests.wrappers.test_history import history
# fixtures


@pytest.fixture()
def annotated_sbase(history, simple_cv_term):
    sbase_species = SBase(libsbml.Species)
    sbase_species.meta_id = 'meta_id'
    sbase_species.cv_terms = simple_cv_term
    sbase_species.history = history
    annotated_sbase = sbase_species
    return annotated_sbase


@pytest.fixture()
def annotation(annotated_sbase):
    # testing history and cv_terms, which are part of annotations but can be accessed via sbase directly
    # and more conveniently. This mostly serves proprietary namespaces, in case they were to exist
    annotation = annotated_sbase.annotation
    return annotation


# tests
def test_default():
    # these are xml_base attributes
    annotation = Annotation()
    assert annotation.name_space_map == {}
    assert annotation.all_tags == ['annotation']
    assert annotation.all_texts == []
    assert annotation.all_attributes == [{}]
    assert annotation.all_comments == []
    assert annotation.all_entities == []
    assert annotation.as_string == '<annotation/>\n'
    assert annotation.max_depth == 0
    assert annotation.doctype == ''
    assert annotation.has_doctype_declaration is False
    assert annotation.xml_version == '1.0'
    assert annotation.has_xml_declaration is False


def test_name_space_map(annotated_sbase, annotation):
    assert annotated_sbase.wrapped.annotation.getNamespaces().getNumNamespaces() == len(annotation.name_space_map)


def test_as_string(annotated_sbase, annotation):
    assert annotated_sbase.wrapped.annotation.toXMLString() == annotation.as_string.strip('\n')


def test_all_tags(annotation):

    assert annotation.all_tags == [
        'annotation',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}RDF',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Description',
        '{http://purl.org/dc/terms/}creator',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Bag',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}li',
        '{http://www.w3.org/2006/vcard/ns#}hasEmail',
        '{http://www.w3.org/2006/vcard/ns#}organization-name',
        '{http://purl.org/dc/terms/}created',
        '{http://purl.org/dc/terms/}W3CDTF',
        '{http://purl.org/dc/terms/}modified',
        '{http://purl.org/dc/terms/}W3CDTF',
        '{http://biomodels.net/biology-qualifiers/}is',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}Bag',
        '{http://www.w3.org/1999/02/22-rdf-syntax-ns#}li', ]


def test_all_texts(annotation):
    expected = 'john@doe.com organization 2000-01-01T00:00:00Z 2000-01-01T00:00:00Z'
    assert ' '.join(''.join(annotation.all_texts).split()) == expected


def test_all_attributes(annotation):
    assert annotation.all_attributes == [
        {},
        {},
        {'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}about': '#meta_id'},
        {},
        {},
        {'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}parseType': 'Resource'},
        {},
        {},
        {'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}parseType': 'Resource'},
        {},
        {'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}parseType': 'Resource'},
        {},
        {},
        {},
        {'{http://www.w3.org/1999/02/22-rdf-syntax-ns#}resource': 'http://identifiers.org/taxonomy/1'}]


def test_remove_annotation(annotated_sbase):
    annotated_sbase.remove_annotation()
    assert annotated_sbase.annotation is None


def find_all_by_tag(annotation):
    assert annotation.findall_by_tag('creator')
    assert annotation.findall_by_tag('created')
    assert annotation.findall_by_tag('hasEmail')


def test_setting_annotation(annotated_sbase, annotation):
    # creators don't get set properly
    copy = annotated_sbase.copy()
    annotated_sbase.remove_annotation()
    annotated_sbase.annotation = annotation

    assert annotated_sbase.cv_terms == copy.cv_terms
    assert annotated_sbase.history.created_date == copy.history.created_date
    assert annotated_sbase.history.modified_dates == copy.history.modified_dates


def test_seting_creator_via_annotation_failing(annotated_sbase, annotation):
    # this shouldn't be failing but it, as of now only a warning is printed with suggestion to set via history
    copy = annotated_sbase.copy()
    annotated_sbase.remove_annotation()
    annotated_sbase.annotation = annotation

    with pytest.raises(Exception):
        assert annotated_sbase.history.creators == copy.history.creators
    with pytest.raises(Exception):
        assert annotated_sbase.history == copy.history
    with pytest.raises(Exception):
        assert annotated_sbase.annotation == annotation

