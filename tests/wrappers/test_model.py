
import pytest
import libsbml

from sbml_tools import config
from sbml_tools.wrappers import Model
from sbml_tools.wrappers import (
    ListOfFunctionDefinitions,
    ListOfUnitDefinitions,
    ListOfCompartments,
    ListOfSpecies,
    ListOfParameters,
    ListOfInitialAssignments,
    ListOfRules,
    ListOfConstraints,
    ListOfReactions, SpeciesReference, ModifierSpeciesReference,
    ListOfEvents, )

# fixtures
# first import all other fixtures from other tests
from tests.wrappers.test_history import history
from tests.wrappers.test_priority import complete_priority
from tests.wrappers.test_delay import complete_delay
from tests.wrappers.test_event_assignment import complete_event_assignment
from tests.wrappers import (
    complete_function_definition,
    complete_unit_definition,
    complete_compartment,
    complete_species,
    complete_parameter,
    complete_initial_assignment,
    complete_algebraic_rule,
    complete_assignment_rule,
    complete_rate_rule,
    complete_constraint,
    complete_reaction,
    complete_event, complete_trigger
    )


@pytest.fixture()
def minimal_model():
    model = libsbml.Model(3, 2)
    minimal_model = Model(model)
    return minimal_model


@pytest.fixture()
def complete_model(
        complete_function_definition,
        complete_unit_definition,
        complete_compartment,
        complete_species,
        complete_parameter,
        complete_initial_assignment,
        complete_algebraic_rule,
        complete_assignment_rule,
        complete_rate_rule,
        complete_constraint,
        complete_reaction,
        complete_event,
        ):
    model = libsbml.Model(3, 2)
    model.setIdAttribute('model_id')
    model.setName('model_name')
    model.setMetaId('model_meta_id')
    model.setSBOTerm(62)
    # model.setNotes()
    # model.setAnnotation()
    model.addFunctionDefinition(complete_function_definition.wrapped)
    model.addUnitDefinition(complete_unit_definition.wrapped)
    model.addCompartment(complete_compartment.wrapped)
    model.addSpecies(complete_species.wrapped)
    model.addParameter(complete_parameter.wrapped)
    model.addInitialAssignment(complete_initial_assignment.wrapped)
    model.addRule(complete_algebraic_rule.wrapped)
    model.addRule(complete_assignment_rule.wrapped)
    model.addRule(complete_rate_rule.wrapped)
    model.addConstraint(complete_constraint.wrapped)
    model.addReaction(complete_reaction.wrapped)
    model.addEvent(complete_event.wrapped)
    complete_model = Model(model)
    return complete_model


# tests
def test_defaults(minimal_model):
    # units
    assert minimal_model.substance_units is None
    assert minimal_model.time_units is None
    assert minimal_model.volume_units is None
    assert minimal_model.area_units is None
    assert minimal_model.length_units is None
    assert minimal_model.extent_units is None

    # lists
    assert minimal_model.function_definitions == ListOfFunctionDefinitions()
    assert not minimal_model.unit_definitions
    assert not minimal_model.compartments
    assert not minimal_model.species
    assert not minimal_model.parameters
    assert not minimal_model.initial_assignments
    assert not minimal_model.rules
    assert not minimal_model.constraints
    assert not minimal_model.reactions
    assert not minimal_model.events


def test_all_lists_of_correct_type(minimal_model):
    assert isinstance(minimal_model.function_definitions, ListOfFunctionDefinitions)
    assert isinstance(minimal_model.unit_definitions, ListOfUnitDefinitions)
    assert isinstance(minimal_model.compartments, ListOfCompartments)
    assert isinstance(minimal_model.species, ListOfSpecies)
    assert isinstance(minimal_model.parameters, ListOfParameters)
    assert isinstance(minimal_model.initial_assignments, ListOfInitialAssignments)
    assert isinstance(minimal_model.rules, ListOfRules)
    assert isinstance(minimal_model.constraints, ListOfConstraints)
    assert isinstance(minimal_model.reactions, ListOfReactions)
    assert isinstance(minimal_model.events, ListOfEvents)


def test_required_attributes(minimal_model):
    assert minimal_model.has_required_attributes


def test_copy(complete_model):
    assert complete_model is not complete_model.copy()
    assert complete_model == complete_model.copy()


def test_complete_model_setup(
        complete_model,
        complete_function_definition,
        complete_unit_definition,
        complete_compartment,
        complete_species,
        complete_parameter,
        complete_initial_assignment,
        complete_algebraic_rule,
        complete_assignment_rule,
        complete_rate_rule,
        complete_constraint,
        complete_reaction,
        complete_event,):
    assert complete_model.function_definitions == ListOfFunctionDefinitions([complete_function_definition])
    assert complete_model.unit_definitions == ListOfUnitDefinitions([complete_unit_definition])
    assert complete_model.compartments == ListOfCompartments([complete_compartment])
    assert complete_model.species == ListOfSpecies([complete_species])
    assert complete_model.parameters == ListOfParameters([complete_parameter])
    assert complete_model.initial_assignments == ListOfInitialAssignments([complete_initial_assignment])
    assert complete_model.rules == ListOfRules([complete_algebraic_rule,
                                                complete_assignment_rule,
                                                complete_rate_rule])
    assert complete_model.constraints == ListOfConstraints([complete_constraint])
    assert complete_model.reactions == ListOfReactions([complete_reaction])
    assert complete_model.events == ListOfEvents([complete_event])


def test_init_from_kwargs(
        complete_model,):
    # we have to set config.ENFORCE_SPECIES_BEFORE_SPECIES_REFERENCES as False, as they are not species in the model
    config.ENFORCE_SPECIES_BEFORE_SPECIES_REFERENCES = False
    config.ENFORCE_COMPARTMENT_BEFORE_SPECIES = False
    model = Model(**complete_model.as_kwargs)
    assert model == complete_model


# model units
def test_set_proper_substance_units(minimal_model):
    for substance_units in ('mole', 'item', 'dimensionless', 'kilogram', 'gram', 'millimole'):
        minimal_model.substance_units = substance_units
        assert minimal_model.substance_units == substance_units  # UnitSId ref set?
        assert minimal_model.unit_definitions[substance_units]  # UnitDefinition exists?


def test_set_improper_substance_units(minimal_model):
    for substance_units in ('second', 'per_gram', 'cubic_gram'):
        with pytest.raises(Exception):
            minimal_model.substance_units = substance_units


def test_set_proper_time_units(minimal_model):
    for time_units in ('second', 'dimensionless', 'hour', 'femtosecond'):
        minimal_model.time_units = time_units
        assert minimal_model.time_units == time_units
        assert minimal_model.unit_definitions[time_units]


def test_set_improper_time_units(minimal_model):
    for time_units in ('millimole', 'per_second', 'second2'):
        with pytest.raises(Exception):
            minimal_model.time_units = time_units


def test_set_proper_volume_units(minimal_model):
    for volume_units in ('litre', 'dimensionless', 'cubic_millimetre', 'metre3'):
        minimal_model.volume_units = volume_units
        assert minimal_model.volume_units == volume_units
        assert minimal_model.unit_definitions[volume_units]


def test_set_improper_volume_units(minimal_model):
    for volume_units in ('mole', 'litre2', 'metre2'):
        with pytest.raises(Exception):
            minimal_model.volume_units = volume_units


def test_set_proper_area_units(minimal_model):
    for area_units in ('metre2', 'dimensionless', 'squared_metre'):
        minimal_model.area_units = area_units
        assert minimal_model.area_units == area_units
        assert minimal_model.unit_definitions[area_units]


def test_set_improper_area_units(minimal_model):
    for area_units in ('second', 'litre2', 'metre3'):
        with pytest.raises(Exception):
            minimal_model.area_units = area_units


def test_set_proper_length_units(minimal_model):
    for length_units in ('metre', 'dimensionless', 'nanometre'):
        minimal_model.length_units = length_units
        assert minimal_model.length_units == length_units
        assert minimal_model.unit_definitions[length_units]


def test_set_improper_length_units(minimal_model):
    for length_units in ('second', 'litre2', 'metre3'):
        with pytest.raises(Exception):
            minimal_model.length_units = length_units


def test_set_proper_extent_units(minimal_model):
    for extent_units in ('mole', 'item', 'dimensionless', 'kilogram', 'gram', 'picogram'):
        minimal_model.extent_units = extent_units
        assert minimal_model.extent_units == extent_units
        assert minimal_model.unit_definitions[extent_units]


def test_set_improper_extent_units(minimal_model):
    for extent_units in ('mole2', 'per_item', 'cubic_pico_gram'):
        with pytest.raises(Exception):
            minimal_model.extent_units = extent_units


def test_set_conversion_factor(minimal_model):
    minimal_model.conversion_factor = 'ConversionFactorSIdRef'
    assert minimal_model.conversion_factor == 'ConversionFactorSIdRef'


# this next part is a repeating pattern for the different lists take make up the model
# function_definition
def test_add_function_definition(minimal_model, complete_function_definition):
    minimal_model.add_function_definition(complete_function_definition)
    assert list(minimal_model.function_definitions) == [complete_function_definition]


def test_set_function_definitions(minimal_model, complete_function_definition):
    minimal_model.function_definitions = [complete_function_definition]
    assert list(minimal_model.function_definitions) == [complete_function_definition]


def test_create_function_definition(minimal_model, complete_function_definition):
    minimal_model.create_function_definition(**complete_function_definition.as_kwargs)
    assert list(minimal_model.function_definitions) == [complete_function_definition]


def test_remove_function_definition(complete_model, complete_function_definition):
    assert complete_model.function_definitions
    function_definition = complete_model.remove_function_definitions(complete_function_definition.id)
    assert not complete_model.function_definitions
    assert function_definition == complete_function_definition


# unit_definition
def test_add_unit_definition(minimal_model, complete_unit_definition):
    minimal_model.add_unit_definition(complete_unit_definition)
    assert list(minimal_model.unit_definitions) == [complete_unit_definition]


def test_set_unit_definitions(minimal_model, complete_unit_definition):
    minimal_model.unit_definitions = [complete_unit_definition]
    assert list(minimal_model.unit_definitions) == [complete_unit_definition]


def test_create_unit_definition(minimal_model, complete_unit_definition):
    minimal_model.create_unit_definition(**complete_unit_definition.as_kwargs)
    assert list(minimal_model.unit_definitions) == [complete_unit_definition]


def test_remove_unit_definition(complete_model, complete_unit_definition):
    assert complete_model.unit_definitions
    unit_definition = complete_model.remove_unit_definitions(complete_unit_definition.id)
    assert not complete_model.unit_definitions
    assert unit_definition == complete_unit_definition


# compartment
def test_add_compartment(minimal_model, complete_compartment):
    minimal_model.add_compartment(complete_compartment)
    assert list(minimal_model.compartments) == [complete_compartment]


def test_set_compartments(minimal_model, complete_compartment):
    minimal_model.compartments = [complete_compartment]
    assert list(minimal_model.compartments) == [complete_compartment]


def test_create_compartment(minimal_model, complete_compartment):
    minimal_model.create_compartment(**complete_compartment.as_kwargs)
    assert list(minimal_model.compartments) == [complete_compartment]


def test_remove_compartment(complete_model, complete_compartment):
    assert complete_model.compartments
    compartment = complete_model.remove_compartments(complete_compartment.id)
    assert not complete_model.compartments
    assert compartment == complete_compartment


# species
def test_add_species(minimal_model, complete_species):
    minimal_model.add_species(complete_species)
    assert list(minimal_model.species) == [complete_species]


def test_set_species(minimal_model, complete_species):
    minimal_model.species = [complete_species]
    assert list(minimal_model.species) == [complete_species]


def test_create_species(minimal_model, complete_species):
    minimal_model.create_species(**complete_species.as_kwargs)
    assert list(minimal_model.species) == [complete_species]


def test_remove_species(complete_model, complete_species):
    assert complete_model.species
    species = complete_model.remove_species(complete_species.id)
    assert not complete_model.species
    assert species == complete_species


# parameter
def test_add_parameter(minimal_model, complete_parameter):
    minimal_model.add_parameter(complete_parameter)
    assert list(minimal_model.parameters) == [complete_parameter]


def test_set_parameter(minimal_model, complete_parameter):
    minimal_model.parameters = [complete_parameter]
    assert list(minimal_model.parameters) == [complete_parameter]


def test_create_parameter(minimal_model, complete_parameter):
    minimal_model.create_parameter(**complete_parameter.as_kwargs)
    assert list(minimal_model.parameters) == [complete_parameter]


def test_remove_parameter(complete_model, complete_parameter):
    assert complete_model.parameters
    parameter = complete_model.remove_parameters(complete_parameter.id)
    assert not complete_model.parameters
    assert parameter == complete_parameter


# initial_assignment
def test_add_initial_assignment(minimal_model, complete_initial_assignment):
    minimal_model.add_initial_assignment(complete_initial_assignment)
    assert list(minimal_model.initial_assignments) == [complete_initial_assignment]


def test_set_initial_assignment(minimal_model, complete_initial_assignment):
    minimal_model.initial_assignments = [complete_initial_assignment]
    assert list(minimal_model.initial_assignments) == [complete_initial_assignment]


def test_create_initial_assignment(minimal_model, complete_initial_assignment):
    minimal_model.create_initial_assignment(**complete_initial_assignment.as_kwargs)
    assert list(minimal_model.initial_assignments) == [complete_initial_assignment]


def test_remove_initial_assignment(complete_model, complete_initial_assignment):
    # must be done by symbol, which .id equates to
    assert complete_model.initial_assignments
    initial_assignment = complete_model.remove_initial_assignments(complete_initial_assignment.id)
    assert not complete_model.initial_assignments
    assert initial_assignment == complete_initial_assignment


# rules
def test_add_rule(minimal_model, complete_algebraic_rule, complete_assignment_rule, complete_rate_rule):
    minimal_model.add_rule(complete_algebraic_rule)
    minimal_model.add_rule(complete_assignment_rule)
    minimal_model.add_rule(complete_rate_rule)
    assert list(minimal_model.rules) == [complete_algebraic_rule, complete_assignment_rule, complete_rate_rule]


def test_set_rule(minimal_model, complete_algebraic_rule, complete_assignment_rule, complete_rate_rule):
    minimal_model.rules = [complete_algebraic_rule, complete_assignment_rule, complete_rate_rule]
    assert list(minimal_model.rules) == [complete_algebraic_rule, complete_assignment_rule, complete_rate_rule]


def test_create_rule(minimal_model, complete_algebraic_rule, complete_assignment_rule, complete_rate_rule):
    minimal_model.create_algebraic_rule(**complete_algebraic_rule.as_kwargs)
    minimal_model.create_assignment_rule(**complete_assignment_rule.as_kwargs)
    minimal_model.create_rate_rule(**complete_rate_rule.as_kwargs)
    assert list(minimal_model.rules) == [complete_algebraic_rule, complete_assignment_rule, complete_rate_rule]


def test_remove_rule(complete_model, complete_algebraic_rule, complete_assignment_rule, complete_rate_rule):
    # must be done by symbol, which .id equates to
    assert len(complete_model.rules) == 3
    algebraic_rule = complete_model.remove_rules(0)  # algebraic rule has no 'id' attribute referencing a variable
    assignment_rule = complete_model.remove_rules(complete_assignment_rule.id)
    rate_rule = complete_model.remove_rules(complete_rate_rule.id)
    assert not complete_model.rules
    rules = [algebraic_rule, assignment_rule, rate_rule]
    assert rules == [complete_algebraic_rule, complete_assignment_rule, complete_rate_rule]


# constraint
def test_add_constraint(minimal_model, complete_constraint):
    minimal_model.add_constraint(complete_constraint)
    assert list(minimal_model.constraints) == [complete_constraint]


def test_set_constraint(minimal_model, complete_constraint):
    minimal_model.constraints = [complete_constraint]
    assert list(minimal_model.constraints) == [complete_constraint]


# def test_create_constraint(minimal_model, complete_constraint):
#     minimal_model.create_constraint(**complete_constraint.as_kwargs)
#     assert list(minimal_model.constraints) == [complete_constraint]


# def test_remove_constraint(complete_model, complete_constraint):
#     assert complete_model.constraints
#     constraint = complete_model.remove_constraint(complete_constraint.id)
#     assert not complete_model.constraints
#     assert constraint == complete_constraint


# reactions
def test_add_reaction(minimal_model, complete_reaction):
    minimal_model.add_reaction(complete_reaction)
    assert list(minimal_model.reactions) == [complete_reaction]


def test_set_reaction(minimal_model, complete_reaction):
    minimal_model.reactions = [complete_reaction]
    assert list(minimal_model.reactions) == [complete_reaction]


def test_create_reaction(minimal_model, complete_reaction):
    minimal_model.create_reaction(**complete_reaction.as_kwargs)
    assert list(minimal_model.reactions) == [complete_reaction]


def test_remove_reaction(complete_model, complete_reaction):
    assert complete_model.reactions
    reaction = complete_model.remove_reactions(complete_reaction.id)
    assert not complete_model.reactions
    assert reaction == complete_reaction


# events
def test_add_event(minimal_model, complete_event):
    minimal_model.add_event(complete_event)
    assert list(minimal_model.events) == [complete_event]


def test_set_event(minimal_model, complete_event):
    minimal_model.events = [complete_event]
    assert list(minimal_model.events) == [complete_event]


def test_create_event(minimal_model, complete_event):
    minimal_model.create_event(**complete_event.as_kwargs)
    assert list(minimal_model.events) == [complete_event]


def test_remove_event(complete_model, complete_event):
    assert complete_model.events
    event = complete_model.remove_events(complete_event.id)
    assert not complete_model.events
    assert event == complete_event


# other tests
def test_reaction_creation_fails_when_species_non_existing(minimal_model):
    config.ENFORCE_SPECIES_BEFORE_SPECIES_REFERENCES = True

    reactants = [SpeciesReference(species='a', stoichiometry=1.0, constant=True),
                 SpeciesReference(species='b', stoichiometry=1.0, constant=True)]
    products = [SpeciesReference(species='c', stoichiometry=2.0, constant=True)]
    modifiers = [ModifierSpeciesReference(species='m')]

    def create_species(identifier):
        minimal_model.create_species(id=identifier, compartment='c',
                                     has_only_substance_units=True, boundary_condition=False, constant=False)

    with pytest.raises(Exception):
        minimal_model.create_reaction(id='reaction_only_reactants', reversible=True, reactants=reactants)
    create_species('a')
    create_species('b')
    minimal_model.create_reaction(id='reaction_only_reactants', reversible=True, reactants=reactants)

    with pytest.raises(Exception):
        minimal_model.create_reaction(id='reaction_only_products', reversible=True, products=products)
    create_species('c')
    minimal_model.create_reaction(id='reaction_only_products', reversible=True, products=products)

    with pytest.raises(Exception):
        minimal_model.create_reaction(id='reaction_only_modifiers', reversible=True, modifiers=modifiers)
    create_species('m')
    minimal_model.create_reaction(id='reaction_only_modifiers', reversible=True, modifiers=modifiers)


# def test_remove_reactions():
#     model = Model()


