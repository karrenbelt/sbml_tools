
import pytest
import cmath
import libsbml
from sbml_tools.wrappers import Parameter


# fixtures
@pytest.fixture()
def incomplete_parameter():
    parameter = libsbml.Parameter(3, 2)
    incomplete_parameter = Parameter(parameter)
    return incomplete_parameter


@pytest.fixture()
def minimal_parameter():
    parameter = libsbml.Parameter(3, 2)
    parameter.setId('parameter_id')
    parameter.setConstant(True)
    minimal_parameter = Parameter(parameter)
    return minimal_parameter


@pytest.fixture()
def complete_parameter():
    parameter = libsbml.Parameter(3, 2)
    parameter.setId('parameter_id')
    parameter.setName('parameter_name')
    parameter.setMetaId('parameter_meta_id')
    # parameter.setSBOTerm()
    # parameter.setNotes()
    # parameter.setAnnotation()
    parameter.setConstant(True)
    parameter.setValue(3)
    parameter.setUnits('millimole_per_second')
    complete_parameter = Parameter(parameter)
    return complete_parameter


# tests
def test_defaults(incomplete_parameter):
    assert incomplete_parameter.value is None
    assert incomplete_parameter.units is None
    assert incomplete_parameter.constant is None


def test_required_attributes(incomplete_parameter, minimal_parameter):
    assert incomplete_parameter.required == list(incomplete_parameter.difference(minimal_parameter))
    assert not incomplete_parameter.has_required_attributes
    assert minimal_parameter.has_required_attributes


def test_setting_value(incomplete_parameter):
    for value in (-1, 0, 1.5):
        incomplete_parameter.value = value
        assert incomplete_parameter.value == value


def test_setting_units(incomplete_parameter):
    for units in ('dimensionless', 'per_second'):
        incomplete_parameter.units = units
        assert incomplete_parameter.units == units


def test_init_from_kwargs(complete_parameter):
    parameter = Parameter(id='parameter_id', name='parameter_name', meta_id='parameter_meta_id',
                          constant=True, value=3, units='millimole_per_second')
    assert complete_parameter == parameter


def test_copy(complete_parameter):
    assert complete_parameter is not complete_parameter.copy()
    assert complete_parameter == complete_parameter.copy()


__all__ = ['incomplete_parameter', 'minimal_parameter', 'complete_parameter']
