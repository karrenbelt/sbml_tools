
import pytest
from sbml_tools.wrappers import SBOTerm, ASTNode


@pytest.fixture()
def sbo_term_9():
    sbo_term_9 = SBOTerm(9)
    return sbo_term_9


def test_empty_sbo_term_equals_zero():
    assert SBOTerm() is not 0
    assert SBOTerm() == 0


def test_sbo_term_as_string(sbo_term_9):
    assert sbo_term_9.as_string == 'SBO:0000009'


def test_sbo_term_name(sbo_term_9):
    assert sbo_term_9.name == 'kinetic constant'


def test_synonym(sbo_term_9):
    assert sbo_term_9.synonym == 'reaction rate constant'


def test_definition(sbo_term_9):
    assert sbo_term_9.definition == 'Numerical parameter that quantifies the velocity of a chemical reaction.'


def test_math():
    assert SBOTerm(28).math == ASTNode.from_string('lambda(kcat, Et, S, Ks, kcat * Et * S / (Ks + S))')


def test_parent_branch(sbo_term_9):
    assert sbo_term_9.parent_branch == SBOTerm(545)


def test_direct_successors():
    assert SBOTerm(10).direct_successors == [SBOTerm(15), SBOTerm(336)]


def test_all_successors():
    assert SBOTerm(10).all_successors == [SBOTerm(15), SBOTerm(604), SBOTerm(336)]


def test_direct_predecessors():
    assert SBOTerm(10).direct_predecessors == [SBOTerm(3)]


def test_all_simple_paths(sbo_term_9):
    assert sbo_term_9.all_simple_paths == [[SBOTerm(545), SBOTerm(2), SBOTerm(9)]]


def test_tags(sbo_term_9):
    assert sbo_term_9.tags == ['KINETIC_CONSTANT',
                               'QUANTITATIVE_PARAMETER',
                               'QUANTITATIVE_SYSTEMS_DESCRIPTION_PARAMETER',
                               'SYSTEMS_DESCRIPTION_PARAMETER']

