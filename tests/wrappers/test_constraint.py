
import pytest
import libsbml
from sbml_tools.wrappers import Constraint, Message, ASTNode


# fixtures
@pytest.fixture()
def minimal_constraint():
    constraint = libsbml.Constraint(3, 2)
    minimal_constraint = Constraint(constraint)
    return minimal_constraint


@pytest.fixture()
def complete_constraint():
    constraint = libsbml.Constraint(3, 2)
    constraint.setId('constraint_id')
    constraint.setName('constraint_name')
    constraint.setMetaId('constraint_meta_id')
    constraint.setSBOTerm(64)
    # constraint.setNotes()
    # constraint.setAnnotation()
    constraint.setMath(libsbml.parseL3Formula('x > 0'))
    constraint.setMessage('<message><p xmlns:html="http://www.w3.org/1999/xhtml"> text </p></message>\n')
    complete_constraint = Constraint(constraint)
    return complete_constraint


# tests
def test_defaults(minimal_constraint):
    assert minimal_constraint.math is None
    assert minimal_constraint.message is None


def test_has_required_attributes(minimal_constraint):
    assert minimal_constraint.has_required_attributes


def test_setting_math(minimal_constraint):
    minimal_constraint.math = 'x*x'
    assert minimal_constraint.math == ASTNode.from_string('x*x')


def test_set_empty_message(minimal_constraint):
    minimal_constraint.message = Message()
    assert minimal_constraint.message.as_string == '<message/>\n'


def test_set_message(minimal_constraint):
    message = Message()
    message.add('constraint violation')
    minimal_constraint.message = message
    assert minimal_constraint.message.as_string == message.as_string


def test_init_from_kwargs(complete_constraint):
    # newline characters (pretty print) don't matter here
    xhtml = '\n\r\t<message>\n\t\r<p xmlns:html="http://www.w3.org/1999/xhtml"> text </p>\n</message>\n'
    message = Message.from_xhtml_string(xhtml=xhtml)
    constraint = Constraint(id='constraint_id', name='constraint_name',
                            meta_id='constraint_meta_id', sbo_term=64,
                            # notes='', annotation='',
                            math='x > 1',
                            message=message)
    assert complete_constraint == constraint


def test_copy(complete_constraint):
    assert complete_constraint is not complete_constraint.copy()
    assert complete_constraint == complete_constraint.copy()


__all__ = ['minimal_constraint', 'complete_constraint']
