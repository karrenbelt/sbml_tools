
import pytest
import libsbml
from sbml_tools.wrappers import ASTNode, RateRule


#fixtures
@pytest.fixture()
def incomplete_rate_rule():
    rate_rule = libsbml.RateRule(3, 2)
    incomplete_rate_rule = RateRule(rate_rule)
    return incomplete_rate_rule


@pytest.fixture()
def minimal_rate_rule():
    rate_rule = libsbml.RateRule(3, 2)
    rate_rule.setVariable('y')
    minimal_rate_rule = RateRule(rate_rule)
    return minimal_rate_rule


@pytest.fixture()
def complete_rate_rule():
    rate_rule = libsbml.RateRule(3, 2)
    rate_rule.setIdAttribute('rate_rule_id')
    rate_rule.setName('rate_rule_name')
    rate_rule.setMetaId('rate_rule_meta_id')
    # rate_rule.setSBOTerm()
    # rate_rule.setNotes()
    # rate_rule.setAnnotation()
    rate_rule.setVariable('y')
    rate_rule.setMath(libsbml.parseL3Formula('1'))
    complete_rate_rule = RateRule(rate_rule)
    return complete_rate_rule


# tests
def test_defaults(incomplete_rate_rule):
    assert incomplete_rate_rule.variable is ''
    assert incomplete_rate_rule.math is None


def test_required_attributes(incomplete_rate_rule, minimal_rate_rule):
    assert incomplete_rate_rule.required == list(incomplete_rate_rule.difference(minimal_rate_rule))
    assert not incomplete_rate_rule.has_required_attributes
    assert minimal_rate_rule.has_required_attributes


def test_setting_math(incomplete_rate_rule):
    incomplete_rate_rule.math = 'x * x'
    assert incomplete_rate_rule.math == ASTNode.from_string('x * x')


def test_init_from_kwargs(complete_rate_rule):
    rate_rule = RateRule(id_attribute='rate_rule_id', name='rate_rule_name', meta_id='rate_rule_meta_id',
                         variable='y', math='1')
    assert complete_rate_rule == rate_rule


def test_copy(complete_rate_rule):
    assert complete_rate_rule is not complete_rate_rule.copy()
    assert complete_rate_rule == complete_rate_rule.copy()


__all__ = ['incomplete_rate_rule', 'minimal_rate_rule', 'complete_rate_rule']
