
import pytest
import libsbml
from sbml_tools.wrappers import ModifierSpeciesReference


# fixtures
@pytest.fixture()
def incomplete_modifier_species_reference():
    modifier_species_reference = libsbml.ModifierSpeciesReference(3, 2)
    incomplete_modifier_species_reference = ModifierSpeciesReference(modifier_species_reference)
    return incomplete_modifier_species_reference


@pytest.fixture()
def minimal_modifier_species_reference():
    modifier_species_reference = libsbml.ModifierSpeciesReference(3, 2)
    modifier_species_reference.setSpecies('x')
    minimal_modifier_species_reference = ModifierSpeciesReference(modifier_species_reference)
    return minimal_modifier_species_reference


@pytest.fixture()
def complete_modifier_species_reference():
    modifier_species_reference = libsbml.ModifierSpeciesReference(3, 2)
    modifier_species_reference.setId('modifier_species_reference_id')
    modifier_species_reference.setName('modifier_species_reference_name')
    modifier_species_reference.setMetaId('modifier_species_reference_meta_id')
    # modifier_species_reference.setSBOTerm()
    # modifier_species_reference.setNotes()
    # modifier_species_reference.setAnnotation()
    modifier_species_reference.setSpecies('x')
    complete_modifier_species_reference = ModifierSpeciesReference(modifier_species_reference)
    return complete_modifier_species_reference


# tests
def test_default(incomplete_modifier_species_reference):
    assert incomplete_modifier_species_reference.species is ''


def test_required_attributes(incomplete_modifier_species_reference, minimal_modifier_species_reference):
    assert (incomplete_modifier_species_reference.required
            == list(incomplete_modifier_species_reference.difference(minimal_modifier_species_reference)))
    assert not incomplete_modifier_species_reference.has_required_attributes
    assert minimal_modifier_species_reference.has_required_attributes


def test_init_from_kwargs(complete_modifier_species_reference):
    modifier_species_reference = ModifierSpeciesReference(id='modifier_species_reference_id',
                                                          name='modifier_species_reference_name',
                                                          meta_id='modifier_species_reference_meta_id',
                                                          species='x')
    assert complete_modifier_species_reference == modifier_species_reference


def test_copy(complete_modifier_species_reference):
    assert complete_modifier_species_reference is not complete_modifier_species_reference.copy()
    assert complete_modifier_species_reference == complete_modifier_species_reference.copy()


__all__ = ['incomplete_modifier_species_reference', 'minimal_modifier_species_reference',
           'complete_modifier_species_reference']
