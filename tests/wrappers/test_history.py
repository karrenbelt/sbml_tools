
import pytest
import libsbml
from sbml_tools.wrappers import Creator, Date, History


@pytest.fixture()
def history():
    default_libsbml_date = libsbml.Date()

    creator = libsbml.ModelCreator()
    creator.setName('John')
    creator.setFamilyName('Doe')
    creator.setEmail('john@doe.com')
    creator.setOrganization('organization')

    history = libsbml.ModelHistory()
    history.addCreator(creator)
    history.setCreatedDate(default_libsbml_date)
    history.addModifiedDate(default_libsbml_date)
    history = History(history)
    return history


def test_history_setters():
    date = Date(libsbml.Date())
    creator = Creator(given_name='John', family_name='Doe', email='john@doe.com', organization='organization')
    history = History()
    history.creators = [creator]
    history.created_date = date
    history.modified_dates = [date]
    assert history.creators == [creator]
    assert history.created_date == date
    assert history.modified_dates == [date]


def test_init_from_kwargs(history):
    date = Date(libsbml.Date())
    creators = [Creator(given_name='John', family_name='Doe', email='john@doe.com', organization='organization')]
    assert history == History(creators=creators, created_date=date, modified_dates=[date])


def test_copy(history):
    assert history is not history.copy()
    assert history == history.copy()
