
import pytest
import libsbml
from sbml_tools.wrappers import FunctionDefinition, ASTNode


# fixtures
@pytest.fixture()
def incomplete_function_definition():
    function_definition = libsbml.FunctionDefinition(3, 2)
    incomplete_function_definition = FunctionDefinition(function_definition)
    return incomplete_function_definition


@pytest.fixture()
def minimal_function_definition():
    function_definition = libsbml.FunctionDefinition(3, 2)
    function_definition.setId('function_definition_id')
    minimal_function_definition = FunctionDefinition(function_definition)
    return minimal_function_definition


@pytest.fixture()
def complete_function_definition():
    function_definition = libsbml.FunctionDefinition(3, 2)
    function_definition.setId('function_definition_id')
    function_definition.setName('function_definition_name')
    function_definition.setMetaId('function_definition_meta_id')
    function_definition.setSBOTerm(64)
    # function_definition.setNotes()
    # function_definition.setAnnotation()
    function_definition.setMath(libsbml.parseL3Formula('lambda(x, y, x * y)'))
    complete_function_definition = FunctionDefinition(function_definition)
    return complete_function_definition


# tests
def test_default_values(incomplete_function_definition):
    assert not incomplete_function_definition.math


def test_required_attributes(incomplete_function_definition, minimal_function_definition):
    assert incomplete_function_definition.required == ['id']
    assert not incomplete_function_definition.has_required_attributes
    assert minimal_function_definition.has_required_attributes


def test_setting_math(incomplete_function_definition):
    function_definition = incomplete_function_definition
    function_definition.math = 'lambda(x, y, z, x * (y - sin(z))'
    assert function_definition.math == ASTNode.from_string('lambda(x, y, z, x * (y - sin(z))')


def test_init_from_kwargs(complete_function_definition):
    lambda_x_times_y = FunctionDefinition(id='function_definition_id', name='function_definition_name',
                                          meta_id='function_definition_meta_id', sbo_term=64,
                                          math='lambda(x, y, x * y)')
    assert lambda_x_times_y == complete_function_definition


def test_copy(complete_function_definition):
    assert complete_function_definition is not complete_function_definition.copy()
    assert complete_function_definition == complete_function_definition.copy()


__all__ = ['incomplete_function_definition', 'minimal_function_definition', 'complete_function_definition']
