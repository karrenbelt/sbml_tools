
import pytest
import libsbml
from sbml_tools.wrappers import EventAssignment, ListOfEventAssignments, ASTNode


import pytest
import libsbml
from sbml_tools.wrappers import EventAssignment, ASTNode


# fixtures
@pytest.fixture()
def incomplete_event_assignment():
    event_assignment = libsbml.EventAssignment(3, 2)
    incomplete_event_assignment = EventAssignment(event_assignment)
    return incomplete_event_assignment


@pytest.fixture()
def minimal_event_assignment():
    event_assignment = libsbml.EventAssignment(3, 2)
    event_assignment.setVariable('x')
    minimal_event_assignment = EventAssignment(event_assignment)
    return minimal_event_assignment


@pytest.fixture()
def complete_event_assignment():
    event_assignment = libsbml.EventAssignment(3, 2)
    event_assignment.setIdAttribute('event_assignment_id')
    event_assignment.setName('event_assignment_name')
    event_assignment.setMetaId('event_assignment_meta_id')
    event_assignment.setSBOTerm(64)
    # event_assignment.setNotes()
    # event_assignment.setAnnotation()
    event_assignment.setVariable('x')
    event_assignment.setMath(libsbml.parseL3Formula('10 mole * y'))
    complete_event_assignment = EventAssignment(event_assignment)
    return complete_event_assignment


# tests
def test_defaults(incomplete_event_assignment, minimal_event_assignment):
    assert (incomplete_event_assignment.required
            == list(incomplete_event_assignment.difference(minimal_event_assignment)))
    assert not incomplete_event_assignment.has_required_attributes
    assert minimal_event_assignment.has_required_attributes


def test_required_attributes(minimal_event_assignment):
    assert minimal_event_assignment.has_required_attributes


def test_setting_math(minimal_event_assignment):
    minimal_event_assignment.math = 'x * x'
    assert minimal_event_assignment.math == ASTNode.from_string('x * x')


def test_init_from_kwargs(complete_event_assignment):
    event_assignment = EventAssignment(id_attribute='event_assignment_id', name='event_assignment_name',
                                       meta_id='event_assignment_meta_id', sbo_term=64,
                                       variable='x', math='10 mole * y')
    assert event_assignment == complete_event_assignment


def test_copy(complete_event_assignment):
    assert complete_event_assignment is not complete_event_assignment.copy()
    assert complete_event_assignment == complete_event_assignment.copy()


__all__ = ['incomplete_event_assignment', 'minimal_event_assignment', 'complete_event_assignment']
