
import sys
import pytest
import libsbml
from sbml_tools.wrappers.unit import Unit, UNIT_KINDS, unit_registry
from sbml_tools import config


# fixtures
@pytest.fixture()
def incomplete_unit():
    unit = libsbml.Unit(3, 2)
    incomplete_unit = Unit(unit)
    return incomplete_unit


@pytest.fixture()
def minimal_unit():
    unit = libsbml.Unit(3, 2)
    unit.setKind(libsbml.UNIT_KIND_GRAM)
    unit.setExponent(1)
    unit.setScale(0)
    unit.setMultiplier(1.0)
    minimal_unit = Unit(unit)
    return minimal_unit


@pytest.fixture()
def complete_unit():
    unit = libsbml.Unit(3, 2)
    unit.setId('unit_id')
    unit.setName('unit_name')
    unit.setMetaId('unit_meta_id')
    # unit.setSBOTerm()
    # unit.setNotes()
    # unit.setAnnotation()
    unit.setKind(libsbml.UNIT_KIND_GRAM)
    unit.setExponent(1)
    unit.setScale(0)
    unit.setMultiplier(1.0)
    complete_unit = Unit(unit)
    return complete_unit


# tests
def test_default_values(incomplete_unit):
    assert incomplete_unit.kind == UNIT_KINDS[libsbml.UNIT_KIND_INVALID]
    assert not incomplete_unit.exponent
    assert not incomplete_unit.scale
    assert not incomplete_unit.multiplier


def test_required_attributes(incomplete_unit, minimal_unit):
    assert incomplete_unit.required == list(incomplete_unit.difference(minimal_unit))
    assert not incomplete_unit.has_required_attributes
    assert minimal_unit.has_required_attributes


def test_init_from_kwargs(minimal_unit):
    assert minimal_unit == Unit(kind='gram', exponent=1, scale=0, multiplier=1.0)


def test_set_kinds(incomplete_unit):
    for kind, name in UNIT_KINDS.items():
        if name in ('invalid', 'celsius'):
            continue
        incomplete_unit.kind = kind
        assert incomplete_unit.kind == dict(liter='litre', meter='metre').get(name, name)


def test_set_proper_exponent(incomplete_unit):
    for exponent in (-1, 0, 1, 100, 1.0, 2**31-1, -2**31):
        incomplete_unit.exponent = exponent
        assert incomplete_unit.exponent == exponent


def test_set_improper_exponent(incomplete_unit):
    # error as should: this in libsbml sets it back to 0 without raising an error
    for exponent in (-1.5, 2.5, 2**31, -2**31-1):
        with pytest.raises(Exception):
            incomplete_unit.exponent = exponent


def test_set_proper_scale(incomplete_unit):
    for scale in (-1, 0, 1, 100, 1.0, 2**31-1, -2**31):
        incomplete_unit.scale = scale
        assert incomplete_unit.scale == scale


def test_set_improper_scale(incomplete_unit):
    for scale in (-1.5, 2.5, 2**31, -2**31-1):
        with pytest.raises(Exception):
            incomplete_unit.scale = scale


def test_set_proper_multiplier(incomplete_unit):
    for multiplier in (-1, 0, 1, -0.5, 1.5, sys.float_info.max, sys.float_info.min):
        incomplete_unit.multiplier = multiplier
        assert incomplete_unit.multiplier == multiplier


def test_set_improper_multiplier(incomplete_unit):
    for multiplier in (float('inf'), -float('inf')):
        with pytest.raises(Exception):
            incomplete_unit.multiplier = multiplier


def test_has_required_attributes(incomplete_unit, minimal_unit):
    assert not incomplete_unit.has_required_attributes
    assert minimal_unit.has_required_attributes


def test_copy(complete_unit):
    assert complete_unit is not complete_unit.copy()
    assert complete_unit == complete_unit.copy()


def test_improper_unit_logic():
    zero_to_negative_power = dict(kind='lux', exponent=-1, scale=0, multiplier=0)
    exponent_zero_not_dimensionless = dict(kind='lux', exponent=0, scale=0, multiplier=1.0)
    dimensionless_not_zero_exponent = dict(kind='dimensionless', exponent=1, scale=0, multiplier=1.0)
    for kwargs in (zero_to_negative_power, exponent_zero_not_dimensionless, dimensionless_not_zero_exponent):
        with pytest.raises(Exception):
            Unit(**kwargs)


def test_equality_when_numerically_close(minimal_unit):
    multiplier = minimal_unit.multiplier - minimal_unit.multiplier * config.IS_CLOSE_RELATIVE_TOLERANCE
    assert minimal_unit == Unit(kind='gram', exponent=1, scale=0, multiplier=multiplier)


def test_convert_to_SI(minimal_unit):
    minimal_unit.convert_to_SI()
    assert minimal_unit == Unit(kind='kilogram', exponent=1, scale=0, multiplier=0.001)


def test_equivalence():
    kilogram = Unit(kind='kilogram', exponent=1, scale=0, multiplier=1.0)
    cubic_gram = Unit(kind='gram', exponent=1, scale=3, multiplier=1.0)
    thousand_grams = Unit(kind='gram', exponent=1, scale=0, multiplier=1000)
    assert kilogram.equals(cubic_gram) and kilogram.equals(thousand_grams)

    hertz = Unit(kind='hertz', exponent=1, scale=0, multiplier=1.0)
    per_second = Unit(kind='second', exponent=-1, scale=0, multiplier=1.0)
    assert per_second.equals(hertz)

    kilo_litre = Unit(kind='liter', exponent=1, scale=0, multiplier=1000.0)
    cubic_meter = Unit(kind='meter', exponent=3, scale=0, multiplier=1.0)
    assert kilo_litre.equals(cubic_meter)


def test_one_unit_as_quantity():
    exponent, scale, multiplier = 2, 2, 4
    unit = Unit(kind='coulomb', exponent=exponent, scale=scale, multiplier=multiplier)
    assert unit.as_quantity == (multiplier * 10**scale * unit_registry.coulomb) ** exponent
    new_unit = Unit.from_quantity(unit.as_quantity, scale=scale)
    assert unit == new_unit


def test_as_quantity_from_quantity():
    # now we try the quantity conversion
    for _, kind in UNIT_KINDS.items():
        if kind in ('celsius', 'invalid', 'dimensionless'):  # known exceptions
            continue

        for exponent in (-1, 1):
            unit = Unit(kind=kind, exponent=exponent, scale=0, multiplier=1.0)
            quantity = unit.as_quantity
            new_unit = Unit.from_quantity(quantity)
            assert unit == new_unit

        for scale in (-1, 0, 1):
            unit = Unit(kind=kind, exponent=exponent, scale=scale, multiplier=3.0)
            quantity = unit.as_quantity
            new_unit = Unit.from_quantity(quantity, scale=scale)
            assert unit == new_unit

        for multiplier in (-1.0, 0.0, 1.0):
            unit = Unit(kind=kind, exponent=1, scale=0, multiplier=multiplier)
            quantity = unit.as_quantity
            new_unit = Unit.from_quantity(quantity)
            assert unit == new_unit


__all__ = ['incomplete_unit', 'minimal_unit', 'complete_unit']
