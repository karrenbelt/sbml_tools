
# from setup import install_requires


def test_import():
    # had issues with this before, so better test this as well
    # NOTE: names of modules don't match setup.py install_requires exactly
    import bidict
    import boolean  # boolean.py
    import cached_property  # cached-property
    import cobra
    import coverage
    import iminuit
    # import ipython
    # import roadrunner  # ModuleNotFoundError: No module named '_roadrunner'
    import lmfit
    import numpy
    import lxml
    import networkx
    import matplotlib
    import optlang
    import owlready2
    import pandas
    import pbalancing
    import pint
    import pytest
    import libsbml  # python-libsbml-experimental
    import sbtab
    import scipy
    import seaborn
    import statsmodels
    import stopit
    import sympy
    import tqdm
    import uncertainties
    import xmltodict
