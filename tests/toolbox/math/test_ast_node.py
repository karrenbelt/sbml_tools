import pytest
import math
import numpy as np
import fractions
import libsbml
from sbml_tools.toolbox.math.ast_node import ASTNode

# fixtures
X = ASTNode(name='x')
Y = ASTNode(name='y')
Z = ASTNode(name='z')
EMPTY = ASTNode()
INTEGER = ASTNode(value=1)
FLOAT = ASTNode(value=3.141592653589793)
FRACTION = ASTNode(value=fractions.Fraction(22, 7))
REAL_E = ASTNode(value=(3., 1))
ROOT = ASTNode(type=libsbml.AST_FUNCTION_ROOT)  # not well-formed


# tests
def test_relational_class_methods():
    assert ASTNode.relational_lt(X, Y) == ASTNode.from_string('x < y')
    assert ASTNode.relational_le(X, Y) == ASTNode.from_string('x <= y')
    assert ASTNode.relational_eq(X, Y) == ASTNode.from_string('x == y')
    assert ASTNode.relational_ne(X, Y) == ASTNode.from_string('x != y')
    assert ASTNode.relational_ge(X, Y) == ASTNode.from_string('x >= y')
    assert ASTNode.relational_gt(X, Y) == ASTNode.from_string('x > y')


@pytest.mark.parametrize(
    'a, b',
    [(FLOAT, FRACTION), (0, INTEGER), (0, FLOAT), (0, FRACTION), (0, REAL_E)]
)
def test_relational_special_methods_for_numbers(a, b):
    assert not a > b
    assert a < b
    assert not a == b
    assert a != b


@pytest.mark.parametrize(
    'a, b',
    [(X, Y), (X, 0), (0, X)]
)
def test_relational_special_methods_raising_exception(a, b):
    # error because non-numerical
    with pytest.raises(TypeError):
        a < b
    with pytest.raises(TypeError):
        a <= b
    with pytest.raises(TypeError):
        a > b
    with pytest.raises(TypeError):
        a >= b


def test_logical_class_methods():
    assert ASTNode.logical_and(X, Y) == ASTNode.from_string('and(x, y)')
    assert ASTNode.logical_or(X, Y) == ASTNode.from_string('or(x, y)')
    assert ASTNode.logical_xor(X, Y) == ASTNode.from_string('xor(x, y)')


def test_logical_special_methods():
    assert X & Y == ASTNode.logical_and(X, Y)
    assert X & 1 == ASTNode.logical_and(X, INTEGER)
    assert 1 & X == ASTNode.logical_and(INTEGER, X)
    # X &= Y  # works

    assert X | Y == ASTNode.logical_or(X, Y)
    assert X | 1 == ASTNode.logical_or(X, INTEGER)
    assert 1 | X == ASTNode.logical_or(INTEGER, X)
    # X |= Y  # works

    assert X ^ Y == ASTNode.logical_xor(X, Y)
    assert X ^ 1 == ASTNode.logical_xor(X, INTEGER)
    assert 1 ^ X == ASTNode.logical_xor(INTEGER, X)
    # X ^= Y  # works


@pytest.mark.parametrize(
    'a, b',
    [(ASTNode.unary_plus(X), ASTNode.from_string('+x')),
     (ASTNode.unary_minus(X), ASTNode.from_string('-x')),
     (ASTNode.abs(X), ASTNode.from_string('abs(x)')),
     ])
def test_unary_class_methods(a, b):
    assert a == b


@pytest.mark.parametrize(
    'a, b',
    [(- X, ASTNode.from_string('-x')),
     (+ X, ASTNode.from_string('+x')),
     (abs(X), ASTNode.from_string('abs(x)')),
     (- - X, ASTNode.from_string('--x')),
     (abs(--X), ASTNode.from_string('abs(--x)')),
     ])
def test_unary_special_methods(a, b):
    assert a == b


@pytest.mark.parametrize(
    'a, b',
    [(int(FLOAT), 3),
     (int(FRACTION), 3),
     (float(FLOAT), 3.141592653589793),
     (float(FRACTION), 3.142857142857143),
     (math.ceil(FLOAT), 4),  # casts to __float__ internally it appears
     (math.floor(FLOAT), 3),
     # (round(FLOAT, 1), )
     # (math.trunc(FLOAT), )
     ])
def test_numeric(a, b):
    assert a == b


def test_numeric_raising_exception():
    with pytest.raises(Exception):
        int(X)
    with pytest.raises(Exception):
        float(X)


@pytest.mark.parametrize(
    'class_method',
    [ASTNode.add, ASTNode.subtract, ASTNode.multiply, ASTNode.divide]
)
def test_arithmetic_raises_exception(class_method):
    with pytest.raises(TypeError):
        class_method()
    with pytest.raises(TypeError):
        class_method(X)


@pytest.mark.parametrize(
    'a, b',
    [(ASTNode.add(X, Y), ASTNode.from_string('x + y')),
     (ASTNode.add(X, Y, Z), ASTNode.from_string('x + y + z')),
     (ASTNode.subtract(X, Y), ASTNode.from_string('x - y')),
     (ASTNode.subtract(X, Y, Z), ASTNode.from_string('x - y - z')),
     (ASTNode.multiply(X, Y), ASTNode.from_string('x * y')),
     (ASTNode.multiply(X, Y, Z), ASTNode.from_string('x * y * z')),
     (ASTNode.divide(X, Y), ASTNode.from_string('x / y')),
     (ASTNode.divide(X, Y, Z), ASTNode.from_string('x / y / z')),
     (ASTNode.power(X, Y), ASTNode.from_string('x ^ y')),
     (ASTNode.power(X, Y, Z), ASTNode.from_string('x ^ y ^ z')),
     ])
def test_class_methods_arithmetic(a, b):
    assert a == b


@pytest.mark.parametrize(
    'a, b',
    [(X + Y, ASTNode.add(X, Y)),
     (X + Y + Z, ASTNode.add(X, Y, Z)),  # NOTE: different tree, but same mathml
     (X + Y + Z, ASTNode.add(ASTNode.add(X, Y), Z)),
     (X + 1, ASTNode.from_string('x + 1')),
     (1 + X, ASTNode.from_string('1 + x')),

     (X - Y, ASTNode.subtract(X, Y)),
     (X - Y - Z, ASTNode.subtract(X, Y, Z)),
     (X - Y - Z, ASTNode.subtract(ASTNode.subtract(X, Y), Z)),
     (X - 1, ASTNode.from_string('x - 1')),
     (1 - X, ASTNode.from_string('1 - x')),

     (X * Y, ASTNode.multiply(X, Y)),
     (X * Y * Z, ASTNode.multiply(X, Y, Z)),  # NOTE: different tree, but same mathml
     (X * Y * Z, ASTNode.multiply(ASTNode.multiply(X, Y), Z)),
     (X * 1, ASTNode.from_string('x * 1')),
     (1 * X,  ASTNode.from_string('1 * x')),

     (X / Y, ASTNode.divide(X, Y)),
     (X / Y / Z, ASTNode.divide(X, Y, Z)),
     (X / Y / Z, ASTNode.divide(ASTNode.divide(X, Y), Z)),
     (X / 1, ASTNode.from_string('x / 1')),
     (1 / X, ASTNode.from_string('1 / x')),

     (X ** Y, ASTNode.power(X, Y)),
     (X ** Y ** Z, ASTNode.power(X, Y, Z)),
     (X ** Y ** Z, ASTNode.power(X, ASTNode.power(Y, Z))),
     (X ** 1, ASTNode.from_string('x ^ 1')),
     (1 ** X, ASTNode.from_string('1 ^ x')),
     ])
def test_special_arithmetic(a, b):
    assert a == b


# @pytest.mark.parametrize(
#     'a, b, c',
#     [(X + Y + Z, ASTNode.add(ASTNode.add(X, Y), Z), ASTNode.add(X, Y, Z)),
#      (X * Y * Z, ASTNode.multiply(ASTNode.multiply(X, Y), Z), ASTNode.multiply(X, Y, Z))])
# def test_difference_class_and_special_methods_add_and_mul(a, b, c):
#     # trees differ: 'operation(a, b, c)' != 'operation(operation(a, b), c))'
#     # NOTE: now that I compare mathml strings first, these are tested as equal
#     assert a == b and not a == c


def test_floor():
    assert ASTNode.floor(X) == ASTNode.from_string('floor(x)')
    assert ASTNode.floor(X / Y) == ASTNode.from_string('floor(x / y)')
    assert X // Y == ASTNode.from_string('floor(x / y)')
    assert X // 1 == ASTNode.from_string('floor(x / 1)')
    assert 1 // X == ASTNode.from_string('floor(1 / x)')


def test_ceil():
    # not precisely the same as ASTNode.from_string('ceil(x)'); different name, but the same mathml
    assert ASTNode.ceil(X) == ASTNode.from_string('ceiling(x)')  # both ceiling and ceil are ok, since we compare mathml
    assert ASTNode.ceil(X / Y) == ASTNode.from_string('ceil(x / y)')
    assert math.ceil(FLOAT) == np.ceil(FLOAT) == 4


def test_modulo():
    assert ASTNode.modulo(X, Y) == ASTNode.from_string('rem(x, y)')
    assert ASTNode.modulo(X, Y) == X % Y
    assert X % 1 == ASTNode.from_string('rem(x, 1)')
    assert 1 % X == ASTNode.from_string('rem(1, x)')


def test_sqrt():
    assert ASTNode.sqrt(X) == ASTNode.from_string('root(x, 2)')
    assert ASTNode.sqrt(9) == ASTNode.from_string('root(9, 2)')


def test_root():
    assert ASTNode.root(X, Y) == ASTNode.from_string('root(x, y)')
    assert ASTNode.root(1, 2) == ASTNode.from_string('root(1, 2)')
    with pytest.raises(Exception):
        math.sqrt(X)
    with pytest.raises(Exception):
        np.sqrt(X)


def test_ln():
    assert ASTNode.ln('exp(1)') == ASTNode.from_string('ln(exp(1))')


def test_log():
    assert ASTNode.log(3, 2) == ASTNode.from_string('log(3, 2)')
    with pytest.raises(Exception):
        X.log(Y)  # must not assume base 10 or anything, else class method can be called as instance method


def test_exp():
    assert ASTNode.exp(X/2) == ASTNode.from_string('exp(x / 2)')


def test_factorial():
    assert ASTNode.factorial(3) == ASTNode.from_string('factorial(3)')


def test_class_method_min():
    assert ASTNode.min(1) == ASTNode.from_string('min(1)')
    assert ASTNode.min(1, 2, 3) == ASTNode.from_string('min(1, 2, 3)')


def test_built_in_min():
    assert min(FLOAT, FRACTION, INTEGER) is INTEGER
    assert min(FLOAT, FRACTION, INTEGER, 0.1) == 0.1
    with pytest.raises(Exception):
        min(X, Y)


def test_class_method_max():
    assert ASTNode.max(1) == ASTNode.from_string('max(1)')
    assert ASTNode.max(1, 2, 3) == ASTNode.from_string('max(1, 2, 3)')


def test_built_in_max():
    assert max(FLOAT, FRACTION, INTEGER) is FRACTION
    assert max(FLOAT, FRACTION, INTEGER, 10) == 10
    with pytest.raises(Exception):
        max(X, Y)
