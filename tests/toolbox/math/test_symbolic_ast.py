
import pytest
import sympy
import sympy.stats
import libsbml
from sbml_tools.toolbox.math import SymbolicASTNode
# from sbml_tools.toolbox.math.mathml_to_sympy import mml2sympy
from sbml_tools.toolbox.math.node_mapping import Minus, Divide, Quotient, Root, Piecewise, Log, Power, Delay, RateOf

from sbml_tools.toolbox.math.node_mapping import ALL_AST_TYPES, MAPPING, WITHOUT_MAPPING, SymbolicMappingError

from tests.simple_ast_nodes import (
    ARITHMETIC, CONSTANT, CSYMBOL, CSYMBOL_FUNCTION, DISTRIB_FUNCTION, LINEAR_ALGEBRA, LOGICAL, NUMERICAL, RELATIONAL,
    SERIES, STATISTICS, TRIGONOMETRIC_FUNCTION, OTHER)
from tests.simple_ast_nodes import ALL_SIMPLE_NODES, X, Y, Z, make_ast_node


x, y, z = sympy.symbols('x y z')


def get_n_children(n: int):
    # utility function to return 1, 2 or 3 children, libsbml.ASTNodes and sympy.Symbols
    assert 0 <= n <= 3, 'n must be between in the range 0-3'
    return (x, y, z)[:n], (X, Y, Z)[:n]


def test_implemented_and_missing_node_mapping_coverage():
    assert not set(MAPPING).intersection(WITHOUT_MAPPING)
    assert not set(MAPPING).union(WITHOUT_MAPPING).symmetric_difference(ALL_AST_TYPES)


# first we test all simple nodes
@pytest.mark.parametrize(
    "ast_type, expected",
    [(libsbml.AST_DISTRIB_FUNCTION_BERNOULLI, sympy.stats.Bernoulli('bernoulli', x)),
     (libsbml.AST_DISTRIB_FUNCTION_BINOMIAL, sympy.stats.Binomial('binomial', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_CAUCHY, sympy.stats.Cauchy('cauchy', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_CHISQUARE, sympy.stats.ChiSquared('chisquare', x)),
     (libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL, sympy.stats.Exponential('exponential', x)),
     (libsbml.AST_DISTRIB_FUNCTION_GAMMA, sympy.stats.Gamma('gamma', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_LAPLACE, sympy.stats.Laplace('laplace', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL, sympy.stats.LogNormal('lognormal', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_NORMAL, sympy.stats.Normal('normal', x, y)),
     (libsbml.AST_DISTRIB_FUNCTION_POISSON, sympy.stats.Poisson('poisson', x)),
     (libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH, sympy.stats.Rayleigh('rayleigh', x)),
     (libsbml.AST_DISTRIB_FUNCTION_UNIFORM, sympy.stats.Uniform('uniform', x, y)),
     ])
def test_distributions(ast_type, expected):
    # reading from string makes regular function in libsbml 5.18.0; ensure libsbml >= 5.18.1
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expr = node.to_sympy()
    assert expr == expected
    with pytest.raises(NotImplementedError):
        SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "ast_type",
    [libsbml.AST_CONSTANT_E,
     libsbml.AST_CONSTANT_FALSE,
     libsbml.AST_CONSTANT_PI,
     libsbml.AST_CONSTANT_TRUE,
     ])
def test_constant(ast_type):
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expected = MAPPING[ast_type]
    expr = node.to_sympy()
    assert expr is expected
    assert node == SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "ast_type, value",
    [(libsbml.AST_INTEGER, 1),  # sympy.S.One singleton
     (libsbml.AST_RATIONAL, (1, 3)),
     (libsbml.AST_REAL, 1.0),
     (libsbml.AST_REAL_E, 2e3),
     ])
def test_numerical(ast_type, value):
    # NOTE: value checking in using __eq__: 1.0 == 1 == 1e0 == fractions.Fraction(2, 2) == True
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expr = node.to_sympy()
    expected = MAPPING[ast_type](*value) if isinstance(value, tuple) else MAPPING[ast_type](value)
    assert expr == expected
    assert node == SymbolicASTNode.from_sympy(expr)


# functions
def test_function():
    node = SymbolicASTNode(ALL_SIMPLE_NODES[libsbml.AST_FUNCTION])
    expr = node.to_sympy()
    assert expr == sympy.Function('function')(x, y)
    assert node == SymbolicASTNode.from_sympy(expr)


def test_lambda():
    # libsbml - variable arg length: first args are variables, last arg is the expression
    # sympy - two args: lambda args first element is a tuple of variable length, second the expr
    node = SymbolicASTNode(ALL_SIMPLE_NODES[libsbml.AST_LAMBDA])
    expr = node.to_sympy()
    assert expr == sympy.Lambda((x, y), x*y)
    assert node == SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "node, expected",
    [(SymbolicASTNode.from_string('piecewise(x, true)'),
      Piecewise((x, True), evaluate=False)),
     (SymbolicASTNode.from_string('piecewise(0, x < 0, 1, true)'),
      Piecewise((0, x < 0), (1, True))),
     (SymbolicASTNode.from_string('piecewise(0, (x >= 0) && (x < 1), 1, (x >= 1) && (x < 2), 2, (x >= 2))'),
      Piecewise((0, (x >= 0) & (x < 1)), (1, (x >= 1) & (x < 2)), (2, x >= 2)))
    ])
def test_piece_wise(node, expected):
    # although libsbml does not require the final 'true' as it is implicit, sympy introduces it
    expr = node.to_sympy(evaluate=False)
    assert expr == expected
    assert node == SymbolicASTNode.from_sympy(expr)


def test_symbol():
    node = SymbolicASTNode(ALL_SIMPLE_NODES[libsbml.AST_NAME])
    expr = node.to_sympy()
    assert expr == sympy.Symbol('name')
    assert node == SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "ast_type, expected",
    [(libsbml.AST_NAME_AVOGADRO, sympy.S.Avogadro),
     # (libsbml.AST_NAME_TIME, sympy.S.Time),
     ])
def test_builtin_csymbols(ast_type, expected):
    # ast_type, expected = (libsbml.AST_NAME_TIME, sympy.S.Time)
    # terminate called after throwing an instance of 'std::logic_error'
    #   what():  basic_string::_S_construct null not valid
    # Process finished with exit code 134 (interrupted by signal 6: SIGABRT)
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expr = node.to_sympy()
    assert expr is expected
    assert node == SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "ast_type, expected",
    [(libsbml.AST_FUNCTION_DELAY, Delay(x, y)),
     (libsbml.AST_FUNCTION_RATE_OF, RateOf(x)),
     ])
def test_builtin_csymbol_functions(ast_type, expected):
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expr = node.to_sympy()
    assert expr == expected
    assert node == SymbolicASTNode.from_sympy(expr)


def test_custom_csymbol_function(name='csymbol_function'):  # TODO: sympy?
    # https://www.w3.org/TR/MathML3/chapter4.html#contm.csymbol
    math_ml = f"""
        <semantics>
          <csymbol>{name}</csymbol>
          <annotation-xml cd="mathmltypes" name="type" encoding="MathML-Content">
            <ci>T</ci>
          </annotation-xml>
        </semantics>
        """
    node = SymbolicASTNode(libsbml.readMathMLFromString(math_ml))
    assert node.type == libsbml.AST_CSYMBOL_FUNCTION
    assert node.string == f'{name}()'
    with pytest.raises(NotImplementedError):
        node.to_sympy()


@pytest.mark.parametrize(
    "ast_type, n",
    [(libsbml.AST_PLUS, 3),
     (libsbml.AST_MINUS, 2),
     (libsbml.AST_TIMES, 3),
     (libsbml.AST_DIVIDE, 2),
     (libsbml.AST_POWER, 2),
     (libsbml.AST_FUNCTION_ROOT, 2),
     (libsbml.AST_FUNCTION_ABS, 1),
     (libsbml.AST_FUNCTION_EXP, 1),
     (libsbml.AST_FUNCTION_LN, 1),
     (libsbml.AST_FUNCTION_LOG, 2),
     (libsbml.AST_FUNCTION_FLOOR, 1),
     (libsbml.AST_FUNCTION_CEILING, 1),
     (libsbml.AST_FUNCTION_FACTORIAL, 1),
     (libsbml.AST_FUNCTION_QUOTIENT, 2),
     (libsbml.AST_FUNCTION_MAX, 3),
     (libsbml.AST_FUNCTION_MIN, 3),
     (libsbml.AST_FUNCTION_REM, 2),  # TODO: check is this supposed to be Modulo?
     ])
def test_simple_arithmetic(ast_type, n):
    sympy_children, ast_children = get_n_children(n)
    node = SymbolicASTNode(ALL_SIMPLE_NODES[ast_type])
    expected = MAPPING[ast_type](*sympy_children, evaluate=False)
    expr = node.to_sympy(evaluate=False)
    assert expr == expected
    assert node == SymbolicASTNode.from_sympy(expr)


@pytest.mark.parametrize(
    "ast_type, n",
    [(libsbml.AST_PLUS, 1),  # somehow also takes 0 and 1 child, TODO: return self
     (libsbml.AST_PLUS, 2),
     (libsbml.AST_MINUS, 1),
     (libsbml.AST_TIMES, 1),  # somehow also takes 0 and 1 child, TODO: return self
     (libsbml.AST_TIMES, 2),
     (libsbml.AST_FUNCTION_ROOT, 1),
     (libsbml.AST_FUNCTION_MAX, 1),
     (libsbml.AST_FUNCTION_MIN, 1),
     ])
def test_correct_number_of_children(ast_type, n):
    # few additional tests not in covered by simple_ast_nodes
    sympy_children, ast_children = get_n_children(n)
    node = SymbolicASTNode(make_ast_node(ast_type, children=ast_children))
    expr = MAPPING[ast_type](*sympy_children, evaluate=False)
    assert node.to_sympy() == expr
    # assert node == SymbolicASTNode.from_sympy(expr)  # fails here, cannot prevent sympy simplification


@pytest.mark.parametrize(
    "ast_type, n",
    [(libsbml.AST_MINUS, 0),
     (libsbml.AST_MINUS, 3),  # == 45
     (libsbml.AST_DIVIDE, 0),  # == 47
     (libsbml.AST_DIVIDE, 1),
     (libsbml.AST_DIVIDE, 3),
     (libsbml.AST_POWER, 0),
     (libsbml.AST_POWER, 1),
     (libsbml.AST_POWER, 3),
     (libsbml.AST_FUNCTION_POWER, 0),
     (libsbml.AST_FUNCTION_POWER, 1),
     (libsbml.AST_FUNCTION_POWER, 3),
     (libsbml.AST_DIVIDE, 0),
     (libsbml.AST_DIVIDE, 1),
     (libsbml.AST_DIVIDE, 3),
     (libsbml.AST_FUNCTION_QUOTIENT, 0),
     (libsbml.AST_FUNCTION_QUOTIENT, 1),
     (libsbml.AST_FUNCTION_QUOTIENT, 3),
     (libsbml.AST_FUNCTION_ROOT, 0),
     (libsbml.AST_FUNCTION_ROOT, 3),
     (libsbml.AST_FUNCTION_ABS, 0),
     (libsbml.AST_FUNCTION_ABS, 2),
     (libsbml.AST_FUNCTION_EXP, 0),
     (libsbml.AST_FUNCTION_EXP, 2),
     (libsbml.AST_FUNCTION_LN, 0),
     (libsbml.AST_FUNCTION_LN, 2),
     (libsbml.AST_FUNCTION_LOG, 0),
     (libsbml.AST_FUNCTION_LOG, 1),
     (libsbml.AST_FUNCTION_LOG, 3),
     (libsbml.AST_FUNCTION_FLOOR, 0),
     (libsbml.AST_FUNCTION_FLOOR, 2),
     (libsbml.AST_FUNCTION_CEILING, 0),
     (libsbml.AST_FUNCTION_CEILING, 2),
     (libsbml.AST_FUNCTION_FACTORIAL, 0),
     (libsbml.AST_FUNCTION_FACTORIAL, 2),
     (libsbml.AST_FUNCTION_QUOTIENT, 1),
     (libsbml.AST_FUNCTION_QUOTIENT, 3),
     (libsbml.AST_FUNCTION_REM, 0),
     (libsbml.AST_FUNCTION_REM, 1),
     (libsbml.AST_FUNCTION_REM, 3),
     ])
def test_incorrect_number_of_children(ast_type, n):  # not comprehensive yet
    sympy_children, ast_children = get_n_children(n)
    with pytest.raises(AssertionError):  # not well formed
        make_ast_node(ast_type, children=ast_children)
    with pytest.raises(Exception):
        MAPPING[ast_type](*sympy_children, evaluate=False)


@pytest.mark.parametrize(
    "ast_type, n",
    [(libsbml.AST_FUNCTION_MAX, 0),
     (libsbml.AST_FUNCTION_MIN, 0),
     ])
def test_empty_min_max(ast_type, n):
    # these are odd cases; libsbml max() and min() without children is considered well-formed,
    # yet I don't know how these are supposed to be interpreted.
    # In sympy however: sympy.Max() == -sympy.oo  &  sympy.Min() == sympy.oo
    _, ast_children = get_n_children(n)
    node = SymbolicASTNode(make_ast_node(ast_type, children=ast_children))
    with pytest.raises(Exception):
        node.to_sympy()

    # a = SymbolicASTNode.from_string('max(x, y, z, 1)')  # cannot take twice the same argument, arguments get sorted
    # assert a.type == libsbml.AST_FUNCTION_MAX
    # assert a.to_sympy() == sympy.Max(1, x, y, z, evaluate=False)
    # # assert a == SymbolicASTNode.from_sympy(a.to_sympy(evaluate=False))
    # assert a.from_sympy(a.to_sympy(evaluate=False)).equals(a)
    #
    # a = SymbolicASTNode.from_string('min(x, y, z, 1)')
    # assert a.type == libsbml.AST_FUNCTION_MIN
    # assert a.to_sympy() == sympy.Min(x, y, z, 1)
    # # assert a == SymbolicASTNode.from_sympy(a.to_sympy(evaluate=False))
    # assert a.from_sympy(a.to_sympy(evaluate=False)).equals(a)


@pytest.mark.parametrize(
    "x_val, y_val",
    [(3, 7),
     # (-3, 7),
     # (3, -7),
     (-3, -7),
     ])
def test_rem(x_val, y_val):
    assert libsbml.L3ParserSettings().getParseModuloL3v2() is False  # parse as piecewise
    piecewise = SymbolicASTNode.from_string(f'x % y').to_sympy()
    modulo = SymbolicASTNode(make_ast_node(libsbml.AST_FUNCTION_REM, children=(X, Y))).to_sympy()
    assert piecewise.subs(dict(x=x_val, y=y_val)) == modulo.subs(dict(x=x_val, y=y_val))


def test_multiple_powers():
    node = SymbolicASTNode.from_string('x ^ y ^ y')
    assert node.type == libsbml.AST_POWER
    assert node.to_sympy() == sympy.Pow(x, sympy.Pow(y, y))
    assert node == SymbolicASTNode.from_sympy(node.to_sympy(evaluate=False))


def test_trigonometric_operators():
    a = SymbolicASTNode.from_string('arccos(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCOS
    assert a.to_sympy() == sympy.acos(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arccosh(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCOSH
    assert a.to_sympy() == sympy.acosh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arccot(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCOT
    assert a.to_sympy() == sympy.acot(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arccoth(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCOTH
    assert a.to_sympy() == sympy.acoth(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arccsc(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCSC
    assert a.to_sympy() == sympy.acsc(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arccsch(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCCSCH
    assert a.to_sympy() == sympy.acsch(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arcsec(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCSEC
    assert a.to_sympy() == sympy.asec(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arcsech(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCSECH
    assert a.to_sympy() == sympy.asech(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arcsin(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCSIN
    assert a.to_sympy() == sympy.asin(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arcsinh(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCSINH
    assert a.to_sympy() == sympy.asinh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arctan(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCTAN
    assert a.to_sympy() == sympy.atan(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('arctanh(x)')
    assert a.type == libsbml.AST_FUNCTION_ARCTANH
    assert a.to_sympy() == sympy.atanh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('cos(x)')
    assert a.type == libsbml.AST_FUNCTION_COS
    assert a.to_sympy() == sympy.cos(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('cosh(x)')
    assert a.type == libsbml.AST_FUNCTION_COSH
    assert a.to_sympy() == sympy.cosh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('cot(x)')
    assert a.type == libsbml.AST_FUNCTION_COT
    assert a.to_sympy() == sympy.cot(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('coth(x)')
    assert a.type == libsbml.AST_FUNCTION_COTH
    assert a.to_sympy() == sympy.coth(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('csc(x)')
    assert a.type == libsbml.AST_FUNCTION_CSC
    assert a.to_sympy() == sympy.csc(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('csch(x)')
    assert a.type == libsbml.AST_FUNCTION_CSCH
    assert a.to_sympy() == sympy.csch(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('sec(x)')
    assert a.type == libsbml.AST_FUNCTION_SEC
    assert a.to_sympy() == sympy.sec(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('sech(x)')
    assert a.type == libsbml.AST_FUNCTION_SECH
    assert a.to_sympy() == sympy.sech(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('sin(x)')
    assert a.type == libsbml.AST_FUNCTION_SIN
    assert a.to_sympy() == sympy.sin(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('sinh(x)')
    assert a.type == libsbml.AST_FUNCTION_SINH
    assert a.to_sympy() == sympy.sinh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('tan(x)')
    assert a.type == libsbml.AST_FUNCTION_TAN
    assert a.to_sympy() == sympy.tan(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())

    a = SymbolicASTNode.from_string('tanh(x)')
    assert a.type == libsbml.AST_FUNCTION_TANH
    assert a.to_sympy() == sympy.tanh(x)
    assert a == SymbolicASTNode.from_sympy(a.to_sympy())


# def test_linear_algebra():
#     # cannot be done from string either
#     # a = ASTNode()
#     # a.type = libsbml.AST_LINEAR_ALGEBRA_DETERMINANT
#     DETERMINANT = libsbml.AST_LINEAR_ALGEBRA_DETERMINANT
#     MATRIX = libsbml.AST_LINEAR_ALGEBRA_MATRIX
#     MATRIXROW = libsbml.AST_LINEAR_ALGEBRA_MATRIXROW
#     OUTER_PRODUCT = libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT
#     SCALAR_PRODUCT = libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT
#     SELECTOR = libsbml.AST_LINEAR_ALGEBRA_SELECTOR
#     TRANSPOSE = libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE
#     VECTOR = libsbml.AST_LINEAR_ALGEBRA_VECTOR
#     VECTOR_PRODUCT = libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT


def test_logical():
    """two operators, EXISTS and FORALL, cannot be reconstructed from strings"""
    a = SymbolicASTNode.from_string('and(x, y)')  # == 'x && y'
    assert a.type == libsbml.AST_LOGICAL_AND
    assert a.to_sympy() == sympy.And(x, y)

    # assert a.type == libsbml.AST_LOGICAL_EXISTS  # array-associated
    # assert a.type == libsbml.AST_LOGICAL_FORALL  # array-associated

    a = SymbolicASTNode.from_string('implies(x, y)')
    assert a.type == libsbml.AST_LOGICAL_IMPLIES
    assert a.to_sympy() == sympy.Implies(x, y)

    a = SymbolicASTNode.from_string('not(x)')  # == '!x'
    assert a.type == libsbml.AST_LOGICAL_NOT
    assert a.to_sympy() == sympy.Not(x)

    a = SymbolicASTNode.from_string('or(x, y)')  # == 'x || y'
    assert a.type == libsbml.AST_LOGICAL_OR
    assert a.to_sympy() == sympy.Or(x, y)

    a = SymbolicASTNode.from_string('xor(x, y)')
    assert a.type == libsbml.AST_LOGICAL_XOR
    assert a.to_sympy() == sympy.Xor(x, y)

    # a = ASTNode()
    # a.type = libsbml.AST_LOGICAL_EXISTS
    # a.type = libsbml.AST_LOGICAL_FORALL
    # a.wrapped.addChild(ASTNode.from_string('x').wrapped)
    # a.wrapped.addChild(ASTNode.from_string('y').wrapped)
    # assert a.is_well_formed
    # ASTNode.from_string(a.string)


def test_relational():
    a = SymbolicASTNode.from_string('eq(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_EQ
    assert a.to_sympy() == sympy.Eq(x, y)

    a = SymbolicASTNode.from_string('geq(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_GEQ
    assert a.to_sympy() == sympy.Ge(x, y)

    a = SymbolicASTNode.from_string('gt(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_GT
    assert a.to_sympy() == sympy.Gt(x, y)

    a = SymbolicASTNode.from_string('leq(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_LEQ
    assert a.to_sympy() == sympy.Le(x, y)

    a = SymbolicASTNode.from_string('lt(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_LT
    assert a.to_sympy() == sympy.Lt(x, y)

    a = SymbolicASTNode.from_string('neq(x, y)')
    assert a.type == libsbml.AST_RELATIONAL_NEQ
    assert a.to_sympy() == sympy.Ne(x, y)


# def test_series():
#     # cannot be represented as string
#     a = SymbolicASTNode()
#     a.type = libsbml.AST_SERIES_PRODUCT
#     a.type = libsbml.AST_SERIES_SUM
#     a.wrapped.addChild(SymbolicASTNode.from_string('x').wrapped)
#     a.wrapped.addChild(SymbolicASTNode.from_string('y').wrapped)
#     assert a.is_well_formed
#
#
# def test_statistics():
#     # cannot be represented as string
#     a = SymbolicASTNode()
#     a.type = libsbml.AST_STATISTICS_MEAN
#     a.wrapped.addChild(SymbolicASTNode.from_string('x').wrapped)
#     a.wrapped.addChild(SymbolicASTNode.from_string('y').wrapped)
#     assert a.is_well_formed
#     # ASTNode.from_string(a.string)
#
#     a = SymbolicASTNode.from_string('mean(x, y)')
#     # assert a.type == libsbml.AST_STATISTICS_MEAN
#     # assert a.type == libsbml.AST_STATISTICS_MEAN
#     # libsbml.AST_STATISTICS_MEDIAN
#     # libsbml.AST_STATISTICS_MODE
#     # libsbml.AST_STATISTICS_MOMENT
#     # libsbml.AST_STATISTICS_SDEV
#     # libsbml.AST_STATISTICS_VARIANCE
#
#
# def test_other():
#     nan = SymbolicASTNode.from_string('notanumber')
#     inf = SymbolicASTNode.from_string('infinity')
#
#     a = SymbolicASTNode()
#     # a.type = ALL_AST_TYPES.inverse['AST_END_OF_CORE']  # cannot be set
#     # a.type = ALL_AST_TYPES.inverse['AST_UNKNOWN']
#     # a.type = ALL_AST_TYPES.inverse['AST_END_OF_CORE']  # cannot be set
#     # libsbml.AST_LAMBDA  # lambda is restricted to use in FunctionDefinition
