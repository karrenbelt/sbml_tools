
import pytest
from sbml_tools.wrappers import Unit, UnitDefinition
from sbml_tools.toolbox.factories.unit_factory import PREDEFINED_KINDS, CUSTOM_KINDS
from sbml_tools.toolbox.factories.unit_definition_factory import UnitDefinitionFactory, UNIT_DEFINITION_PATTERN


def test_unit_definition_pattern():

    test_cases = [
        'mole_per_second',  # mole / second
        'per_mole_per_second',  # 1 / mole / second
        'millimole_per_gram_per_second',
        'joule_per_kelvin_per_mole',  # joule / kelvin / mole
        'per_second_per_mole',  # 1 / second / mole
        'kilojoule_per_mole',  # 1000 joule / mole
        'kilogram_times_meter_per_squared_second',  # kilogram * meter / second^2 (== newton)
        'kilogram_per_squared_micrometre',
        ]

    for s in test_cases:
        print(UNIT_DEFINITION_PATTERN.split(s))


def test():
    # from sbml_tools.wrappers import Model
    # model = Model(level=3, version=1)
    udef_factory = UnitDefinitionFactory(level=3, version=1)

    dimensionless = udef_factory('dimensionless')

    # unit_definitions composed of a single unit
    for identifier in PREDEFINED_KINDS:
        if identifier == 'dimensionless':
            continue  # known exception

        udef = udef_factory(identifier)
        print(udef.id, udef.as_quantity)

    for identifier in CUSTOM_KINDS:
        udef = udef_factory(identifier)
        print(udef.id, udef.as_quantity)

    test_cases = ['per_cubicyottamole', 'per_nano_second2', 'per_cubicyottamole', 'per_mole2', 'sextic_zsecond',
                  'second_per_mole_times_gram']
    for identifier in test_cases:
        unit_definition = udef_factory(identifier)
        print(unit_definition.id, unit_definition.as_quantity)

    # unit_definitions composed of a multiple units
    test_cases = [
        'mole_per_second',  # mole / second
        'per_millimole_per_second',  # 1 / millimole / second
        'millimole_per_gram_per_second',
        'mmole_per_gram_per_hour',
        'joule_per_kelvin_per_mole',  # joule / kelvin / mole
        'per_second_per_mole',  # 1 / second / mole
        'kilojoule_per_mole',  # 1000 joule / mole
        'kilogram_times_metre_per_squared_second',  # kilogram * meter / second^2 (== newton)
        'kilogram_per_squared_millimetre',  # == 1e9 grams / meter ** 2
        ]

    for identifier in test_cases:
        unit_definition = udef_factory(identifier)
        print(unit_definition.id, unit_definition.as_quantity)

    # proper test case - complex unit definition involving custom unit (per hour)
    identifier = 'mmole_per_gram_per_hour'
    units = [Unit(kind='mole', exponent=1, scale=-3, multiplier=1.0),
             Unit(kind='gram', exponent=-1, scale=0, multiplier=1.0),
             Unit(kind='second', exponent=-1, scale=0, multiplier=3600.0)]
    manual_unit = UnitDefinition(id=identifier, units=units)
    factory_unit = udef_factory(identifier)
    assert manual_unit == factory_unit

    # more tests
    kilogram = UnitDefinition(id='kilogram', units=[Unit(kind='kilogram', exponent=1, scale=0, multiplier=1.0)])
    kilogram_from_factory = udef_factory('kilogram')
    assert kilogram.as_quantity == kilogram_from_factory.as_quantity
    assert kilogram.equals(kilogram_from_factory) is False
    assert kilogram.version != kilogram_from_factory.version

    kilogram = UnitDefinition(level=3, version=1, id='kilogram',
                              units=[Unit(level=3, version=1, kind='kilogram', exponent=1, scale=0, multiplier=1.0)])
    assert kilogram.equals(kilogram_from_factory)
