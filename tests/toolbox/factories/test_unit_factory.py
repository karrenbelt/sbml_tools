
import pytest
from sbml_tools.toolbox.factories.unit_factory import (UnitFactory,  MetricPrefix, exp_prefixes,
                                                       PREDEFINED_KINDS, CUSTOM_KINDS, UNIT_PATTERN,)


def test_unit_pattern():
    # only unit - must exist
    kinds = PREDEFINED_KINDS.copy()
    kinds.remove('dimensionless')  # special case
    metric_prefixes = [k for k in vars(MetricPrefix) if not k.startswith('_')]

    for s in kinds:
        inverse, exp_prefix, metric_prefix, kind, exp_suffix = UNIT_PATTERN.match(s).groups()
        print(inverse, exp_prefix, metric_prefix, kind, exp_suffix)

    for s in metric_prefixes:
        s += 'metre'
        inverse, exp_prefix, metric_prefix, kind, exp_suffix = UNIT_PATTERN.match(s).groups()
        print(inverse, exp_prefix, metric_prefix, kind, exp_suffix)

    for s in exp_prefixes:
        s += 'metre'
        inverse, exp_prefix, metric_prefix, kind, exp_suffix = UNIT_PATTERN.match(s).groups()
        print(inverse, exp_prefix, metric_prefix, kind, exp_suffix)

    for s in ['1', '-1', '_2', '_33', '_0', '1.0', '1.5']:
        # TODO: float ok if is_integer, else not
        # TODO: special case zero suffix --> dimensionless
        # negative values should in identifiers make invalid sbml ids: should yield None
        s = 'metre' + s
        inverse, exp_prefix, metric_prefix, kind, exp_suffix = UNIT_PATTERN.match(s).groups()
        print(inverse, exp_prefix, metric_prefix, kind, exp_suffix)

    complex_cases = ['per_cubicyottamole', 'per_nano_second2', 'per_cubicyottamole', 'per_mole2', 'sextic_zsecond2']
    for s in complex_cases:
        inverse, exp_prefix, metric_prefix, kind, exp_suffix = UNIT_PATTERN.match(s).groups()
        print(inverse, exp_prefix, metric_prefix, kind, exp_suffix)


def test():
    # model = Model(level=3, version=1)
    u_factory = UnitFactory(level=3, version=1)

    for unit_kind in u_factory.implemented_unit_kinds:
        unit = u_factory.create_unit(unit_kind)

    per_cubicyottamole = u_factory.create_unit('per_cubicyottamole')
    per_hour = u_factory.create_unit('per_hour')

    assert (u_factory.create_unit('per_squaredhour')
            == u_factory.create_unit('per_squared_hour')
            == u_factory.create_unit('per_hour2')
            == u_factory.create_unit('per_hour_2')
            )

    # squared_second_2 = u_factory.create_unit('squared_second_2')  # error as should
    # per_per_hour = u_factory.create_unit('per_per_hour')  # error as should
    # second_per_mole = u_factory.create_unit('second_per_mole')  # error as should


