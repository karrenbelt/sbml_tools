
import pytest
from sbml_tools.wrappers import SBMLDocument, Model
from sbml_tools.toolbox.factories import ParameterFactory


# fixtures
@pytest.fixture()
def minimal_factory():
    doc = SBMLDocument(level=3, version=2)
    model = doc.create_model()
    minimal_factory = ParameterFactory(model, create_local_parameters=False)
    return minimal_factory


@pytest.fixture()
def factory_with_model_units():
    doc = SBMLDocument(level=3, version=2)
    model = doc.create_model()
    model.time_units = 'second'
    model.extent_units = 'mole'
    model.substance_units = 'mole'
    model.volume_units = 'litre'
    model.area_units = 'squared_metre'
    model.length_units = 'metre'
    factory_with_model_units = ParameterFactory(model, create_local_parameters=False)
    return factory_with_model_units


@pytest.fixture()
def factory_with_single_reaction():
    doc = SBMLDocument(level=3, version=2)
    model = doc.create_model()
    model.create_compartment(id='c', name='cytosol', constant=True, spatial_dimensions=3, units='femtolitre')
    species_identifiers = ['A', 'B', 'C', 'D', 'E', 'M']
    for identifier in species_identifiers:  # ensure that units and has_only_substance_units match
        model.create_species(id=identifier, compartment='c', has_only_substance_units=True,
                                 boundary_condition=False, constant=False, initial_amount=100,
                                 substance_units='millimole')
    model.create_reaction(id='rxn', reversible=True, reactants='A + B', products='C + D', modifiers='E M')
    factory_with_single_reaction = ParameterFactory(doc.model, create_local_parameters=False)
    return factory_with_single_reaction


@pytest.fixture()
def factory_with_single_reaction_and_model_units():
    # idea is to have units of species and compartment not match defaults at model level
    doc = SBMLDocument(level=3, version=2)
    model = doc.create_model()
    model.time_units = 'second'
    model.extent_units = 'mole'
    model.substance_units = 'mole'
    model.volume_units = 'litre'
    model.area_units = 'squared_metre'
    model.length_units = 'metre'
    model.create_compartment(id='c', name='cytosol', constant=True, spatial_dimensions=3, units='femtolitre')
    species_identifiers = ['A', 'B', 'C', 'D', 'E', 'M']
    for identifier in species_identifiers:  # ensure that units and has_only_substance_units match
        model.create_species(id=identifier, compartment='c', has_only_substance_units=True,
                                 boundary_condition=False, constant=False, initial_amount=100,
                                 substance_units='millimole')
    model.create_reaction(id='rxn', reversible=True, reactants='A + B', products='C + D', modifiers='E M')
    factory_with_single_reaction_and_model_units = ParameterFactory(doc.model, create_local_parameters=False)
    return factory_with_single_reaction_and_model_units


def test_change_default_units(minimal_factory):
    new_units = dict(
        # R='joule_per_kelvin_per_mole',
        # T='kelvin',
        # pH='dimensionless',
        I='millimole_per_litre',
        mu0='kilojoule_per_mole',
        mu='kilojoule_per_mole',
        dmu0='kilojoule_per_mole',
        # keq='dimensionless',
        kf='millimole_per_second',
        kr='millimole_per_second',
        kcatf='per_second',
        kcatr='per_second',
        vmaxf='millimole_per_second',
        vmaxr='millimole_per_second',
        km='millimole',
        ka='millimole',
        ki='millimole',
        # b=,
        )
    for symbol, units in new_units.items():
        minimal_factory.parameter_defaults[symbol]['units'] = units


# tests
def test_parameter_created(minimal_factory):
    R = minimal_factory.ideal_gas_constant()
    assert R == minimal_factory.model.parameters['R']


def test_unit_definition_created(minimal_factory):
    R = minimal_factory.ideal_gas_constant()
    assert R.model.unit_definitions[0].id == R.units


def test_create_and_get(minimal_factory):
    model = minimal_factory.model
    assert not model.parameters
    R1 = minimal_factory.ideal_gas_constant()  # create
    R2 = minimal_factory.ideal_gas_constant()  # get
    assert R1 == R2
    assert len(model.parameters) == 1


def test_create_matching_existing(minimal_factory):
    T = minimal_factory.thermodynamic_temperature(value=100)
    assert T == minimal_factory.thermodynamic_temperature(value=100)
    with pytest.raises(Exception):
        minimal_factory.thermodynamic_temperature(value=101)


def test_unit_inference_from_model(factory_with_model_units):
    model = factory_with_model_units.model
    compartment = model.create_compartment(id='c', constant=True, spatial_dimensions=3)
    factory_with_model_units.ionic_strength(compartment)
    assert compartment.model.unit_definitions[-1].id == f'{model.substance_units}_per_{model.volume_units}'


# compartment parameters
def test_ph(minimal_factory):
    compartment = minimal_factory.model.create_compartment(id='c', constant=True)
    pH_cytosol = minimal_factory.ph(compartment)
    assert minimal_factory.model.unit_definitions[pH_cytosol.units]


def test_ionic_strength(minimal_factory):
    compartment = minimal_factory.model.create_compartment(id='c', constant=True)
    with pytest.raises(Exception):  # 1. dimensionality should be 3
        minimal_factory.ionic_strength(compartment)
    compartment.spatial_dimensions = 3
    compartment.units = 'litre'
    compartment.model.substance_units = 'millimole'  # also required; else cannot infer unit from compartment + model
    I_c = minimal_factory.ionic_strength(compartment)
    assert I_c.units == 'millimole_per_litre'


# species parameters
def test_mu0(factory_with_single_reaction):
    species = factory_with_single_reaction.model.species['A']
    mu0 = factory_with_single_reaction.standard_chemical_potential(species, units='joule_per_mole')


def test_mu(factory_with_single_reaction):
    species = factory_with_single_reaction.model.species['A']
    mu = factory_with_single_reaction.chemical_potential(species, units='decajoule_per_petagram')


# reaction parameters
def test_keq(factory_with_single_reaction):
    reaction = factory_with_single_reaction.model.reactions['rxn']
    factory_with_single_reaction.equilibrium_constant(reaction)


def test_local_keq(factory_with_single_reaction):
    factory_with_single_reaction.create_local_parameters = True
    reaction = factory_with_single_reaction.model.reactions['rxn']
    assert not reaction.local_parameters
    factory_with_single_reaction.equilibrium_constant(reaction)
    assert reaction.local_parameters

# reaction-species parameters


# call method with minimal arguments for specific symbols
def test_call(factory_with_single_reaction_and_model_units):
    factory = factory_with_single_reaction_and_model_units
    R = factory('R')
    T = factory('T')

    compartment = factory.model.compartments[0]
    pH_c = factory('pH', compartment=compartment)
    pH_I = factory('I', compartment=compartment)

    species = factory.model.species['A']
    mu0_A = factory('mu0', species=species, units='kilojoule_per_mole')  # must pass units
    mu_A = factory('mu', species=species, units='kilojoule_per_mole')  # must pass units

    reaction = factory.model.reactions[0]
    rxn_dmu0 = factory('dmu0', reaction=reaction, units='kilojoule_per_mole')  # must pass units
    rxn_keq = factory('keq', reaction=reaction)
    rxn_kf = factory('kf', reaction=reaction)
    rxn_kr = factory('kr', reaction=reaction)
    rxn_kcatf = factory('kcatf', reaction=reaction)
    rxn_kcatr = factory('kcatr', reaction=reaction)
    rxn_vmaxf = factory('vmaxf', reaction=reaction)
    rxn_vmaxr = factory('vmaxr', reaction=reaction)

    modifier = factory.model.species['M']
    rxn_km_A = factory('km', reaction=reaction, species=species)
    rxn_ki_M = factory('ki', reaction=reaction, species=modifier)
    rxn_ka_M = factory('ka', reaction=reaction, species=modifier)
    rxn_b_M = factory('b', reaction=reaction, species=modifier)
    rxn_w_M = factory('w', reaction=reaction, species=modifier, units='item')

    generated_parameters = [R, T, pH_c, pH_I, mu0_A, mu_A, rxn_dmu0, rxn_keq, rxn_kf, rxn_kr, rxn_kcatf, rxn_kcatr,
                            rxn_vmaxf, rxn_vmaxr, rxn_km_A, rxn_ki_M, rxn_ka_M, rxn_b_M, rxn_w_M]

    assert list(factory.model.parameters) == generated_parameters

