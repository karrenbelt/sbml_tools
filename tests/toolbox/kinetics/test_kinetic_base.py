
import pytest
from sbml_tools.toolbox.kinetics.kinetic_base import KineticBase
from sbml_tools.toolbox.kinetics.generalized_mass_action import GeneralizedMassAction
from sbml_tools.toolbox.kinetics.test_case_models import simple_reactions_model
from tests.concreter import concreter


def test_attributes():
    doc = simple_reactions_model()
    doc.model.reactions[0].create_kinetic_law()
    # KineticBase.check_applicability =
    base = concreter(KineticBase)(doc.model.reactions[0])

    base.reactants
    base.products
    base.modifiers
    base.n_products
    base.n_reactants
    base.n_modifiers
    base.order_reactants
    base.order_products
    base.has_all_constant_stoichiometries
    base.has_all_integer_stoichiometries
    base.has_only_substance_units
    base.has_consistent_species_has_only_substance_units
    base.has_consistent_species_units

    base.catalysts
    base.enzymes
    base.genes
    base.ribonucleic_acids
    base.messenger_ribonucleic_acids
    base.inhibitors
    base.allosteric_activators
    base.competitive_inhibitors
    base.irreversible_inhibitor
    base.complete_inhibitors
    base.partial_inhibitors
    base.activators
    base.allosteric_activators
    base.non_allosteric_activators


def test_non_integer_stoichiometry():

    doc = simple_reactions_model()
    self = concreter(KineticBase)(doc.model.reactions[8])
    assert not self.has_all_integer_stoichiometries
    self.make_integer_stoichiometries()
    assert self.has_all_integer_stoichiometries


