
import pytest
from sbml_tools.toolbox.kinetics import MichaelisMenten, RateLawNotApplicableError
from sbml_tools.toolbox.kinetics.test_case_models import simple_reactions_model
from sbml_tools.toolbox.kinetics.test_case_models import reactions_model_with_modifiers


@pytest.mark.parametrize('idx', [0, 4, 5])
def test_incorrect_number_of_reactants(idx):
    doc = simple_reactions_model()
    self = MichaelisMenten(doc.model.reactions[idx])
    assert self.check_applicability() == 'incorrect stoichiometry of reactants'


@pytest.mark.parametrize('idx', [6, 7])
def test_incorrect_number_of_products(idx):
    doc = simple_reactions_model()
    self = MichaelisMenten(doc.model.reactions[idx])
    self.reaction.reversible = True
    assert self.check_applicability() == 'incorrect stoichiometry of products'


def test_no_modifiers():
    doc = simple_reactions_model()
    self = MichaelisMenten(doc.model.reactions[1])
    assert self.check_applicability() == 'reaction has no modifier assigned as enzyme'


def test_no_annotated_enzyme():
    doc = reactions_model_with_modifiers()
    self = MichaelisMenten(doc.model.reactions[0])
    assert self.check_applicability() == 'reaction has no modifier assigned as enzyme'


def test_annotated_enzyme():
    doc = reactions_model_with_modifiers()
    self = MichaelisMenten(doc.model.reactions[0])
    self.reaction.modifiers[0].sbo_term = 'enzymatic catalyst'
    assert self.check_applicability() is None
    assert self.create_equation().string == 's3 * r1_kcatf_s3 * s1 / (km_s1 + s1)'
    self.reaction.reversible = True
    assert self.create_equation().string == 's3 * (r1_kcatf_s3 * s1 / r1_km_s1 - r1_kcatr_s3 * s2 / r1_km_s2) / ' \
                                            '(1 + s1 / r1_km_s1 + s2 / r1_km_s2)'


def test_three_modifiers():
    doc = reactions_model_with_modifiers()
    self = MichaelisMenten(doc.model.reactions[2])
    self.reaction.modifiers[0].sbo_term = 'enzymatic catalyst'
    self.reaction.modifiers[1].sbo_term = 'enzymatic catalyst'
    self.reaction.modifiers[2].sbo_term = 'enzymatic catalyst'
    assert self.check_applicability() is None
    assert self.create_equation().string == 's3 * r3_kcatf_s3 * s1 / (km_s1 + s1) + ' \
                                            's4 * r3_kcatf_s4 * s1 / (km_s1 + s1) + ' \
                                            's5 * r3_kcatf_s5 * s1 / (km_s1 + s1)'

    self.reaction.modifiers[1].sbo_term = 'inhibitor'
    self.reaction.modifiers[2].sbo_term = 'inhibitor'
    assert self.create_equation().string == '(r3_ki_s4 / (r3_ki_s4 + s4)) * ' \
                                            '(r3_ki_s5 / (r3_ki_s5 + s5)) * ' \
                                            '(s3 * r3_kcatf_s3 * s1 / (km_s1 + s1))'

    self.reaction.modifiers[1].sbo_term = 'potentiator'
    self.reaction.modifiers[2].sbo_term = 'potentiator'
    assert self.create_equation().string == '(s4 / (r3_ka_s4 + s4)) * ' \
                                            '(s5 / (r3_ka_s5 + s5)) * ' \
                                            '(s3 * r3_kcatf_s3 * s1 / (km_s1 + s1))'



