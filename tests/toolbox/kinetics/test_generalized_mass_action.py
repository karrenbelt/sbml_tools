
import pytest
from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.kinetics import GeneralizedMassAction
from sbml_tools.toolbox.kinetics.test_case_models import simple_reactions_model
from sbml_tools.toolbox.kinetics.test_case_models import reactions_model_with_modifiers


# TODO: parameterize tests
#  trees are different, compare infix notation strings?

# @pytest.fixture
# def doc():
#     doc = simple_reactions_model()
#     return doc


def test_simple_reactions():

    doc = simple_reactions_model()
    haldane_substituted = True

    self = GeneralizedMassAction(doc.model.reactions[0])
    assert self.create_equation() == ASTNode(name='r1_kf')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r1_kf - r1_kr')
    with pytest.raises(Exception):
        self.create_equation(haldane_substituted)

    self = GeneralizedMassAction(doc.model.reactions[1])
    assert self.create_equation() == ASTNode.from_string('r2_kf * s1')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r2_kf * s1 - r2_kr')
    with pytest.raises(Exception):
        self.create_equation(haldane_substituted)

    self = GeneralizedMassAction(doc.model.reactions[2])
    assert self.create_equation() == ASTNode.from_string('r3_kf')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r3_kf - r3_kr * s1')
    with pytest.raises(Exception):
        self.create_equation(haldane_substituted)

    self = GeneralizedMassAction(doc.model.reactions[3])
    assert self.create_equation() == ASTNode.from_string('r4_kf * s1')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r4_kf * s1 - r4_kr * s2')
    assert self.create_equation(haldane_substituted) == ASTNode.from_string('r4_kf * s1 * (1 - s2 / s1 / r4_keq)')

    self = GeneralizedMassAction(doc.model.reactions[4])
    assert self.create_equation() == ASTNode.from_string('r5_kf * s1^2')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r5_kf * s1^2 - r5_kr * s2')
    assert self.create_equation(haldane_substituted) == ASTNode.from_string('r5_kf * s1^2 * (1 - s2 / s1^2 / r5_keq)')

    self = GeneralizedMassAction(doc.model.reactions[5])
    assert self.create_equation() == ASTNode.from_string('r6_kf * s1 * s2')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r6_kf * s1 * s2 - r6_kr * s3')
    assert self.create_equation(haldane_substituted) == ASTNode.from_string('r6_kf * s1 * s2 * (1 - s3 / (s1 * s2) / r6_keq)')

    self = GeneralizedMassAction(doc.model.reactions[6])
    assert self.create_equation() == ASTNode.from_string('r7_kf * s1')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r7_kf * s1 - r7_kr * s2^2')
    assert self.create_equation(haldane_substituted) == ASTNode.from_string('r7_kf * s1 * (1 - s2^2 / s1 / r7_keq)')

    self = GeneralizedMassAction(doc.model.reactions[7])
    assert self.create_equation() == ASTNode.from_string('r8_kf * s1')
    self.reaction.reversible = True
    assert self.create_equation() == ASTNode.from_string('r8_kf * s1 - r8_kr * s2 * s3')
    assert self.create_equation(haldane_substituted) == ASTNode.from_string('r8_kf * s1 * (1 - s2 * s3 / s1 / r8_keq)')

    # invalid because of non-integer stoichiometries
    self = GeneralizedMassAction(doc.model.reactions[8])
    assert self.create_equation() == ASTNode.from_string('r10_kf * s1^0.1 * s2')
    with pytest.raises(Exception):
        self.assign_equation()  #
    self.reaction.reversible = True
    with pytest.raises(Exception):
        self.assign_equation()  # r10_kf * s1^0.1 * s2 - r10_kr * s3^0.3
    # with pytest.raises(Exception):
    #     self.assign_equation(haldane_substituted)  # r10_kf * s1^0.1 * s2 * (1 - s3^0.3 / (s1^0.1 * s2) / r10_keq)


def test_reactions_model_with_one_modifier():

    doc = reactions_model_with_modifiers()

    # single catalyst
    self = GeneralizedMassAction(doc.model.reactions[0])
    self.reaction.modifiers[0].sbo_term = 'catalyst'
    assert self.create_equation().string == 's3 * (r1_kf_s3 * s1)'
    self.reaction.reversible = True
    assert self.create_equation().string == 's3 * (r1_kf_s3 * s1 - r1_kr_s3 * s2)'

    # single activator
    self.reaction.reversible = False
    self.reaction.modifiers[0].sbo_term = 'potentiator'
    assert self.create_equation().string == '(s3 / (r1_ka_s3 + s3)) * r1_kf * s1'
    self.reaction.reversible = True
    assert self.create_equation().string == '(s3 / (r1_ka_s3 + s3)) * (r1_kf * s1 - r1_kr * s2)'

    # single inhibitor
    self.reaction.reversible = False
    self.reaction.modifiers[0].sbo_term = 'inhibitor'
    assert self.create_equation().string == '(r1_ki_s3 / (r1_ki_s3 + s3)) * r1_kf * s1'
    self.reaction.reversible = True
    assert self.create_equation().string == '(r1_ki_s3 / (r1_ki_s3 + s3)) * (r1_kf * s1 - r1_kr * s2)'


def test_reactions_model_with_two_modifier():
    # trees are different, comparing infix notation strings
    doc = reactions_model_with_modifiers()

    # two alysts
    self = GeneralizedMassAction(doc.model.reactions[1])
    self.reaction.modifiers[0].sbo_term = 'catalyst'
    self.reaction.modifiers[1].sbo_term = 'catalyst'
    assert self.create_equation().string == 's3 * (r2_kf_s3 * s1) + s4 * (r2_kf_s4 * s1)'
    self.reaction.reversible = True
    expected = 's3 * (r2_kf_s3 * s1 - r2_kr_s3 * s2) + s4 * (r2_kf_s4 * s1 - r2_kr_s4 * s2)'
    assert self.create_equation().string == expected

    # two activators
    self.reaction.reversible = False
    self.reaction.modifiers[0].sbo_term = 'potentiator'
    self.reaction.modifiers[1].sbo_term = 'potentiator'
    assert self.create_equation().string == '(s3 / (r2_ka_s3 + s3)) * (s4 / (r2_ka_s4 + s4)) * r2_kf * s1'
    self.reaction.reversible = True
    expected = '(s3 / (r2_ka_s3 + s3)) * (s4 / (r2_ka_s4 + s4)) * (r2_kf * s1 - r2_kr * s2)'
    assert self.create_equation().string == expected

    # two inhibitors
    self.reaction.reversible = False
    self.reaction.modifiers[0].sbo_term = 'inhibitor'
    self.reaction.modifiers[1].sbo_term = 'inhibitor'
    expected = '(r2_ki_s3 / (r2_ki_s3 + s3)) * (r2_ki_s4 / (r2_ki_s4 + s4)) * r2_kf * s1'
    assert self.create_equation().string == expected
    self.reaction.reversible = True
    expected = '(r2_ki_s3 / (r2_ki_s3 + s3)) * (r2_ki_s4 / (r2_ki_s4 + s4)) * (r2_kf * s1 - r2_kr * s2)'
    assert self.create_equation().string == expected


def test_reactions_model_with_three_modifiers():

    doc = reactions_model_with_modifiers()

    # two catalysts
    self = GeneralizedMassAction(doc.model.reactions[2])
    self.reaction.modifiers[0].sbo_term = 'catalyst'
    self.reaction.modifiers[1].sbo_term = 'potentiator'
    self.reaction.modifiers[2].sbo_term = 'inhibitor'
    expected = '(s4 / (r3_ka_s4 + s4)) * (r3_ki_s5 / (r3_ki_s5 + s5)) * s3 * (r3_kf_s3 * s1)'
    assert self.create_equation().string == expected
    self.reaction.reversible = True
    expected = '(s4 / (r3_ka_s4 + s4)) * (r3_ki_s5 / (r3_ki_s5 + s5)) * s3 * (r3_kf_s3 * s1 - r3_kr_s3 * s2)'
    assert self.create_equation().string == expected
