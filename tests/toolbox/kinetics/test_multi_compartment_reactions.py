from roadrunner import RoadRunner
from sbml_tools.toolbox.kinetics.test_case_models import simple_two_compartment_model


def test_different_sized_compartments_amounts_and_concentrations():
    # roadrunner deals with amounts / concentrations in different compartments
    doc = simple_two_compartment_model()

    k1 = doc.model.create_parameter(id='k1', constant=True, value=1.0)
    doc.model.reactions[0].kinetic_law = 'k1 * s1'
    doc.model.reactions[1].kinetic_law = 'k1 * s3'
    rr = RoadRunner(doc.wrapped.toSBML())
    r = rr.simulate(0, 10)
    assert all(r['[s1]'] == r['[s3]']) and all(r['[s2]'] == r['[s4]'])

