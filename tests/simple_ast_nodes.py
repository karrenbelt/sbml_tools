
import pytest
import libsbml

"""
This file contains a minimalistic example of all types of ASTNodes present in libsbml

if an ASTNode type can at most have one child, the example will have one: 'x'
if the ASTNode type can at most have two children, the example will have two: 'x' and 'y'
if the ASTNode type can have more than two children, the example will have three: 'x', 'y' and 'z'

NOTE: also see: toolbox -> math -> node_mapping.py
"""


def make_ast_node(typ: int, name=None, value=None, children=None) -> libsbml.ASTNode:
    """utility function for astnodes that cannot be (re)constructed from strings"""
    node = libsbml.ASTNode()
    assert node.setType(typ) is 0, f'cannot set type {typ}'
    if name is not None:
        assert node.setName(name) is 0, f'cannot set name {name}'
    if value is not None:
        if isinstance(value, (int, float)):
            value = (value, )
        assert node.setValue(*value) is 0, f'cannot set value {value}'
    if children is not None:
        if isinstance(children, libsbml.ASTNode):
            children = [children]
        for child in children:  # IMPORTANT: deepcopy the child
            assert node.addChild(child.deepCopy()) is 0,  f'cannot set child {child}'
    assert node.getType() == typ, f'type changed from {typ} to {node.getType()}'
    assert node.isWellFormedASTNode(), f'not well formed: {libsbml.formulaToL3String(node)}'
    return node


X = make_ast_node(libsbml.AST_NAME, name='x')
Y = make_ast_node(libsbml.AST_NAME, name='y')
Z = make_ast_node(libsbml.AST_NAME, name='z')
# ZERO = make_ast_node(libsbml.AST_INTEGER, value=0)
# ONE = make_ast_node(libsbml.AST_INTEGER, value=1)


def _make_custom_csymbol_function():
    # TODO: make utility function, include to utils.py
    # https://www.w3.org/TR/MathML3/chapter4.html#contm.csymbol
    math_ml = """
    <semantics>
      <csymbol encoding="text" definitionURL=""> customname </csymbol>
      <annotation-xml cd="mathmltypes" name="type" encoding="MathML-Content">
        <ci>T</ci>
      </annotation-xml>
    </semantics>
    """
    node = libsbml.readMathMLFromString(math_ml)
    assert node.isWellFormedASTNode()
    return node


class ARITHMETIC:

    PLUS = make_ast_node(libsbml.AST_PLUS, children=(X, Y, Z))
    MINUS = make_ast_node(libsbml.AST_MINUS, children=(X, Y))
    POWER = make_ast_node(libsbml.AST_POWER, children=(X, Y))
    DIVIDE = make_ast_node(libsbml.AST_DIVIDE, children=(X, Y))
    TIMES = make_ast_node(libsbml.AST_TIMES, children=(X, Y, Z))

    FUNCTION_ABS = make_ast_node(libsbml.AST_FUNCTION_ABS, children=X)
    FUNCTION_CEILING = make_ast_node(libsbml.AST_FUNCTION_CEILING, children=X)
    FUNCTION_EXP = make_ast_node(libsbml.AST_FUNCTION_EXP, children=X)
    FUNCTION_FACTORIAL = make_ast_node(libsbml.AST_FUNCTION_FACTORIAL, children=X)
    FUNCTION_FLOOR = make_ast_node(libsbml.AST_FUNCTION_FLOOR, children=X)
    FUNCTION_LN = make_ast_node(libsbml.AST_FUNCTION_LN, children=X)
    FUNCTION_LOG = make_ast_node(libsbml.AST_FUNCTION_LOG, children=(X, Y))
    FUNCTION_MAX = make_ast_node(libsbml.AST_FUNCTION_MAX, children=(X, Y, Z))
    FUNCTION_MIN = make_ast_node(libsbml.AST_FUNCTION_MIN, children=(X, Y, Z))
    FUNCTION_POWER = make_ast_node(libsbml.AST_FUNCTION_POWER, children=(X, Y))
    FUNCTION_QUOTIENT = make_ast_node(libsbml.AST_FUNCTION_QUOTIENT, children=(X, Y))
    FUNCTION_REM = make_ast_node(libsbml.AST_FUNCTION_REM, children=(X, Y))
    FUNCTION_ROOT = make_ast_node(libsbml.AST_FUNCTION_ROOT, children=(X, Y))


class CONSTANT:

    E = make_ast_node(libsbml.AST_CONSTANT_E)
    FALSE = make_ast_node(libsbml.AST_CONSTANT_FALSE)
    PI = make_ast_node(libsbml.AST_CONSTANT_PI)
    TRUE = make_ast_node(libsbml.AST_CONSTANT_TRUE)


class CSYMBOL:

    TIME = make_ast_node(libsbml.AST_NAME_TIME)
    AVOGADRO = make_ast_node(libsbml.AST_NAME_AVOGADRO)


class CSYMBOL_FUNCTION:
    # https://www.w3.org/TR/MathML3/chapter4.html#contm.csymbol
    DELAY = make_ast_node(libsbml.AST_FUNCTION_DELAY, children=(X, Y))
    RATE_OF = make_ast_node(libsbml.AST_FUNCTION_RATE_OF, children=X)
    CUSTOM_CSYMBOL_FUNCTION = _make_custom_csymbol_function()


class DISTRIB_FUNCTION:
    assert tuple(map(int, libsbml.__version__.split('.'))) >= (5, 18, 1)  # must have libsbml >= 5.18.1
    BERNOULLI = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_BERNOULLI, children=X)
    BINOMIAL = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_BINOMIAL, children=(X, Y))
    CAUCHY = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_CAUCHY, children=(X, Y))
    CHISQUARE = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_CHISQUARE, children=X)
    EXPONENTIAL = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL, children=X)
    GAMMA = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_GAMMA, children=(X, Y))
    LAPLACE = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_LAPLACE, children=(X, Y))
    LOGNORMAL = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL, children=(X, Y))
    NORMAL = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_NORMAL, children=(X, Y))
    POISSON = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_POISSON, children=X)
    RAYLEIGH = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH, children=X)
    UNIFORM = make_ast_node(libsbml.AST_DISTRIB_FUNCTION_UNIFORM, children=(X, Y))


class LINEAR_ALGEBRA:
    DETERMINANT = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_DETERMINANT, children=(X, Y))
    MATRIX = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_MATRIX, children=(X, Y))
    MATRIXROW = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_MATRIXROW, children=(X, Y))
    OUTER_PRODUCT = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT, children=(X, Y))
    SCALAR_PRODUCT = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT, children=(X, Y))
    SELECTOR = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_SELECTOR, children=(X, Y))
    TRANSPOSE = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE, children=(X, Y))
    VECTOR = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_VECTOR, children=(X, Y))
    VECTOR_PRODUCT = make_ast_node(libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT, children=(X, Y))


class LOGICAL:
    AND = make_ast_node(libsbml.AST_LOGICAL_AND, children=(X, Y))
    IMPLIES = make_ast_node(libsbml.AST_LOGICAL_IMPLIES, children=(X, Y))
    NOT = make_ast_node(libsbml.AST_LOGICAL_NOT, children=X)
    OR = make_ast_node(libsbml.AST_LOGICAL_OR, children=(X, Y))
    XOR = make_ast_node(libsbml.AST_LOGICAL_XOR, children=(X, Y))
    EXISTS = make_ast_node(libsbml.AST_LOGICAL_EXISTS, children=(X, Y))
    FORALL = make_ast_node(libsbml.AST_LOGICAL_FORALL, children=(X, Y))


class RELATIONAL:

    EQ = make_ast_node(libsbml.AST_RELATIONAL_EQ, children=(X, Y))
    GEQ = make_ast_node(libsbml.AST_RELATIONAL_GEQ, children=(X, Y))
    GT = make_ast_node(libsbml.AST_RELATIONAL_GT, children=(X, Y))
    LEQ = make_ast_node(libsbml.AST_RELATIONAL_LEQ, children=(X, Y))
    LT = make_ast_node(libsbml.AST_RELATIONAL_LT, children=(X, Y))
    NEQ = make_ast_node(libsbml.AST_RELATIONAL_NEQ, children=(X, Y))


class NUMERICAL:

    INTEGER = make_ast_node(libsbml.AST_INTEGER, value=1)
    RATIONAL = make_ast_node(libsbml.AST_RATIONAL, value=(1, 3))
    REAL = make_ast_node(libsbml.AST_REAL, value=1.0)
    REAL_E = make_ast_node(libsbml.AST_REAL_E, value=(2.0, 3))
    # NAN = libsbml.parseL3Formula('notanumber')
    # INF = libsbml.parseL3Formula('infinity')


class SERIES:
    # cannot be represented as string
    PRODUCT = make_ast_node(libsbml.AST_SERIES_PRODUCT, children=(X, Y))
    SUM = make_ast_node(libsbml.AST_SERIES_SUM, children=(X, Y))


class STATISTICS:
    # cannot be represented as string
    MEAN = make_ast_node(libsbml.AST_STATISTICS_MEAN, children=(X, Y))
    MEDIAN = make_ast_node(libsbml.AST_STATISTICS_MEDIAN, children=(X, Y))
    MODE = make_ast_node(libsbml.AST_STATISTICS_MODE, children=(X, Y))
    MOMENT = make_ast_node(libsbml.AST_STATISTICS_MOMENT, children=(X, Y))
    SDEV = make_ast_node(libsbml.AST_STATISTICS_SDEV, children=(X, Y))
    VARIANCE = make_ast_node(libsbml.AST_STATISTICS_VARIANCE, children=(X, Y))


class TRIGONOMETRIC_FUNCTION:

    ARCCOS = make_ast_node(libsbml.AST_FUNCTION_ARCCOS, children=X)
    ARCCOSH = make_ast_node(libsbml.AST_FUNCTION_ARCCOSH, children=X)
    ARCCOT = make_ast_node(libsbml.AST_FUNCTION_ARCCOT, children=X)
    ARCCOTH = make_ast_node(libsbml.AST_FUNCTION_ARCCOTH, children=X)
    ARCCSC = make_ast_node(libsbml.AST_FUNCTION_ARCCSC, children=X)
    ARCCSCH = make_ast_node(libsbml.AST_FUNCTION_ARCCSCH, children=X)
    ARCSEC = make_ast_node(libsbml.AST_FUNCTION_ARCSEC, children=X)
    ARCSECH = make_ast_node(libsbml.AST_FUNCTION_ARCSECH, children=X)
    ARCSIN = make_ast_node(libsbml.AST_FUNCTION_ARCSIN, children=X)
    ARCSINH = make_ast_node(libsbml.AST_FUNCTION_ARCSINH, children=X)
    ARCTAN = make_ast_node(libsbml.AST_FUNCTION_ARCTAN, children=X)
    ARCTANH = make_ast_node(libsbml.AST_FUNCTION_ARCTANH, children=X)
    COS = make_ast_node(libsbml.AST_FUNCTION_COS, children=X)
    COSH = make_ast_node(libsbml.AST_FUNCTION_COSH, children=X)
    COT = make_ast_node(libsbml.AST_FUNCTION_COT, children=X)
    COTH = make_ast_node(libsbml.AST_FUNCTION_COTH, children=X)
    CSC = make_ast_node(libsbml.AST_FUNCTION_CSC, children=X)
    CSCH = make_ast_node(libsbml.AST_FUNCTION_CSCH, children=X)
    SEC = make_ast_node(libsbml.AST_FUNCTION_SEC, children=X)
    SECH = make_ast_node(libsbml.AST_FUNCTION_SECH, children=X)
    SIN = make_ast_node(libsbml.AST_FUNCTION_SIN, children=X)
    SINH = make_ast_node(libsbml.AST_FUNCTION_SINH, children=X)
    TAN = make_ast_node(libsbml.AST_FUNCTION_TAN, children=X)
    TANH = make_ast_node(libsbml.AST_FUNCTION_TANH, children=X)


class OTHER:
    """
    LAMBDA:
      - may only be used in function definitions
      - may not be nested (lambda containing lambda)
      - may not contain references to symbols other than those declared inside of it
        or identifiers declared by other function definitions,
      - all symbols they use must be passed through their parameters
      """
    NAME = make_ast_node(libsbml.AST_NAME, name='name')
    FUNCTION = make_ast_node(libsbml.AST_FUNCTION, name='function', children=(X, Y))
    LAMBDA = make_ast_node(libsbml.AST_LAMBDA, children=(X, Y, make_ast_node(libsbml.AST_TIMES, children=(X, Y))))
    FUNCTION_PIECEWISE = make_ast_node(libsbml.AST_FUNCTION_PIECEWISE, children=(X, Y, Z))  # x if y else z
    UNKNOWN = make_ast_node(libsbml.AST_UNKNOWN)
    # AST_END_OF_CORE  # cannot be set


ALL_SIMPLE_NODES = {v.getType(): v for ast_group in
                    (ARITHMETIC, CONSTANT, CSYMBOL, CSYMBOL_FUNCTION, DISTRIB_FUNCTION, LINEAR_ALGEBRA, LOGICAL,
                     NUMERICAL, RELATIONAL, SERIES, STATISTICS, TRIGONOMETRIC_FUNCTION, OTHER)
                    for k, v in vars(ast_group).items() if not k.startswith('__')}

