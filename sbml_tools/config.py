from sbml_tools.utils import FrozenClass
from dataclasses import dataclass
import libsbml

# file that contains global variables used in wrappers

# default sbml level and version used in wrapper.py, when it cannot be determined through associated document or model
DEFAULT_SBML_LEVEL = 3
DEFAULT_SBML_VERSION = 2

# throw errors if units are not given or cannot be inferred, or when they are not of the correct type in certain cases
ENFORCE_UNITS: bool = True

# controls whether unit definitions are automatically added to the model, when passing unit definition identifier
# strings to setters of Model, Compartment, Species, Parameters and LocalParameters objects
AUTOMATED_UNIT_DEFINITION_CREATION: bool = True

# when annotations are generated, these need to be associated to the sbml object by means of a meta id
AUTOMATED_META_ID_GENERATION: bool = True

# otherwise default is <Date 2000-01-01T00:00:00Z>
AUTOMATED_DATE_INIT_TO_CURRENT: bool = True

# local parameters may otherwise be initialized without setting a value,
# however no other sbml constructs (e.g. initial assignment) can set them later as they live in a local namespace
# ENFORCE_LOCAL_PARAMETER_INIT_VALUES = True

# species must specify a compartment, hence force the user to create the compartment first
# and ensure the referenced compartment exists
ENFORCE_COMPARTMENT_BEFORE_SPECIES: bool = True

# if a reaction is associated with a model, ensure that species with the corresponding identifiers have been created
# before reactions involving them can be created.
ENFORCE_SPECIES_BEFORE_SPECIES_REFERENCES: bool = True

# species references id attribute is optional, but required for referencing (e.g. rule, initial assignment)
# setting it automatically as species name suffixed with '_reference'
AUTOMATED_SPECIES_REFERENCE_ID_GENERATION = True

# enforce that compartment / species / species reference / parameter exist, before referencing
# ENFORCE_EXISTENCE_INITIAL_ASSIGNMENT_SYMBOL: bool = True
ENFORCE_REFERENCE_EXISTS: bool = True
#

# using in __eq__ checking of wrapper when attributes are numerical
IS_CLOSE_RELATIVE_TOLERANCE: float = 1e-09
IS_CLOSE_ABSOLUTE_TOLERANCE: float = 0.0

# one can change existing attributes, but not set new ones, preventing erroneously doing so instead of using a setter
FREEZE_CLASSES_AFTER_INIT: bool = True

# enforce that assignment of SBO terms are valid; they must exist and be of the correct branch; see sbo_term.py
VALIDATE_SBO_ASSIGNMENT: bool = True

# currently only used in parameter factory
USE_DEFAULT_UNITS = False

# TODO: math options
L3PS = libsbml.L3ParserSettings()
# l3ps.setModel
# l3ps.setComparisonCaseSensitivity(libsbml.L3P_COMPARE_BUILTINS_CASE_INSENSITIVE) # L3P_COMPARE_BUILTINS_CASE_SENSITIVE
# l3ps.setParseAvogadroCsymbol(libsbml.L3P_AVOGADRO_IS_CSYMBOL) # L3P_AVOGADRO_IS_NAME
# l3ps.setParseCollapseMinus(libsbml.L3P_EXPAND_UNARY_MINUS) # L3P_COLLAPSE_UNARY_MINUS
# l3ps.setParseL3v2Functions(libsbml.L3P_PARSE_L3V2_FUNCTIONS_AS_GENERIC) # L3P_PARSE_L3V2_FUNCTIONS_DIRECTLY
# l3ps.setParseLog(libsbml.L3P_PARSE_LOG_AS_ERROR) # L3P_PARSE_LOG_AS_LOG10 / L3P_PARSE_LOG_AS_LN
# l3ps.setParseModuloL3v2(libsbml.L3P_MODULO_IS_PIECEWISE) # libsbml.L3P_MODULO_IS_REM, libsbml.L3P_MODULO_IS_PIECEWISE
# l3ps.setParsePackageMath() # libsbml.L3P_PARSE_PACKAGE_MATH_AS_GENERIC / libsbml.L3P_PARSE_PACKAGE_MATH_DIRECTLY
# l3ps.setParseUnits


# default settings to be used during construction.
# All are set to None, but if desired can be set such that these arguments don't need to be passed explicitly anymore
class Singleton(type):

    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


@dataclass
class Model:
    substance_units: str = None
    time_units: str = None
    area_units: str = None
    length_units: str = None
    extent_units: str = None
    conversion_factor: str = None


@dataclass
class Compartment:
    spatial_dimensions: int = None
    size: float = None
    units: str = None
    constant: bool = None


@dataclass
class Species:
    compartment: str = None
    initial_amount: float = None
    initial_concentration: float = None
    substance_units: str = None
    has_only_substance_units: bool = None
    boundary_condition: bool = None
    constant: bool = None
    conversion_factor: str = None


@dataclass
class Parameter:
    value: float = None
    units: str = None
    constant: bool = None


@dataclass
class SpeciesReference:
    stoichiometry: float = None
    constant: bool = None


@dataclass
class Reaction:
    reversible: bool = None
    compartment: str = None


@dataclass
class Trigger:
    initial_value: bool = None
    persistent: bool = None


@dataclass
class Event:
    use_values_from_trigger_time: bool = None


class Defaults(FrozenClass, metaclass=Singleton):
    """
    if these are set, sbml_tools.wrapper.Wrapper will use the defaults specified here if the respective
    keyword arguments are not passed explicitly.
    """

    def __init__(self):
        self.model = Model()
        self.compartment = Compartment()
        self.species = Species()
        self.parameter = Parameter()
        self.species_reference = SpeciesReference()
        self.reaction = Reaction()
        self.trigger = Trigger()
        self.event = Event()
        self._freeze()

# global defaults
# defaults = Defaults()
# # print(id(defaults))
# # defaults.compartment.constant = True
#
# def test():
#
#     from sbml_tools import wrappers
#     defaults.compartment.constant = True
#     c = wrappers.Compartment(id='a')
