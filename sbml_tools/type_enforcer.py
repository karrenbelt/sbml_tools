
import pytest
import typing
import inspect
from contextlib import suppress
from functools import wraps
from dataclasses import dataclass, field

# In development: a decorator that performs type checking at runtime


def strict_types(function):
    """
    a decorator that enforces the variables passed to a function to be of the type specified in the type hints.
    only works from single types at the moment (e.g. int, float, str, list, set, dict), or a union of these.
    Very un-pythonic.
    """
    def type_checker(*args, **kwargs):
        hints = typing.get_type_hints(function)
        all_args = kwargs.copy()
        all_args.update(dict(zip(function.__code__.co_varnames, args)))

        for argument, argument_type in ((i, type(j)) for i, j in all_args.items()):
            if argument in hints:
                if getattr(hints[argument], '__module__', None) == typing.__name__:
                    if hints[argument].__origin__ is typing.Union:
                        hints[argument] = hints[argument].__args__
                    elif hints[argument].__origin__ is typing.Generic:
                        pass  # e.g. Dict
                    else:
                        raise NotImplementedError('lots of work to unpack all the typing contents')
                print(argument, argument_type, hints[argument])
                if not issubclass(argument_type, hints[argument]):
                    raise TypeError('Type of {} is {} and not {}'.format(argument, argument_type, hints[argument]))

        result = function(*args, **kwargs)

        if 'return' in hints:
            if type(result) != hints['return']:
                raise TypeError('Type of result is {} and not {}'.format(type(result), hints['return']))

        return result

    return type_checker


# @strict_types
# def f(a: typing.Union[int, float]):
#     return a
# f(1)
# f(1.0)


def enforce_types(wrapped):
    """
    enforces type on __init__
    """
    spec = inspect.getfullargspec(wrapped)

    def check_types(*args, **kwargs):
        params = dict(zip(spec.args, args))
        params.update(kwargs)
        print(params)
        for name, value in params.items():
            with suppress(KeyError):
                type_hint = spec.annotations[name]
                if isinstance(type_hint, typing._SpecialForm):
                    continue
                actual_type = getattr(type_hint, "__origin__", type_hint)
                actual_type = type_hint.__args__ if isinstance(actual_type, typing._SpecialForm) else actual_type
                if not isinstance(value, actual_type):
                    raise TypeError(
                        f"Expected type '{type_hint}' for attribute '{name}' but received type '{type(value)}')"
                    )

    def decorate(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            check_types(*args, **kwargs)
            return func(*args, **kwargs)

        return wrapper

    if inspect.isclass(wrapped):
        wrapped.__init__ = decorate(wrapped.__init__)
        # wrapped.__setattr__ = decorate(wrapped.__setattr__)
        return wrapped

    return decorate(wrapped)


@enforce_types
@dataclass
class Strict:

    attribute: int = None

    def __setattr__(self, key, value):
        if key not in self.__dataclass_fields__:
            raise ValueError(f'adding new fields not allowed')
        annotated_type = self.__annotations__[key]
        default_type = type(self.__dataclass_fields__[key].default)
        annotation = (annotated_type, default_type)
        # print(value, annotation)
        if not isinstance(value, annotation):
            raise TypeError(f'expecting {annotation} got {type(value)}')
        self.__dict__[key] = value


s = Strict()
s.attribute = 1

s = Strict(attribute=1)

with pytest.raises(Exception):
    s = Strict(attribute='1')

with pytest.raises(TypeError):
    s.attribute = '2'  # error as should

s.attribute = 3

s.attribute = None

