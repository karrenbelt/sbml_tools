
from sbml_tools.wrappers import SBMLDocument
from sbml_tools.toolbox.kinetics.sbo_utils import NAME_TO_SBO_TERM, SBO


def setup_test_model():
    doc = SBMLDocument()
    doc.create_model(id='test')
    # create compartment
    doc.model.create_compartment(id='c', name='cytosol', constant=True, spatial_dimensions=3, units='litre')
    # create species
    species_identifiers = ['A', 'B', 'C', 'D', 'E']
    for identifier in species_identifiers:  # ensure that units and has_only_substance_units match
        doc.model.create_species(id=identifier, compartment='c', has_only_substance_units=True,
                                 boundary_condition=False, constant=False, initial_amount=100,
                                 substance_units='millimole')
    # create parameters
    # doc.model.create_parameter(id='kcatf', constant=False, value=1.0)
    # create reactions
    doc.model.create_reaction(id='rxn', reversible=True, reactants='A + B', products='C + D', modifiers='E')
    reaction = doc.model.get('rxn')
    # annotate SBO terms
    for s_ref in reaction.reactants:
        s_ref.sbo_term = NAME_TO_SBO_TERM['reactant']
    for s_ref in reaction.products:
        s_ref.sbo_term = NAME_TO_SBO_TERM['product']
    for m_ref in reaction.modifiers:
        m_ref.sbo_term = NAME_TO_SBO_TERM['enzymatic catalyst']

    return doc