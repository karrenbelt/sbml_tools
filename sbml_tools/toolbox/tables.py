import libsbml
import pandas as pd
from typing import Union
from sbml_tools.toolbox import read_sbml
# from sbml_tools.wrappers import SBMLDocument


# TODO:
#  1. decide whether to parse annotations, constraint messages and notes, if so translate
#  2. model attributes
#  3. convert entire model to "Document" of table
#  4. back conversion of tables to sbml model (setters)


# class Tables:
#
#     def __init__(self, sbml):
#         self.doc = SBMLDocument(read_sbml(sbml))
#
#     @property
#     def function_definitions(self):
#         self.doc.model.function_definitions


class Tables:
    """
    Convert the sbml model to DataFrames
    This resembles SBtab somewhat.
    Note that this is not complete:
    - packages are not implemented
    - moreover, every SBase derived item can have base_attributes, including list_of, species reference, etc
      I am only returning sbase attribute of primary elements in the list (e.g. not species reference, etc.)
    """

    base_attrs = ['id', 'name', 'meta_id', 'sbo_term', 'notes', 'annotation']

    model_attrs = ['substance_units', 'time_units', 'volume_units', 'area_units',
                   'length_units', 'extent_units', 'conversion_factor']

    lists = ['function_definitions', 'unit_definitions', 'compartments', 'species', 'parameters',
             'initial_assignments', 'rules', 'constraints', 'reactions', 'events']

    def __init__(self, sbml):
        self.doc = read_sbml(sbml)
        self.read_math = libsbml.formulaToL3String
        if self.doc.getNumPlugins():
            packages = [self.doc.getPlugin(i).getPackageName() for i in range(self.doc.getNumPlugins())]
            print(f'model contains packages, which have not been implemented: {packages}')

    @property
    def model(self) -> pd.Series:  # TODO: convert the entire document at once
        fields = ('time_units', 'extent_units', 'substance_units', 'volume_units', 'area_units', 'length_units')
        return pd.DataFrame([{attr: getattr(self.doc.model, attr) for attr in fields}])

    def _translate(self, df):
        if 'sbo_term' in df:  # -1 is the default when nothing is set
            df['sbo_term'].replace(-1, None, inplace=True)
        if 'notes' in df:  # xhtml notes
            df['notes'] = df['notes'].apply(lambda x: libsbml.XMLNode_convertXMLNodeToString(x) if x else None)
        if 'annotation' in df:  # annotations can be nested (tree)
            # print('annotation translation not implemented')
            df['annotation'] = df['annotation'].apply(lambda x: x is not None)
        if 'message' in df:  # xhtml message (constraints)
            # print('message translation not tested')
            df['message'].apply(lambda x: libsbml.XMLNode.convertXMLNodeToString(x))
        for math in ('math', 'trigger', 'priority', 'delay'):
            if math in df:
                df[math] = df[math].apply(lambda x: self.read_math(x) if x else None)
        # if 'event_assignments' in df:
        #     df['event_assignments'] = [{k: self.read_math(v) if v else None}
        #                                for b in df['event_assignments'] for x in b for k, v in x.items()]
        return df

    @property
    def function_definitions(self) -> pd.DataFrame:
        attrs = self.base_attrs + ['math']
        df = pd.DataFrame([{x: getattr(c, x) for x in attrs} for c in self.doc.model.function_definitions])
        return self._translate(df)

    @property
    def unit_definitions(self) -> pd.DataFrame:
        # unit_definition_attrs = self.base_attrs + ['units']
        unit_attrs = ['kind', 'exponent', 'scale', 'multiplier']  # + self.base_attrs
        d = {}
        for u_def in self.doc.model.unit_definitions:
            # TODO: parse base attributes here at least?
            for i, unit in enumerate(u_def.units):
                d[(u_def.id, i)] = {x: getattr(unit, x) for x in unit_attrs}
        return pd.DataFrame(d)

    @property
    def compartments(self) -> pd.DataFrame:
        attrs = self.base_attrs + ['spatial_dimensions', 'size', 'units', 'constant']
        df = pd.DataFrame([{x: getattr(c, x) for x in attrs} for c in self.doc.model.compartments])
        return self._translate(df)

    @property
    def species(self) -> pd.DataFrame:
        attrs = self.base_attrs + ['compartment', 'initial_amount', 'initial_concentration', 'substance_units',
                                   'has_only_substance_units', 'boundary_condition', 'constant', 'conversion_factor']
        df = pd.DataFrame([{x: getattr(s, x) for x in attrs} for s in self.doc.model.species])
        return self._translate(df)

    @property
    def parameters(self) -> pd.DataFrame:
        attrs = self.base_attrs + ['value', 'units', 'constant']
        df = pd.DataFrame([{x: getattr(p, x) for x in attrs} for p in self.doc.model.parameters])
        return self._translate(df)

    @property
    def initial_assignments(self) -> pd.DataFrame:
        attrs = self.base_attrs + ['symbol', 'math']
        df = pd.DataFrame([{x: getattr(i, x) for x in attrs} for i in self.doc.model.initial_assignments])
        return self._translate(df)

    @property
    def rules(self) -> pd.DataFrame:
        # 3 subclasses, algebraic rules don't have variables associated
        attrs = ['variable', 'math']
        df = pd.DataFrame([{x: getattr(r, x, None) for x in attrs} for r in self.doc.model.rules])
        df['class'] = ['rule' if r.isAssignment() else 'rate' if r.isRate() else 'algebraic'
                       for r in self.doc.model.rules]
        return self._translate(df)

    @property
    def constraints(self) -> pd.DataFrame:
        attrs = ['math', 'message']
        df = pd.DataFrame([{x: getattr(c, x) for x in attrs} for c in self.doc.model.constraints])
        return self._translate(df)

    @property
    def reactions(self):
        l = []
        for r in self.doc.model.reactions:
            d = dict()
            d['reversible'] = r.reversible
            d['compartment'] = r.compartment
            d['reactants'] = ' + '.join([f'{s_ref.stoichiometry} {s_ref.species}' for s_ref in r.reactants])
            d['products'] = ' + '.join([f'{s_ref.stoichiometry} {s_ref.species}' for s_ref in r.products])
            d['modifiers'] = [m_ref.species for m_ref in r.modifiers]
            d['constant'] = [s_ref.species for s_ref in list(r.products) + list(r.reactants) if s_ref.constant]
            d['math'] = r.getKineticLaw().math if r.isSetKineticLaw() else None
            d['local_parameters'] = ([{x: getattr(p, x) for x in ['id', 'value', 'units']}
                                      for p in r.getKineticLaw().local_parameters] if r.isSetKineticLaw() else None)
            l.append(d)
        return self._translate(pd.DataFrame(l))

    @property
    def events(self):
        l = []
        for e in self.doc.model.events:
            d = dict()
            d['use_values_from_trigger_time'] = e.use_values_from_trigger_time
            d['initial_value'] = e.getTrigger().initial_value if e.isSetTrigger() else None
            d['persistent'] = e.getTrigger().persistent if e.isSetTrigger() else None
            d['trigger'] = e.getTrigger().math if e.isSetTrigger() else None
            d['priority'] = e.getPriority().math if e.isSetPriority() else None
            d['delay'] = e.getDelay().math if e.isSetDelay() else None
            d['event_assignments'] = '; '.join([f'{a.variable} = {self.read_math(a.math)}' for a in e.event_assignments])
            l.append(d)
        return self._translate(pd.DataFrame(l))

    def convert(self):
        return dict(
            doc=pd.DataFrame([dict(level=self.doc.level, version=self.doc.version)]),
            model=self.model,
            function_definitions=self.function_definitions,
            unit_definition=self.unit_definitions,
            compartments=self.compartments,
            species=self.species,
            parameters=self.parameters,
            initial_assignments=self.initial_assignments,
            rules=self.rules,
            constraints=self.constraints,
            reactions=self.reactions,
            events=self.events,
            )

    def print(self):
        from tabulate import tabulate
        for k, v in self.convert().items():
            tab = tabulate(v, headers='keys', tablefmt='psql')
            print(k.replace('_', ' ').capitalize(), '\n', tab)


def test():

    from sbml_tools import settings
    tables = Tables(settings.CHRISTODOULOU_MODEL)


    tables.model
    tables.function_definitions
    tables.unit_definitions
    tables.compartments
    tables.species
    tables.parameters
    tables.initial_assignments
    tables.rules
    tables.constraints
    tables.reactions
    tables.events

    frames = tables.convert()
    tables.print()

