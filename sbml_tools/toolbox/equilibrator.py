import os

import numpy as np
import pandas as pd
from tqdm import tqdm
import libsbml
import matplotlib.pyplot as plt
from typing import Union
import collections
from equilibrator_api import ComponentContribution, parse_reaction_formula, Q_
from cached_property import cached_property
from sbml_tools.sbml_io import read_sbml
from sbml_tools import utils, settings
from sbml_tools.wrappers.annotations import MIRIAM, URL_TO_MIRIAM_IDX
from sbml_tools.toolbox import IdentifierAnnotator

logger = utils.setup_logger(settings.LOG_DIR, __name__)


DEFAULT_PH  = Q_(7.4,  'dimensionless')
DEFAULT_PMG = Q_(10,   'dimensionless')
DEFAULT_T   = Q_(37.0, 'degC')
DEFAULT_I   = Q_(0.25, 'M')


class ModelEquilibrator:

    def __init__(self, sbml: Union[str, libsbml.SBMLDocument],
                 ph=DEFAULT_PH, p_mg=DEFAULT_PMG, temperature=DEFAULT_T, ionic_strength=DEFAULT_I):
        self.doc = read_sbml(sbml)
        self.ph = ph
        self.p_mg = p_mg
        self.temperature = temperature.to('degK')
        self.ionic_strength = ionic_strength

    @cached_property
    def component_contribution(self):
        return ComponentContribution(self.p_mg, self.ph, self.ionic_strength, self.temperature)

    @property
    def annotated_species_identifiers(self):
        return IdentifierAnnotator(self.doc).annotated_species_identifiers

    # cache
    def equilibrate_model(self):
        # bigg.metabolite, kegg.compound, chebi, or metanetx.chemical
        annotations = self.annotated_species_identifiers
        d = collections.defaultdict(dict)

        # TODO: there might be multiple identifiers of the same database, now overwriting; should store all
        for species in self.doc.model.species:
            for annotation in annotations.get(species.id, []):
                identifier = annotation.split('/')[-1]
                if 'bigg.metabolite' in annotation:
                    d[species.id]['bigg'] = f"bigg.metabolite:{identifier}"
                elif 'kegg.compound' in annotations:
                    d[species.id]['kegg'] = f"kegg:{identifier}"
                elif 'chebi' in annotation:
                    d[species.id]['chebi'] = identifier
                elif 'metanetx.chemical' in annotation:
                    d[species.id]['metanetx'] = f"metanetx.chemical:{identifier}"

        def get_anno(s_ref):
            for k in ('bigg', 'kegg', 'chebi', 'metanetx'):
                if k in d[s_ref.species]:
                    return d[s_ref.species][k]
            raise ValueError(f'no annotation found for {s_ref.species}')

        phased_reactions = dict()
        for reaction in tqdm(self.doc.model.reactions, desc='equilibrating'):
            reactants = [f'{s_ref.stoichiometry} {get_anno(s_ref)}' for s_ref in reaction.reactants]
            products = [f'{s_ref.stoichiometry} {get_anno(s_ref)}' for s_ref in reaction.products]
            rxn_str = ' + '.join(reactants) + ' = ' + ' + '.join(products)
            phased_reaction = parse_reaction_formula(rxn_str)
            if not phased_reaction.is_balanced():
                logger.warn(f'{reaction} is not balanced:\n{rxn_str}')
                continue
            phased_reactions[reaction.id] = phased_reaction

        try:
            dG0_prime, U = self.component_contribution.standard_dg_prime_multi(list(phased_reactions.values()))
        except Exception as e:
            raise ValueError(f'failed to equilibrate the model:\n{e}')

        covariance = pd.DataFrame(U.magnitude, columns=phased_reactions, index=phased_reactions)
        dG0_prime = pd.Series(dG0_prime.magnitude.flatten(), index=phased_reactions)
        return dG0_prime, covariance

    @staticmethod
    def plot(dG0_prime, covariance, show=True, filepath=None):
        """plotting marginals for visual inspection"""
        marginals = pd.DataFrame(zip(dG0_prime.values, np.sqrt(covariance.values.diagonal()), covariance),
                                 columns=['mean', 'std', 'reaction'])
        plt.figure(figsize=(12, 5))
        ax = marginals.reset_index().plot(x='index', y='mean', kind='scatter', yerr=marginals['std']*3)
        ax.set_title('Thermodynamics', fontsize=20)
        ax.set_ylabel("ΔG0'", fontsize=16)
        ax.set_xlabel('Reaction', fontsize=16)
        #ax.tick_params(axis='x', which='major', labelsize=6)
        ax.set_xticks(range(marginals.shape[0]))
        ax.set_xticklabels(marginals['reaction'], rotation=90)

        # annotating some 'extreme' values
        stats = marginals['mean'].describe(percentiles=[.03, .5, .97])
        outliers = marginals[(marginals['mean'] > stats['97%']) | (marginals['mean'] < stats['3%'])]
        xy = outliers['mean']
        for k, p in xy.items():
            ax.errorbar(x=xy.index, y=xy.values, yerr=marginals.loc[xy.index, 'std'] * 3, fmt='o', color='red')
            ax.annotate(outliers.loc[k]['reaction'], (k, p), xytext=(10, -5), textcoords='offset points',
                        family='sans-serif', fontsize=10, color='darkslategrey')

        plt.tight_layout()
        if show:
            plt.show()
        if filepath:
            fig = plt.gcf()
            fig.savefig(filepath)
            logger.info(f'saved equilibrator parameter data figure at:\n{filepath}')


def test():
    sbml = settings.ECOLI_CORE_MODEL
    eq = ModelEquilibrator(sbml)
    dG0_prime, covariance = eq.equilibrate_model()
    filepath = os.path.join(settings.FIGURE_DIR, f'e_coli_core_dG0_prime.pdf')
    eq.plot(dG0_prime, covariance, filepath=filepath)





