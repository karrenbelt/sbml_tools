# Symbolic expression manipulation using sympy

[libsbml-math](http://sbml.org/Software/libSBML/docs/formatted/python-api/libsbml-math.html)

## ASTNode
Extends the behaviour of the ASTNode from the wrappers folder to facilitate construction of 
mathematical equations. Used in the kinetics section of the toolbox. 
Probably needs to be renamed or integrated with the wrapper.

## SymbolicASTNode
There are several important things to consider when trying to convert to sympy and back.
As a preface, it's important to know that 
- mathematics is represented by Abstract Syntax Trees. 
Therefore there exists a difference between mathematical equivalence and structural similarity. 
- libsbml (L3V2) uses MathML 2.0

1. sympy does not remember the order of arguments. It orders them by alphabet, as per "ord"
 (e.g. max(x, y, 1) --> max(1, x, y)
2. sympy likes to evaluate (e.g. "1 + 2" will become "3"). For testing purposes, sympy
takes the keyword argument "evaluate=<bool>". This doesn't always work as desired.
3. There exist ASTNodes in libsbml that have no direct counter part in sympy. To illustrate, 
"a - b" in libsbml is a subtraction, in sympy this is transformed into an addition: "a + -b".
Although these are mathematically equivalent, they do not yield the same syntax tree and are
thus structurally distinct. Thus, conversion to and from sympy will yield an equivalent but 
different expression. I tried to address this by introducing separate classes for such cases.
This allows for a one-to-one mapping between sympy and ASTNodes, when passing "evaluate=False".
4. sympy's MathML printer is not complete, nor flawless. I've raised issues on their github, 
as I've tried monkey patching their printer, but decided it's better to walk the tree again.
5. there is no MathML parser in sympy either, only a printer 

Other tools tried but not in use
- http://mathdom.sourceforge.net/. Python2.4 
- https://github.com/leerobert/mml2sympy. I failed the first two conversions I tried.

## node_mapping.py
A one-to-one mapping between libsbml ASTNodes and sympy

## Matrices
Old stuff that needs to be revised