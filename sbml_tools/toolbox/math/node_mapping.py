import operator
import libsbml
import sympy
import sympy.stats
import numpy as np
from bidict import bidict
from sympy.core.singleton import Singleton
from sympy.core.compatibility import with_metaclass
import itertools
from decimal import Decimal
from sbml_tools.wrappers.ast_node import ALL_AST_TYPES, AST_TAGS
# This file contains mappings of libsbml ASTNode types to sympy nodes
# TODO: investigate sympy.UnevaluatedExpr


class SymbolicMappingError(Exception):
    pass


# we define a set of classes needed to generate a one-to-one mapping between libsbml and sympy objects
class Time(with_metaclass(Singleton, sympy.Basic)):
    pass


class Avogadro(with_metaclass(Singleton, sympy.Basic)):
    pass


class FloatE(sympy.Float):

    @property
    def exponent(self):
        (sign, digits, exponent) = Decimal(float(self)).as_tuple()
        return len(digits) + exponent - 1

    @property
    def mantissa(self):
        return float(Decimal(float(self)).scaleb(-self.exponent).normalize())


class Delay(sympy.Function):
    nargs = 2  # forces one to provide two arguments to init


class RateOf(sympy.Function):
    nargs = 1  # forces one to provide one argument to init


class Minus(sympy.Add):
    # Minus(4, 2, 1, evaluate=False)
    def __new__(cls, *args, **kwargs):
        if not 1 <= len(args) <= 2:  # as per libsbml
            raise SymbolicMappingError(f'Minus takes 1 or 2 arguments')
        if len(args) == 1:  # unary minus case
            return super().__new__(cls, operator.neg(args[0]), **kwargs)
        return super().__new__(cls, args[0], *map(operator.neg, args[1:]), **kwargs)

    @property
    def inverse_args(self):  # TODO: figure out if this must be done recursively
        return (self.args[0],) + tuple(map(operator.neg, self.args[1:]))


class Divide(sympy.Mul):
    def __new__(cls, *args, **kwargs):
        # if len(args) != 2:  # as per libsbml
        #     raise SymbolicMappingError(f'{cls} takes 2 arguments: {args}')
        return super().__new__(cls, args[0], *map(lambda arg: sympy.Pow(arg, -1), args[1:]), **kwargs)

    @property
    def inverse_args(self):  # TODO: figure out if this must be done recursively
        return (self.args[0], ) + tuple(map(lambda arg: sympy.Pow(arg, -1), self.args[1:]))


class Quotient(Divide):
    # Quotient(5, 3, evaluate=False)
    def __repr__(self):
        return f"{self.__class__.__name__}{self.inverse_args}"


class Piecewise(sympy.Piecewise):
    # Piecewise(x, evaluate=False)
    def __new__(cls, *args, **kwargs):
        # unpacking tuples, if those are given (like in sympy.Piecewise)
        if all(isinstance(x, tuple) for x in args):
            args = list(itertools.chain.from_iterable(args))
        if len(args) % 2:  # if uneven, last one is the otherwise condition: always true otherwise
            args += (True, )
        return super().__new__(cls, *zip(args[::2], args[1::2]), **kwargs)


class Power(sympy.Pow):
    # Power(5, 3, 2, evaluate=False)
    def __new__(cls, *args, **kwargs):
        if len(args) != 2:
            raise SymbolicMappingError(f'Power takes 2 arguments')
        return super().__new__(cls, args[0], *map(lambda arg: sympy.Pow(arg, -1), args[1:]), **kwargs)


class Ln(sympy.ln):
    def __new__(cls, *args, **kwargs):
        if len(args) != 1:
            raise SymbolicMappingError(f'Ln takes 1 argument')
        return super().__new__(cls, *args, **kwargs)


class Log(sympy.log):
    def __new__(cls, *args, **kwargs):
        if len(args) != 2:
            raise SymbolicMappingError(f'Log takes 2 arguments')
        return super().__new__(cls, *args, **kwargs)


class Root(sympy.Pow):
    # Root(256, 4, evaluate=False)
    def __new__(cls, *args, **kwargs):
        if not 1 <= len(args) <= 2:
            raise SymbolicMappingError(f'{cls} takes 1 or 2 arguments')
        if len(args) == 1:
            args = (*args, 2)
        return super().__new__(cls, args[0], *map(lambda arg: sympy.Pow(arg, -1), args[1:]), **kwargs)

    @property
    def inverse_args(self):  # always has 2 args
        return (self.args[0], ) + tuple(map(lambda arg: sympy.Pow(arg, -1), self.args[1:]))

    @property
    def name(self):
        if len(self.args[1]) == 2:
            return 'sqrt'
        return 'root'

    def __repr__(self):
        return f"{self.__class__.__name__}{self.inverse_args}"


DISTRIBUTIONS = bidict({  # TODO: implement
    libsbml.AST_DISTRIB_FUNCTION_BERNOULLI: sympy.stats.Bernoulli,  # or sympy.bernoulli ?
    libsbml.AST_DISTRIB_FUNCTION_BINOMIAL: sympy.stats.Binomial,
    libsbml.AST_DISTRIB_FUNCTION_CAUCHY: sympy.stats.Cauchy,
    libsbml.AST_DISTRIB_FUNCTION_CHISQUARE: sympy.stats.ChiSquared,
    libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL: sympy.stats.Exponential,
    libsbml.AST_DISTRIB_FUNCTION_GAMMA: sympy.stats.Gamma,
    libsbml.AST_DISTRIB_FUNCTION_LAPLACE: sympy.stats.Laplace,
    libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL: sympy.stats.LogNormal,
    libsbml.AST_DISTRIB_FUNCTION_NORMAL: sympy.stats.Normal,
    libsbml.AST_DISTRIB_FUNCTION_POISSON: sympy.stats.Poisson,
    libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH: sympy.stats.Rayleigh,
    libsbml.AST_DISTRIB_FUNCTION_UNIFORM: sympy.stats.Uniform,
    })

CONSTANTS = bidict({
    libsbml.AST_CONSTANT_E: sympy.E,
    libsbml.AST_CONSTANT_FALSE: sympy.false,
    libsbml.AST_CONSTANT_PI: sympy.pi,
    libsbml.AST_CONSTANT_TRUE: sympy.true,
    })

NUMERICAL = bidict({
    libsbml.AST_INTEGER: sympy.Integer,
    libsbml.AST_RATIONAL: sympy.Rational,
    libsbml.AST_REAL: sympy.Float,
    libsbml.AST_REAL_E: FloatE,
    })

FUNCTIONS = bidict({
    libsbml.AST_FUNCTION: sympy.Function,
    libsbml.AST_LAMBDA: sympy.Lambda,
    libsbml.AST_FUNCTION_PIECEWISE: Piecewise,
    })

CSYMBOLS = bidict({
    libsbml.AST_NAME_TIME: sympy.S.Time,
    libsbml.AST_NAME_AVOGADRO: sympy.S.Avogadro,
    })

CFUNCTIONS = bidict({
    libsbml.AST_FUNCTION_DELAY: Delay,
    libsbml.AST_FUNCTION_RATE_OF: RateOf,
    # libsbml.AST_CSYMBOL_FUNCTION: None,
    })

TRIGONOMETRIC_OPERATORS = bidict({
    libsbml.AST_FUNCTION_ARCCSCH: sympy.acsch,
    libsbml.AST_FUNCTION_ARCSECH: sympy.asech,
    libsbml.AST_FUNCTION_ARCCOS: sympy.acos,
    libsbml.AST_FUNCTION_ARCCOSH: sympy.acosh,
    libsbml.AST_FUNCTION_ARCCOT: sympy.acot,
    libsbml.AST_FUNCTION_ARCCOTH: sympy.acoth,
    libsbml.AST_FUNCTION_ARCCSC: sympy.acsc,
    libsbml.AST_FUNCTION_ARCSEC: sympy.asec,
    libsbml.AST_FUNCTION_ARCSIN: sympy.asin,
    libsbml.AST_FUNCTION_ARCSINH: sympy.asinh,
    libsbml.AST_FUNCTION_ARCTAN: sympy.atan,
    libsbml.AST_FUNCTION_ARCTANH: sympy.atanh,
    libsbml.AST_FUNCTION_COS: sympy.cos,
    libsbml.AST_FUNCTION_COSH: sympy.cosh,
    libsbml.AST_FUNCTION_COT: sympy.cot,
    libsbml.AST_FUNCTION_COTH: sympy.coth,
    libsbml.AST_FUNCTION_CSC: sympy.csc,
    libsbml.AST_FUNCTION_CSCH: sympy.csch,
    libsbml.AST_FUNCTION_SEC: sympy.sec,
    libsbml.AST_FUNCTION_SECH: sympy.sech,
    libsbml.AST_FUNCTION_SIN: sympy.sin,
    libsbml.AST_FUNCTION_SINH: sympy.sinh,
    libsbml.AST_FUNCTION_TAN: sympy.tan,
    libsbml.AST_FUNCTION_TANH: sympy.tanh,
    })


ARITHMETIC_OPERATORS = bidict({
    libsbml.AST_PLUS: sympy.Add,
    libsbml.AST_MINUS: Minus,  # sympy.Add with all secondary arguments negative
    libsbml.AST_POWER: sympy.Pow,  # operator.pow,
    libsbml.AST_FUNCTION_POWER: Power,  # operator.pow,
    libsbml.AST_DIVIDE: Divide,  # sympy.Mul with all secondary arguments inverted
    libsbml.AST_TIMES: sympy.Mul,  # operator.mul,

    libsbml.AST_FUNCTION_CEILING: sympy.ceiling,
    libsbml.AST_FUNCTION_ABS: sympy.Abs,
    libsbml.AST_FUNCTION_ROOT: Root,
    libsbml.AST_FUNCTION_FACTORIAL: sympy.factorial,
    libsbml.AST_FUNCTION_FLOOR: sympy.floor,
    libsbml.AST_FUNCTION_EXP: sympy.exp,
    libsbml.AST_FUNCTION_LN: Ln,
    libsbml.AST_FUNCTION_LOG: Log,  # exceptional case, need to get the base (also sympy.log is the same as sympy.ln)
    libsbml.AST_FUNCTION_MAX: sympy.Max,
    libsbml.AST_FUNCTION_MIN: sympy.Min,
    libsbml.AST_FUNCTION_QUOTIENT: Quotient,
    libsbml.AST_FUNCTION_REM: sympy.Mod,
    })

LOGICAL_OPERATORS = bidict({
    libsbml.AST_LOGICAL_AND: sympy.And,
    # libsbml.AST_LOGICAL_EXISTS: None,
    # libsbml.AST_LOGICAL_FORALL: None,
    libsbml.AST_LOGICAL_IMPLIES: sympy.Implies,
    libsbml.AST_LOGICAL_NOT: sympy.Not,
    libsbml.AST_LOGICAL_OR: sympy.Or,
    libsbml.AST_LOGICAL_XOR: sympy.Xor,
    })

LINEAR_ALGEBRA = {
    libsbml.AST_LINEAR_ALGEBRA_DETERMINANT,
    libsbml.AST_LINEAR_ALGEBRA_MATRIX,
    libsbml.AST_LINEAR_ALGEBRA_MATRIXROW,
    libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT,
    libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT,
    libsbml.AST_LINEAR_ALGEBRA_SELECTOR,
    libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE,
    libsbml.AST_LINEAR_ALGEBRA_VECTOR,
    libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT,
    }

RELATIONAL_OPERATORS = bidict({
    libsbml.AST_RELATIONAL_EQ: sympy.Eq,
    libsbml.AST_RELATIONAL_GEQ: sympy.Ge,
    libsbml.AST_RELATIONAL_GT: sympy.Gt,
    libsbml.AST_RELATIONAL_LEQ: sympy.Le,
    libsbml.AST_RELATIONAL_LT: sympy.Lt,
    libsbml.AST_RELATIONAL_NEQ: sympy.Ne,
    })

UNKNOWN = {
    libsbml.AST_END_OF_CORE,
    libsbml.AST_UNKNOWN,
    }

SERIES = {
    libsbml.AST_SERIES_PRODUCT,
    libsbml.AST_SERIES_SUM,
    }

STATISTICS = {
    libsbml.AST_STATISTICS_MEAN,
    libsbml.AST_STATISTICS_MEDIAN,
    libsbml.AST_STATISTICS_MODE,
    libsbml.AST_STATISTICS_MOMENT,
    libsbml.AST_STATISTICS_SDEV,
    libsbml.AST_STATISTICS_VARIANCE,
    }

VARIABLES = bidict({libsbml.AST_NAME: sympy.Symbol})

ALL_OPERATORS = bidict({**TRIGONOMETRIC_OPERATORS, **ARITHMETIC_OPERATORS, **LOGICAL_OPERATORS, **RELATIONAL_OPERATORS})


MAPPING = bidict({
    **VARIABLES,
    **DISTRIBUTIONS,
    **CONSTANTS,
    **NUMERICAL,
    **FUNCTIONS,
    **CSYMBOLS,
    **CFUNCTIONS,
    **TRIGONOMETRIC_OPERATORS,
    **ARITHMETIC_OPERATORS,
    **LOGICAL_OPERATORS,
    **RELATIONAL_OPERATORS,
    })

WITHOUT_MAPPING = {
    libsbml.AST_CSYMBOL_FUNCTION,
    libsbml.AST_LOGICAL_EXISTS,
    libsbml.AST_LOGICAL_FORALL,
    *LINEAR_ALGEBRA,
    *UNKNOWN,
    *SERIES,
    *STATISTICS,
    }


# ALL = {*DISTRIBUTIONS, *CONSTANTS, *NUMERICAL, *FUNCTIONS, *CSYMBOLS, *CFUNCTIONS,
#        *TRIGONOMETRIC_OPERATORS, *ARITHMETIC_OPERATORS, *LOGICAL_OPERATORS, *RELATIONAL_OPERATORS,
#        *LINEAR_ALGEBRA, *UNKNOWN, *SERIES, *STATISTICS, *VARIABLES,
#        }

## this is needed to back translate sympy to csymbols and csymbol functions
# CSYMBOL_MATHML = {
#     "<ci>time</ci>" : '<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/time"> time </csymbol>',
#     "<ci>avogadro</ci>":'<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/avogadro"> avogadro </csymbol>',
#     # "<ci>delay</ci>": '<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/delay"> delay </csymbol>',
#     # "<ci>rate_of</ci>":'<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/rateOf"> rate_of </csymbol>',
#     }

# special cases, also in arithmetic operators; want to catch unary operators first in sympy conversion
UNARY_OPERATORS = {
    libsbml.AST_PLUS: operator.pos,
    libsbml.AST_MINUS: operator.neg,
    }
