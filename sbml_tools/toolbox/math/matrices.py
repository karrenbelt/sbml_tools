
from typing import Sized, Dict, List, Union, Tuple, Mapping, Collection
import collections
from functools import lru_cache
import numpy as np
import pandas as pd
import sympy
import libsbml
from sbml_tools import read_sbml
import scipy.sparse

from sbml_tools import utils
from sbml_tools.wrappers import Model, ASTNode

# import autograd.numpy as np  # Thinly-wrapped numpy
# from autograd import grad    # The only autograd function you may ever need


# TODO: This is far from ready
#  symengine, local parameters, sparse and dense matrices
#  for scipy: dok lil and coo can be used to construct, then convert to CSR / CSC for mat operations
#  scipy requires non-floats, otherwise values are stored (unlike sympy)


# def to_dense(cols: Sized, rows: Sized, data: Dict[tuple, float]):  # TODO: speed up by allocating space first
#     return np.asarray([[(data[(x, y)] if (x, y) in data else 0) for y in range(len(rows))] for x in range(len(cols))])
#
#
# def get_ids(list_of):
#     return list(map(lambda x: x.id, list_of))
#
#
# def to_symbols(list_of):
#     return list(map(lambda x: sympy.Symbol(x.id), list_of))


# def parse_kinetic_law(kinetic_law):
#     return

@lru_cache(128)
def create_state_space_mapping(model):
    """
    u - input vector
        all boundary species & non-constant parameters
    x - states vector
        all non-constant compartment or species
    p - parameter vector
        all constant compartments, species and parameters

    Higher order derivatives are normally decomposed, but we don't have these in metabolism.

    The 'constant' attribute is used to decide whether a compartment, species or parameter is variable or true parameter
    # TODO: rules, events, function definition references
    """
    x, u, p = ([] for _ in range(3))
    for compartment in model.compartments:
        p.append(compartment) if compartment.constant else x.append(compartment)
    for species in model.species:
        p.append(species) if species.constant else u.append(species) if species.boundary_condition else x.append(species)
    for parameter in model.parameters:
        p.append(parameter) if parameter.constant else u.append(parameter)
    for local_parameter in model.local_parameters:
        p.append(local_parameter)  # local parameters are always constant

    x, u, p = (collections.OrderedDict(enumerate(_)) for _ in (x, u, p))
    return x, u, p


def cast_matrix(rows: Collection, cols: Collection, sparse_matrix: dict, matrix_type='dict'):
    """
    could use matrix._update(sparse_matrix) to construct the dok_matrix
    https://github.com/scipy/scipy/issues/8338
    """
    if matrix_type not in ('dict', 'scipy_dok', 'sympy_sparse', 'numpy', 'dataframe', 'sympy'):
        raise ValueError()
    shape = (len(rows), len(cols))

    if matrix_type == 'dict':
        matrix = sparse_matrix
    else:
        data_types = [type(x) for x in set(sparse_matrix.values())]
        data_type = data_types[0] if len(data_types) == 1 else None
        matrix = scipy.sparse.dok_matrix(shape, dtype=data_type)
        for (row, col), v in sparse_matrix.items():
            matrix[(row, col)] = v
        if matrix_type == 'sympy_sparse':
            matrix = sympy.SparseMatrix(*shape, sparse_matrix)  # infers int if all int
        elif matrix_type == 'numpy':
            matrix = matrix.toarray()
        elif matrix_type == 'dataframe':
            matrix = pd.DataFrame(matrix.toarray(), index=rows, columns=cols)
        elif matrix_type == 'sympy':
            matrix = sympy.Matrix(matrix.toarray())

    return matrix


class Matrices:
    """
    Matrices of the model.

    NOTE: kinetic laws might ignore species that are referenced

    TODO: note that the order is highly important,
     and might be different if you parse species from reactions
    """
    def __init__(self, model: Model, matrix_type='numpy') -> None:
        self.model = model
        self.matrix_type = matrix_type
        # check stoichiometry, built from species references, with species

    @property
    def species_vector(self) -> np.array:
        return np.array(self.model.species)

    @property
    def parameter_vector(self) -> list:
        return np.array(list(self.model.parameters) + self.model.local_parameters)

    @property
    def reaction_vector(self) -> np.array:
        return np.array(self.model.reactions)

    @property
    def complexes(self) -> List[Dict[str, int]]:
        complexes = []
        for reaction in self.model.reactions:
            for species_references in (reaction.reactants, reaction.products):
                complex = {s.species: s.stoichiometry for s in species_references}
                if complex not in complexes:
                    complexes.append(complex)
        return complexes

    @property
    def complex_matrix(self) -> Dict[Tuple[int, int], float]:
        """species x complexes"""
        sparse_complex_matrix = {}
        for reaction in self.model.reactions:
            for species_references in (reaction.reactants, reaction.products):
                complex = {r.species: r.stoichiometry for r in species_references}
                for s in complex:
                    sparse_complex_matrix[(self.model.species_ids.index(s), self.complexes.index(complex))] = complex[s]
        complex_matrix = cast_matrix(self.model.species, self.complexes, sparse_complex_matrix, self.matrix_type)
        return complex_matrix

    @property
    def incidence_matrix(self) -> Dict[Tuple[int, int], int]:
        """complexes x reactions"""
        incidence_matrix = {}
        complexes = self.complexes
        for i, reaction in enumerate(self.model.reactions):
            reactants = {s_ref.species: s_ref.stoichiometry for s_ref in reaction.reactants}
            products = {s_ref.species: s_ref.stoichiometry for s_ref in reaction.products}
            if reactants != products:
                incidence_matrix[(complexes.index(reactants), i)] = -1
                incidence_matrix[(complexes.index(products), i)] = 1
        incidence_matrix = cast_matrix(self.complexes, self.model.reactions, incidence_matrix, self.matrix_type)
        return incidence_matrix

    @property
    def parameter_matrix(self):
        """parameters x reactions"""
        parameter_matrix = {}
        parameter_ids = self.model.parameter_ids
        parameter_vector = list(self.parameter_vector)
        for i, reaction in enumerate(self.model.reactions):
            local_parameter_ids = [local_parameter.id for local_parameter in reaction.local_parameters]
            for symbol in reaction.kinetic_law_symbols:
                if symbol in local_parameter_ids:
                    local_parameter = reaction.local_parameters[symbol]
                    parameter_matrix[(parameter_vector.index(local_parameter), i)] = 1
                elif symbol in parameter_ids:
                    parameter = self.model.parameters[symbol]
                    parameter_matrix[(parameter_vector.index(parameter), i)] = 1
        parameter_matrix = cast_matrix(parameter_vector, self.model.reactions, parameter_matrix, self.matrix_type)
        return parameter_matrix

    @property
    def stoichiometric_matrix(self):
        """species x reactions"""
        stoichiometric_matrix = dict()
        for i, reaction in enumerate(self.model.reactions):
            for species_references, sign in [(reaction.reactants, -1), (reaction.products, 1)]:
                for s in species_references:
                    stoichiometric_matrix[(self.model.species.index(s.species), i)] = s.stoichiometry * sign
        stoichiometric_matrix = cast_matrix(self.model.species, self.model.reactions,
                                            stoichiometric_matrix, self.matrix_type)
        return stoichiometric_matrix

    @property
    def regulatory_matrix(self):
        """species x reactions"""
        regulatory_matrix = {}
        for i, reaction in enumerate(self.model.reactions):
            for m_ref in reaction.modifiers:
                regulatory_matrix[(self.model.species.index(m_ref.species), i)] = 1
        regulatory_matrix = cast_matrix(self.model.species, self.model.reactions, regulatory_matrix, self.matrix_type)
        return regulatory_matrix

    def state_space(self):
        # need to inline function definitions, rules and events
        x, u, p = create_state_space_mapping(self.model)
        return

    # differential equations
    # @property
    # def xdot(self) -> np.ndarray:
    #     return np.dot(self.stoichiometric_matrix, self.rates)
    #
    # @property
    # def symbolic_xdot(self) -> sympy.MutableDenseMatrix:
    #     return sympy.Matrix(self.stoichiometric_matrix.dot(sympy.Matrix(self.kinetic_laws)))
    #
    # @property
    # def symbolic_jacobian(self) -> sympy.MutableDenseMatrix:
    #     return self.symbolic_xdot.jacobian(self.symbolic_species_ids)
    #
    # @property
    # def symbolic_sensitivities(self) -> sympy.MutableDenseMatrix:
    #     return self.symbolic_xdot.jacobian(self.symbolic_parameter_ids)
    #
    # @property
    # def symbolic_flux_sensitivities(self) -> sympy.MutableDenseMatrix:
    #     return sympy.Matrix(self.kinetic_laws).jacobian(self.symbolic_parameter_ids)
    #
    # def _solve_symbolic_matrix(self, matrix : sympy.Matrix):
    #     values = {**dict(zip(self.species_ids, self.species_initial_concentrations)),
    #               **dict(zip(self.parameter_ids, self.parameter_values)),
    #               **dict(zip(self.compartment_ids, self.compartment_sizes))}
    #     lambdified = sympy.lambdify(matrix.free_symbols, matrix)
    #     return lambdified(*[values[k] for k in inspect.signature(lambdified).parameters.keys()])
    #
    # @property
    # def jacobian(self) -> np.ndarray:
    #     return self._solve_symbolic_matrix(self.symbolic_jacobian)
    #
    # @property
    # def sensitivity(self) -> np.ndarray:
    #     return self._solve_symbolic_matrix(self.symbolic_sensitivities)
    #
    # @property
    # def flux_sensitivities(self) -> np.ndarray:
    #     return self._solve_symbolic_matrix(self.symbolic_flux_sensitivities)

    # def deficiency(self):
    #     """deficiency of the reaction network"""
    #     return np.linalg.matrix_rank(self.incidence_matrix) - np.linalg.matrix_rank(self.stoichiometric_matrix)
    #
    # def null_space(self):
    #     """null space of the stoichiometric matrix"""
    #     return scipy.linalg.null_space(self.stoichiometric_matrix)


def test_cast_matrix():
    import time
    shape = (100, 100)
    sparse_matrix = dict()
    for i in range(shape[0]):
        for j in range(shape[1]):
            if np.random.rand() < 0.1:
                sparse_matrix[(i, j)] = np.random.rand()

    rows = list(range(shape[0]))
    cols = list(range(shape[1]))
    for as_type in ('dict', 'scipy_dok', 'numpy', 'dataframe', 'sympy_sparse', 'sympy'):
        t0 = time.time()
        s = cast_matrix(rows, cols, sparse_matrix, matrix_type=as_type)
        print(as_type, time.time() - t0)


def test():
    from sbml_tools import settings
    from sbml_tools.wrappers import SBMLDocument
    from sbml_tools.toolbox.sbml_io import read_sbml
    from sbml_tools.toolbox.math import SymbolicASTNode
    from sbml_tools.toolbox.kinetics.test_case_models import simple_reactions_model
    import sympy

    doc = SBMLDocument(read_sbml(settings.CHASSAGNOLE_MODEL))
    doc.model.differential_equations
    # doc = simple_reactions_model()
    self = Matrices(doc.model, matrix_type='sympy_sparse')
    self.model.species.index('cpep')

    doc.model.reactions[4].parameters

    # sympy.Matrix(self.stoichiometric_matrix.dot(sympy.Matrix([law.math.string for law in self.model.reactions.kinetic_laws])))

    # rates = []
    # for law in self.model.reactions.kinetic_laws:
    #     node = SymbolicASTNode(law.math.wrapped)
    #     rates.append(sympy.sympify(node.string.replace('^', '**')))
    #     # rates.append(node.to_sympy())
    # self.rates = rates

    self.complexes
    self.complex_matrix
    self.incidence_matrix
    self.stoichiometric_matrix
    self.regulatory_matrix

    assert (self.stoichiometric_matrix == self.complex_matrix.dot(self.incidence_matrix)).all()

    s = self.stoichiometric_matrix




# class Convenience:#, Tables):
#     """ extend the functionality of base classes """
#     identifiable = ('function_definition', 'unit_definition', 'compartment', 'species', 'parameter',
#                     'reaction', 'event')
#
#     def __init__(self, sbml, math_level='sympy',
#                  validate: bool = True, check_warnings=False):
#         super().__init__(sbml, math_level, validate, check_warnings)
#
#     # @property
#     # def local_parameter_ids(self):
#     #     return [p.id for r in self.reactions if r.isSetKineticLaw() for p in r.getKineticLaw().local_parameters]
#
#     # @property
#     # def parameter_ids_not_in_kinetic_laws(self):
#     #     return set(self.parameter_ids + self.local_parameter_ids).difference(map(str, self.all_kinetic_law_symbols))
#
#     @property
#     def reversible_reactions(self) -> list:
#         return [r for r in self.reactions if r.reversible]
#
#     @property
#     def irreversible_reactions(self) -> list:
#         return [r for r in self.reactions if not r.reversible]
#
#     @property
#     def exchange_reactions(self):
#         return [r for r in self.reactions if (len(r.reactants) == 0 or len(r.products) == 0)]
#
#     @property
#     def exchange_reaction_ids(self):
#         return [r.id for r in self.reactions if (len(r.reactants) == 0 or len(r.products) == 0)]
#
#     @property
#     def n_exchange_reactions(self):
#         return len(self.exchange_reactions)
#
#     @property
#     def exchange_species_ids(self):
#         return sum([[s_ref.species for s_ref in r.reactants] for r in self.exchange_reactions], [])
#
#     @property
#     def transport_reactions(self):
#         return [r for r in self.doc.model.reactions
#                 if len(set([self.doc.model.getSpecies(s_ref.species).compartment for s_ref in r.reactants]
#                            + [self.doc.model.getSpecies(s_ref.species).compartment for s_ref in r.products])) > 1]
#
#     @property
#     def transport_reaction_ids(self):
#         return [r.id for r in self.transport_reactions]
#
#     @property
#     def kinetic_reactions(self):
#         return [r for r in self.reactions if r.isSetKineticLaw()]
#
#     @property
#     def kinetic_reaction_ids(self):
#         return [r.id for r in self.reactions if r.isSetKineticLaw()]
#
#     @property  # TODO: other math_levels
#     def all_kinetic_law_symbols(self):
#         symbols = [self.read_math(r.getKineticLaw().math) for r in self.doc.model.reactions
#                    if r.isSetKineticLaw() and r.getKineticLaw().isSetMath()]
#         symbols = [x.free_symbols for x in symbols if isinstance(x, sympy.expr.Expr)]
#         return set(itertools.chain.from_iterable(symbols))
#
#     @property
#     def fluxes(self):
#         d = dict(zip(self.parameter_ids, self.parameter_values))
#         d.update(dict(zip(self.species_ids, self.species_initial_concentrations)))
#         return sympy.Matrix(self.kinetic_laws).subs(d)