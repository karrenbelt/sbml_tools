import pytest
import ast
import math
import numbers
import fractions
import operator
import functools
from more_itertools import consume
import numpy as np
import libsbml
from typing import Optional, Union

from sbml_tools.wrappers import SimpleSpeciesReference, Species
from sbml_tools.wrappers.ast_node import _as_ast_node
from sbml_tools.wrappers import ASTNode as WrappedASTNode
from sbml_tools.wrappers import Species, SpeciesReference


# TODO:
#  this purpose of this ASTNode is to be used in the construction of rate laws
#  1. relational operators
#  should also return ASTNodes? I decided not to because of min() and max() behaviour
#  if so also need consider overwriting __eq__ and __ne__, which are used in object comparison (e.g. comparing models)
#  2. merging with ast_node wrapper:
#  only if relational operators don't return ASTNodes will this be possible

# def have_equal_precedence(node1, node2) -> bool:



class ASTNodeError(TypeError):
    pass


@functools.total_ordering
class ASTNode(WrappedASTNode):
    """
    This class extends the wrapped ASTNode class with methods to facilitate expression building, by providing
    1. Class methods for creating an ASTNode of a specific type by passing children as arguments.
    2. Special methods for 'infix'-style construction of expressions

    Every method of this class returns a new instance, except for the relational operators (==, !=, <, >, <=, and >=).
    The special methods for these return booleans, because relational operators are used in various other functions,
    such as for example sorting. The convenience gained there is deemed minimal in comparison to the risk of unexpected
    behavior. To illustrate:
     - sorted([ASTNode(value=1), ASTNode(value=3)]) would return: [[ASTNode(value=3), ASTNode(value=1)])
     - if ASTNode(value=3) > ASTNode(value=4): would return True
     - max([ASTNode(name='y'), ASTNode(name='x')]) would return: ASTNode(name='x'), whereas max('xy') returns 'y'
    Neither does it allow for delegation to the other instance that preserves order of the tree:
     - 1 < ASTNode(name='x') does not yield ASTNode.from_string('1 < x') but ASTNode.from_string('x > 1').
    To build such constructs one can use the class method (e.g. ASTNode.relational_lt(3, 4)), which are not evaluated

    Deep copies are created when combining nodes.
    As such, no expression contains references to instances that are part of another expressions.

    NOTE: class methods can be called on instances, but likely do not yield the desired behaviour
    """
    def __init__(self, ast_node: libsbml.ASTNode = None, **kwargs):
        # name = kwargs.get('name')
        # if name:
        #     kwargs['name'] = (name.species if isinstance(name, SimpleSpeciesReference)
        #                       else name.id if isinstance(name, Species) else name)
        super().__init__(ast_node, **kwargs)

    # def _as_numeric(self, ast_node=None) -> numbers.Number:
    #     # for the rich comparison methods: __lt__, __le__, __gt__ and __ge__
    #     if ast_node is None:
    #         ast_node = self
    #     if isinstance(ast_node, numbers.Number):
    #         return ast_node
    #     if isinstance(ast_node, ASTNode) and ast_node.is_number:
    #         if ast_node.units:  # else e.g. 5mg > 1g
    #             raise ASTNodeError(f'unit checking not implemented, found: {ast_node}')
    #         return ast_node.value
    #     raise ASTNodeError(f'non-numeric value: {ast_node}')

    def _combine(self, ast_type: int,
                 other: Optional[Union['ASTNode', int, float, fractions.Fraction]] = ASTNodeError, reverse=False):
        # both nodes are copied to ensure no references are kept
        if other is None:
            raise ASTNodeError(f"cannot combine ASTNode with {None}")

        node = ASTNode(type=ast_type, children=self.copy())

        if other is not ASTNodeError:  # for unary operators, default cannot be None
            other = _as_ast_node(other).copy()
            equal_precedence = node.type == other.type and other.type in (libsbml.AST_PLUS, libsbml.AST_TIMES)
            children = other.children if equal_precedence else (other, )
            consume(map(node.prepend_child if reverse else node.add_child, children))

        assert node.is_well_formed, f'ASTNode not well-formed:\n{node}'
        return node

    # relational operators
    @classmethod
    def relational_lt(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_LT, children=(a, b))

    @classmethod
    def relational_le(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_LEQ, children=(a, b))

    @classmethod
    def relational_eq(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_EQ, children=(a, b))

    @classmethod
    def relational_ne(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_NEQ, children=(a, b))

    @classmethod
    def relational_gt(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_GT, children=(a, b))

    @classmethod
    def relational_ge(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_RELATIONAL_GEQ, children=(a, b))

    def __lt__(self, other) -> bool:
        # < operator
        if isinstance(other, numbers.Number):
            other = ASTNode(value=other)
        if isinstance(other, type(self)) and other.is_number:
            if self.units or other.units:  # else e.g. 5mg > 1g
                raise ASTNodeError(f"unit checking not implemented, found: '{self}' and '{other}'")
            return self.value < other.value
        raise ASTNodeError(f'non-numeric value: {other}')
        # return self.__class__(type=libsbml.AST_RELATIONAL_LT, children=(self, other))

    # def __le__(self, other) -> bool:
    #     # <= operator
    #     return self._as_numeric() <= self._as_numeric(other)
    #     # return self.__class__(type=libsbml.AST_RELATIONAL_LEQ, children=(self, other))
    #
    # def __gt__(self, other) -> bool:
    #     # > operator
    #     return self._as_numeric() > self._as_numeric(other)
    #     # return self.__class__(type=libsbml.AST_RELATIONAL_GT, children=(self, other))
    #
    # def __ge__(self, other) -> bool:
    #     # >= operator
    #     return self._as_numeric() >= self._as_numeric(other)
    #     # return self.__class__(type=libsbml.AST_RELATIONAL_GEQ, children=(self, other))

    # logical operators
    @classmethod
    def logical_and(cls, *children) -> 'ASTNode':
        return cls(type=libsbml.AST_LOGICAL_AND, children=children)

    @classmethod
    def logical_or(cls, *children) -> 'ASTNode':
        return cls(type=libsbml.AST_LOGICAL_OR, children=children)

    @classmethod
    def logical_xor(cls, *children) -> 'ASTNode':
        return cls(type=libsbml.AST_LOGICAL_XOR, children=children)

    @classmethod
    def logical_not(cls, child):
        return cls(type=libsbml.AST_LOGICAL_NOT, children=child)

    def __and__(self, other) -> 'ASTNode':
        # & operator
        return self._combine(libsbml.AST_LOGICAL_AND, other)

    def __rand__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_LOGICAL_AND, other, reverse=True)

    def __iand__(self, other):  # work without implementation because of __and__ and __rand__?
        # &= operator
        return NotImplemented

    def __or__(self, other) -> 'ASTNode':
        # ^ operator
        return self._combine(libsbml.AST_LOGICAL_OR, other)

    def __ror__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_LOGICAL_OR, other, reverse=True)

    def __ior__(self, other):
        return NotImplemented

    def __xor__(self, other) -> 'ASTNode':
        # | operator
        return self._combine(libsbml.AST_LOGICAL_XOR, other)

    def __rxor__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_LOGICAL_XOR, other, reverse=True)

    def __ixor__(self, other):
        # |= operator
        return NotImplemented

    def __invert__(self):
        # ~ operator
        return self._combine(libsbml.AST_LOGICAL_NOT)

    def __lshift__(self, other):
        # << operator
        return NotImplemented

    def __rlshift__(self, other):
        return NotImplemented

    def __ilshift__(self, other):
        # <<= operator
        return NotImplemented

    def __rshift__(self, other):
        # >> operator
        return NotImplemented

    def __rrshift__(self, other):
        return NotImplemented

    def __irshift__(self, other):
        # >>= operator
        return NotImplemented

    # unary arithmetic operators
    @classmethod
    def unary_plus(cls, child) -> 'ASTNode':
        return child.copy()

    @classmethod
    def unary_minus(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_MINUS, children=child)

    @classmethod
    def abs(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_ABS, children=child)

    def __pos__(self) -> 'ASTNode':
        # unary + operator
        return self  # self._combine(None, libsbml.AST_PLUS)

    def __neg__(self) -> 'ASTNode':
        # unary - operator
        return self._combine(libsbml.AST_MINUS)

    def __abs__(self) -> 'ASTNode':
        # abs() operator
        return self._combine(libsbml.AST_FUNCTION_ABS)

    def __index__(self):
        # operator.index() operator
        return NotImplemented

    def __complex__(self):
        # complex() operator
        return NotImplemented

    # def __round__(self, n: Optional[int] = None) -> ASTNode:
    #     # round() operator
    #     return self.__class__(value=round(self.value, n))

    # def __trunc__(self) -> ASTNode:
    #     # math.trunc() operator
    #     # return self.__class__(value=math.trunc(self.value))
    #     return self._combine(None, ast_type=libsbml.AST_FUNCTION_FLOOR)  # same as floor; should return int instead of float
    #
    # def __ceil__(self) -> ASTNode:
    #     # math.ceil() operator
    #     # return self.__class__(value=math.ceil(self.value))
    #     return self._combine(None, ast_type=libsbml.AST_FUNCTION_CEILING)

    @classmethod
    def ceil(cls, child):
        return cls(type=libsbml.AST_FUNCTION_CEILING, children=child)

    @classmethod
    def floor(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_FLOOR, children=child)

    # def __floor__(self) -> ASTNode:
    #     # math.floor() operator
    #     # return self.__class__(value=math.floor(self.value))
    #     return self._combine(None, libsbml.AST_FUNCTION_FLOOR)

    # arithmetic operators
    @classmethod
    def add(cls, a, b, *children):
        # appends all children to one node, unlike when using __add__
        return cls(type=libsbml.AST_PLUS, children=(a, b, *children))

    @classmethod
    def subtract(cls, a, b, *children):
        node = cls(type=libsbml.AST_MINUS, children=(a, b))
        return functools.reduce(operator.sub, (node, *children))

    @classmethod
    def multiply(cls, a, b, *children):
        # appends all children to one node, unlike when using __mul__
        return cls(type=libsbml.AST_TIMES, children=(a, b, *children))

    @classmethod
    def divide(cls, a, b, *children):
        node = cls(type=libsbml.AST_DIVIDE, children=(a, b))
        return functools.reduce(operator.truediv, (node, *children))

    @classmethod
    def power(cls, a, b, *children):
        # children = list(map(_as_ast_node, (a, b, *children)))  # [::-1]
        children = (a, b, *children)[::-1]
        node = cls(type=libsbml.AST_POWER, children=(children[1], children[0]))
        for c in children[2:]:
            node = cls(type=libsbml.AST_POWER, children=(c, node))
        return node
        # return functools.reduce(operator.pow, (*children[:-2], node))

    @classmethod
    def quotient(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_QUOTIENT, children=(a, b))

    @classmethod
    def root(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_ROOT, children=(a, b))

    @classmethod
    def sqrt(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_ROOT, children=(child, 2))

    @classmethod
    def ln(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_LN, children=(child,))

    @classmethod
    def log(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_LOG, children=(a, b))

    @classmethod
    def exp(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_EXP, children=(child,))

    @classmethod
    def modulo(cls, a, b) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_REM, children=(a, b))

    @classmethod
    def min(cls, *children) -> 'ASTNode':  # TODO return self if self.value or self.name
        return cls(type=libsbml.AST_FUNCTION_MIN, children=children)

    @classmethod
    def max(cls, *children) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_MAX, children=children)

    @classmethod
    def factorial(cls, child) -> 'ASTNode':
        return cls(type=libsbml.AST_FUNCTION_FACTORIAL, children=(child,))

    def __add__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_PLUS, other)

    def __radd__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_PLUS, other, reverse=True)

    def __iadd__(self, other):
        # += operator
        return NotImplemented

    def __sub__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_MINUS, other)

    def __rsub__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_MINUS, other, reverse=True)

    def __isub__(self, other):
        # -= operator
        return NotImplemented

    def __mul__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_TIMES, other)

    def __rmul__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_TIMES, other, reverse=True)

    def __imul__(self, other):
        # *= operator
        return NotImplemented

    def __truediv__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_DIVIDE, other)

    def __rtruediv__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_DIVIDE, other, reverse=True)

    def __itruediv__(self, other):
        # /= operator
        return NotImplemented

    def __pow__(self, other, modulo=None) -> 'ASTNode':
        return self._combine(libsbml.AST_POWER, other)  # AST_FUNCTION_POWER

    def __rpow__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_POWER, other, reverse=True)

    def __ipow__(self, other):
        # **= operator
        return NotImplemented

    def __matmul__(self, other):
        # @ operator
        return NotImplemented

    def __rmatmul__(self, other):
        return NotImplemented

    def __imatmul__(self, other):
        # @= operator
        return NotImplemented

    def __floordiv__(self, other) -> 'ASTNode':
        node = self._combine(libsbml.AST_DIVIDE, other)
        return node._combine(libsbml.AST_FUNCTION_FLOOR)

    def __rfloordiv__(self, other) -> 'ASTNode':
        node = self._combine(libsbml.AST_DIVIDE, other, reverse=True)
        return node._combine(libsbml.AST_FUNCTION_FLOOR)

    def __ifloordiv__(self, other):
        # //= operator
        return NotImplemented

    def __mod__(self, other) -> 'ASTNode':
        # % operator
        return self._combine(libsbml.AST_FUNCTION_REM, other)

    def __rmod__(self, other) -> 'ASTNode':
        return self._combine(libsbml.AST_FUNCTION_REM, other, reverse=True)

    def __imod__(self, other):
        # %= operator
        return NotImplemented

    def __divmod__(self, other):
        # divmod() operator
        return NotImplemented
