
import re
from typing import Union, Set
import libsbml
import sympy
import sympy.stats
from sympy.printing import print_tree, latex

from sbml_tools.wrappers.ast_node import ALL_AST_TYPES, ASTNode, check_ast_node
from sbml_tools.toolbox.math.node_mapping import (
    NUMERICAL, CONSTANTS, VARIABLES, ALL_OPERATORS, UNARY_OPERATORS, CSYMBOLS, FUNCTIONS, CFUNCTIONS,
    DISTRIBUTIONS,
    )

from sbml_tools.toolbox.math.node_mapping import Minus, Quotient, Divide, Root, Piecewise, Power

# sympy.srepr
# sympy.preorder_traversal
# sympy.postorder_traversal


# lambda SBML notes:
#     - may only be used in function definitions
#     - may not be nested (lambda containing lambda)
#     - may not contain references to symbols other than those declared inside of it
#       or identifiers declared by other function definitions,
#     - all symbols they use must be passed through their parameters


class SymbolicASTNodeError(Exception):
    pass


class SymbolicASTNode(ASTNode):
    """
    Class for wrapping ASTNode of libsbml and converting it to Sympy and back.
    evaluate = False prevents simplification of the expression
    there are known cases where this doesn't work; in sympy.sympify. Parsing strings is generally a bad idea.
    e.g. sympy.sympify('cos3.1, evaluate=False), direct usage if sympy.cos(3.1, evaluate=False) it works though
    """
    def __init__(self, ast_node: libsbml.ASTNode = None, **kwargs):
        if ast_node:  # and ast_node.__class__.__name__ == self.__class__.__name__:
            check_ast_node(ast_node)
        super().__init__(ast_node, **kwargs)

    def to_sympy(self, evaluate=True):

        if self.type in CSYMBOLS:
            return CSYMBOLS[self.type]
        elif self.type in CONSTANTS:  # == self.wrapped.isConstant()
            return CONSTANTS[self.type]
        elif self.type in VARIABLES:
            return VARIABLES[self.type](self.name)  # generates the symbol
        elif self.type in NUMERICAL:
            return NUMERICAL[self.type](self.value)

        else:
            children = [child.to_sympy(evaluate) for child in self.children]
            if self.type in ALL_OPERATORS:
                if len(children) == 1 and self.type in UNARY_OPERATORS:
                    return UNARY_OPERATORS[self.type](*children)  # operator pos / neg
                if self.type in (libsbml.AST_FUNCTION_MIN, libsbml.AST_FUNCTION_MAX):
                    if len(children) == 0:
                        raise SymbolicASTNodeError(f'empty {self} evaluates to {ALL_OPERATORS[self.type]()} in sympy')
                return ALL_OPERATORS[self.type](*children, evaluate=evaluate)
            elif self.type in FUNCTIONS or self.type in CFUNCTIONS:
                if self.type == libsbml.AST_LAMBDA:
                    return FUNCTIONS[self.type](children[:-1], children[-1])
                elif self.type == libsbml.AST_FUNCTION:
                    return FUNCTIONS[self.type](self.name)(*children)
                elif self.type == libsbml.AST_FUNCTION_PIECEWISE:
                    return FUNCTIONS[self.type](*children, evaluate=evaluate)
                elif self.type == libsbml.AST_FUNCTION_DELAY:
                    return CFUNCTIONS[self.type](*children)
                elif self.type == libsbml.AST_FUNCTION_RATE_OF:
                    return CFUNCTIONS[self.type](*children)
                # elif self.type == libsbml.AST_CSYMBOL_FUNCTION: A  # all other custom functions
                #     raise ValueError(f"Unsupported ASTNode type: {ALL_AST_TYPES[self.type]}")

            elif self.type in DISTRIBUTIONS:
                return DISTRIBUTIONS[self.type](self.name, *children)

        raise NotImplementedError(f'not implemented {ALL_AST_TYPES[self.type]}')

    @classmethod
    def from_sympy(cls, expr):  # mathml printer is not sufficient
        expr_type = type(expr)
        if expr in CSYMBOLS.inverse:
            return cls(type=CSYMBOLS.inverse[expr])  # avogadro set name automatically, for time this is not the case
        elif expr in CONSTANTS.inverse:
            return cls(type=CONSTANTS.inverse[expr])
        elif expr_type in VARIABLES.inverse:
            return cls(type=VARIABLES.inverse[expr_type], name=str(expr))

        elif isinstance(expr, sympy.Number):
            for n in expr.__class__.__mro__:
                if n in NUMERICAL.inverse:
                    node = cls(type=NUMERICAL.inverse[n])
                    if node.type == libsbml.AST_REAL_E:  # FloatE
                        node.value = (expr.mantissa, expr.exponent)
                    elif node.type == libsbml.AST_INTEGER:
                        node.value = int(expr)
                    elif node.type == libsbml.AST_RATIONAL:
                        node.value = tuple(map(int, sympy.fraction(expr)))
                    else:
                        node.wrapped.setValue(float(expr))
                    return node
            raise ValueError(f'number {expr} not found in NUMERICAL')

        elif isinstance(expr, sympy.stats.rv.RandomSymbol):
            raise NotImplementedError('sympy to distributions not implemented')

        else:
            if expr_type in (Minus, Quotient, Divide, Root):
                children = [cls.from_sympy(arg) for arg in expr.inverse_args]
            elif isinstance(expr, Piecewise):  # a tuple of tuples containing pairs
                children = [cls.from_sympy(arg) for tup in expr.args for arg in tup]
            elif expr_type == sympy.Lambda:
                lambda_args, lambda_expr = expr.args
                children = [cls.from_sympy(arg) for arg in (*lambda_args, lambda_expr)]
            else:
                children = [cls.from_sympy(arg) for arg in expr.args]

            if expr_type in ALL_OPERATORS.inverse:
                node = cls(type=ALL_OPERATORS.inverse[expr_type])
                node.children = children
                if node.type == libsbml.AST_FUNCTION_POWER:  # name == 'sqr'
                    node.wrapped.setName(expr.name)
                return node

            if expr_type in CFUNCTIONS.inverse:
                node = cls(type=CFUNCTIONS.inverse[expr_type])
                node.children = children
                print(f"warning; translating '{expr}' to regular sympy function")
                return node

            for n in expr.__class__.__mro__:
                if n in FUNCTIONS.inverse:
                    node = cls(type=FUNCTIONS.inverse[n])
                    if node.type == libsbml.AST_FUNCTION:
                        node.name = expr.name
                    node.children = children
                    return node

                # if len(children) == 1 and self.type in UNARY_OPERATORS:  # == self.wrapped.isOperator()
                #     return UNARY_OPERATORS[self.type](*children)  # operator pos / neg
                # # return ALL_OPERATORS[self.type](*children, evaluate=evaluate)

        raise NotImplementedError(f'not implemented {expr_type}')

    @property
    def free_symbols(self) -> Set[sympy.Symbol]:
        return self.to_sympy().free_symbols

    def lambdify(self, modules=None):  # if the expression contains nested functions, those will not be arguments
        if modules is None:
            modules = ["scipy", "numpy"]
        expr = self.to_sympy()
        if isinstance(expr, sympy.Lambda):
            return sympy.lambdify(expr, modules=modules)
        return sympy.lambdify(expr.free_symbols, expr, modules=modules)

    @property
    def symbolic_repr(self):
        return sympy.srepr(self.to_sympy())

    def pprint(self):
        sympy.pprint(self.to_sympy())

    def print_sympy_tree(self, evaluate=True):
        print_tree(self.to_sympy(evaluate))

    @property
    def latex(self):
        return latex(self.to_sympy())

    def equals(self, other: Union[int, float, sympy.Expr, 'SymbolicASTNode']):
        if isinstance(other, self.__class__):
            other = other.to_sympy()
        return self.to_sympy().equals(other)

    # @property
    # def mathml(self):  # not working properly?
    #     return mathml(self.to_sympy())


