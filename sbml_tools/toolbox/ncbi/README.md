# Parsing NCBI Taxonomy

The NCBI taxdump contains many files, only two of which I cared about parsing:
- names.dmp
- nodes.dmp

### Names.dmp
Used to construct a simple mapping between taxonomy ids and different types of names
(e.g. scientific name, synonym, etc.)

### Nodes.dmp
The nodes are parsed to generate the taxonomic tree in the Taxonomy class.

## Taxonomy
This class initialize from the parsed names and nodes data. It can be used to
- query it to find taxonomy ids by scientific or other name
- query it to find the parent nodes of an organism, which will return a dictionary 
containing the hierarchical linkage upto the root node (e.g. to find the phylum of a species)
- construct the actual taxonomy tree from the nodes
- query the taxonomy tree to extract the subtree (e.g. to find all organisms belonging to 
the same phylum)
