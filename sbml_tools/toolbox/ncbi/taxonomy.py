
import os
import collections
from typing import Union, Optional, Set, Dict
from cached_property import cached_property
from sbml_tools import settings
from sbml_tools.utils import cache_file, timer
from tqdm import tqdm


def parse_names(use_cached=True):
    # tax_id        -- the id of node associated with this name
    # name_txt      -- name itself
    # unique name   -- the unique variant of this name if name not unique
    # name class    -- (synonym, common name, ...)

    @cache_file(file_path=os.path.join(settings.CACHE_DIR, 'ncbi_taxonomy_names.p'), use_cached=use_cached)
    def parse():
        tax2name = dict()
        with open(settings.NCBI_TAXONOMY_NAMES, 'r') as file:
            # columns: tax_id | name_txt | unique_name | name_type | \n
            name_classes = ('acronym', 'anamorph', 'authority', 'blast name', 'common name', 'equivalent name',
                            'genbank acronym', 'genbank common name', 'genbank synonym', 'in-part', 'includes',
                            'scientific name', 'synonym', 'teleomorph')

            for line in tqdm(file, desc='parsing NCBI taxonomy names'):
                tax_id, name_txt, unique_name, name_class, _ = [t.strip() for t in line.split("|")]
                tax_id = int(tax_id)
                if tax_id not in tax2name:
                    tax2name[tax_id] = dict()
                if name_class not in tax2name[tax_id]:
                    tax2name[tax_id][name_class] = []
                tax2name[tax_id][name_class].append(name_txt)
        return tax2name
    return parse()


def parse_nodes(use_cached=True):
    # tax_id                            -- node id in GenBank taxonomy database
    # parent tax_id				        -- parent node id in GenBank taxonomy database
    # rank					            -- rank of this node (superkingdom, kingdom, ...)
    # embl code				            -- locus-name prefix; not unique
    # division id				        -- see division.dmp file
    # inherited div flag  (1 or 0)		-- 1 if node inherits division from parent
    # genetic code id				    -- see gencode.dmp file
    # inherited GC  flag  (1 or 0)		-- 1 if node inherits genetic code from parent
    # mitochondrial genetic code id		-- see gencode.dmp file
    # inherited MGC flag  (1 or 0)		-- 1 if node inherits mitochondrial gencode from parent
    # GenBank hidden flag (1 or 0)      -- 1 if name is suppressed in GenBank entry lineage
    # hidden subtree root flag (1 or 0) -- 1 if this subtree has no sequence data yet
    # comments				            -- free-text comments and citations
    # plastid genetic code id           -- see gencode.dmp file
    # inherited PGC flag  (1 or 0)      -- 1 if node inherits plastid gencode from parent
    # specified_species			        -- 1 if species in the node's lineage has formal name
    # hydrogenosome genetic code id     -- see gencode.dmp file
    # inherited HGC flag  (1 or 0)      -- 1 if node inherits hydrogenosome gencode from parent

    @cache_file(file_path=os.path.join(settings.CACHE_DIR, 'ncbi_taxonomy_nodes.p'), use_cached=use_cached)
    def parse():
        tax_parents = {}
        with open(settings.NCBI_TAXONOMY_NODES, 'r') as file:
            for line in tqdm(file, desc='parsing NCBI taxonomy nodes'):
                tax_id, parent_tax_id, rank = [t.strip() for t in line.split("|")][:3]
                tax_id, parent_tax_id = int(tax_id), int(parent_tax_id)
                assert tax_id not in tax_parents, 'cannot generate a unique mapping'
                tax_parents[tax_id] = dict(tax_id=parent_tax_id, rank=rank)
        return tax_parents
    return parse()


class Node:
    def __init__(self, val):
        self.val = val
        self.children = []

    def __repr__(self):
        return f'{self.__class__.__name__} {self.val}'


class Taxonomy:

    @timer
    def __init__(self, use_cached=True):
        # takes approx 3 seconds to load the files if cached
        self.names = parse_names(use_cached)
        self.nodes = parse_nodes(use_cached)
        self.root = None
        self.node_map = None

    def _find_parents(self, tax_id: int):
        nodes = [tax_id]
        parent_id = None
        while parent_id != 1:
            tax_id = nodes[-1]
            parent_id = self.nodes[tax_id]['tax_id']
            nodes.append(parent_id)
        return nodes

    @cached_property
    def _tax_id_by_name(self) -> dict:
        # 3_009_162 entries, of which 785_462 non-scientific names, and containing, and 2277 map non-unique
        # good idea to always report / check the scientific name
        syn = collections.defaultdict(set)
        for k, v in self.names.items():
            for names in v.values():
                for name in set(names):
                    syn[name].add(k)
        return dict(syn)

    def get_tax_id_by_name(self, name) -> Union[int, Set[int]]:
        tax_ids = self._tax_id_by_name.get(name.capitalize(), set())
        if len(tax_ids) == 1:
            return list(tax_ids)[0]
        return tax_ids

    def get_parents(self, query: Union[int, str]) -> Optional[Dict[str, str]]:
        if isinstance(query, str):
            tax_id = self.get_tax_id_by_name(query)
            if isinstance(tax_id, set):
                names = [self.names[k]['scientific name'][0] for k in tax_id]
                print(f'{query} maps to zero or multiple ids: {names}')
                return None
            query = tax_id
        nodes = self._find_parents(query)
        return dict([(self.nodes[k]['rank'], self.names[k]['scientific name'][0]) for k in nodes])

    def all_name_classes(self):
        return {k for v in self.names.values() for k in v}

    def all_ranks(self):
        return {v['rank'] for k, v in self.nodes.items()}

    @cached_property
    def taxonomic_tree(self) -> Node:  # 655_383_561 bytes
        self.root = Node(1)
        parent = self.root
        self.node_map = {parent.val: parent}
        for tax_id in tqdm(self.names, desc='constructing taxonomic tree'):
            parents = self._find_parents(tax_id)
            for parent_id in parents[::-1]:
                if parent_id not in self.node_map:
                    node = Node(parent_id)
                    self.node_map[parent_id] = node
                    parent.children.append(node)
                else:
                    parent = self.node_map[parent_id]

        return self.root

    def get_subtree(self, query) -> Node:
        if isinstance(query, str):
            query = self.get_tax_id_by_name(query)
        if self.node_map is None:
            self.taxonomic_tree  # construct the tree
        return self.node_map[query]


def test():
    taxonomy = Taxonomy(True)
    taxonomy.get_parents('Cystobacter')  # scientific name
    taxonomy.get_parents('T4')           # genbank acronym
    taxonomy.get_parents('pennyfish')    # genbank common name

    # root = taxonomy.taxonomic_tree
    parents = taxonomy.get_parents('Flectobacillus')
    parents = taxonomy.get_parents('Escherichia coli')
    order = parents.get('order')
    subtree = taxonomy.get_subtree(order)
