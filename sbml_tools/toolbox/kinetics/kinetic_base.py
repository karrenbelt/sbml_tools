
from abc import ABC, abstractmethod
from typing import Union, Optional, List, Set, Tuple
import fractions
import functools
import itertools
import numpy as np

import libsbml
from sbml_tools.wrappers import utils, KineticLaw
from sbml_tools.utils import NAME_TO_SBO_TERM
from sbml_tools.wrappers.sbo_term import SBO_DAG, SBO
from sbml_tools.wrappers import Compartment, Species, Reaction
from sbml_tools.wrappers import SimpleSpeciesReference, SpeciesReference, ModifierSpeciesReference
from sbml_tools.wrappers import ListOfSpeciesReferences, ListOfModifierSpeciesReferences
from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.validator import check

from sbml_tools.toolbox.factories import ParameterFactory
from sbml_tools.toolbox.kinetics import sbo_utils
from sbml_tools.toolbox.kinetics.ignored_compounds import IGNORED_COMPOUNDS

# TODO:
#  1. filter IGNORED_COMPOUNDS
#  2. factories
#  ASTNode unit check


class KineticEquationError(Exception):
    pass


class RateLawNotApplicableError(Exception):
    pass


class InconsistentSpeciesError(Exception):
    pass


class KineticBase(ABC):  # TODO: make plugin for KineticLaw instead?
    """
    Abstract base class for kinetic equations.
    The abstract methods 'check_applicability' and 'create_equation' need to be overwritten by inheriting classes,
    these are called by 'assign_equation'.

    Every parameter that is generated should call self._create_parameter(),
    which ensures a call to the parameter factory is made and automatically adds an annotated parameter to the model.

    modifier ontology tree structure:
        catalysts
            catalysts
                enzymes
                RNA
                mRNA

        activators
            allosteric activators
            non-allosteric activators

        inhibitors
            irreversible
            reversible
                competitive
                uncompetitive
                non-competitive (allosteric)
    """

    def __init__(self, reaction: Reaction, create_local_parameters: bool = False):
        # if not kinetic_law.reaction or not not kinetic_law.reaction.model:
        #     raise ValueError(f'reaction must be part of a model to ensure parameter and unit inference / creating')
        self.reaction = reaction

        if not create_local_parameters and not self.reaction.id:
            raise ValueError(f'reaction must have an id assigned: {reaction}')
        self.create_local_parameters = create_local_parameters
        # self.validate_species_reference_sbo()
        self.parameter_factory = ParameterFactory(reaction.model, create_local_parameters)
        self.validate_species()  # TODO: combine with check_applicability ?

    @abstractmethod
    def check_applicability(self) -> Optional[str]:
        raise NotImplementedError('must implement a method to verify rate law applicability')

    @abstractmethod
    def create_equation(self):
        raise NotImplementedError('must implement a method to create the kinetic equation')

    @property
    def kinetic_law(self) -> KineticLaw:
        return self.reaction.kinetic_law

    def assign_equation(self):
        message = self.check_applicability()
        if message:
            raise RateLawNotApplicableError(f'\nrate law {self} not applicable for {self.reaction}: {message}')
        self.reaction.kinetic_law = self.create_equation()

    def _create_parameter(self, name, species: SimpleSpeciesReference = None) -> ASTNode:
        # gets or creates the parameter, and returns an ASTNode with that name
        # prefix = '' if self.create_local_parameters else self.reaction.id
        # name = '_'.join(filter(None, [prefix, parameter, species]))
        parameter = self.parameter_factory(name, reaction=self.reaction, species=species)
        return ASTNode(name=parameter.id)

    # def get_species_term(self, species):
    #     node = ASTNode(name=species.id)
    #     if species.has_only_substance_units:
    #         if bring_to_concentration:
    #             node /= ASTNode(species.compartment)
    #         else:
    #             node *= ASTNode(species.compartment)
    #     return

    @property
    def reactants(self) -> List[SpeciesReference]:
        return list(self.reaction.reactants)  # TODO: filter IGNORED_COMPOUNDS

    @property
    def products(self) -> List[SpeciesReference]:
        return list(self.reaction.products)  # TODO: filter IGNORED_COMPOUNDS

    @property
    def modifiers(self) -> List[ModifierSpeciesReference]:
        return list(self.reaction.modifiers)  # TODO: filter IGNORED_COMPOUNDS

    @property
    def n_reactants(self) -> int:
        # number of distinct species
        return len(self.reactants)

    @property
    def n_products(self) -> int:
        # number of distinct species
        return len(self.products)

    @property
    def n_modifiers(self) -> int:
        return len(self.modifiers)

    @property
    def reversible(self) -> bool:
        return self.reaction.reversible

    @property
    def species(self) -> List[Species]:
        return [ref.get_species() for ref in self.reactants + self.products + self.modifiers]

    @property
    def compartments(self) -> Set[Compartment]:  # need to identify compartments of species, and their dimensionality
        return {species.get_compartment() for species in self.species}

    @property
    def n_compartments(self):
        return len(self.compartments)

    @property
    def order_reactants(self) -> Union[int, float]:  # useful for when raising to a power, particularly in sympy
        n = sum([s.stoichiometry for s in self.reactants])
        return int(n) if isinstance(n, float) and n.is_integer() else n

    @property
    def order_products(self) -> Union[int, float]:
        n = sum([s.stoichiometry for s in self.products])
        return int(n) if isinstance(n, float) and n.is_integer() else n

    @property
    def has_all_constant_stoichiometries(self) -> bool:
        return all([s_ref.constant for s_ref in list(self.reactants) + list(self.products)])

    @property
    def has_all_integer_stoichiometries(self) -> bool:
        return all([s_ref.stoichiometry.is_integer() for s_ref in list(self.reactants) + list(self.products)])

    @property
    def has_consistent_species_units(self) -> bool:
        return len(set([s.substance_units for s in self.species])) <= 1

    @property
    def has_consistent_species_has_only_substance_units(self) -> bool:
        return len(set([s.has_only_substance_units for s in self.species])) <= 1

    @property
    def has_only_substance_units(self):
        assert self.has_consistent_species_has_only_substance_units
        return all([s.has_only_substance_units for s in self.species])

    def make_integer_stoichiometries(self):
        stoichiometric_coefficients = [s.stoichiometry for s in list(self.reactants) + list(self.products)]
        denominators = [fractions.Fraction(x).limit_denominator().denominator for x in stoichiometric_coefficients]
        least_common_multiple = functools.reduce(lambda a, b: a * b // np.gcd(a, b), denominators)
        for s_ref in self.reactants + self.products:
            s_ref.stoichiometry = s_ref.stoichiometry * least_common_multiple
        print(f"multiplied stoichiometries in {self.reaction.id} with {least_common_multiple}")

    def validate_species(self):
        utils.ensure_species_exist(self.reaction.model, self.reaction)
        if not self.has_all_constant_stoichiometries:  # excluding ignored compounds
            raise NotImplementedError(f'species found having varying stoichiometries in {self.reaction}')
        if not self.has_consistent_species_units:
            raise InconsistentSpeciesError(f'species have varying units: {self.reaction}')
        if not self.has_consistent_species_has_only_substance_units:
            raise InconsistentSpeciesError(f'species have varying has_only_substance_units: {self.reaction}')
        if self.n_compartments > 3:
            raise NotImplementedError(f'reaction involves species in more than three compartments: {self.reaction}')
        for compartment in self.compartments:
            if compartment.spatial_dimensions not in (2, 3):
                raise NotImplementedError(f'{self.reaction.id} involves species in a '
                                          f'{compartment.spatial_dimensions}D compartment')

    def _get_modifiers_with_participant_role(self, participant_role):
        return [m for m in self.modifiers if m.sbo_term in sbo_utils.successors(NAME_TO_SBO_TERM[participant_role])]

    @property
    def genes(self):
        return self._get_modifiers_with_participant_role('gene')

    # putative catalysts
    @property
    def catalysts(self):
        return self._get_modifiers_with_participant_role('catalyst')

    @property
    def enzymes(self):
        # subclass of catalysts
        return self._get_modifiers_with_participant_role('enzymatic catalyst')

    @property
    def ribonucleic_acids(self):
        return self._get_modifiers_with_participant_role('ribonucleic acid')

    @property
    def messenger_ribonucleic_acids(self):
        return self._get_modifiers_with_participant_role('messenger RNA')

    # @property
    # def non_enzymatic_catalysts(self):
    #     return list(set(self.catalysts).difference(self.enzymes))

    # inhibition
    @property
    def inhibitors(self):
        return self._get_modifiers_with_participant_role('inhibitor')

    @property
    def irreversible_inhibitor(self):
        """ Covalently binding the enzyme. All other inhibition is reversible """
        return self._get_modifiers_with_participant_role('irreversible inhibitor')

    @property
    def competitive_inhibitors(self):
        """ Binds at the active site of the enzyme. Increases the apparent Km, but does not affect the Vmax """
        return self._get_modifiers_with_participant_role('competitive inhibitor')

    @property
    def complete_inhibitors(self):
        """ subclass of non-competitive inhibition """
        return self._get_modifiers_with_participant_role('complete inhibitor')

    @property
    def partial_inhibitors(self):
        """ subclass of non-competitive inhibition """
        return self._get_modifiers_with_participant_role('partial inhibitor')

    @property
    def uncompetitive_inhibitors(self):
        """ Binds the enzyme-substrate complex allosterically. Reduces both Vmax and the apparent Km """
        return self._get_modifiers_with_participant_role('uncompetitive inhibitor')

    @property
    def allosteric_inhibitors(self):
        """ non-competitive inhibition. Reduces the Vmax, but not the apparent Km """
        return self._get_modifiers_with_participant_role('allosteric inhibitor')

    # activation
    @property
    def activators(self):
        return self._get_modifiers_with_participant_role('potentiator')

    @property
    def allosteric_activators(self):
        """ non-competitive activation. Increases the Vmax, but not the apparent Km """
        return self._get_modifiers_with_participant_role('allosteric activator')

    @property
    def non_allosteric_activators(self):
        return self._get_modifiers_with_participant_role('non-allosteric activator')

    def validate_species_reference_sbo(self):
        def check_sbo(reference, participant_role: str):
            if reference.sbo_term is None:
                raise ValueError(f'sbo term not annotated for {reference}')
            elif reference.sbo_term not in sbo_utils.successors(NAME_TO_SBO_TERM[participant_role]):
                raise ValueError(f'{participant_role} {reference} sbo term not properly annotated')

        for reactant in self.reactants:
            check_sbo(reactant, 'reactant')
        for product in self.products:
            check_sbo(product, 'product')
        for modifier in self.modifiers:
            check_sbo(modifier, 'modifier')

    def __repr__(self):
        return f'<{self.__class__.__name__}>'
