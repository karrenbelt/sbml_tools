
from typing import Tuple, Optional
import operator
# import functools
from functools import reduce

from sbml_tools import config
from sbml_tools.wrappers import SimpleSpeciesReference, SpeciesReference, ModifierSpeciesReference
from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.kinetics.kinetic_base import KineticBase, KineticEquationError
from sbml_tools.toolbox.kinetics.sbo_utils import NAME_TO_SBO_TERM
# from sbml_tools.toolbox.factories.parameter_factory import ParameterFactory


# TODO: rename kcatf and kcatr as kf and kr ?


def create_term(s_ref):

    species = s_ref.get_species()
    node = ASTNode(name=species.id)

    # conversion_factor = species.conversion_factor if species.conversion_factor else species.model.conversion_factor
    # if conversion_factor:
    #     raise NotImplementedError(f'conversion factor found on {species} or {species.model}')
    # node = ASTNode(name=conversion_factor) * node
    # if species.has_only_substance_units and species.initial_amount is not None:
    #     pass  # node = ASTNode(name=species.compartment) * node
    # elif not species.has_only_substance_units and species.initial_concentration is not None:
    #     node *= ASTNode(name=species.compartment)  # quantity / volume / time --> quantity / time
    # else:
    #     raise InconsistentSpeciesError('has_only_substance_units and amount / concentration do not match')

    if s_ref.stoichiometry == 1.0:
        return node
    power = int(s_ref.stoichiometry) if s_ref.stoichiometry.is_integer() else s_ref.stoichiometry
    return node ** ASTNode(value=power)  # , units='dimensionless')


class GeneralizedMassAction(KineticBase):
    """
    Most general kinetic rate equation. It is applicable to all reactions, whether catalyzed by an enzyme or not,
    irrespective of the number of reactants and / or products involved.

    for example:
    - basal transcription can be modelled with zeroth order mass action kinetics
    - mono-exponential decay (degradation) can be modelled with first order irreversible mass action kinetics

    Modifier references need to be annotated to be recognized (e.g. catalyst, potentiator, inhibitor)
    """
    def __init__(self, reaction, create_local_parameters=False):
        super().__init__(reaction, create_local_parameters)

    def check_applicability(self) -> Optional[str]:
        if not self.has_all_integer_stoichiometries:
            return f'reaction has non-integer stoichiometries'

    # @property
    # def function_definition(self):  # TODO: get this from SBO['math'] by SBO ?
    #     return

    def kf(self, catalyst: SpeciesReference = None):
        """forward rate constant"""
        return self._create_parameter('kf', catalyst)

    def kr(self, catalyst: SpeciesReference = None):
        """reverse rate constant"""
        return self._create_parameter('kr', catalyst)

    def kcatf(self, catalyst: SpeciesReference = None):
        """forward rate constant"""
        return self._create_parameter('kcatf', catalyst)

    def kcatr(self, catalyst: SpeciesReference = None):
        """reverse rate constant"""
        return self._create_parameter('kcatr', catalyst)

    def ka(self, modifier: ModifierSpeciesReference):
        """activation constant"""
        return self._create_parameter('ka', modifier)

    def ki(self, modifier: ModifierSpeciesReference):
        """inhibitory constant"""
        return self._create_parameter('ki', modifier)

    @property
    def keq(self):
        """equilibrium constant"""
        return self._create_parameter('keq')

    @property
    def mass_action_reactants(self):
        if not self.reactants:
            raise KineticEquationError(f'reaction {self.reaction.id} has no reactants')
        if len(self.reactants) == 1:
            return create_term(self.reactants[0])
        return reduce(operator.mul, [create_term(s_ref) for s_ref in self.reactants])

    @property
    def mass_action_products(self):
        if not self.products:
            raise KineticEquationError(f'reaction {self.reaction.id} has no products')
        if len(self.products) == 1:
            return create_term(self.products[0])
        return reduce(operator.mul, [create_term(s_ref) for s_ref in self.products])

    @property
    def mass_action_ratio(self):
        return self.mass_action_products / self.mass_action_reactants

    def create_mod_factor(self, s_ref, activating: bool):
        node = ASTNode(name=s_ref.species)
        if activating:
            return node / (self.ka(s_ref) + node)
        return self.ki(s_ref) / (self.ki(s_ref) + node)

    @property
    def _activation_factors(self):
        # A / (k + A)
        if self.activators:
            return reduce(operator.mul, [self.create_mod_factor(s_ref, True) for s_ref in self.activators])

    @property
    def _inhibition_factors(self):
        # k / (k + I)
        if self.inhibitors:
            return reduce(operator.mul, [self.create_mod_factor(s_ref, False) for s_ref in self.inhibitors])

    def create_equation(self, haldane_substituted=False):
        rates = []
        for i in range(max(1, len(self.catalysts))):
            catalyst = self.catalysts[i] if self.catalysts else None
            fwd = self.kf(catalyst)
            if self.reactants:
                fwd *= self.mass_action_reactants
            if self.reversible:
                if not haldane_substituted:
                    rev = self.kr(catalyst)
                    if self.products:
                        rev *= self.mass_action_products
                    rates += [catalyst.species * (fwd - rev) if catalyst else fwd - rev]
                else:
                    rates += [catalyst.species * fwd * (1 - self.mass_action_ratio / self.keq) if catalyst
                              else fwd * (1 - self.mass_action_ratio / self.keq)]
            else:
                rates += [catalyst.species * fwd if catalyst else fwd]

        rates = ASTNode.add(*rates) if len(rates) > 1 else rates[0]
        terms = [self._activation_factors, self._inhibition_factors, rates]
        eq = reduce(operator.mul, filter(None, terms))
        return eq

    # def assign_sbo(self):
    #     self.order_reactants, self.order_products, self.reaction.reversible, self.n_reactants
    #     sbo = NAME_TO_SBO_TERM['mass action rate law']
    #     if self.reversible:
    #         sbo = NAME_TO_SBO_TERM['mass action rate law for reversible reactions']
    #     else:
    #         sbo = NAME_TO_SBO_TERM['mass action rate law for irreversible reactions']


def test():

    from sbml_tools.toolbox import SemanticTestCases
    test_cases = SemanticTestCases()

    multi_compartment_cases = test_cases.select_by_test_tags('multi_compartment')
    for case in multi_compartment_cases:
        print(case, case.synopsis)
        doc = case.load_document()
        sizes = [compartment.size for compartment in doc.model.compartments]
        print(sizes)

    # selecting those with compartments of different dimensions
    d = dict()
    for case in multi_compartment_cases:
        doc = case.load_document()
        dims = set()
        for c in doc.model.compartments:
            dims.add(c.spatial_dimensions)
        d[case.number] = dims

    # case = test_cases[908]
    #  '00908': {2, 3},
    #  '00909': {1, 3},
    #  '00910': {1, 2},

    test_case = test_cases[0]   # NOTE, if you load the same again, you crash?
    doc = test_case.load_document()

    reaction = doc.model.reactions[0]
    self = GeneralizedMassAction(reaction)

    self.kinetic_law
    # doc.model.time_units = 'second'

    # # law.kcatf
    # doc.model.time_units = 'second'
    # kcatf = law.kcatf


    # none with both tags
    test_case_amount = test_cases.select_by_test_tags('amount')
    test_case_concentration = test_cases.select_by_test_tags('concentration')

    for case in test_case_amount:

        doc = case.load_document()
        for species in doc.model.species:
            print(species.has_only_substance_units)
        #1/0




