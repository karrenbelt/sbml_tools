

from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.kinetics import GeneralizedMassAction


class Convenience(GeneralizedMassAction):
    """
    sbo 429
    """
    def __init__(self, reaction, create_local_parameters=False):
        super().__init__(reaction, create_local_parameters)

    def create_equation(self):
        rates = []
        for i in range(max(1, len(self.catalysts))):
            numerator = []
            denominator = []
            catalyst = self.catalysts[i] if self.catalysts else None
            if self.reaction.reversible and self.products:
                numerator.minus

        return


