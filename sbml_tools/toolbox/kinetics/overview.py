


RATE_LAW_CATEGORIES = dict(
    non_enzymatic=dict(
        description="Spontaneous reactions and reactions with a catalyst that is no enzyme"
    ),
    gene_regulatory=dict(
        description="Reactions that produce RNA or produce polypeptide molecules from an empty set of reactants "
                    "or whose reactants are genes or RNA molecules and that have genes or RNA molecules as modifiers."
    ),
    uni_uni=dict(
        description="Enzyme-catalyzed reactions with one reactant of stoichiometry one and if reversible also one "
                    "product of stoichiometry one"
    ),
    bi_uni=dict(
        description="Enzyme-catalyzed reactions with two reactants, i.e., an integer stoichiometry two on the "
                    "reactant side, and if reversible one product of stoichiometry one"
    ),
    bi_bi=dict(
        description="Enzyme-catalyzed reactions with two reactants, i.e., an integer stoichiometry twoon the "
                    "reactant side, and if reversible two products that also have a stoichiometry of two."
    ),
    arbitrary=dict(
        description="Enzyme-catalyzed reactions with an arbitrary number of reactants and products."
    ),
    integer_stoichiometries=dict(
        description="Reactions whose participants have only integer stoichiometric values."
    ),
    irreversible=dict(
        description="Reactions whose net flux proceeds only in forward direction."
    ),
    modulated=dict(
        description="Reactions whose velocity is influenced by modifiers, such as activators(stimulators), "
                    "inhibitors, an (enzymatic) catalysts."
    ),
    reversible=dict(
        description="Reactions that can proceed in forward and reverse direction."
    ),
    zeroth_reactant_order=dict(
        description="Reactions in which the effects of reactants do not contribute to the velocity."
    ),
    zeroth_product_order=dict(
        description="The effects of products do not influence the velocity of these reactions."
    ),
    )


RATE_LAWS = dict(
# Additive Model Linear
# Additive Model Non Linear
# Common Modular Rate Law (CM)
# Convenience Kinetics
# Direct Binding Modular RateLaw (DM)
# Enzymatic Rate Law for Competitive Inhibition of Irreversible Uni-reactant Enzymesby Non-Exclusive Non-Cooperative Inhibitors
# Enzymatic Rate Law for IrreversibleNon-modulated Non-interacting Reactant Enzymes
# Force Dependent Modular Rate Law (FM)
# Generalized Mass Action
# Hill Equation
# Hill-Hinze Equation
# Hill-Radde Equation
# H-System
# Michaelis-Menten
# NetGeneratorLinear
# NetGeneratorNon-Linear
# Ordered Mechanism (compulsory-orderternary-complex mechanism)
# Ping-Pong Mechanism (substituted enzymemechanism)
# Power Law Modular Rate Law (PM)
# Random Order Ternary-Complex Mechanism
# Simultaneous Binding Modular Rate Law(SM)
# S-System
# Vohradský’s equation
# Weaver’s equation
# Zeroth Order Forward GeneralizedMass-Action
# Zeroth Order Reverse GeneralizedMass-Action
)


# allowed sbo terms on reaction modifiers (children, that is more specific terms, are also allowed)
# ALLOWED_MODIFIER_SBO_PARENTS = dict(
#     catalyst=NAME_TO_SBO_TERM['catalyst'],  # any non-enzymatic or enzymatic catalyst
#     enzymatic_catalyst=NAME_TO_SBO_TERM['enzymatic catalyst'],  # solely enzymatic catalysts
#     inhibitor=NAME_TO_SBO_TERM['inhibitor'],  # ['inhibitor', 'competitive inhibitor',  'non-competitive inhibitor',
#  # 'partial inhibitor', 'complete inhibitor', 'silencer', 'irreversible inhibitor', 'allosteric inhibitor',
#  # 'uncompetitive inhibitor']
#     activator=NAME_TO_SBO_TERM['potentiator'],  # ['potentiator', 'allosteric activator', 'non-allosteric activator']
# )
#
# PUTATIVE_ENZYMES = dict(
#     siRNA=NAME_TO_SBO_TERM['small interfering RNA'],
#     complex=NAME_TO_SBO_TERM['non-covalent complex'],
#     generic_protein=NAME_TO_SBO_TERM['polypeptide chain'],
#     macromolecule=NAME_TO_SBO_TERM['macromolecule'],
#     receptor=NAME_TO_SBO_TERM['receptor'],
#     simple_molecule=NAME_TO_SBO_TERM['ribonucleic acid'],
#     tructated_protein=NAME_TO_SBO_TERM['chemical macromolecule'],
#     unknown_molecule=NAME_TO_SBO_TERM['material entity of unspecified nature'],
# )