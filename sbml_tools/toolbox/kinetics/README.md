# Kinetics

When certain rate laws are special cases of other rate laws, the most generic form is introduced, where at a later
stage the parameters can be set to obtain the special case. For activation and inhibition we will also use generic 
formulae as prefactors to rate equations, because often the precise mechanism of binding remains unknown.
When multiple catalysts are assigned to a reaction, a rate law for each of them will be formulated.

In order to properly formulate rate laws the role of participants needs to be defined, which is achieved by 
specifying the corresponding Systems Biology Ontology (SBO) term. 

It is important to make clear that a “kinetic law” in SBML is not identical to a traditional rate law.
"In multi-compartmental models, to be able to specify a rate law only once and then use it unmodified in
equations for different species, the rate law needs to be expressed in terms of an intensive property, that is,
species quantity/time, rather than the extensive property typically used, species quantity/size/time. As a
result, modelers and software tools in general cannot insert traditional textbook rate laws unmodified into
the math element of a KineticLaw. The unusual term “kinetic law” was chosen to alert users to this difference." - 
SBML documentation, section 4.11.7: Mathematical interpretation of SBML reactions and kinetic laws

The continuous, but not the discrete, reaction scheme is supported.
We do not support: varying stoichiometries, inconsistent species units, mixing amounts and concentrations,
reactions involving more than three compartments, and compartments that are not 2 or 3-dimensional.

UniUni: MichaelisMenten, Hill
BiUni:
UniBi:
BiBi:

## KineticBase
Base class for kinetic equations

recognized modifiers SBO terms:
    catalyst, enzymatic catalyst, gene, ribonucleic acid, messenger RNA, 
    (allosteric / competitive/ irreversible / uncompetitive / complete / partial) inhibitor,
    potentiator, allosteric activator, non-allosteric activator

## GeneralizedMassAction
continuous scheme
zero-th order, first, ..., n-th order
reversible & irreversible
activation / inhibition

## MichaelisMenten

