import networkx as nx
from sbml_tools.wrappers.sbo_term import SBO_DAG, SBO, NAME_TO_SBO_TERM


def sbo_is_child_of(sbo_id: int, parent: int) -> bool:
    return sbo_id in list(nx.dfs_tree(SBO_DAG, parent))


def successors(sbo_id: int):
    return list(nx.dfs_tree(SBO_DAG, sbo_id))


def successor_names(sbo_id: int):
    return [SBO[k].get('name') for k in nx.dfs_tree(SBO_DAG, sbo_id)]



def test():
    for sbo_id, data in SBO.items():
        if data.get('math'):
            print(sbo_id)
            print(data['name'])
            print(data['math'])
            print()
