
from typing import List
from sbml_tools.wrappers import CVTerm, Species

# TODO: add more annotations than merely KEGG identifiers


class AnnotatedSpeciesSet(set):
    """
    this class is used to define a set of annotated species
    whose annotations are used to filter them when formulating rate laws
    """
    def add_species(self, identifier, name, resources):
        # set the meta_id, otherwise it is set automatically when adding annotations,
        # and we need it to be equal for hash equality
        species = Species(id=identifier, name=name, meta_id='meta_id', compartment='ignored_compounds',
                          has_only_substance_units=False, boundary_condition=False, constant=False)

        cv_term = CVTerm()
        cv_term.qualifier_type = 'BIOLOGICAL'
        cv_term.biological_qualifier = 'IS'
        for recourse in resources:
            cv_term.add_resource(recourse)

        species.cv_terms = [cv_term]
        self.add(species)


IGNORED_COMPOUNDS = AnnotatedSpeciesSet()
IGNORED_COMPOUNDS.add_species('H', 'proton', ['http://identifiers.org/kegg.compound/C00080'])
IGNORED_COMPOUNDS.add_species('H2', 'hydrogen', ['http://identifiers.org/kegg.compound/C00282'])
IGNORED_COMPOUNDS.add_species('H2O', 'water', ['http://identifiers.org/kegg.compound/C00001'])

IGNORED_COMPOUNDS.add_species('Zn2', 'zinc ion', ['http://identifiers.org/kegg.compound/C00038'])
IGNORED_COMPOUNDS.add_species('Ca2', 'calcium ion', ['http://identifiers.org/kegg.compound/C00076'])
IGNORED_COMPOUNDS.add_species('Cu2', 'cupper ion', ['http://identifiers.org/kegg.compound/C00070'])
IGNORED_COMPOUNDS.add_species('Co2', 'cobalt ion', ['http://identifiers.org/kegg.compound/C00175'])
IGNORED_COMPOUNDS.add_species('K', 'potassium ion', ['http://identifiers.org/kegg.compound/C00238'])
IGNORED_COMPOUNDS.add_species('Ni2', 'nickel ion', ['http://identifiers.org/kegg.compound/C00291'])
IGNORED_COMPOUNDS.add_species('Cl', 'chloride ion', ['http://identifiers.org/kegg.compound/C00698'])
IGNORED_COMPOUNDS.add_species('Fe2', 'iron(II) ion', ['http://identifiers.org/kegg.compound/C14818'])
IGNORED_COMPOUNDS.add_species('Fe3', 'iron(III) ion', ['http://identifiers.org/kegg.compound/C14819'])

# HCL
# H2SE
