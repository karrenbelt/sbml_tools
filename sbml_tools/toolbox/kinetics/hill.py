
from typing import Optional
from sbml_tools.wrappers import SpeciesReference
from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.kinetics.kinetic_base import KineticBase


class Hill(KineticBase):
    """
    Non-mechanistic rate law

    e.g. used for transcription and translation

    sbo terms: 192, 195, 198

    NOTE: genes do not change in amount of concentration and should be set as boundary = True
    """
    def __init__(self, reaction, create_local_parameters=False):
        super().__init__(reaction, create_local_parameters)

    def ks(self, species: SpeciesReference):
        """half saturation constant"""
        return self._create_parameter('ks', species)

    @property
    def w(self):
        """hill coefficient"""
        return self._create_parameter('w')

    def kcatf(self, catalyst: SpeciesReference = None):
        """forward rate constant"""
        return self._create_parameter('kcatf', catalyst)

    @property
    def keq(self):
        """equilibrium constant"""
        return self._create_parameter('keq')

    def check_applicability(self) -> Optional[str]:
        return

    def create_equation(self):  # Uni Uni
        rates = []
        reactant = self.reactants[0]
        product = self.products[0]
        for i in range(max(1, len(self.catalysts))):
            catalyst = self.catalysts[i] if self.catalysts else None
            # translation
            rate = (ASTNode(name=reactant.species) ** self.w /
                    (ASTNode(name=reactant.species) ** self.w + self.ks(self.reactants[0]) ** self.w))

            # transcription
            rate = 1 - rate

            self.kcatf(catalyst) * rate

            # if self.reversible:
            #     rate = 1 - ASTNode(name=product.species) / (self.keq * ASTNode(name=reactant.species))
            #     rate = ASTNode(name=catalyst.species) * ASTNode(name=reactant.species) / self.ks(self.reactants[0]) * rate
            # optional enzyme

        return


def test():

    from sbml_tools.toolbox.kinetics.test_case_models import simple_reactions_model

    doc = simple_reactions_model()
    self = Hill(doc.model.reactions[4])


    # example: translation


    # example: transcription




