
from typing import Optional
import operator
from functools import reduce
from sbml_tools.wrappers import SpeciesReference
from sbml_tools.toolbox.math import ASTNode
from sbml_tools.toolbox.kinetics import GeneralizedMassAction


# TODO: introduce two regulation parameters


class MichaelisMenten(GeneralizedMassAction):
    """
    Uni-Uni Henry-Michaelis-Menten / Van Slyke-Cullen / Briggs-Haldane Kinetics

    In principle this rate law can be covered by convenience kinetics,
    however it allows for modelling more specific types of inhibition.

    - no inhibition (sbo terms:  28, 29, 30, 31)
    - mixed inhibition (sbo term: 265)
    - competitive inhibition (sbo term: 260)
    - uncompetitive inhibition (sbo term: 262)
    - non-competitive inhibition (sbo term: 266)

    We formulate a model for mixed inhibition, from which competitive, uncompetitive and non-competitive can be
    obtained by settings the parameters accordingly.
    """
    def __init__(self, reaction, create_local_parameters=False):
        super().__init__(reaction, create_local_parameters)

    def check_applicability(self) -> Optional[str]:
        if not self.order_reactants == 1:
            return 'incorrect stoichiometry of reactants'
        if not self.order_products == 1 and self.reversible:
            return 'incorrect stoichiometry of products'
        if not self.enzymes:
            return 'reaction has no modifier assigned as enzyme'

    # def explicit_numerator(self):
    #     return (self.kcatf(self.enzymes[0].species) * reduce(operator.mul, [prime(s_ref.species) for s_ref in self.reactants])
    #             - self.kcatr(self.enzymes[0].species) * reduce(operator.mul, [prime(s_ref.species) for s_ref in self.products]))

    def km(self, species: SpeciesReference):
        """michaelis constant"""
        return self._create_parameter('km', species)

    def create_equation(self) -> ASTNode:
        rates = []
        reactant = self.reactants[0]
        product = self.products[0]
        # modifiers = self.modifiers.copy()  # so we can pop them
        for enzyme in self.enzymes:
            if self.reversible:
                numerator = (self.kcatf(enzyme) * ASTNode(name=reactant.species) / self.km(reactant)
                             - self.kcatr(enzyme) * ASTNode(name=product.species) / self.km(product))
                denominator = (1 + ASTNode(name=reactant.species) / self.km(reactant)
                               + ASTNode(name=product.species) / self.km(product))

                # for modifier in self.competitive_inhibitors:
                #     denominator += ASTNode(name=modifier.species) / self.ki(modifier.species)
                # for modifier in self.uncompetitive_inhibitors:
                #     pass  # requires both modes; need two parameters
                # for modifier in self.allosteric_inhibitors:
                #     denominator *= 1 + ASTNode(name=modifier.species) / self.ki(modifier.species)
            else:
                numerator = self.kcatf(enzyme.species) * ASTNode(name=reactant.species)
                denominator = ASTNode(name=f'km_{reactant.species}') + ASTNode(name=reactant.species)

            rates += [ASTNode(name=enzyme.species) * numerator / denominator]

        rates = ASTNode.add(*rates) if len(rates) > 1 else rates[0]
        terms = [self._activation_factors, self._inhibition_factors, rates]
        eq = reduce(operator.mul, filter(None, terms))
        return eq





