
import inspect
from sbml_tools import config
from sbml_tools.wrappers import SBMLDocument, Reaction, ModifierSpeciesReference, Unit, Trigger
from sbml_tools.utils import NAME_TO_SBO_TERM
from sbml_tools.toolbox import SemanticTestCases
from sbml_tools.toolbox import Tables


TEST_CASES = SemanticTestCases()

# for case in TEST_CASES.select_by_test_tags('assigned_variable_stoichiometry'):
#     if '3.2' in case.levels:
#         break
# doc = case.load_document()


# simple_reactions_model
# simple_reactions_model_with_modifiers
# complex_species_model
# simple_two_compartment_model
# simple_membrane_passive_transport_model
# simple_membrane_active_transport_model


def setup_model(model_id: str):
    # base model setup for tests
    doc = SBMLDocument()
    model = doc.create_model(id=model_id)
    model.time_units = 'second'
    model.substance_units = 'mole'
    model.extent_units = 'mole'
    model.length_units = 'metre'
    model.area_units = 'metre2'
    model.volume_units = 'litre'
    return doc


def setup_populated_model(model_id: str,
                          n_function_definition=10,
                          n_unit_definitions=10,
                          n_compartments=10,
                          n_species=10,
                          n_parameters=10,
                          n_initial_assignments=10,
                          n_algebraic_rules=10,
                          n_assignment_rules=10,
                          n_rate_rules=10,
                          n_constraints=10,
                          n_reactions=10,
                          n_events=10,
                          ):
    doc = setup_model(model_id)

    for i in range(n_function_definition):
        doc.model.create_function_definition(id=f'f{i + 1}')

    for i in range(len(doc.model.unit_definitions), n_unit_definitions):
        units = [Unit(kind='second', exponent=i, scale=0, multiplier=1.0)]
        doc.model.create_unit_definition(id=f'u{i + 1}', units=units)

    for i in range(n_compartments):
        doc.model.create_compartment(id=f'c{i + 1}', constant=True, spatial_dimensions=3, size=1.0)

    for i in range(n_species):
        doc.model.create_species(id=f's{i + 1}', compartment='c1', has_only_substance_units=True,
                                 boundary_condition=False, constant=False)

    for i in range(n_parameters):
        doc.model.create_parameter(id=f'p{i + 1}', constant=True, value=1.0, units='per_second')

    for i in range(n_initial_assignments):
        doc.model.create_initial_assignment(symbol=f'i{i + 1}')

    for i in range(n_algebraic_rules):
        doc.model.create_algebraic_rule()

    for i in range(n_assignment_rules):
        doc.model.create_assignment_rule(variable=f'ar{i + 1}')

    for i in range(n_rate_rules):
        doc.model.create_rate_rule(variable=f'rr{i + 1}')

    for i in range(n_constraints):
        doc.model.create_constraint()

    for i in range(n_reactions):
        doc.model.create_reaction(id=f'r{i + 1}', reversible=True)

    for i in range(n_events):
        trigger = Trigger(initial_value=True, persistent=False)
        doc.model.create_event(id=f'e{i + 1}', use_values_from_trigger_time=True, trigger=trigger)

    return doc


def simple_reactions_model():
    """
    purpose is to test the formulation of kinetic laws in accordance with:
    - the number of reactants and products
    """
    doc = setup_populated_model(inspect.currentframe().f_code.co_name)
    doc.model.remove_reactions()

    # empty
    doc.model.create_reaction(id='r1', reversible=False)

    # only reactants / products
    doc.model.create_reaction(id='r2', reversible=False, reactants='s1')
    doc.model.create_reaction(id='r3', reversible=False, products='s1')

    # uni-uni
    doc.model.create_reaction(id='r4', reversible=False, reactants='s1', products='s2')

    # bi-uni
    doc.model.create_reaction(id='r5', reversible=False, reactants='2.0 s1', products='s2')
    doc.model.create_reaction(id='r6', reversible=False, reactants='s1 + s2', products='s3')

    # uni-bi
    doc.model.create_reaction(id='r7', reversible=False, reactants='s1', products='2 s2')
    doc.model.create_reaction(id='r8', reversible=False, reactants='s1', products='s2 + s3')

    # non-integer stoichiometry
    doc.model.create_reaction(id='r10', reversible=False, reactants='0.1 s1 + s2', products='0.3 s3')

    # zero stoichiometry - now yields error: stoichiometries must be strictly positive
    # doc.model.create_reaction(id='r11', reversible=False, reactants='0 s1', products='0 s2')

    return doc


def reactions_model_with_modifiers():
    """
    simple reactions model with catalysts as modifiers
    purpose: to test reactions with
    - one or more catalysts
    - one or more modifiers (activators / inhibitors)
    - a combination of the two

    NOTE: sbo terms on the species and species references can be changed for further testing
    """
    doc = setup_populated_model('reactions_model_with_modifiers')
    doc.model.remove_reactions()
    r1 = doc.model.create_reaction(id='r1', reversible=False, reactants='s1', products='s2', modifiers='s3')
    r2 = doc.model.create_reaction(id='r2', reversible=False, reactants='s1', products='s2', modifiers='s3, s4')
    r3 = doc.model.create_reaction(id='r3', reversible=False, reactants='s1', products='s2',  modifiers='s3, s4, s5')

    return doc


def complex_species_model():
    """
    purpose: to test simple reactions involving all combinations of constant and boundary attributes:
    - one reaction that generates s1 from nothing, where s1 is a constant         boundary species
    - one reaction that generates s2 from s1     , where s2 is a non-constant non-boundary species
    - one reaction that generates s4 from s2     , where s3 is a     constant non-boundary species
    - one reaction that eliminates s4            , where s4 is a non-constant     boundary species
    """
    doc = setup_model(inspect.currentframe().f_code.co_name)

    c1 = doc.model.create_compartment(id='c1', constant=True, spatial_dimensions=3, size=1.0)

    s1 = doc.model.create_species(id='s1', compartment='c1',
                                  has_only_substance_units=True, boundary_condition=True, constant=True)

    s2 = doc.model.create_species(id='s2', compartment='c1',
                                  has_only_substance_units=True, boundary_condition=False, constant=False)

    s3 = doc.model.create_species(id='s3', compartment='c1',
                                  has_only_substance_units=True, boundary_condition=False, constant=True)

    s4 = doc.model.create_species(id='s4', compartment='c1',
                                  has_only_substance_units=True, boundary_condition=True, constant=False)

    # assert s3.can_be_reactant_or_product is False
    r1 = doc.model.create_reaction(id='r1', reversible=False, products='s1')
    r2 = doc.model.create_reaction(id='r2', reversible=False, reactants='s1', products='s2')
    r3 = doc.model.create_reaction(id='r3', reversible=True, reactants='s2', products='s4', modifiers='s3')
    r4 = doc.model.create_reaction(id='r4', reversible=False, reactants='s4')

    return doc


def simple_two_compartment_model():
    """
    purpose: to test generation of rate laws involving species in different compartments of a different size
    - one reaction involving species amounts
    - one reaction involving species concentrations
    """
    doc = setup_model(inspect.currentframe().f_code.co_name)
    model = doc.model

    c1 = model.create_compartment(id='c1', constant=True, spatial_dimensions=3, size=1.0)
    c2 = model.create_compartment(id='c2', constant=True, spatial_dimensions=3, size=2.0)

    s1 = model.create_species(id='s1', compartment='c1', initial_amount=1.0,
                              has_only_substance_units=True, boundary_condition=False, constant=False)
    s2 = model.create_species(id='s2', compartment='c2', initial_amount=0.0,
                              has_only_substance_units=True, boundary_condition=False, constant=False)

    s3 = model.create_species(id='s3', compartment='c1', initial_concentration=1.0,
                              has_only_substance_units=False, boundary_condition=False, constant=False)
    s4 = model.create_species(id='s4', compartment='c2', initial_concentration=0.0,
                              has_only_substance_units=False, boundary_condition=False, constant=False)

    r1 = model.create_reaction(id='r1', reversible=False, reactants='s1', products='s2')
    r2 = model.create_reaction(id='r2', reversible=False, reactants='s3', products='s4')

    return doc


# def simple_membrane_passive_transport_model():
#     """
#     purpose is to test generation of rate laws involving passive transport over a membrane
#     - two 3D compartments, one 2D compartment (membrane)
#
#     1. simple diffusion:
#     - through lipid bilayer (e.g. oxygen, carbon dioxide, fat-soluble vitamins)
#
#     2. facilitated diffusion
#     - through lipoprotein channel (e.g. highly selective sodium or potassium channel, voltage or chemical gated)
#
#     3. filtration
#
#     4. osmosis
#
#     test the effect of:
#     - membrane surface area and permeability, gradients (chemical, electric, pressure)
#
#     """
#     doc = setup_model(inspect.currentframe().f_code.co_name)
#     model = doc.model
#
#     c1 = model.create_compartment(id='c1', constant=True, spatial_dimensions=3)  # litre
#     c2 = model.create_compartment(id='c2', constant=True, spatial_dimensions=2)  # metre2
#     c3 = model.create_compartment(id='c3', constant=True, spatial_dimensions=3)  # litre
#
#     s1 = model.create_species(id='s1', compartment='c1',
#                               has_only_substance_units=True, boundary_condition=False, constant=False)
#     s2 = model.create_species(id='s2', compartment='c3',
#                               has_only_substance_units=True, boundary_condition=False, constant=False)
#
#     return doc
#
#
# def simple_membrane_active_transport_model():
#     """
#     purpose is to test generation of rate laws involving facilitated transport over a membrane
#     - two 3D compartments, one 2D compartment (membrane)
#     - species in the membrane to facilitate transport
#
#     active transport:
#     - primary (ATP-powered pumps, e.g. sodium-potassium pump, proton-pump)
#     - antiporter (e.g. sodium-calcium exchanger)
#     - symporter (e.g.  sodium-glucose linked transporter)
#     """
#     doc = setup_model(inspect.currentframe().f_code.co_name)
#     model = doc.model
#
#     c1 = model.create_compartment(id='c1', constant=True, spatial_dimensions=3)  # litre
#     c2 = model.create_compartment(id='c2', constant=True, spatial_dimensions=2)  # metre2
#     c3 = model.create_compartment(id='c3', constant=True, spatial_dimensions=3)  # litre
#
#     s1 = model.create_species(id='s1', compartment='c1',
#                               has_only_substance_units=True, boundary_condition=False, constant=False)
#     s2 = model.create_species(id='s2', compartment='c2',
#                               has_only_substance_units=True, boundary_condition=False, constant=False)
#     s2 = model.create_species(id='s3', compartment='c3',
#                               has_only_substance_units=True, boundary_condition=False, constant=False)
#
#     r1 = model.create_reaction(id='r1', reversible=False,
#                                reactants='s1', products='s2',
#                                )
#
#     return doc

def test_substitute_lambda():
    from sbml_tools.wrappers.utils import substitute_lambda
    from sbml_tools.wrappers import ASTNode

    # lambda - lambda substitution
    f1 = ASTNode.from_string('lambda(x, f2(x))')
    f2 = ASTNode.from_string('lambda(x, f3(x))')
    f3 = ASTNode.from_string('lambda(x, true)')
    self, symbol, other = f1, 'f2', f2
    substitute_lambda(self, symbol, other)
    assert f1 == ASTNode.from_string('lambda(x, f3(x))')

    self, symbol, other = f2, 'f3', f3
    substitute_lambda(self, symbol, other)
    assert f2 == ASTNode.from_string('lambda(x, true)')

    # math - lambda substitution
    f1 = ASTNode.from_string('lambda(x, x + 1)')
    math = ASTNode.from_string('f1(y)')
    self, symbol, other = math, 'f1', f1
    substitute_lambda(self, symbol, other)
    assert math == ASTNode.from_string('y + 1')

    f1 = ASTNode.from_string('lambda(x, x + 1)')
    math = ASTNode.from_string('f1(f1(y) + f1(z))')
    self, symbol, other = math, 'f1', f1
    substitute_lambda(self, symbol, other)
    assert math == ASTNode.from_string('y + 1 + (z + 1) + 1')

    f1 = ASTNode.from_string('lambda(x, x + 1)')
    math = ASTNode.from_string('f1(f1(y) / f1(z))')
    self, symbol, other = math, 'f1', f1
    substitute_lambda(self, symbol, other)
    assert math == ASTNode.from_string('(y + 1) / (z + 1) + 1')


def function_definitions_model():  #
    from sbml_tools.wrappers import ASTNode
    doc = setup_populated_model('initial_assignments')
    doc.model.function_definitions['f1'].math = 'lambda(x, f2(x))'
    doc.model.function_definitions['f2'].math = 'lambda(x, f3(x))'
    doc.model.function_definitions['f3'].math = 'lambda(x, true)'

    doc.model.function_definitions.substitute_dependencies()
    expected = ASTNode.from_string('lambda(x, true)')
    assert doc.model.function_definitions['f1'].math == expected

    doc.model.function_definitions['f1'].math = 'lambda(a, a + 1)'
    doc.model.function_definitions['f2'].math = 'lambda(b, b * 2)'
    doc.model.function_definitions['f3'].math = 'lambda(c, f1(c) / 3)'
    doc.model.function_definitions['f4'].math = 'lambda(d, f2(d) - 4)'
    doc.model.function_definitions['f5'].math = 'lambda(e, f3(f4(e)))'
    doc.model.function_definitions.substitute_dependencies()
    expected = ASTNode.from_string('lambda(e, (e * 2 - 4) + 1) / 3 ) ')
    assert doc.model.function_definitions['f5'].math == expected


def initial_assignment_model():
    doc = setup_populated_model('initial_assignments')
    doc.model.remove_initial_assignments()
    doc.model['r1'].reactants = 's1'
    for symbol in ('c1', 's1', 's1_reference', 'p1'):
        initial_assignment = doc.model.create_initial_assignment(symbol=symbol, math='0')
        assert doc.model.get(symbol) == initial_assignment.object

    # test make the assignment; replace all symbols in equations with the math element
    # we will not evaluate the ast node, so we can only substitute (the children of) another ast node
    from sbml_tools.wrappers import ASTNode
    node = ASTNode.from_string('c1 * s1 * p1')  # not sure how to inline variable stoichiometries
    other = ASTNode.from_string('piecewise(0, x > y, 1)')  # 0 if x > y else 1
    node.replace('c1', other)

    for initial_assignment in doc.model.initial_assignments:
        assignee = doc.model[initial_assignment.symbol]
        # if compartment, then size
        # if species, then amount / concentration
        # if species_reference, then stoichiometry
        # if parameter, then value


def rules_model():
    doc = setup_populated_model('rules')
    doc.model.create_compartment(id=f'c2', constant=True, spatial_dimensions=3, size=1.0)
    doc.model.create_parameter(id='p1', constant=True, value=1.0, units='per_second')
    doc.model.create_reaction(id='r1', reversible=True, reactants='s1 + s2', products='s3 + s4', modifiers='s5')

    doc.model.create_algebraic_rule(math='1')
    doc.model.create_assignment_rule(variable='c1', math='0')
    doc.model.create_rate_rule(variable='s1', math='0')

