import copy
import pickle
import warnings
import itertools
from typing import Optional, Union

import numpy as np
import pandas as pd
import libsbml
import matplotlib.pyplot as plt
import stopit
import roadrunner

from sbml_tools import settings, read_sbml, write_sbml, check
from sbml_tools.utils import LineGraph
from sbml_tools import PickableABC
# from rrplugins import Plugin, readAllText

roadrunner.Config.setValue(roadrunner.Config.ALLOW_EVENTS_IN_STEADY_STATE_CALCULATIONS, True)

flatten_list = lambda l: [item for sublist in l for item in sublist]

LOGGER_LEVELS = {
    'fatal': 1,
    'critical': 2,
    'error': 3,
    'warnings': 4,
    'notice': 5,
    'information': 6,
    'debug': 7,
    'trace': 8,
    }

# @staticmethod
def set_matrix_format(named_array, fmt='pandas', index=None): # TODO: maybe not implement this
    # strip: [], ', ee(), uee(), cc(), ucc(), eigen(), eigenReal(), eigenImag(), init(), stoich()
    if 'fmt' == None:
        return named_array

    if fmt == 'numpy':
        return np.array(named_array.data.tolist())

    elif fmt == 'pandas':

        if named_array.shape[0] == 1:
            if index is None:
                index = named_array.colnames
            return pd.Series(named_array.data.tolist()[0], index=index)

        if index is None:
            index = named_array.rownames
        return pd.DataFrame(named_array.data.tolist(), columns=named_array.colnames, index=index)

    else:
        raise ValueError(f'unrecognized matrix format {type(fmt)}')


def check_local_parameters(doc, promote_local_parameters):
    local_parameters = [p for r in doc.model.reactions if r.isSetKineticLaw()
                        for p in r.getKineticLaw().local_parameters]
    if local_parameters:
        if not promote_local_parameters:
            warnings.warn(f"local parameters present, cannot be set in roadrunner, and they shadow globals")
            # for the sake of consistency; this is done with species and rates as well. Roadrunner crashes otherwise
            for p in local_parameters:
                if np.isnan(p.value):
                    warnings.warn(f"local parameter {p.id} has a nan value assigned. Setting value to 0.0")
                    check(p.setValue(0.0))
        else:
            convProps = libsbml.ConversionProperties()
            convProps.addOption("promoteLocalParameters", True, "Promotes all Local Parameters to Global ones")
            check(doc.convert(convProps))
            print('promoted local parameters to global parameters')
    return


def check_kinetic_laws(doc):
    for reaction in doc.model.reactions:
        kinetic_law = reaction.getKineticLaw()
        if kinetic_law: # if non-existent, roadrunner will set them to zero
            if not kinetic_law.isSetMath(): # but if existent but without, sbml is valid, yet roadrunner crashes
                check(kinetic_law.setMath(libsbml.parseL3Formula('0')))
    return


class RoadRunnerWrapper:
    """
    Wraps a RoadRunner instance.
    TODO & NOTES:
        2. highly recommended against using models with local parameters, as these cannot be accessed through roadrunner
        3. if you set the compartment size, this will affect the concentrations
        4. one needs to reset the model after simulation otherwise the final state will be preserved and used in the next
    """

    # some (all?) can be set in time_course_selections
    identifiable = ('compartment', 'global_parameter', 'floating_species', 'reaction', 'event',
                    'derivative', 'control_coefficient', 'floating_species_concentration', 'boundary_species')

    def __init__(self, sbml : Optional[Union[str, libsbml.SBMLDocument]], promote_local_parameters = True):
        if isinstance(sbml, libsbml.SBMLDocument):
            doc = sbml
        else: # if isinstance(sbml, str):
            doc = read_sbml(sbml) # deals with .gz

        check_local_parameters(doc, promote_local_parameters)
        check_kinetic_laws(doc) # roadrunner will crash if not existent

        # for the sake of consistency; this is done with species and rates as well. Otherwise crashes / out of range
        for p in doc.model.parameters:
            if np.isnan(p.value):
                warnings.warn(f"parameter {p.id} has a nan value assigned. Setting value to 0.0")
                check(p.setValue(0.0))

        self._rr = roadrunner.RoadRunner(doc.toSBML())
        self._y_hat = None

    @property
    def unpickable(self):
        return ('_rr')

    def save(self, filepath):
        obj = self.copy()
        obj._rr = obj._rr.getCurrentSBML()
        with open(filepath, 'wb') as f:
            pickle.dump(obj, f)

    @classmethod
    def load(cls, filepath):
        with open(filepath, 'rb') as f:
            obj = pickle.load(f)
        return cls(obj._rr)

    def copy(self):
        obj = copy.deepcopy(self)
        setattr(obj, '_rr', roadrunner.RoadRunner(self._rr.getCurrentSBML()))
        return obj

    def disable_logging(self): # useful for optimizations in which many simulations might fail
        roadrunner.Logger.disableLogging()

    def enable_logging(self):
        roadrunner.Logger.enableConsoleLogging()

    @property
    def logger_level(self):
        return {v:k for k,v in LOGGER_LEVELS.items()}[roadrunner.Logger.getLevel()]

    @logger_level.setter
    def logger_level(self, level: Union[str, int]):
        if isinstance(level, str):
            level = LOGGER_LEVELS[level]
        roadrunner.Logger.setLevel(level)

    @property
    def _all_ids(self):
        return [getattr(self, a + '_ids') for a in self.identifiable]

    @property
    def all_ids(self):
        return flatten_list(self._all_ids)

    # solvers
    @property
    def integrator(self):
        return self._rr.getIntegrator()

    @integrator.setter
    def integrator(self, integrator : str):
        self._rr.setIntegrator(integrator)

    @property
    def solver_settings(self):
        return pd.Series({setting: self.integrator.getSetting(setting) for setting in self.integrator.getSettings()})

    @solver_settings.setter
    def solver_settings(self, settings : dict): # does not seem to check if settings are correct
        for setting, value in settings.items():
            self.integrator.setSetting(setting, value)

    def reset_solver_settings(self):
        self.integrator.resetSettings()

    @property
    def steady_state_solver(self): # there are only nleq1 and nleq2 (default), no need for a setter
        return self._rr.getSteadyStateSolver()

    # model attributes
    @property
    def model(self):
        if self._rr.model is None:
            raise ValueError(f"no model associated")
        return self._rr.model

    @property
    def compartment_ids(self):
        return self.model.getCompartmentIds()

    @property
    def global_parameter_ids(self): # TODO: local parameters can not be accessed through python
        return self.model.getGlobalParameterIds()

    @property
    def floating_species_ids(self):
        return self.model.getFloatingSpeciesIds()

    @property
    def floating_species_concentration_ids(self):
        return list(map(lambda s: f"[{s}]", self.floating_species_ids))

    @property
    def boundary_species_ids(self):
        return self.model.getBoundarySpeciesIds()

    @property
    def species_ids(self):
        return self.floating_species_ids + self.boundary_species_ids

    @property
    def reaction_ids(self):
        return self.model.getReactionIds()

    @property
    def event_ids(self):
        return self.model.getEventIds()

    @property
    def derivative_ids(self):
        return list(map(lambda s: f"{s}'", self.floating_species_ids))

    @property
    def control_coefficient_ids(self):
        return [f'cc({x[0]}, {x[1]})' for x in itertools.product(self.floating_species_ids, self.global_parameter_ids)]

    @property
    def initial_conditions(self):
        """return the initial values from the moment of initialization,
        ignores values being set after that, but before simulation"""
        return pd.Series(dict(zip(self.floating_species_ids, self._rr.model.getFloatingSpeciesInitConcentrations())))

    @property
    def current_conditions(self):
        return pd.Series(dict(zip(self.floating_species_ids, self._rr.model.getFloatingSpeciesConcentrations())))

    @property
    def global_parameter_values(self):
        return pd.Series(dict(zip(self.global_parameter_ids, self._rr.model.getGlobalParameterValues())))

    ### Stoichiometry
    @property
    def stoichiometry(self):
        return self._rr.getFullStoichiometryMatrix()

    @property
    def reduced_stoichiometry(self):
        return self._rr.getReducedStoichiometryMatrix()

    @property
    def extended_stoichiometry(self):
        return self._rr.getExtendedStoichiometryMatrix()

    ### Steady-state
    @property
    def is_conserved_moiety_analysis_enabled(self):
        return self._rr.conservedMoietyAnalysis

    def enable_conserved_moiety_analysis(self):
        """needed to solve for steady states in the presence of moiety conserved cycles in the model"""
        self._rr.conservedMoietyAnalysis = True

    def disable_conserved_moiety_analysis(self):
        self._rr.conservedMoietyAnalysis = False

    @property
    def steady_state(self):
        return self._rr.steadyState()

    @property
    def steady_state_values(self):
        ss = self._rr.getSteadyStateValuesNamedArray()
        return pd.Series(ss.data.tolist()[0], index = map(lambda x: x[1:-1], ss.colnames))

    ### Kinetics properties
    @property
    def full_jacobian(self):
        j = self._rr.getFullJacobian()
        return pd.DataFrame(j.data.tolist(), columns=j.colnames, index=j.rownames)

    @property
    def reduced_jacobian(self):
        j = self._rr.getReducedJacobian()
        return pd.DataFrame(j.data.tolist(), columns=j.colnames, index=j.rownames)

    @property
    def full_eigenvalues(self):
        ids = [x[6:-1] for x in self._rr.getEigenValueIds()[::3]]
        return pd.Series(self._rr.getFullEigenValues(), index=ids)

    @property
    def reduced_eigenvalues(self):
        ids = [x[6:-1] for x in self._rr.getEigenValueIds()[::3]]
        return pd.Series(self._rr.getReducedEigenValues(), index=ids)

    @property
    def is_stable(self):
        return all([_.real <= 0 for _ in self._rr.getFullEigenValues()])

    @property
    def derivatives(self):
        species = self._rr.getFloatingSpeciesConcentrationsNamedArray().colnames
        return pd.Series(self._rr.getRatesOfChange(), index=species)

    @property
    def reaction_rates(self):
        rates = self._rr.getReactionRates()
        return pd.Series(rates.data.tolist(), index=self.reaction_ids)

    # Metabolic Control Analysis (MCA)
    @property
    def species_elasticities(self):
        e = self._rr.getUnscaledElasticityMatrix()
        return pd.DataFrame(e.data.tolist(), columns=e.colnames, index=e.rownames)

    @property
    def scaled_species_elasticities(self):
        e = self._rr.getScaledElasticityMatrix()
        return pd.DataFrame(e.data.tolist(), columns=e.colnames, index=e.rownames)

    def get_control_coefficient(self, state_or_flux, parameter) -> float:
        return self._rr.getCC(state_or_flux, parameter)

    def get_elasticity_coefficient(self, reaction, parameter_or_species) -> float:
        return self._rr.getEE(reaction, parameter_or_species)

    @property
    def flux_control_coefficients(self):
        cc = self._rr.getUnscaledFluxControlCoefficientMatrix()
        return pd.DataFrame(cc.data.tolist(), columns=cc.colnames, index=cc.rownames)

    @property
    def scaled_flux_control_coefficients(self):  # self._rr.getReducedEigenValues()
        cc = self._rr.getScaledFluxControlCoefficientMatrix()
        return pd.DataFrame(cc.data.tolist(), columns=cc.colnames, index=cc.rownames)

    @property
    def concentration_control_coefficients(self):
        return self._rr.getUnscaledConcentrationControlCoefficientMatrix()

    @property
    def scaled_concentration_control_coefficients(self):
        return self._rr.getScaledConcentrationControlCoefficientMatrix()

    # Simulation
    @property
    def time_course_selections(self) -> list:
        return self._rr.timeCourseSelections

    @time_course_selections.setter
    def time_course_selections(self, time_course_selections : list, force_to_include_time=True):
        if force_to_include_time and 'time' not in time_course_selections:
            time_course_selections.insert(0, 'time')
        self._rr.timeCourseSelections = time_course_selections

    @property
    def time_course_selection_per_type(self):
        d = dict(zip(self.identifiable, self._all_ids))
        per_type = {}
        for k, ids in d.items():
            l = set(self.time_course_selections).intersection(ids)
            if l: per_type[k] = l
        return per_type

    def reset(self):
        """resets the model to the state of roadrunner initialization, 
        by default the final state of the simulation is kept"""
        self._rr.reset()

    def assign_values(self, values):
        """can only assign values to variables / parameters that occur in the model"""
        any(self._rr.model.setValue(k, v) for k,v in values.items())

    @property
    def y_hat(self):
        return self._y_hat

    def simulate(self, initial_values=None, start=0, end=5, points=None, selections=None, steps=None,
                 max_runtime=30, seed=None):

        if points is steps is None:
            steps = 50

        if seed is not None:
            self._rr.getIntegrator().seed = seed

        if initial_values is not None:
            self.assign_values(initial_values)

        # roadrunner might get stuck, in which case we will return None
        with stopit.ThreadingTimeout(max_runtime) as to_ctx_mgr:
            assert to_ctx_mgr.state == to_ctx_mgr.EXECUTING

            try: # NOTE: do not associated NamedArrays with the object (unpickable)
                y_hat = self._rr.simulate(start, end, points, selections, steps)
                y_hat = pd.DataFrame(y_hat.data.tolist(), columns=y_hat.colnames)
            except Exception as e:
                y_hat = None # otherwise the previous simulation might still be here
                print(e) # disable this with roadrunner.Logger settings

        if not to_ctx_mgr.state == to_ctx_mgr.EXECUTED: # code: 0
            print(f'stopit timeout on simulation, code: {to_ctx_mgr.state}')

        self._y_hat = y_hat
        return y_hat

    def fit(self, cost_function, **kwargs):
        raise NotImplementedError()
        
    def plot(self):  # TODO: main plotting should be outside of this class

        y_hat = self.y_hat
        ts_selections = self.time_course_selection_per_type

        if y_hat is None:
            raise ValueError(f"successfully simulate the model first")

        time = y_hat['time']
        selected_not_simulated = set(self.time_course_selections).difference(y_hat.columns)
        if selected_not_simulated:
            raise ValueError(f'not all time_course_selections have been simulated:\n{selected_not_simulated}' +
                             '\nselect time courses first before you simulate the model')

        max_per_row = 3
        n_rows = int(np.ceil(len(ts_selections) / max_per_row))
        new = LineGraph(subplots=(n_rows, max_per_row), figsize=(14, 6))
        for i, (name, ids) in enumerate(ts_selections.items()):
            for id in ids:
                new.plot_line(time, y_hat[id], label=id, ax_idx=i)
            new.fig.axes[i].title.set_text(name)

        plt.tight_layout()

    def to_sbml_document(self, level: Optional[int] = None, version: Optional[int] = None) -> libsbml.SBMLDocument:
        """ new instance doc with current roadrunner parametrization"""
        if level or version:
            return read_sbml(self._rr.getCurrentSBML(level, version))
        return read_sbml(self._rr.getCurrentSBML())

    def write_model(self, filename, overwrite=False, level = None, version = None):
        doc = self.to_sbml_document(level, version)
        write_sbml(doc, filename, overwrite=overwrite)

    def __eq__(self, other):
        if not isinstance(other, self.__class__):
            return False
        if not self.__dict__.keys() == other.__dict__.keys():
            return False
        for k, v in self.__dict__.items():
            if k == '_rr':
                if not self._rr.getCurrentSBML() == other._rr.getCurrentSBML():
                    return False
            elif not v == other.__dict__.get(k):
                return False
        return True

    def __ne__(self, other):
        return not self.__eq__(other)


def test():

    r = RoadRunnerWrapper(settings.ECOLI_CORE_MODEL)
    r = RoadRunnerWrapper(settings.CHASSAGNOLE_MODEL)
    r.time_course_selections = (['time'] + r.floating_species_concentration_ids + r.derivative_ids
                                + r.reaction_ids + r.boundary_species_ids)
    r.simulate()
    r.plot()
    plt.show()

    ## testing new feature (local param detection, nan-values


    #plt.show()
