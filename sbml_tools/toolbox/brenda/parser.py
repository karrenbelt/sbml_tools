
import os
import re
from typing import List, Dict, Iterable, Optional, Union
import pickle

import numpy as np
import pandas as pd
from tqdm import tqdm
import collections
from sbml_tools import utils, settings
from sbml_tools.toolbox.brenda import regex, owl
from cached_property import cached_property
import munch
from sbml_tools.toolbox.ncbi import Taxonomy
# from sbml_tools.wrappers.annotations import MIRIAM, URL_TO_MIRIAM_IDX
import logging
logger = utils.setup_logger(settings.LOG_DIR, __name__)
logger.setLevel(logging.INFO)

# MIRIAM[URL_TO_MIRIAM_IDX['http://identifiers.org/ec-code/']]['pattern']
# MIRIAM[URL_TO_MIRIAM_IDX['http://identifiers.org/uniprot/']]['pattern']
# MIRIAM[URL_TO_MIRIAM_IDX['http://identifiers.org/swiss-model/']]
# MIRIAM[URL_TO_MIRIAM_IDX['http://identifiers.org/chebi/']]

# TODO: SN translate to CHEBI?
## for each EC_number, generate individual proteins
## '<0x96>' sometimes found in data. encoded using windows-1252 codec (outdated latin codec). encoding='cp1252'?

CHEBI = owl.get_chebi()


def compound_to_chebi(name: str) -> Optional[str]:
    entry = CHEBI.get(name)
    if entry:
        return entry['key'].replace('_', ':')


# The following attributes on proteins may be retrieved:
BRENDA_ATTRS = dict(
    PR='PROTEIN',
    RN='RECOMMENDED_NAME',
    SN='SYSTEMATIC_NAME',
    SY='SYNONYMS',
    RE='REACTION',
    RT='REACTION_TYPE',
    ST='SOURCE_TISSUE',
    LO='LOCALIZATION',
    NSP='NATURAL_SUBSTRATE_PRODUCT',
    SP='SUBSTRATE_PRODUCT',
    TN='TURNOVER_NUMBER',
    KM='KM_VALUE',
    PHO='PH_OPTIMUM',
    PHR='PH_RANGE',
    PHS='PH_STABILITY',
    SA='SPECIFIC_ACTIVITY',
    TO='TEMPERATURE_OPTIMUM',
    TR='TEMPERATURE_RANGE:',
    CF='COFACTOR',
    AC='ACTIVATING_COMPOUND',
    IN='INHIBITORS',
    KI='KI_VALUE',
    ME='METAL_IONS',
    MW='MOLECULAR_WEIGHT',
    PM='POSTTRANSLATIONAL_MODIFICATION',
    SU='SUBUNITS',
    PI='PI_VALUE',
    AP='APPLICATION',
    EN='ENGINEERING',
    CL='CLONED',
    CR='CRYSTALLIZATION',
    PU='PURIFICATION',
    REN='RENATURED',
    GS='GENERAL_STABILITY',
    OSS='ORGANIC_SOLVENT_STABILITY',
    OS='OXIDATION_STABILITY',
    SS='STORAGE_STABILITY',
    TS='TEMPERATURE_STABILITY',
    RF='REFERENCE',
    KKM='KCAT_KM_VALUE',
    EXP='EXPRESSION',
    IC50='IC50_VALUE',
    )

UNITS = dict(
    # substrate associated values
    TN="1/s",
    KM="mM",
    KI="mM",
    IC50="mM",
    # other values
    PHO="dimensionless",
    PHR="dimensionless",
    PHS="dimensionless",
    SA="µmol/min/mg",
    KKM="1/mM/s",
    TO='°C',
    TR='°C',
    TS='°C',
    MW="dalton",
    PI="dimensionless",
    )

MANUAL_CURATED_ECS = {'4..2.1.52': '4.2.1.52'}


def file_md5_hash(filepath=settings.BRENDA_DOWNLOAD, encoding='utf-8'):
    """calculating the hash of the BRENDA file takes ~1 second"""
    if not os.path.isfile(filepath):
        raise FileNotFoundError(filepath)
    import hashlib
    hasher = hashlib.md5()
    with open(filepath, 'r', encoding=encoding) as f:
        hasher.update(f.read().encode(encoding))
    return hasher.hexdigest()


class Entry:

    def __init__(self, ec_number: str, attr_id: str, attr_data: str, line_no: int):
        self.ec_number = ec_number
        self.attr_id = attr_id
        self.attr_data = attr_data
        self.line_no = line_no
        self.filtered = False  # tag to indicate if the entry was filtered during processing

    def __str__(self):
        return f"#Line {self.line_no}, EC {self.ec_number}, {self.attr_id}: {self.attr_data}"

    def __repr__(self):
        return f"{self.__class__.__name__} {self.ec_number} {self.attr_id} - filtered: {self.filtered}"

    @property
    def key(self):
        return hash(''.join([self.ec_number, self.attr_id, self.attr_data]))


class ParserError(Exception):
    pass


class Protein(munch.Munch):

    def __init__(self, ec_number: str, protein_id: int, species: str, taxonomy_id: Optional[int],
                 database_ids: list, database: str, comment: str, references: List[int]):
        self.ec_number = ec_number
        self.protein_id = protein_id
        self.species = species
        self.taxonomy_id = taxonomy_id
        self.database_ids = database_ids
        self.database = database
        self.comment = comment
        self.references = references

        # RE, RT, SN, RN fields are defined at the level of the EC Number
        for field in ('RF', 'SY', 'ST', 'LO', 'NSP', 'SP', 'TN', 'KM', 'PHO', 'PHR', 'SA', 'TO', 'TR', 'CF', 'AC', 'IN',
                      'KI', 'ME', 'MW', 'PM', 'SU', 'PI', 'CL', 'CR', 'PU', 'REN', 'GS', 'OSS', 'KKM', 'EXP',
                      'AP', 'EN', 'OS', 'PHS', 'SS', 'TS', 'IC50'):
            setattr(self, field, [])

    def __repr__(self):
        return f"{' '.join([self.__class__.__name__] + self.database_ids).strip()}"


class ECNumber(munch.Munch):

    def __init__(self, ec_number: str):
        super().__init__()
        self.ec_number = ec_number
        self.proteins = {}
        self.ambiguous_proteins = {}  # collections.defaultdict(list)
        self.ref2id = {}  # collections.defaultdict(list)

        for field in ('RN', 'SN'):
            setattr(self, field, '')

        for field in ('RE', 'RT', 'SY'):
            setattr(self, field, [])

    def __repr__(self):
        return f"{self.__class__.__name__} {self.ec_number}"


class Parser:

    def __init__(self, filepath=settings.BRENDA_DOWNLOAD, encoding='utf-8'):
        if not os.path.isfile(filepath):
            raise FileNotFoundError(filepath)

        self.filepath = filepath
        self.encoding = encoding

        self._redirect = dict()  # EC numbers that redirect to other EC numbers
        self._deleted = dict()   # deleted EC numbers that do not redirect
        self._ec_data = dict()

        # Used for diagnosis and debugging
        self._statistics = None
        self._seen = set()             # set of hashes of entries for fast testing of duplicate entries
        self._entries = []             # all entries - dict for fast lookup
        self._duplicate_proteins = []  # filtered protein entries

    @staticmethod
    def read_from_cache(filepath=settings.BRENDA_DOWNLOAD, encoding='utf-8') -> 'Parser':
        cache_filepath = os.path.join(settings.CACHE_DIR, 'brenda_' + file_md5_hash(filepath, encoding))
        with open(cache_filepath, 'rb') as f:
            return pickle.load(f)

    def write_to_cache(self):
        cache_filepath = self.get_cache_filepath()
        with open(cache_filepath, 'wb') as f:
            pickle.dump(self, f, pickle.HIGHEST_PROTOCOL)
            print(f'cache file written to {cache_filepath}')

    def get_cache_filepath(self):
        return os.path.join(settings.CACHE_DIR, 'brenda_' + file_md5_hash(self.filepath, self.encoding))

    def parse_brenda(self, write_cache: bool = True):
        """
        parsing brenda ~1 minute VS reading from the cache file 10 seconds, using Parser.ec_data 30 ns
        if writing to cache, Parser._entries and Parser._seen are emptied beforehand to save space
        """
        ec_number = None
        in_attribute = False
        ncbi_taxonomy = Taxonomy()
        with open(self.filepath, 'r', encoding=self.encoding) as f:
            for i, line in enumerate(tqdm(f.readlines(), desc='parsing brenda data')):
                line = line.rstrip()
                # grab the E.C. number if an ID tag is found, initialize the key
                if line.startswith('ID'):
                    for malformatted_ec, curated_ec in MANUAL_CURATED_ECS.items():
                        if malformatted_ec in line:
                            line = line.replace(malformatted_ec, curated_ec)
                            logger.info(f'replaced {malformatted_ec} with {curated_ec} in Line {i}')
                    ec_numbers = regex.EC_PATTERN.findall(line)
                    ec_number = ec_numbers.pop(0)
                    # storing entries for inspection and statistics
                    entry = Entry(ec_number, 'ID', '', line_no=i)
                    self._seen.add(entry.key)
                    self._entries.append(entry)
                    # if there is more than 1 E.C. number, it is transferred or deleted and we redirect
                    # sometimes redirects to more than 1 E.C. number (requires manual curation by user)
                    if re.search(r'tranferred|transferred|deleted', line, re.IGNORECASE):
                        if ec_numbers:  # TODO: EC numbers to (filtered) entries
                            self._redirect[ec_number] = dict(ec_numbers=ec_numbers, line=line)
                        else:
                            self._deleted[ec_number] = dict(info=line)
                    elif len(ec_numbers) > 1:
                        raise ParserError(f'multiple E.C. numbers found: {line}')
                    # still parse the redirected, some contain non-redundant information
                    self._ec_data[ec_number] = ECNumber(ec_number)

                # now with the ec number initialized, process the EC numbers' BRENDA attribute data
                elif ec_number in self._ec_data:
                    # on first occurrence of a BRENDA attribute, we step in
                    if not in_attribute:
                        # create new
                        attr_id = line.split("\t")[0].strip()
                        if attr_id in BRENDA_ATTRS:
                            attr_data = "\t".join(line.split("\t")[1:])
                            line_no = i
                            in_attribute = True
                    # any subsequent line, we check if we encounter a new occurrence of a brenda attribute
                    # if next attribute id found, we process and store the current data, and start a new one
                    # if the next_attr_id is in BRENDA_ATTRS, it always equals the current attribute_id
                    else:
                        next_attr_id = line.split("\t")[0].strip()
                        if next_attr_id in BRENDA_ATTRS:
                            entry = Entry(ec_number, attr_id, attr_data, line_no)
                            self._process_and_store(entry, ncbi_taxonomy)
                            attr_data = "\t".join(line.split("\t")[1:])
                            line_no = i
                        # if not a new attribute id while there is data: multiline entry, thus we concatenate it
                        elif line:
                            attr_data += " " + line.strip()
                        # if not a new attribute id and also no data: process the last occurrence and step out
                        elif not line:
                            entry = Entry(ec_number, attr_id, attr_data, line_no)
                            self._process_and_store(entry, ncbi_taxonomy)
                            in_attribute = False

            # remove the ambiguous proteins from the others
            for ec_number in self._ec_data.values():
                ec_number.proteins = {protein_id: protein for protein_id, protein in ec_number.proteins.items()
                                      if protein_id not in ec_number.ambiguous_proteins}

            # calculate statistics here here and cache
            total = pd.Series(collections.Counter([x.attr_id for x in self._entries]))
            filtered = pd.Series(collections.Counter([x.attr_id for x in self._entries if x.filtered is True]))
            filtered['PR'] = len(self._duplicate_proteins)
            self._statistics = (pd.concat([total.rename('total'), filtered.rename('filtered')], axis=1)
                                .fillna(0).astype(int))

            del ncbi_taxonomy  # having this giant object around when attempting to write can cause SIGKILL
            if write_cache:  # we clean up some more to save space
                self._entries = []
                self._seen = set()
                self.write_to_cache()
        return self._ec_data

    def _process_and_store(self, entry: Entry, ncbi_taxonomy: Taxonomy) -> None:
        """
        processing brenda attribute data by regex pattern matching
        - RF: reference, info, optional pubmed id
        - RN, SN: we do nothing; only contains a mandatory 'info' part
        - RE, RT: info and optional comment
        - SY: combination of formats; ids and references are optional, thus the "lose" pattern
        - rest: uses a strict pattern: only the comment part is optional, ids, info and references are mandatory
        """
        def to_int(ids: str) -> list:
            """converting comma and space separated strings into a list of integers (e.g. ids and refs)"""
            return list(map(int, re.split(r'[,\s]', ids)))

        def filter_info(info: str) -> Optional[str]:
            """we filter out useless entries: '-999', 'more'"""
            if info == 'more' or info.startswith('more = ?') or '{more}' in info or info.startswith('-999'):
                return  # filter these out
            return info

        self._entries.append(entry)
        if entry.key in self._seen:
            entry.filtered = True
            logger.warning(f"duplicate entry not stored: {entry}")
            return
        self._seen.add(entry.key)

        ec_number = self._ec_data[entry.ec_number]
        if entry.attr_id == "PR":
            # proteins are always the first attribute field of an EC number
            protein_id, info, comment, references = regex.STRICT_PATTERN.match(entry.attr_data).groups()
            species, database_ids, database = regex.PR_PATTERN.match(info).groups()
            database_ids = regex.UNIPROT_PATTERN.findall(database_ids) if database_ids else []
            protein_id = int(protein_id)
            references = to_int(references)
            tax_id = ncbi_taxonomy.get_tax_id_by_name(species)
            protein = Protein(ec_number=entry.ec_number, protein_id=protein_id, species=species, taxonomy_id=tax_id,
                              database_ids=database_ids, database=database, comment=comment, references=references)

            if protein_id in ec_number.proteins:  # there are just over 3000 duplicate entries here
                first = ec_number.proteins[protein_id]
                if first.species != protein.species:  # 104 of these
                    if protein_id not in ec_number.ambiguous_proteins:
                        ec_number.ambiguous_proteins[protein_id] = [first]
                    ec_number.ambiguous_proteins[protein_id].append(protein)
                elif all([first[k] == protein[k] for k in first if not k == 'database']):
                    self._duplicate_proteins.append(protein)  # same data, just different database listed
                else:
                    raise ValueError(f'unexpected differences found:\n{first}\n{protein}')
            else:
                ec_number.proteins[protein_id] = protein
                for reference in references:
                    if reference not in ec_number.ref2id:
                        ec_number.ref2id[reference] = []
                    # one reference can map to multiple protein ids
                    ec_number.ref2id[reference].append(protein_id)

        # now we process fields that are EC number level specific and not protein level specific
        elif entry.attr_id in ("RN", "SN"):  # store as string
            # RECOMMENDED_NAME, SYSTEMATIC_NAME (optional)
            info = filter_info(entry.attr_data)
            if ec_number[entry.attr_id] != '':
                raise ValueError('expected single entry for recommended and systematic names')
            if info:
                ec_number[entry.attr_id] = info
            else:
                entry.filtered = True
                logger.warning(f"entry not stored: {entry}")

        elif entry.attr_id in ("RE", "RT"):  # store as dict
            # REACTION, REACTION_TYPE. Note: not all reactions have an "=" sign, some are descriptive
            info, comment = regex.RE_RT_PATTERN.match(entry.attr_data).groups()
            info = filter_info(info)
            data = dict(info=info, comment=comment)
            if info and data not in ec_number[entry.attr_id]:
                ec_number[entry.attr_id].append(data)
            else:
                entry.filtered = True
                logger.warning(f"entry not stored: {entry}")

        # the synonyms field can relate either to the EC number, or a specific protein
        elif entry.attr_id == "SY":
            # this is more loosely formatted, so we parse it as such
            protein_ids, info, comment, references = regex.LOSE_PATTERN.match(entry.attr_data).groups()
            protein_ids = to_int(protein_ids) if protein_ids is not None else []
            references = to_int(references) if references is not None else []
            info = filter_info(info)
            if info:
                links_to_protein = False
                for protein_id in protein_ids:
                    protein = ec_number.proteins.get(protein_id)
                    if protein is None:
                        logger.warning(f'synonym not linked to protein: {entry}')
                    else:
                        links_to_protein = True
                        references = list(set(protein.references).intersection(references))  # why not preserve order?
                        data = dict(info=info, comment=comment, references=references)
                        if data not in protein[entry.attr_id]:
                            protein[entry.attr_id].append(data)
                        else:  # strict in order to ensure no filtered entries are not in filtered entries
                            raise ValueError(f'unexpected duplicate information found {entry}, '
                                             f'entry might be filtered and not have entry.filtered == True')

                if protein_ids and not links_to_protein:  # not going to store this on EC number instead
                    entry.filtered = True
                    logger.warning(f"entry not linked to any protein: {entry}")

                if not protein_ids:
                    if comment:
                        logger.warning(f'comment not stored for synonym: {entry}')
                    if references:
                        logger.warning(f'reference not stored for synonym: {entry}')
                    ec_number[entry.attr_id].append(info)

        # the reference field is linked by reference id to proteins
        elif entry.attr_id == 'RF':
            # REFERENCES - cannot easily match title and journal, as "."'s can be in the title
            reference, info, pubmed_id = regex.PATTERN_RF.match(entry.attr_data).groups()
            reference = int(reference)
            pubmed_id = int(pubmed_id) if pubmed_id else None
            protein_ids = ec_number.ref2id.get(reference, [])
            links_to_protein = False
            for protein_id in protein_ids:
                protein = ec_number.proteins.get(protein_id)
                if protein is None:
                    logger.warning(f'reference {reference} links to non-existing protein id: {entry}')
                else:
                    links_to_protein = True
                    data = {reference: {'paper': info, 'pubmed_id': pubmed_id}}
                    if data not in protein[entry.attr_id]:
                        protein[entry.attr_id].append(data)
                    else:
                        raise ValueError(f'unexpected duplicate information found {entry}, '
                                         f'entry might be filtered and not have entry.filtered == True')

            if not links_to_protein:
                entry.filtered = True
                logger.warning(f"entry not linked to protein: {entry}")

        # all other protein specific fields
        else:  # STRICT_PATTERN - {'CL', 'CR', 'PR', 'PU', 'REN'} have all len(ids) == 1
            protein_ids, info, comment, references = regex.STRICT_PATTERN.match(entry.attr_data).groups()
            protein_ids = to_int(protein_ids)
            references = to_int(references)
            info = filter_info(info)
            if not info:
                entry.filtered = True
                logger.warning(f"entry not stored: {entry}")
                return  # prevents data processing and storage

            links_to_protein = False
            for protein_id in protein_ids:
                protein = ec_number.proteins.get(protein_id)
                if protein is None:
                    logger.warning(f'entry cannot be linked to protein: {entry}')
                    continue

                # only select the references that belong to this protein
                protein_references = list(set(protein.references).intersection(references))

                # TODO: currently also matching invalid ranges (e.g. 0.25-0)
                if entry.attr_id in ('TN', 'KM', 'KI', 'IC50'):
                    # TURNOVER_NUMBER, KM_VALUE, KI_VALUE, IC50_VALUE
                    data = dict(comment=comment, references=protein_references)
                    match = regex.VALUE_COMPOUND_PATTERN.match(info)
                    if match:
                        value, substrate = match.groups()
                        data['value'] = float(value)
                    else:
                        match = regex.VALUE_RANGE_COMPOUND_PATTERN.match(info)
                        if match:
                            value_range, substrate = match.groups()
                            data['range'] = value_range
                        else:
                            entry.filtered = True
                            logger.error(f"not a proper value or range: {entry}")
                            return

                    data['substrate'] = substrate
                    data['chebi'] = compound_to_chebi(substrate)
                    data['factories'] = UNITS[entry.attr_id]

                elif entry.attr_id in ('PHO', 'PHS', 'PHR', 'SA', 'KKM', 'TO', 'TR', 'TS', 'MW', 'PI'):
                    # PH_OPTIMUM. PH_STABILITY, PH_RANGE, SPECIFIC_ACTIVITY, KCAT_KM_VALUE,
                    # TEMPERATURE_OPTIMUM, TEMPERATURE_RANGE, TEMPERATURE_STABILITY, MOLECULAR_WEIGHT, PI_VALUE,
                    data = dict(comment=comment, references=protein_references)
                    match = regex.VALUE_PATTERN.match(info)
                    if match:
                        data['value'] = float(match.group(1))
                    else:
                        match = regex.VALUE_RANGE_PATTERN.match(info)
                        if match:
                            data['range'] = match.group(1)
                        else:
                            entry.filtered = True
                            logger.error(f"not a proper value or range: {entry}")
                            return

                    data['factories'] = UNITS[entry.attr_id]

                elif entry.attr_id in ('AC', 'IN', 'CF', 'ME', 'OSS'):
                    # ACTIVATING_COMPOUND, INHIBITION, COFACTOR, METALS_IONS, ORGANIC_SOLVENT_STABILITY
                    chebi = compound_to_chebi(info)
                    data = dict(name=info, chebi=chebi, comment=comment, references=protein_references)
                elif entry.attr_id in ('ST', 'LO', 'PM', 'SU', 'AP', 'EN'):
                    # SOURCE_TISSUE, LOCALIZATION, POSTTRANSLATIONAL_MODIFICATION, SUBUNITS, APPLICATION, ENGINEERING
                    data = dict(info=info, comment=comment, references=protein_references)
                elif entry.attr_id in ('NSP', 'SP'):  # TODO: convert to chebi?
                    # optional reversibility pattern: {} or {r}, after comment, before references
                    data = dict(info=info, comment=comment, references=protein_references)
                elif entry.attr_id in ('CL', 'CR', 'PU', 'REN', 'GS', 'OS', 'SS', 'EXP'):
                    # CLONED, CRYSTALLIZATION,PURIFICATION, RENATURED, GENERAL_STABILITY,
                    # OXIDATION_STABILITY, STORAGE_STABILITY, EXPRESSION
                    data = dict(info=info.strip('()'), comment=comment, references=protein_references)
                else:
                    raise ParserError(f'unknown attribute identifier found: {entry.attr_id}')

                # only if non-duplicate information is found, we link to protein and don't filter the entry
                if data not in protein[entry.attr_id]:
                    links_to_protein = True
                    protein[entry.attr_id].append(data)

                if not links_to_protein:
                    entry.filtered = True

    @property
    def statistics(self):
        return self._statistics

    @cached_property
    def ec_data(self):
        if not self._ec_data:
            self.parse_brenda()
        return self._ec_data

    def query(self, ec_number, protein,
              species, source_tissue, localization,
              attributes):
        # retrieve proteins (E.g.
        # pd.DataFrame(dict([(k, pd.Series(v)) for k,v in p.items() if k in ('ST', 'NSP',)])).T.dropna(how='all')
        return

    def check_ec_number(self, ec_number):  # TODO:
        if ec_number in self._deleted:
            raise ValueError(f"E.C. number has been deleted {self._deleted[ec_number]['line']}")
        elif ec_number in self._redirect:
            ec_numbers = self._redirect[ec_number]['ec_numbers']
            if len(ec_numbers) > 1:
                logger.error(f"E.C. number transferred to multiple others: {ec_numbers}, cannot retrieve automatically")
            else:
                logger.warning(f'ec_number transferred, retrieving content for {ec_numbers[0]}')
                ec_number = ec_numbers[0]  # TODO: not tested if all redirecting is good
        return self.ec_data.get(ec_number)

    def query_by_protein(self, uniprot_id):  # TODO:
        if not self._pr2ec:
            for ec_number, v in self.ec_data.items():
                for item in v.get('PR', []):
                    for prot_id in item['uniprot_ids']:
                        self._pr2ec[prot_id] = ec_number
        ec_data = self.ec_data.get(self._pr2ec.get(uniprot_id))
        if ec_data:
            for item in ec_data['PR']:
                if uniprot_id in item['uniprot_ids']:
                    organism_id = item['ids']
                    break
            protein_data = dict()
            for k, items in ec_data.items():
                for item in items:
                    if 'ids' in item:
                        if item['ids'] == organism_id:
                            protein_data[k] = item
            return protein_data

    @cached_property
    def proteins(self):
        proteins = []
        for v in self.ec_data.values():
            proteins.extend(v.proteins.values())
        return proteins

    @property
    def species_count(self):
        return pd.Series(collections.Counter([protein.species for protein in self.proteins]))

    @property
    def species(self):
        # return set([protein.species for protein in self.proteins])
        return set([protein.species for protein in self.proteins])


def test():

    parser = Parser()
    parser.parse_brenda(write_cache=True)

    # parser.proteins
    # parser.species
    # statistics = parser.statistics
    #
    # for e in parser._entries:
    #     if e.filtered is True and e.attr_id == 'KI':
    #         print(e.line_no, e.attr_data)


def test2():

    parser = Parser.read_from_cache()
    species = parser.species  # species = set([protein.species for protein in parser.proteins])
    taxonomy = Taxonomy()

    tax_ids = []
    for s in species:
        try:
            tax_ids.append(taxonomy.get_tax_id_by_name(s))
        except:
            tax_ids.append(None)

    print(len(species))
    print(len(tax_ids))
    print('singles', len([tax_id for tax_id in tax_ids if isinstance(tax_id, int)]))
    print('multiple', len([tax_id for tax_id in tax_ids if isinstance(tax_id, set)]))



# TODO: this is old
def parse_model_parameters(sbml, organism, use_cached=True, drop_outside: str = 'species',
                           drop_mutants : bool = True, mutant_penalty=10, distance_penalty=2, convert_mM_to_M=True):
    """
    The following model annotations are used:
    - ec-codes for reactions
    - chebi ids for metabolites

    Parses:
    - Catalytic constants (kcat)
    - Michaelis Menten constants (km)
    - Inhibitory constants (ki)

    Parameters priors are calculated. Mutant data can be ignored, or taken into consideration, applying a penalty term,
    while calculating the prior. The same holds for data originating from other organism, which can be ignored or
    penalized using the crude taxonomic distance metric. In case mutant data and data from other organisms is used,
    a weighted geometric average is calculated, resulting in a broader prior, but with better model coverage.
    NOTE: A.M. >= G.M. inequality

    Catalytic constants are split into forward and backward constants, where multiple values for different substrates
    participating in the same side of the reaction are averaged.

    Relies on parameter naming conventions as in Kinetizer (rxn_kcatf, rxn_kcatr, etc)
    """
    base = SbmlBase(sbml)
    # generate a suffix hash that encodes the information used to parse the data for caching
    anno = [base.reaction_annotated_identifiers['ec-code'], base.species_annotated_identifiers['chebi']]
    kwargs = str(sorted([(k, str(v)) for k, v in locals().items() if k not in ['base']]))
    suffix = utils.md5_sum(kwargs + ''.join([a.to_string() for a in anno]))

    def gather(df_ptype, ec_codes, chebis, ptype):
        """ gathering all brenda data related to reaction / metabolite pairs present in the model"""
        l = []
        combinations = list(itertools.product(ec_codes.items(), chebis.items()))
        for ((r_id, ec_numbers), (s_id, chebi_ids)) in tqdm(combinations, desc=f'getting brenda model {ptype} data'):
            data = df_ptype[(df_ptype['ec-code'].isin(ec_numbers)) & (df_ptype['chebi'].isin(chebi_ids))]
            if not data.empty:
                data['r_id'] = r_id
                data['s_id'] = s_id
                l.append(data)
        return pd.concat(l)

    def process_kcat(kcat_parameters):
        # in case of kcat, we perform one additional operation:
        # split into kcatf and kcatr, and merge constants in those two groups for different species
        # NOTE: it doesn't matter if the same species with different ids due to compartment suffix occur, since
        # these will not be on the same side of the reaction
        kcatf, kcatr = [], []
        for (r_id, s_id), data in kcat_parameters.groupby(['r_id', 's_id']):
            reaction = base.get_by_id(r_id)
            if s_id in [s_ref.species for s_ref in reaction.reactants]:
                data.symbol = 'kcatf'
                kcatf.append(data)
            elif s_id in [s_ref.species for s_ref in reaction.products]:
                data.symbol = 'kcatr'
                kcatr.append(data)
            else: # some kcat for a reaction metabolite pair not in the model, which we filter here as well
                logger.info(f'kcat found for {r_id} {s_id}, but not part of the model')
        return pd.concat(kcatf), pd.concat(kcatr)

    def aggregate(parameters, ptype):
        """averaging over all data, weighted by distance from the organism and a mutant penalty"""
        geometric, linear = {}, {} # geometric is preferred, but we will calculate both
        for (r_id, s_id), data in parameters.groupby(['r_id', 's_id']):
            if ptype in ('kcatf', 'kcatr'):
                s_id = np.nan # as we merge the data for the different substrates for the kcats
            w = 1 / (mutant_penalty ** data['mutant'].values * distance_penalty ** data['distance'].values)
            l = data['value'].values
            geometric[r_id, s_id] = utils.weighted_geometric_mean(l, w)
            linear[r_id, s_id] = utils.weighted_mean(l, w)
        df = pd.concat([pd.Series(geometric), pd.Series(linear)], axis=1)
        df.columns = ['geometric', 'linear']
        return df

    def annotate_identifiers(table):
        # assigning parameter ids according to Kinetizer conventions
        ids = pd.concat([table.reaction[table.symbol == 'km'] + '_km_' + table.species[table.symbol == 'km'].apply(utils.clipper),
                         table.reaction[table.symbol == 'ki'] + '_ki_' + table.species[table.symbol == 'ki'].apply(utils.clipper),
                         table.reaction[table.symbol == 'kcatf'] + '_kcatf',
                         table.reaction[table.symbol == 'kcatr'] + '_kcatr',
                         ])
        ids.name = 'id'
        return pd.concat([table, ids], axis=1)

    @utils.cache_file(os.path.join(settings.CACHE_DIR, f'brenda_parameters_{suffix}.p'), use_cached=use_cached)
    def parse_brenda_parameters():

        taxonomic_levels = ['species', 'genus', 'family', 'order', 'class', 'phylum', 'kingdom', None]
        if drop_outside not in taxonomic_levels:
            raise ValueError(f"got drop_distance '{drop_outside}', should be in {taxonomic_levels}")
        drop_distance = taxonomic_levels.index(drop_outside)

        df = parse_brenda(organism)

        if convert_mM_to_M:
            df.loc[df['factories'] == 'mM', 'value'] /= 1000
            df.loc[df['factories'] == 'mM', 'factories'] = 'M'

        units = dict(df[['symbol', 'factories']].drop_duplicates().values)
        units['kcatf'], units['kcatr']  = units['kcat'], units['kcat']

        # we calculate some summary statistics over the whole population to fill in gaps (std, gstd) later
        summary = {}
        percentiles = np.array(list(range(5, 100, 5))) / 100
        for ptype, group in df.groupby('symbol'):
            stats = group['value'].describe(percentiles=percentiles)
            geometric = utils.geometric_mean(group['value'])
            stats.loc['gmean'], stats.loc['gstd'] = geometric.n, geometric.s
            summary[ptype] = stats

        columns = summary.keys()
        summary = pd.concat(summary.values(), axis=1)
        summary.columns = columns

        # for plotting an overview of the distributions
        brenda_overview = dict()
        brenda_overview['clean'] = df
        brenda_overview[organism] = df[df.organism == organism]
        brenda_overview[f'{organism} WT'] = df[(df.organism == organism) & (df.mutant == False)]
        plot_overview(brenda_overview)

        if drop_distance is not len(taxonomic_levels) - 1:
            df = df[df.distance <= drop_distance]
        if drop_mutants:
            df = df[df.mutant == False]

        # obtain model annotations
        ec_codes = base.reaction_annotated_identifiers['ec-code'].dropna()
        chebis = base.species_annotated_identifiers['chebi'].dropna()

        if ec_codes is None: raise ValueError('reactions not annotated, annotate ec-code identifiers to reactions')
        if chebis is None: raise ValueError('species not annotated, annotate CHEBI identifiers to species')

        # we gather all raw brenda data related to the model, which allows inspection (individual values, notes)
        parameter_data = {}
        for ptype, df_ptype in df.groupby('symbol'):
            parameter_data[ptype] = gather(df_ptype, ec_codes, chebis, ptype)

        # plotting the raw data for inspection
        for k, df in parameter_data.items():
            d = {}
            for (r_id, s_id), data in df.groupby(['r_id', 's_id']):
                d[f'{utils.clipper(r_id)}_{utils.clipper(s_id)}'] = data['value'].values
            unit = units[k]
            name = {'kcat':'Catalytic constants', 'km':'Michaelis-Menten constants', 'ki':'Inhibitory constants'}[k]
            fig = plt.figure(figsize=(14,6))
            ax = sns.boxplot(data=list(d.values()))
            ax.set_title(name, fontsize=20)
            ax.set_ylabel(f'{unit}', fontsize=16)
            ax.set_xlabel(f'Enzyme - Metabolite pair', fontsize=16)
            ax.set_yscale('log')
            ax.set_xticklabels(list(d), rotation=90)#, fontsize=8)
            plt.tight_layout()
            fp = os.path.join(settings.FIGURE_DIR, f'{base.model.id}_{k}.pdf')
            fig.savefig(fp)
            logger.info(f'saved raw parameter data figure at:\n{fp}')

        # first we need to process the kcats, split them into kcatf and kcatr and merge values for species
        parameter_data['kcatf'], parameter_data['kcatr'] = process_kcat(parameter_data['kcat'])
        del parameter_data['kcat']

        # and in the next step we aggregate the data, estimating the geometric and linear mean and std
        for ptype, parameters in parameter_data.items():
            parameter_data[ptype] = aggregate(parameters, ptype)

        table = pd.concat(parameter_data.values(), keys=parameter_data.keys()).reset_index()
        table['gmean'], table['gstd'] = zip(*table['geometric'].apply(lambda x: [x.n, x.s]))
        table['mean'], table['std'] = zip(*table['linear'].apply(lambda x: [x.n, x.s]))
        table = (table.drop(['geometric', 'linear'], axis=1)
                 .rename(columns={'level_0':'symbol', 'level_1':'reaction', 'level_2': 'species'}))
        table['unit'] = [units[x] for x in table.symbol]

        # in case of only one measurement, meaning no standard deviation, we set those to NaN
        # this prevents the sampler from always return the mean, and forces one to assign a std / gstd manually
        table.loc[table['gstd'] == 1.0, 'gstd'] = np.nan
        table.loc[table['std'] == 0.0, 'std'] = np.nan
        table = annotate_identifiers(table)

        return table, summary

    table, summary = parse_brenda_parameters()
    return table, summary


def test():

    sbml = settings.ECOLI_CORE_MODEL
    organism = 'Escherichia coli'
    use_cached = False
    drop_outside = 'species'
    drop_mutants = True
    mutant_penalty = 10
    distance_penalty = 2
    convert_mM_to_M = True
    ignore_extracellular = True
    table, summary = parse_model_parameters(sbml, organism, use_cached, drop_outside, drop_mutants,
                                            mutant_penalty, distance_penalty, convert_mM_to_M)


# all BRENDA citations
#
# Primary publication
#
#     BRENDA in 2019: a European ELIXIR core data resource
#     Jeske L., Placzek S., Schomburg I., Chang A., Schomburg D., Nucleic Acids Res., in print (2019)
#
#
# Other relevant publications
#
#     BRENDA in 2017: new perspectives and new tools in BRENDA
#     Placzek S., Schomburg I., Chang A., Jeske L., Ulbrich M., Tillack J., Schomburg D., Nucleic Acids Res., 45:D380-388 (2017)
#
#     The BRENDA enzyme information system-From a database to an expert system.
#     Schomburg I., Jeske L., Ulbrich M., Placzek S., Chang A., Schomburg D., J Biotechnol., 261:194-206 (2017)
#
#     BRENDA - From a database to a centre of excellence.
#     Schomburg I., Schomburg D., Systembiologie.de, 10:18-21 (2016)
#
#     BRENDA in 2015: exciting developments in its 25th year of existence
#     Chang A., Schomburg I., Placzek S., Jeske L., Ulbrich M., Xiao M., Sensen C.W., Schomburg D., Nucleic Acids Res., 43:D439-446 (2015)
#
#     BRENDA in 2013: integrated reactions, kinetic data, enzyme function data, improved disease classification: new options and contents in BRENDA.
#     Schomburg I., Chang A., Placzek S., Söhngen C., Rother M., Lang M., Munaretto C., Ulas S., Stelzer M., Grote A. Scheer M., Schomburg D., Nucleic Acids Res., 41:764-772 (2013)
#
#     Development of a classification scheme for disease-related enzyme information.
#     Söhngen C., Chang A., Schomburg D., BMC Bioinformatics, 12:329 (2011)
#
#     BRENDA, the enzyme information system in 2011.
#     Scheer M., Grote A., Chang A., Schomburg I., Munaretto C., Rother M., Söhngen C., Stelzer M., Thiele J., Schomburg D., Nucleic Acids Res., 39:670-676 (2011)
#
#     The BRENDA Tissue Ontology (BTO): the first all-integrating ontology of all organisms for enzyme sources.
#     Gremse M., Chang A., Schomburg I., Grote A., Scheer M., Ebeling C., Schomburg D., Nucleic Acids Res., 39:D507-13 (2011)
#
#     BKM-react, an integrated biochemical reaction database
#     Lang M., Stelzer M., Schomburg D., BMC Bioinformatics, 12:42 (2011)
#
#     EnzymeDetector: an integrated enzyme function prediction tool and database.
#     Quester S., Schomburg D., BMC Bioinformatics, 12:376 (2011)
#
#     BRENDA, AMENDA and FRENDA the enzyme information system: new content and tools in 2009.
#     Chang A., Scheer M., Grote A., Schomburg I., Schomburg D., Nucleic Acids Res., 37: D588-D592 (2009)
#
#     BRENDA, AMENDA and FRENDA: the enzyme information system in 2007.
#     Barthelmes J., Ebeling C., Chang A., Schomburg I., Schomburg D., Nucleic Acids Res., 35:D511-D514 (2007)
#
#     Structural Analysis and Prediction of Protein Mutant Stability using Distance and Torsion Potentials: Role of Secondary Structure and Solvent Accessibility.
#     Parthiban V, Gromiha MM, Hoppe C, Schomburg D., Proteins, 66(1):41-52 (2007)
#
#     Computational modeling of protein mutant stability: analysis and optimization of statistical potentials and structural features reveal insights into prediction model development.
#     Parthiban V, Gromiha MM, Abhinandan M., Schomburg D., BMC Structural Biology, 7:54 (2007)
#
#     CUPSAT: prediction of protein stability upon point mutations.
#     Parthiban V, Gromiha MM., Schomburg D., Nucleic Acid Res., 34:W239-42 (2006)
#
#     BRENDA, the enzyme database: updates and major new developments.
#     Schomburg I., Chang A., Ebeling C., Gremse M., Heldt C., Huhn G., Schomburg D., Nucleic Acids Res., 32: D431-D433 (2004)
#
#     Review of the BRENDA Database.
#     Pharkya P., Nikolaev E.V., Maranas C.D., Metab Eng., 5(2):71-73 (2003)
#
#     BRENDA: a resource for enzyme data and metabolic information.
#     Schomburg I., Chang A., Hofmann O., Ebeling C., Ehrentreich F., Schomburg D., Trends Biochem Sci., 27(1):54-56 (2002)
#
#     BRENDA, enzyme data and metabolic information
#     Schomburg, I., Chang, A., Schomburg, D., Nucleic Acids Res. 30, 47-49 (2002)
#