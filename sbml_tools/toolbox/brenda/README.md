
These scripts have been adapted from:
- https://github.com/Midnighter/BRENDA-Parser
- https://github.com/alexandra-zaharia/BRENDA-Parser
- https://github.com/matthiaskoenig/brendapy

Formatting of the BRENDA enzyme database flat file is not for the cat

General notes:
- Stored per E.C. number: "ID 1.1.1.1", 7345 entries in total
- 114 E.C. numbers have been deleted
- 967 E.C. numbers entries have been transferred and now redirect to other E.C. numbers
  Some that are transferred, still contain information that has or has not been transferred.
- another 52 E.C. numbers have no protein entries
- Some brenda attribute are reported twice per E.C. number (e.g. KI, AC for enzyme 1.1.1.1)

Proteins:
- Each EC number in BRENDA is associated with an zero, one or many proteins. 
- These proteins have an integer identifier per EC number; their combination is unique
    - However, some proteins in the list actually have the same protein id: they only differ in database of origin.
- They also have integer literature references; these can be one or many 
- Protein identifiers can be used to retrieve information relating to other attributes:
    - except for these fields; they relate to the EC number: RE, RN, RT, SN
    - the synonyms (SY) field contains a mix entries with or without reference to protein id
    - the references (RF); these are linked through reference ids
- In a set of cases EC number attributes cannot be linked to proteins; they appear to have malformatted protein ids, 
their protein ids do not occur in the EC numbers PR field.
- It is also possible to retrieve information on the basis of literature reference ids.
- Currently, protein information is retrieved by organism id first; if an attribute is found with a protein id 
that does not link to any protein, the literature id is used.  

Parsing notes:
- frequently one OR multiple white spaces are present, use regex: \s+
- multiline entries start consecutive lines with a tab "\t"
   - this turns out to not always be true; it appears more robust to match the next attribute id
- some info fields contain "more" or "more = ?" or "{more}", which makes them useless
- some values are -999 or -999.0, those are useless also

There appear to exist these types of data formats:
- numeric organism/reference ids to are comma or space separated.
- PROTEIN_IDS: comma or space separated integers (e.g. #1#, #11,12 13#) 
- REFERENCE_IDS: comma or space separated integers (e.g. <1>, <11,12 13>) 
- INFO: content depends on the attribute 
- COMMENT: #numeric organism id(s)# comment <numeric reference id(s)> - this field is optional
- VALUE: float / integer or range (e.g. 7, 7.2, 7-8, 7-7.5 or 7.2-7.5, 0.25-0)
- COMPOUND: alphanumeric string with special characters (e.g. beta-D-glucose 1-phosphate)
1. attribute fields: RE, RT, SN, RN
    - pattern: info (COMMENT)
    - These relate to the EC number, not individual proteins
2. attribute fields: PR, ST, LO, NSP, SP, TN, KM, PHO, PHR, SA, TO, TR, CF, AC, IN, KI, 
                     ME, MW, PM, SU, PI, CL, CR, PU, REN, GS, OSS, KKM, EXP
    - pattern: #numeric organism id(s)# info (COMMENT) <numeric reference id(s)>
    - These relate to protein entries
3. attribute field: RF
    - pattern: <numeric reference id> reference {Pubmed:123321}
    - Pubmed id can be empty: {Pubmed:}
4. attribute field: SY
    - a mixture of type 1 and 2:

BRENDA attributes:
- ID, E.C. number
    - primary database key
- PR, PROTEIN
    - enumerates all proteins 
    - can have zero, one or multiple protein identifiers
    - can have zero or one databases of origin listed  
    - always one protein id, allows for retrieval of other attribute data
    - one or many literature reference id(s), allows for retrieval of other attribute data
    - example: #1# Gallus gallus D4Q9Z5 and Q9UBX8 and O60513 <147,148>
- RN, RECOMMENDED_NAME
    - pattern: INFO
    - IUPAC
    - example: anthocyanidin 3-O-coumaroylrutinoside 5-O-glucosyltransferase
- SN, SYSTEMATIC_NAME
     - pattern: INFO
     - example: (1->3)-beta-D-glucan:phosphate alpha-D-glucosyltransferase
- SY, SYNONYMS
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - example 1:  chloroorienticin B synthase
    - example 2: #1# GtfA <1,2>
- RE, REACTION
    - pattern: INFO (COMMENT)
    - example: geraniol + NADP+ = geranial + NADPH + H+ (#4# specifically acts on geraniol <3>)
- RT, REACTION_TYPE
    - pattern: INFO (COMMENT)
    - example: hexosyl group transfer
- ST, SOURCE_TISSUE
    - protein_ids info (COMMENT) references
    - example: #3# xylem (#3# differentiated and differentiating cells <3>) <3>
- LO, LOCALIZATION
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - example 1: #1,2# cytoplasm <1>
    - example 2: #10# more (#10# extravacuolar <5>) <5>
- NSP, NATURAL_SUBSTRATE_PRODUCT
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - reaction string with optional {ir}, {r} or {}, indicating reversibility
    - example 1: #1# dTDP-beta-L-rhodosamine + aklavinone = dTDP + aclacinomycin T <1,3>
    - example 2: #2# GDP-D-mannuronate + (alginate)n = GDP + (alginate)n+1 {ir} <1>
- SP, SUBSTRATE_PRODUCT
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - example: #4# UDP-glucose + type VII collagen = ? <33>
- TN, TURNOVER_NUMBER
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE {COMPOUND}
    - optional comment can contain: pH, temperature, molar concentration, mutant, cosubstrate, cofactor
    - example 1: #1# 163 {sn-glycerol}  (#1# pH 7.5, 30°C <2>) <2>
    - example 2: #1# -999 {more}  <22>
- KM, KM_VALUE
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE {COMPOUND}
    - optional comment can contain: pH, temperature, molar concentration, mutant, isozyme, cosubstrate, cofactor
    - example: #10# 33 {benzaldehyde}  (#10# isozyme ADH1, in the presence of 0.25 mM
               NADH, in 83 mM potassium phosphate, 40 mM KCl, and 0.25 mM EDTA buffer, pH 7.3, at 30°C <202>) <202>
- PHO, PH_OPTIMUM
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
    - example: #1,2# 5.5 (#2# the activity is low at neutral pH and almost no activity is observed at pH 8.5 <3>) <1,3>
- PHR, PH_RANGE
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
    - example: #6# 2.7-6.5 (#6# about 50% of maximal activity at pH 2.7 and at pH 6.5 <15>) <15>
- PHS, PH_STABILITY
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
- SA, SPECIFIC_ACTIVITY
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
    - optional comment can contain: pH, temperature, molar concentration, mutant, isozyme, cosubstrate, cofactor
      and substrate (substrate X, substrates X and Y, X as substrate, X as a substrate)
    - example 1: #10# 55.0 (#10# reaction with guanosine <8>) <8>
    - example 2: #16# 1.96 (#16# A270P mutant protein, pH 8.5, temperature not specified in the publication <23>) <23>
- TO, TEMPERATURE_OPTIMUM
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
- TR, TEMPERATURE_RANGE:
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE
    - optional comment on maximal activity and X% of max activity at Y°C
    - example: #8# 15-37 (#8# 15°C: about 40% of maximal activity, 37°C: maximal activity <32>) <32>
- CF, COFACTOR
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: COMPOUND
    - example: #1# ADP (#1# activation <6>) <6>
- AC, ACTIVATING_COMPOUND
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - example: #1# cAMP (#1# the activity with CMP and UMP requires activation by cAMP <1>) <1>
- IN, INHIBITORS
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - example: #1# Mg2+ (#1# Mgcl2, inhibits above 1 mM <1>) <1>
- KI, KI_VALUE
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE {COMPOUND}
    - optional comment can contain: substrate, pH, temperature, concentration, mutant, isozyme, cosubstrate, cofactor
- ME, METALS_IONS
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: COMPOUND
    - optional comment can contain: substrate, pH, temperature, concentration, mutant, isozyme, cosubstrate, cofactor,
      X% activation, X% of initial activity, 1.2fold activation, 1.1 fold activation
    - example: #2# Rb+ (#2# enhances base exchange activity, 1.51-fold activation by 1 mM RbCl <20>) <20>
- MW, MOLECULAR_WEIGHT
    - pattern: PROTEIN_IDS INFO (COMMENT) REFERENCE_IDS
    - INFO pattern: VALUE 
    - optional comment on determination (e.g. SDS-PAGE, calculated, gel filtration, mass spectrometry, etc.)
    - example: #13# 16000 (#13# 6 * 16000, SDS-PAGE <31>) <31>

# left of here
- PM, POSTTRANSLATIONAL_MODIFICATION
    - type of modification (e.g. phosphoprotein, glycoprotein, proteolytic modification, etc.) - 37 categories
- SU, SUBUNITS
    - type of subunits (e.g. monomer, octamer, homotetramer, trimer, dodecamer, etc)
- PI, PI_VALUE
    - isoelectric point
- AP, APPLICATION
    - application (e.g. energy production, pharmacology, degradation, biotechnology, etc.)
- EN, ENGINEERING
    - mutations (e.g. T48C or T48S/W57M/W93A)
- CL, CLONED
    - (e.g. expressed in, expression in, overexpression, expressed). Sometimes no data, otherwise between "()"
- CR, CRYSTALLIZATION
    - crystallization conditions, can contain resolution. Sometimes no data, otherwise between "()"
- PU, PURIFICATION
    - (e.g. alleloenzyme(s) (should be alloenzyme), class I isoenzyme(s)). Sometimes no data, otherwise between "()"
- REN, RENATURED
    - conditions under which the protein was renatured. Sometimes no data, otherwise between "()"
- GS, GENERAL_STABILITY
    - Sometimes no data, otherwise between "()"
- OSS, ORGANIC_SOLVENT_STABILITY
    - solvent, optional comment
- OS, OXIDATION_STABILITY
    - Sometimes no data, otherwise between "()"
- SS, STORAGE_STABILITY
    - conditions and duraction of storage. Sometimes no data, otherwise between "()"
- TS, TEMPERATURE_STABILITY
    - integer, range (E.g. 56, 50-60), 
- RF, REFERENCE
    - literature reference
- KKM, KCAT_KM_VALUE
    - float / integer value, or range 
    optional comment can contain: pH, temperature, molar concentration, mutant, isozyme, cosubstrate, cofactor
- EXP, EXPRESSION
    - up, down, more
- IC50, IC50_VALUE
    - float / integer value or range + {substrate}, 
    optional comment can contain: pH, temperature, molar concentration, mutant, isozyme, cosubstrate, cofactor
