
import os
from sbml_tools import settings
from tqdm import tqdm
from sbml_tools.utils import cache_file


# -----------------------------------------
# BRENDA Tissue Ontology (BTO) and CHEBI
# -----------------------------------------
def parse_owl(owl_path):
    """ Parse the OWL information."""
    from owlready2 import get_ontology, ThingClass

    onto = get_ontology(owl_path).load()

    d_key = {}
    d_label = {}

    for c in tqdm(onto.classes(), desc=f"parsing {owl_path}"):

        label = c.label
        if not label:
            label = None
        elif len(label) == 1:
            label = label[0]
        else:
            raise ValueError(f'single label expected: {label}')

        ancestors = {c.name for c in c.ancestors() if c.name not in ["owl.Thing", "Thing"]}
        synonyms = c.hasExactSynonym + c.hasRelatedSynonym
        description = ThingClass.__getattr__(c, "IAO_0000115")
        if description and len(description) > 1:
            raise ValueError(f'single description expected: {description}')
        description = description[0] if description else ''

        item = dict(key=c.name, label=label, ancestors=ancestors, synonyms=synonyms, description=description,)

        d_key[c.name] = item
        if label:
            d_label[label] = item

    # Add all the synonyms to the dictionary
    for key, item in d_key.items():
        for name in item["synonyms"]:
            if name in d_label:
                key_duplicate = d_label[name]['key']
                if key != key_duplicate:
                    # logging.error(f"Duplicate synonym: '{name}', mismatch: '{key}' vs '{key_duplicate}'")
                    pass
            else:
                d_label[name] = d_key[key]

    return d_label


def get_bto(use_cached=True):
    @cache_file(file_path=os.path.join(settings.CACHE_DIR, 'bto.p'), use_cached=use_cached)
    def parse():
        return parse_owl(owl_path=settings.BTO_OWL)
    return parse()


def get_chebi(use_cached=True):
    @cache_file(file_path=os.path.join(settings.CACHE_DIR, 'chebi.p'), use_cached=use_cached)
    def parse():
        return parse_owl(owl_path=settings.CHEBI_OWL)
    return parse()


# get_bto(False)
# get_chebi(False)
