import re


def compile_mutant_pattern():
    keywords = ('mutant', 'mutated', 'mutation', 'variant', 'engineered', 'recombinant', 'heterologous')
    return re.compile(f"{'|'.join(keywords)}")


def compile_temperature_pattern():
    """
    -?          - an optional hyphen
    \d+         - 1+ digits
    (?:\.\d+)?  - an optional fractional part
    \s*         - 0+ whitespaces
    °           - the degree symbol
    \s*         - 0+ whitespaces
    c           - c char

    tb = block that is repeated twice (to match an optional range part)
         and matches negative and fractional temperature values
    tx = The (?:\s*-\s*<tb>)? matches 1 or 0 repetitions of a hyphen enclosed with 0+ whitespaces
         and then the same block as described above

    note that ° and c are required (at the end)
    """
    tb = r'-?\d+(?:\.\d+)?(?:\s*°)?(?:\s*c)?'
    rx = r'{0}(?:\s*-\s*{0})?\s*°\s*c'.format(tb)
    return re.compile(rx, re.IGNORECASE)


def compile_ph_pattern():
    tb = r'\.?\d+(?:\.\d+)?'
    rx = r'pH\s*{0}(?:\s*-\s*(?:pH)?\s*{0})?'.format(tb)
    return re.compile(rx, re.IGNORECASE)


def compile_ec_pattern():
    pattern = r'\b[a-z|A-Z|_]*([1-7]{1}\.-\.-\.-|[1-7]{1}\.\d+\.-\.-|[1-7]{1}\.\d+\.\d+\.-|[1-7]{1}\.\d+\.\d+\.\d+)'
    return re.compile(pattern)


def compile_reference_pattern():
    return re.compile(r"^<(\d+?)> (.+) {Pubmed:\s*(\d*)\s*}", re.IGNORECASE)


# def compile_all_pattern():
#     return re.compile(r"^#([,\d\s]+?)#(.+)<([,\d\s]+)>")
#
# def compile_value_pattern():
#     """strict value pattern"""
#     proper_float = r'(?<!\.)(?:\d+\.\d+)(?!\.)'
#     short_float = r'(?<!\.\d)(?:\.\d+)(?!\.)'
#     proper_int = r'(?<!\.)(?:\d+)(?!\.)'
#     pat = rf'({proper_float}|{proper_int}|{short_float})'
#     return re.compile(pat)

def compile_value_pattern():
    # integer pattern
    decint = r"(?:(?:[1-9](?:_?\d)*|0)(?![.eE]))"  # r"(?:[1-9](?:_?\d)*|0+(_?0)*)"
    # binint = r"(?:0[bB](?:_?[01])+)"
    # octint = r"(?:0[oO](?:_?[0-7])+)"
    # hexint = r"(?:0[xX](?:_?[0-9a-fA-F])+)"
    # integer = rf"(?:{decint}|{binint}|{octint}|{hexint})"
    # float pattern
    digitpart = r"(?:\d(?:_?\d)*)"
    exponent = rf"(?:[eE][-+]?{digitpart})"
    fraction = rf"(?:\.{digitpart})"
    pointfloat = rf"(?:{digitpart}?{fraction}|{digitpart}\.)"
    exponentfloat = rf"(?:(?:{digitpart}|{pointfloat}){exponent})"
    floatnumber = rf"(?:{pointfloat}|{exponentfloat})"
    # number pattern
    number = rf"([-+]?(?:{decint}|{floatnumber}))"
    return re.compile(number)

# pat = compile_value_pattern()
# pat.findall('.123 5.6 5 7. .7 1e-5 1e5 1e+4 +1 -3 -3.0')


def compile_value_range_pattern():
    value_pattern = compile_value_pattern().pattern[1:-1]
    rx = rf'({value_pattern}-{value_pattern})'
    return re.compile(rx)

# pat = compile_value_range_pattern()
# pat.findall('1-2 1.-2 2.-1 1.-1.')


def compile_value_compound_pattern():
    """strict value pattern followed by compound in curly brackets"""
    # in brenda, values are preceded by # and succeeded by {}. They can also be ranges (e.g. '0.01-0.02' or '1-3')
    value_pattern = compile_value_pattern().pattern
    compound = r'\{(.*?)\}'
    rx = rf'{value_pattern}\s*{compound}'
    # rx = r'([\d.]+(?:e?-[\d.]+)?)\s*\{(.*)\}'  #.format(value_pattern, compound_pattern)
    return re.compile(rx)

# pat = compile_value_compound_pattern()
# pat.findall('.123 {x} 5.6 {1,2-x} 5 {3} 7. {s} .7 {s} 1e-5 {b} 1e5 {d} 1e+4 {s} +1 {1} -3 {.0} -3.0 {p}')


def compile_value_range_compound_pattern():
    """strict value pattern followed by compound in curly brackets"""
    # in brenda, values are preceded by # and succeeded by {}. They can also be ranges (e.g. '0.01-0.02' or '1-3')
    value_range_pattern = compile_value_range_pattern().pattern
    compound = r'\{(.*?)\}'
    rx = rf'{value_range_pattern}\s*{compound}'
    # rx = r'([\d.]+(?:e?-[\d.]+)?)\s*\{(.*)\}'  #.format(value_pattern, compound_pattern)
    return re.compile(rx)

# pat = compile_value_range_compound_pattern()
# pat.findall('1-2 {a} 1.-2 {b} 2.-1 {c} 1.-1. {d}')


def compile_comment_pattern():
    # s = '#177,189,191# atherospermidine (#177,189,191# competitive inhibition <141>) <141>'
    rx = r'\(#[\d+].*[\d+]>\)'
    # COMMENT_PATTERN = re.compile(rx)
    # COMMENT_PATTERN.findall(s)
    return re.compile(rx)


def compile_db_pattern():
    """used to match the database identifier in the protein (PR) field """
    return re.compile(r'\s*(SwissProt|UniProt|TrEMBL|GenBank)\s*', re.IGNORECASE)


def compile_pr_pattern():
    """used to match organism, optional protein identifier, optional database"""
    species = r'.*'
    protein_id = r'(?:[A-N,R-Z][0-9](?:[A-Z][A-Z, 0-9][A-Z, 0-9][0-9]){1,2})|(?:[O,P,Q][0-9][A-Z, 0-9][A-Z, 0-9][A-Z, 0-9][0-9])(?:\.\d+)?'
    database_id = r'SwissProt|UniProt|TrEMBL|GenBank'
    protein_ids = r"({0}(?:.*{0})?)".format('(?:' + protein_id + ')')
    pat = re.compile(r'^(?:(.*?)(?:\s{0}|$))\s*({1})?'.format(protein_ids, database_id), re.IGNORECASE)

    s = 'Bacillus subtilis Q81K34 UniProt'
    assert pat.match(s).groups() == ('Bacillus subtilis', 'Q81K34', 'UniProt')
    s = 'Bacillus subtilis Q81K34'
    assert pat.match(s).groups() == ('Bacillus subtilis', 'Q81K34', None)
    s = 'Thermoanaerobacterium sp.'
    assert pat.match(s).groups() == ('Thermoanaerobacterium sp.', None, None)
    s = 'Lactococcus lactis subsp. lactis Q2HYA0 UniProt'
    assert pat.match(s).groups() == ('Lactococcus lactis subsp. lactis', 'Q2HYA0', 'UniProt')
    s = 'Drosophila sp. (in: Insecta)'
    assert pat.match(s).groups() == ('Drosophila sp. (in: Insecta)', None, None)  # remove brackets?
    s = 'Pseudoalteromonas sp. IAM14594 Q9Z6D6 UniProt'  # IAM14594 == species, not uniprot
    assert pat.match(s).groups() == ('Pseudoalteromonas sp. IAM14594', 'Q9Z6D6', 'UniProt')
    s = 'Methanococcus maripaludis P62426 and P60780 and P62378 SwissProt'
    assert pat.match(s).groups() == ('Methanococcus maripaludis', 'P62426 and P60780 and P62378', 'SwissProt')

    return pat


MUTANT_PATTERN = compile_mutant_pattern()
TEMPERATURE_PATTERN = compile_temperature_pattern()
PH_PATTERN = compile_ph_pattern()
EC_PATTERN = compile_ec_pattern()
# PATTERN_ALL = compile_all_pattern()
VALUE_PATTERN = compile_value_pattern()
VALUE_RANGE_PATTERN = compile_value_range_pattern()
VALUE_COMPOUND_PATTERN = compile_value_compound_pattern()
VALUE_RANGE_COMPOUND_PATTERN = compile_value_range_compound_pattern()

# BRENDA_VALUE_PATTERN = compile_brenda_value_pattern()
PATTERN_RF = compile_reference_pattern()
# VALUE_SUBSTRATE_PATTERN = compile_value_compound_pattern()
COMMENT_PATTERN = compile_comment_pattern()
DB_PATTERN = compile_db_pattern()
PR_PATTERN = compile_pr_pattern()


# s = """betaine + L-homocysteine = dimethylglycine + L-methionine (#4# mechanism <7>; #5# ordered bi-bi mechanism with homocysteine the first substrate to be added and methionine the last product released <3,8>)"""
# s = """betaine + L-homocysteine = dimethylglycine + L-methionine """
# RE_RT_PATTERN.match(s).groups()

# SY_PATTERN = re.compile(r'^(?:#(.*?)#)?\s*{?(.*?)\}?\s*(?:\((#.*?)\))*\s*(?:<(\d.*?)>)*$')
optional_organism = r'(?:#(.*?)#)*'
info = r'(.*?)'
optional_comment = r'(?:\((#.*?>)\))*'
optional_reference = r'(?:<(\d.*?)>)*'
RE_RT_PATTERN = re.compile('^{0}\s*{1}$'.format(info, optional_comment))
LOSE_PATTERN = re.compile('^{0}\s*{1}\s*{2}\s*{3}$'.format(optional_organism, info, optional_comment, optional_reference))


s = """methyltransferase, betaine-homocysteine"""
assert LOSE_PATTERN.match(s).groups() == (None, 'methyltransferase, betaine-homocysteine', None, None)
s = """#3,4# BHMT2 (R)-1,3-whatever <35,43,45>"""
assert LOSE_PATTERN.match(s).groups() == ('3,4', 'BHMT2 (R)-1,3-whatever', None, '35,43,45')
s = """#8,10# PMT2 something (#10,20# isoform <17,18>) <10,12,17,18>"""
assert LOSE_PATTERN.match(s).groups() == ('8,10', 'PMT2 something', '#10,20# isoform <17,18>', '10,12,17,18')

# split on and return this pattern
# UNIPROT_PATTERN = re.compile(MIRIAM[URL_TO_MIRIAM_IDX['http://identifiers.org/uniprot/']]['pattern'].strip('^$'))
UNIPROT_PATTERN = re.compile(r'((?:[A-N,R-Z][0-9](?:[A-Z][A-Z, 0-9][A-Z, 0-9][0-9]){1,2})|(?:[O,P,Q][0-9][A-Z, 0-9][A-Z, 0-9][A-Z, 0-9][0-9])(?:\.\d+)?)')

organism = r'#([\d,\s]*?)#'
info = r'(.*?)'  # cannot strip "(" and ")" because compounds might be parsed wrongly
reference = r'<([\d,\s]*?)>'
STRICT_PATTERN = re.compile('^{0}\s*{1}\s*{2}\s*{3}$'.format(organism, info, optional_comment, reference))

s = "#6# Paramecium tetraurelia   <7>"
STRICT_PATTERN.match(s).groups()
s = "#7# Mus musculus Q3U2J5 UniProt <12,13>"
STRICT_PATTERN.match(s).groups()
s = "#4# Homo sapiens O75648 SwissProt <5>"
STRICT_PATTERN.match(s).groups()
s = "#7# Mus musculus O55060 (#7# isozyme GSTM3-3 <12>) <12,32>"
STRICT_PATTERN.match(s).groups()
s = """#75# Salmonella enterica subsp. enterica serovar Typhimurium P0A2E1 UniProt <113>"""
UNIPROT_PATTERN.findall(s)

s = "#11# Ruminococcus gnavus A7B4V1 UniProt (#11# bifunctional (EC 4.1.1.81) activities (EC 2.7.1.177) <10>) <10>"
STRICT_PATTERN.match(s).groups()
s = "#9# (Proteases, fairly resistant to proteases: <1>, resistant to trypsin and chymotrypsin) <1,11>"
STRICT_PATTERN.match(s).groups()
s = "#7# 5,5-dithiobis(2-nitrobenzoic acid) (#7# 50% residual activity at 0.3 mg/ml <8>) <8>"
STRICT_PATTERN.match(s).groups()



# regular expression testing
def test_mutant_pattern():
    s = 'Some strain comments: mutant mutated mutation variant engineered recombinant'
    mutant_keywords = MUTANT_PATTERN.findall(s)
    assert mutant_keywords == ['mutant', 'mutated', 'mutation', 'variant', 'engineered', 'recombinant']


def test_temperature_pattern():
    s = """some temperatures 30° c - 50 ° c  2°c  34.5 °c 30°c - 40 °c and -45.5° - -56.5° c
              range 30c - 50 °c" or 30c - 40"""
    temperatures = TEMPERATURE_PATTERN.findall(s)
    assert temperatures == ['30° c - 50 ° c', '2°c', '34.5 °c', '30°c - 40 °c', '-45.5° - -56.5° c', '30c - 50 °c']


def test_ph_pattern():
    s = "Some pH's pH 7.0 Ph 3 ph .2 ph 3 - 7.2, ph 2 - ph 3.1 ph2 ph5-6"
    phs = PH_PATTERN.findall(s)
    assert phs == ['pH 7.0', 'Ph 3', 'ph .2', 'ph 3 - 7.2', 'ph 2 - ph 3.1', 'ph2', 'ph5-6']


def test_ec_pattern():
    positives = 'ec1.1.1.1 another 2.3.4.5 and E.C.3.5.1.0 & 1.1.2.232, 1.-.-.- 2.33.2.1 EC_num1.2.3.4'
    positives_expected = ['1.1.1.1', '2.3.4.5', '3.5.1.0', '1.1.2.232', '1.-.-.-', '2.33.2.1', '1.2.3.4']
    assert re.findall(EC_PATTERN, positives, re.IGNORECASE) == positives_expected
    negatives = '22.1.1.1, 8.1.2.3, 4.6.3. 4.3.-.1'
    assert re.findall(EC_PATTERN, negatives, re.IGNORECASE) == []


def test_reference_pattern():
    s = """<1> Author, A.B; Coauthor, A.B.; Paper Title. J. of Science (0000) 19, 813-829. {Pubmed:6794566}"""
    assert PATTERN_RF.match(s).group(0) == s


def test_value_pattern():
    positives = 'some values 1.51 5 5.5 .2   '
    positives_expected = ['1.51', '5', '5.5', '.2']
    negatives = 'NaN 3. 1.2.3.4  .4.4 1.2.'
    assert VALUE_PATTERN.findall(positives) == positives_expected
    assert VALUE_PATTERN.findall(negatives) == []


def test_brenda_value_pattern():
    s = '#25# 0.0028 {species} #25# 1 {metabolite} #25# 0.3 {} # 0.31-.3 {5-a-8-p-thdb[b,f]azocine} #1# 1-3 {}'
    assert BRENDA_VALUE_PATTERN.findall(s) == ['0.0028', '1', '0.3', '0.31-.3', '1-3']


def test_substrate_pattern():
    s = 'KM	#10# 72 {Butanal}  (#10# pH 7.0, 30°C <285>) <285>'
    s2 = """#31# 35.9 (#31#
           O-{5-O-[(E)-feruloyl]-alpha-L-arabinofuranosyl}-(1
           3)-O-beta-D-xylopyranosyl-(1,4)-D-xylopyranose as substrate, 30°C <22>)
           <22>"""
    SUBSTRATE_PATTERN = compile_substrate_pattern()
    SUBSTRATE_PATTERN.findall(s)