# Toolbox
A set of tools for working with SBML models

TODO:
- model annotation
- sbtab conversion
- parameter balancing
- brenda parsing
- equilibrator
- cobra
- kinetizer
- roadrunner
- units (pint)
- data integration
- utility functions for model building

## symbolic_math.py
extends ASTNode.py
- to allow translation to and from sympy

## validator.py
for validation of sbml documents