import os
import re
import libsbml
import tempfile
import warnings
import fractions
import itertools
import functools
import collections
from typing import Optional, Union, Iterable, Dict, Tuple, List

import numpy as np
import pandas as pd
import scipy
import scipy.stats
import statsmodels.api as sm
import cobra
import cobra.test
from cobra.core.gene import eval_gpr, parse_gpr
from cobra.medium import minimal_medium
import escher
import boolean
from tqdm import tqdm
import matplotlib.pyplot as plt
import seaborn as sns
import networkx as nx
from cached_property import cached_property

from sbml_tools import utils
from sbml_tools.sbml_io import read_sbml
from sbml_tools import validate_sbml
from sbml_tools.utils import get_compartment_from_suffix, arrow_match, exists, igetattr, get_method_kwargs

BASE_ATTR = ['id', 'name', 'annotation', 'notes']
COBRA_MODEL_ATTR = BASE_ATTR + ['compartments', 'description'] # objective.expression, objective..direction
COBRA_REACTION_ATTR = BASE_ATTR + ['reaction', 'lower_bound', 'upper_bound', 'subsystem', 'gene_reaction_rule']
COBRA_METABOLITE_ATTR = BASE_ATTR + ['compartment', 'formula', 'charge']
COBRA_GENE_ATTR = BASE_ATTR  + ['functional']
COBRA_GROUP_ATTR = BASE_ATTR  + ['kind']

bool_alg = boolean.BooleanAlgebra()


def add_from_formula(reaction, formula):
    for i, match in enumerate(arrow_match(formula)):
        if match is not None:
            reactant_str, product_str = [s.strip() for s in formula.split(match.group(0))]
            reaction.bounds = ((-1000.0, 1000.0), (0.0, 1000.0), (-1000.0, 0.0))[i]
            break
    else:
        raise ValueError(f"no reaction arrow detected in {formula}")
    if reactant_str: reaction = add_metabolites_from_reaction_str(reactant_str, reaction, -1)
    if product_str: reaction = add_metabolites_from_reaction_str(product_str, reaction, 1)
    return reaction


def add_metabolites_from_reaction_str(reaction_str, reaction, sign):
    for s in [s.strip() for s in reaction_str.split(' + ')]:
        parts = s.split()
        if len(parts) == 1: stoichiometric_coefficient, metabolite_id = 1.0, parts[0].strip()
        elif len(parts) == 2: stoichiometric_coefficient, metabolite_id = parts
        else: raise ValueError(f"malformatted reaction string {reaction_str}, substring: {s}")
        metabolite = cobra.Metabolite(metabolite_id, compartment=get_compartment_from_suffix(metabolite_id))
        reaction.add_metabolites({metabolite: sign * float(stoichiometric_coefficient)})
    return reaction


def to_table(dict_list, attributes): # getter
    l = []
    for item in dict_list:
        d = collections.OrderedDict()
        for attr in attributes:
            d[attr] = getattr(item, attr)
        l.append(d)

    if l:
        return pd.DataFrame(l).set_index('id').sort_index()


def get_inversed_annotations(objects):
    """"""
    annotations = collections.defaultdict(list)
    for x in objects:
        for _, anno in x.annotation.items():
            if isinstance(anno, list):
                for a in anno:
                    annotations[a].append(x)
            else:
                annotations[anno].append(x)
    return annotations


def merge_dict_of_dict(d1, d2):
    d = {}
    for k1 in d1:
        for k2 in d2:
            if isinstance(k1, tuple):
                d[k1 + (k2,)] = {**d1[k1], **d2[k2]}
            else:
                d[k1, k2] = {**d1[k1], **d2[k2]}
    return d


CobraTable = collections.namedtuple('CobraTable', 'model reactions metabolites genes')

# TODO: 'f_replace' read_sbml_model and write_sbml_model introduces prefixed notation (tested: bugged, don't try)

class CobraWrapper(cobra.Model):

    """wraps a cobra model"""

    def __init__(self, id_or_model : Optional[Union[str, cobra.Model]] = None,
                 name : str = ''):
        if isinstance(id_or_model, cobra.Model):
            cobra_model = id_or_model
        elif isinstance(id_or_model, libsbml.SBMLDocument):
            cobra_model = cobra.io.read_sbml_model(id_or_model.toSBML())
        elif isinstance(id_or_model, str) and os.path.exists(id_or_model):
            extension = id_or_model.split('.')[-1]
            cobra_model = {
                'json': cobra.io.load_json_model,
                'yml': cobra.io.load_yaml_model,
                'mat': cobra.io.load_matlab_model}.get(extension, cobra.io.read_sbml_model)(id_or_model)
        elif isinstance(id_or_model, str) or id_or_model is None:
            cobra_model = cobra.Model(id_or_model, name=name)
        else:
            raise TypeError(f'unexpected input: {id_or_model} of type {type(id_or_model)}')

        super().__init__(cobra_model) # TODO: this is now called twice

        try: # I believe the limit is 1000 variables / reactions or so for the cplex community editions
            self.optimize()
        except:
            print(f'defaulting from cplex to glpk solver')
            self.solver = 'glpk'

    def write_model(self, filepath, validate=True, overwrite=False):
        if validate: self.validate()
        extension = filepath.split('.')[-1]
        assert extension in ('xml', 'json', 'yml', 'mat'), f"unrecognized file extension: {extension}"
        if not overwrite:
            assert not os.path.exists(filepath), 'file exists, if you wish then explicitly set overwrite=True'
        {'xml': cobra.io.write_sbml_model,
         'json': cobra.io.save_json_model,
         'yml': cobra.io.save_yaml_model,
         'mat': cobra.io.save_matlab_model}[extension](self, filepath)
        print(f'model written to {filepath}')

    def to_sbml_document(self):
        with tempfile.NamedTemporaryFile(mode='w') as temp:
            cobra.io.write_sbml_model(self, temp.name)
            temp.seek(0)
            doc = read_sbml(temp.name)
        return doc

    ### read and associate BiGG data files: http://bigg.ucsd.edu/data_access
    @cached_property
    def all_bigg_reactions(self):
        return utils.get_bigg_reaction_identifier_mapping()

    @cached_property
    def all_bigg_metabolites(self):
        return utils.get_bigg_metabolite_identifier_mapping()

    @cached_property
    def universal_bigg_model(self):
        return utils.get_universal_model()

    ### taxonomy
    @cached_property
    def taxonomy_to_scientific_name(self):
        return utils.get_taxonomy_id_to_scientific_name()

    @cached_property
    def scientific_name_to_taxonomy(self):
        return utils.invert_dict(self.taxonomy_to_scientific_name)

    @property
    def organism(self):
        taxonomy_id = self.annotation.get('taxonomy')
        if taxonomy_id:
            return self.taxonomy_to_scientific_name[int(taxonomy_id)]

    @organism.setter
    def organism(self, scientific_name):
        self.annotation['taxonomy'] = self.scientific_name_to_taxonomy[scientific_name]

    ### return IDs
    @property
    def compartment_ids(self):
        return list(self.compartments)

    @property
    def reaction_ids(self):
        return [obj.id for obj in self.reactions]

    @property
    def metabolite_ids(self):
        return [obj.id for obj in self.metabolites]

    @property
    def gene_ids(self):
        return [obj.id for obj in self.genes]

    @property
    def group_ids(self):
        return [obj.id for obj in self.groups]

    @property
    def exchange_ids(self):
        return [r.id for r in self.exchanges]

    @property
    def biomass_ids(self): # only this should return None, else rather return an empty lists / dataframe, etc
        return [r for r in self.reaction_ids if 'biomass' in r.lower()]

    @property
    def biomass_id(self): # only this should return None, else rather return an empty lists / dataframe, etc
        biomass_ids = self.biomass_ids
        if len(biomass_ids) == 1:
            return biomass_ids[0]
        elif len(biomass_ids) > 1:
            warnings.warn(f'multiple biomass ids present in the model, use .biomass_ids')

    @property
    def extracellular_metabolites(self):
        return [m for m in self.metabolites if m.compartment=='e']

    @property
    def metabolites_per_compartment(self):
        d = collections.defaultdict(lambda: [])
        [d[m.compartment].append(m) for m in self.metabolites]
        return pd.Series(d)

    @property
    def metabolite_ids_without_compartment_suffix(self):
        return [re.sub("|".join(['_' + x for x in self.compartments]), '', obj.id) for obj in self.metabolites]

    ### annotations
    @property
    def reaction_annotations(self):
        return pd.DataFrame.from_dict({r.id: r.annotation for r in self.reactions}, orient='index')

    @property
    def metabolite_annotations(self):
        return pd.DataFrame.from_dict({m.id: m.annotation for m in self.metabolites}, orient='index')

    @property
    def gene_annotations(self):
        return pd.DataFrame.from_dict({m.id: m.annotation for m in self.genes}, orient='index')

    ### reactions
    @property
    def transport_reactions(self):
        return [r for r in self.reactions if len(set([m.compartment for m in r.metabolites])) > 1]

    @property
    def transport_reaction_ids(self):
        return [r.id for r in self.transport_reactions]

    def unbalanced_reactions(self, exclude_boundary=True, exclude_biomass=True):
        d = {}
        for r in self.reactions:
            if (r.boundary and exclude_boundary) or (exclude_biomass and r.id in self.biomass_ids):
                continue
            imbalance = r.check_mass_balance()
            if imbalance:
                d[r.id] = imbalance
        return pd.DataFrame(d)

    @property
    def subsystems(self):
        return [g for g in self.groups if g.annotation['sbo'] == 'SBO:0000633']

    def subsystem_enrichment(self, reactions: Iterable[cobra.Reaction], alpha=0.05, method='holm-sidak'):
        """assuming gorups are subsystems, composed of reactions"""
        M = len(self.reactions)
        n = len(reactions)
        result = np.empty((len(self.subsystems), 3))
        for i, subsystem in enumerate(self.subsystems):
            N = len(subsystem.members)
            x = len(set(reactions).intersection(subsystem.members))
            pval = scipy.stats.hypergeom.sf(x - 1, M, n, N)
            result[i, :] = [N, x, pval]
        reject, result[:, -1] = sm.stats.multipletests(pvals=result[:, -1], alpha=alpha, method=method)[:2]
        result = np.column_stack((result, reject))
        columns = ['id', 'name', 'group_size', 'successes', 'adjusted_p_values', 'enrichment']
        data = zip([[s.id, s.name] for s in self.subsystems], result)
        df = pd.DataFrame([list(itertools.chain(*i)) for i in data], columns=columns).set_index('id')
        df['enrichment'] = df['enrichment'].astype(bool)
        return df

    def _subsystem_flux_enrichment(self, non_zero_fluxes, alpha=0.05, method='holm-sidak'):
        """
        M = number of reactions (universe)
        N = number of subsystem reactions (successes)
        n = number of differential reaction rates (sample size)
        x = number of differential reaction rates in the subsystem (number of successes in sample)
        """
        M = len(self.reactions)
        n = len(non_zero_fluxes)
        result = np.empty((len(self.subsystems), 3))
        for i, subsystem in enumerate(self.subsystems):
            N = len(subsystem.members)
            x = len(set(non_zero_fluxes.keys()).intersection([m.id for m in subsystem.members]))
            pval = scipy.stats.hypergeom.sf(x - 1, M, n, N)
            result[i, :] = [N, x, pval]

        # multiple testing correction
        reject, result[:, -1] = sm.stats.multipletests(pvals=result[:, -1], alpha=alpha, method=method)[:2]
        return np.column_stack((result, reject))

    def subsystem_flux_enrichment(self, flux_solution: Optional[pd.Series] = None,
                             alpha=0.05, method='holm-sidak'):
        """"""
        if flux_solution is None:
            flux_solution = self.optimize().fluxes

        non_zero_fluxes = flux_solution[flux_solution.abs() > self.tolerance]
        result = self._subsystem_flux_enrichment(non_zero_fluxes, alpha=alpha, method=method)

        # formatting
        columns = ['id', 'name', 'group_size', 'successes', 'adjusted_p_values', 'enrichment']
        data = zip([[s.id, s.name] for s in self.subsystems], result)
        df = pd.DataFrame([list(itertools.chain(*i)) for i in data], columns=columns).set_index('id')
        df['enrichment'] = df['enrichment'].astype(bool)
        return df

    def iterative_subsystem_flux_enrichment(self, flux_solution: Optional[pd.Series] = None,
                                       alpha=0.05, method='holm-sidak', filepath=None, show=False):
        """
        a flux solution is sorted by absolute values, from high to low
        then we iterate from 1 to n, and perform enrichment analyses
        """
        if flux_solution is None:
            flux_solution = self.optimize().fluxes

        abs_fluxes = flux_solution.abs()
        non_zero_fluxes = abs_fluxes[abs_fluxes > self.tolerance].sort_values(ascending=False)

        arr = np.empty((non_zero_fluxes.shape[0], len(self.subsystems)))
        for i in tqdm(range(non_zero_fluxes.shape[0]), desc='iterative enrichment'):
            arr[i, :] = self._subsystem_flux_enrichment(non_zero_fluxes[:i + 1], alpha=alpha, method=method)[:, 2]
        df = pd.DataFrame(arr, columns=[sub.id for sub in self.subsystems])

        if show or filepath:
            data = df.replace(0, df[df!=0].min().min()).apply(lambda x: -np.log10(x))
            fig = plt.figure(figsize=(14, 6))
            plt.axhline(y=-np.log10(alpha), linestyle='dashed', color='red')
            sns.violinplot(data=data, scale='count', cut=0)
            plt.title('Iterative subsystem enrichment analysis')
            plt.ylabel('-log10(adj. p-value)')
            if filepath:
                plt.savefig(filepath)
            if show:
                plt.show()

        return df

    @staticmethod
    def nutrient_combinations(supplements):
        return dict(functools.reduce(merge_dict_of_dict, supplements))

    def nutrient_combinations_table(self, supplements):
        return pd.DataFrame(self.nutrient_combinations(supplements)).fillna(0)

    def screen(self, base_medium: dict, supplements : Union[dict, list, tuple], gene_knock_outs : list):
        """
        perform a metabolic flux screen:
        a grid search through medium supplements and gene knockout mutants
        """
        if isinstance(supplements, (list, tuple)):
            if all([isinstance(d, dict) for d in supplements]):
                supplements = self.nutrient_combinations(supplements)

        gene_knock_outs = [self.genes.get_by_id(x) if isinstance(x, str) else x for x in gene_knock_outs]

        d = {}
        with self as model: # context manager
            combinations = list(itertools.product(supplements, gene_knock_outs))
            for medium_id, gene in tqdm(combinations, desc='screening'):
                model.medium = {**base_medium, **supplements[medium_id]}
                for gene in gene_knock_outs:
                    gene.functional = False
                    d[(medium_id, gene.id)] = model.optimize().fluxes

        return pd.DataFrame(d)

    def group_screen(self, screen):
        screen[screen.abs() < self.tolerance] = 0
        differences = screen.drop_duplicates()

        d = collections.defaultdict(tuple)
        for col_levels in itertools.product(*differences.columns.levels):
            data = differences[col_levels]
            d[tuple(data.values)] += (col_levels,)

        inv_d = utils.invert_dict(d)
        groups = {f'group{i}': k for i, k in enumerate(inv_d)}
        differences = pd.DataFrame(inv_d.values(), columns=differences.index, index=groups).T
        return differences, groups

    ### matrices
    @property
    def stoichiometric_matrix(self):
        return cobra.util.array.create_stoichiometric_matrix(self, array_type='DataFrame')

    @property
    def bool_matrix(self):
        return self.stoichiometric_matrix.astype(bool)

    @property
    def metabolite_adjacency_matrix(self):
        N = np.zeros((len(self.metabolites), len(self.metabolites)))
        for i, metabolite in enumerate(self.metabolites):
            for reaction in metabolite.reactions:
                for m in reaction.metabolites:
                    j = self.metabolites.index(m)
                    N[i, j] = 1
        return pd.DataFrame(N, columns=self.metabolite_ids, index=self.metabolite_ids)

    @property
    def reaction_adjacency_matrix(self):
        N = np.zeros((len(self.reactions), len(self.reactions)))
        for i, reaction in enumerate(self.reactions):
            for metabolite in reaction.metabolites:
                for r in metabolite.reactions:
                    j = self.reactions.index(r)
                    N[i, j] = 1
        return pd.DataFrame(N, columns=self.reaction_ids, index=self.reaction_ids)

    def incidence_matrix(self):
        ## TODO: might be useful connecting groups of metabolites converted together
        raise NotImplementedError()

    ### graphs
    @property
    def metabolite_adjacency_graph(self):
        return nx.from_pandas_adjacency(self.metabolite_adjacency_matrix)

    @property
    def reaction_adjacency_graph(self):
        return nx.from_pandas_adjacency(self.reaction_adjacency_matrix)

    def metabolic_network_graph(self, exclude : Optional[List[str]] = None, fluxes=None):
        # TODO: fluxes implementation, needs to be scaled as well I think
        if exclude is None: exclude = []
        if fluxes is None: fluxes = {}
        graph = nx.DiGraph()
        metabolites = [m for m in self.metabolites if m.id not in exclude]
        graph.add_nodes_from(metabolites)
        for metabolite in metabolites:
            for reaction in [r for r in metabolite.reactions if r.id not in exclude]:
                d = {}
                for m in [m for m in reaction.metabolites if m.id not in exclude]:
                    d[(metabolite, m, fluxes.get(reaction.id, 3.0))] = reaction.id
                graph.add_weighted_edges_from(d, label=reaction.id)
        return graph

    def plot_metabolic_network_graph(self, exclude : Optional[List[str]] = None, fluxes=None):
        ## TODO: somehow the edge_labels overlap with nodes, needs fixing
        graph = self.metabolic_network_graph(exclude=exclude)

        # some arbitrary cutoffs for raising exceptions on network drawing
        if graph.number_of_nodes() > 100:
            raise ValueError(f'graph contains too many nodes: {graph.number_of_nodes()}')
        if graph.number_of_edges() > 1000:
            raise ValueError(f'graph contains too many edges: {graph.number_of_edges()}')

        pos = nx.drawing.nx_agraph.graphviz_layout(graph)
        plt.figure(figsize=(25,25))

        edges = graph.edges()
        labels = {k: v['label'] for k, v in graph.edges.items()}
        # [graph[u][v]['label'] for u, v in edges]
        weights = [graph[u][v]['weight'] for u, v in edges]
        nx.draw(graph, pos, with_labels=True, edge_color='black', width=weights, linewidths=1,
                node_size=1000, node_color='pink', alpha=0.9)

        nx.draw_networkx_edge_labels(graph, pos, edge_labels=labels, font_color='black')
        plt.show()

    @property
    def n_connections_per_metabolite(self):
        bool_matrix = self.bool_matrix
        return bool_matrix.sum(axis=1)

    @property
    def reactions_with_non_integer_stoichiometries(self):
        l = []
        for r in self.reactions:
            for metabolite, coefficient in r.metabolites.items():
                if not coefficient.is_integer() and r not in l:
                    l.append(r)
        return l

    def make_integer_stoichiometries(self, exclude_biomass : bool = True, exclude : Optional[List[str]] = None):
        if not exclude: exclude = []
        for r in self.reactions_with_non_integer_stoichiometries:
            if (r.id in exclude) or (r.id in self.biomass_ids and exclude_biomass):
                continue
            stoich_coefficients = r.metabolites.values()
            denominators = [fractions.Fraction(x).limit_denominator().denominator for x in stoich_coefficients]
            lcm = functools.reduce(lambda a, b: a * b // np.gcd(a, b), denominators)
            r.add_metabolites({k: v * lcm - v for k,v in r.metabolites.items()})
            print(f"multiplied stoichiometries in {r.id} with {lcm}")

    # def multiply_stoichiometry(self, reaction : str, factor : int): # if integers are required
    #     reaction = self.get_by_id(reaction)
    #     reaction *= factor

    ### reaction bounds
    @property
    def bounds(self):
        return pd.DataFrame.from_dict({r.id: r.bounds for r in self.reactions}, orient='index', columns=['lb', 'ub'])

    @bounds.setter
    def bounds(self, bounds : Dict[str, Tuple[float, float]]):
        for k,v in bounds.items():
            self.get_by_id(k).bounds = v

    ### getters
    @property
    def _core(self):
        return [self] + self.metabolites + self.reactions + self.genes + self.groups

    def get_by_id(self, id : str):
        '''doesn't crash on missing keys, searches all entities for what should be uniquely identifiable objects'''
        entities = [obj for obj in self._core if obj.id == id]
        n_entities = len(entities)
        assert not n_entities > 1, f"More than two entities found, {entities}, SBML model is invalid"
        if n_entities == 1:
            return entities[0]

    def get_by_name(self, name):
        return [obj for obj in self._core if obj.name == name]

    def get_by_annotation(self, annotation : str):
        """retrieve objects on the basis of their annotation"""
        return get_inversed_annotations(self._core).get(annotation)

    def get_reaction_by_annotation(self, annotation : str):
        return get_inversed_annotations(self.reactions).get(annotation)

    def get_metabolite_by_annotation(self, annotation : str):
        return get_inversed_annotations(self.metabolites).get(annotation)

    def get_gene_by_annotation(self, annotation : str):
        return get_inversed_annotations(self.genes).get(annotation)

    ### convert model to and from dataframes
    @property
    def model_table(self):
        d = collections.OrderedDict()
        for attr in COBRA_MODEL_ATTR:
            d[attr] = getattr(self, attr)
        d['objectives'] = self.objective_dict
        d['objective_direction'] = self.objective.direction
        return pd.Series(d)

    @property
    def reaction_table(self):
        return to_table(self.reactions, COBRA_REACTION_ATTR)

    @property
    def metabolite_table(self):
        return to_table(self.metabolites, COBRA_METABOLITE_ATTR)

    @property
    def gene_table(self):
        return to_table(self.genes, COBRA_GENE_ATTR)

    @property
    def group_table(self):
        table = to_table(self.groups, COBRA_GROUP_ATTR)
        table = table.join(pd.DataFrame({'members': {g.id: [member.id for member in g.members] for g in self.groups}}))
        return table.reindex(self.group_ids)

    @classmethod
    def from_table(cls, table):
        """uses the reaction table primarily, because associations with the model object are required"""
        assert isinstance(table, CobraTable), f"must be a CobraTable object, not {type(table)}"
        model = cobra.Model()
        for attr in COBRA_MODEL_ATTR:
            setattr(model, attr, table.model[attr])

        reactions = []
        if exists(table.reactions):
            for _, entry in table.reactions.reset_index().iterrows():
                reaction = cobra.Reaction()
                for attr in COBRA_REACTION_ATTR:
                    if attr == 'reaction':
                        add_from_formula(reaction, entry[attr])
                    else:
                        setattr(reaction, attr, entry[attr])
                reactions.append(reaction)
        model.add_reactions(reactions)

        if exists(table.metabolites):
            for _, entry in table.metabolites.reset_index().iterrows():
                metabolite = model.metabolites.get_by_id(entry['id'])
                for attr in COBRA_METABOLITE_ATTR:
                    setattr(metabolite, attr, entry[attr])

        if exists(table.genes):
            for _, entry in table.genes.reset_index().iterrows():
                gene = model.genes.get_by_id(entry['id'])
                for attr in COBRA_GENE_ATTR:
                    setattr(gene, attr, entry[attr])

        model.objective =  {model.reactions.get_by_any(k)[0]:v for k,v in table.model['objectives'].items()}
        model.objective.direction = table.model['objective_direction']
        return cls(model)

    @property
    def to_table(self): # TODO: groups
        return CobraTable(self.model_table, self.reaction_table, self.metabolite_table, self.gene_table)

    ## model objective
    @property
    def objective_dict(self):
        objectives = collections.OrderedDict()
        for arg in self.objective.expression.args:
            coefficient, rxn_id = str(arg).split('*')
            if rxn_id in self.reactions:
                objectives[rxn_id] = float(coefficient)
        return objectives

    @property
    def objective_str(self):
        objectives = self.objective_dict
        if len(objectives.keys()) == 1 and sum(objectives.values()) == 1.0:
            return list(objectives)[0]
        return ', '.join([str(f"{k}: {v}") for k,v in objectives.items()])

    def fluxes(self):
        return self.optimize().fluxes

    def shadow_prices(self):
        return pd.Series({m.id: m.shadow_price for m in self.metabolites})

    def shadow_prices_medium_components(self):
        return self.shadow_prices()[[m.id for m in self.metabolites_in_medium]]

    ### flux sampling
    def sample(self, n, method : str = 'optgp', remove_invalid : bool = True, thinning : int = 100,
               processes : int = None, seed : int = None): # not implemented optgp batch sampling
        """ if multiple processes are given, the sampler will default to optgp. Default processes for otpgp is 4
        (default Configuration.processes), and the nearest multiple hereof (rounded upwards) is returned. """
        assert method in ('optgp', 'achr'), f"method '{method}' not recognized, pick 'optgp' or 'achr'"
        if processes and method != 'optgp': method = 'optgp'
        sampler = igetattr(cobra.sampling, f'{method}sampler')(self, thinning=thinning, seed=seed)
        if processes: sampler.processes = processes
        sample = sampler.sample(n)
        if remove_invalid: sample = sample[sampler.validate_species_refererence_sbo(sample) == "v"]
        if len(sample) < n: warnings.warn(f"removed {n - len(sample)} invalid samples")
        return sample[:n]

    ### associating flux analysis methods
    def fba(self, objective_sense=None, raise_error=False):
        return self.optimize(**get_method_kwargs(locals()))

    def fva(self, reaction_list=None, loopless=False, fraction_of_optimum=1.0, pfba_factor=None, processes=None):
        return cobra.flux_analysis.variability.flux_variability_analysis(self, **get_method_kwargs(locals()))

    def pfba(self, fraction_of_optimum=1.0, objective=None, reactions=None):
        return cobra.flux_analysis.pfba(self, **get_method_kwargs(locals()))

    def room(self, solution=None, linear=False, delta=0.03, epsilon=0.001):
        return cobra.flux_analysis.room(self, **get_method_kwargs(locals()))

    def moma(self, solution=None, linear=True):
        return cobra.flux_analysis.moma(self, **get_method_kwargs(locals()))

    def geometric_fba(self, epsilon=1e-06, max_tries=200, processes=None):
        return cobra.flux_analysis.geometric_fba(self, **get_method_kwargs(locals()))

    def loopless(self, fluxes=None):
        return cobra.flux_analysis.loopless_solution(self, fluxes=fluxes)

    def gapfill(self, universal=None, lower_bound=0.05, penalties=None, demand_reactions=True,
                exchange_reactions=False, iterations=1) -> List[list]:
        return cobra.flux_analysis.gapfill(self, **get_method_kwargs(locals()))

    def production_envelope(self, reactions, objective=None, carbon_sources=None, points=20, threshold=None):
        return cobra.flux_analysis.production_envelope(self, **get_method_kwargs(locals()))

    def find_blocked_reactions(self, reaction_list : Iterable = None, zero_cutoff : float = None,
                               open_exchanges : bool = False, processes : int = None):
        return cobra.flux_analysis.find_blocked_reactions(self, **get_method_kwargs(locals()))

    def remove_blocked_reactions(self):
        model = cobra.flux_analysis.fastcc(self)
        removed = {item: [r.id for r in getattr(self, item) if r not in getattr(model, item)]
                   for item in ('reactions', 'metabolites', 'genes')}
        self._model = model
        return removed

    ### medium
    def find_minimal_medium(self, min_objective_value=0.1, exports=False,
                            minimize_components=False, open_exchanges=False):
        return minimal_medium(self, **get_method_kwargs(locals()))

    @property
    def metabolites_in_medium(self):
        metabolites = set()
        for exchange_reaction_id in list(self.medium):
            reaction = self.reactions.get_by_id(exchange_reaction_id)
            for metabolite in reaction.metabolites:
                metabolites.add(metabolite)
        return metabolites

    @property
    def elements_in_medium(self):
        elements = set()
        for metabolite in self.metabolites_in_medium:
            for element in metabolite.elements:
                elements.add(element)
        return elements

    def find_element_in_medium(self, element):
        reactions = []
        for exchange_reaction_id in list(self.medium):
            reaction = self.reactions.get_by_id(exchange_reaction_id)
            for metabolite in reaction.metabolites:
                if element in metabolite.elements:
                    reactions.append(reaction)
        return reactions

    ### model validation
    def validate(self, offcheck: Iterable = 'u', check_warnings : bool = False):
        with tempfile.NamedTemporaryFile(mode='w') as temp:
            cobra.io.write_sbml_model(self, temp.name)
            temp.seek(0)
            validate_sbml(temp.name, offcheck, check_warnings)

    ### functions to conveniently add or update metabolites and reactions
    def add_or_update_metabolite(self, metabolite : Union[str, cobra.Metabolite],
                                 compartment: Optional[str] = None,
                                 name : Optional[str] = None,
                                 formula : Optional[str] = None,
                                 charge : Optional[int] = None,
                                 ):
        """create or update metabolite information, and add to model"""
        if isinstance(metabolite, str):
            if self.get_by_id(metabolite): metabolite = self.get_by_id(metabolite)
            else: metabolite = cobra.Metabolite(metabolite)
        assert isinstance(metabolite, cobra.Metabolite), f"not a metabolite: {metabolite}"
        self.add_metabolites([metabolite])

        kwargs = locals()
        for attr in ['name', 'compartment', 'formula', 'charge']:
            if kwargs[attr]: setattr(metabolite, attr, kwargs[attr])

        if not metabolite.compartment:
            if '_' in metabolite.id: metabolite.compartment = get_compartment_from_suffix(metabolite.id)[-1]
            else: raise ValueError(f"compartment must be specified (e.g. as suffix '_c') to yield a valid SBML file")
        return metabolite

    def add_or_update_reaction(self, reaction : Union[str, cobra.Reaction],
                               reactants : Optional[Dict[str, float]] = None,
                               products: Optional[Dict[str, float]] = None,
                               name: Optional[str] = None,
                               lower_bound : Optional[float] = None,
                               upper_bound : Optional[float] = None,
                               gene_reaction_rule : Optional[str] = None,
                               subsystem: Optional[str] = None,
                               ):
        """create or update reaction information, and add to model"""
        if isinstance(reaction, str):
            if self.get_by_id(reaction): reaction = self.get_by_id(reaction)
            else: reaction = cobra.Reaction(reaction)
        assert isinstance(reaction, cobra.Reaction), f"not a reaction: {reaction}"
        self.add_reactions([reaction])

        kwargs = locals()
        for attr in ['name', 'lower_bound', 'upper_bound', 'gene_reaction_rule', 'subsystem']:
            if kwargs[attr]: setattr(reaction, attr, kwargs[attr])

        if not reactants: reactants = {}
        if not products: products = {}
        [self.add_or_update_metabolite(met) for met in {**reactants, **products}]
        reaction.add_metabolites({**{k: (-v if v > 0 else v) for k, v in reactants.items()}, **products})
        return reaction

    ### add reactions from string representation for lazy building
    def from_reaction_formulae(self, formulae : dict):
        # not using the cobra reaction setter, because it: does not add compartments, adds stoich 1 instead of 1.0
        for id, formula in formulae.items():
            reaction = cobra.Reaction(id)
            self.add_reactions([reaction])
            add_from_formula(reaction, formula)

    def print_reactions(self):
        for reaction in self.reactions:
            print(f"{reaction.id}: {reaction.reaction}")

    def reactions_from_genes(self, genes):
        return {gene.id: [r for r in gene.reactions] for gene in genes}

    ### gene and reactions deletions / essentiality
    @property
    def essential_genes_per_reaction(self):
        return {reaction.id: {gene.id: not eval_gpr(parse_gpr(reaction.gene_reaction_rule)[0], {gene.id})
                              for gene in reaction.genes} for reaction in self.reactions}

    @property
    def gene_reaction_rules(self):
        return pd.Series({reaction.id: reaction.gene_reaction_rule for reaction in  self.reactions})

    @property
    def reactions_without_genes(self):
        return [r for r in self.reactions if not r.genes]

    @property
    def knocked_out_genes(self):
        return [gene for gene in self.genes if not gene.functional]

    def single_gene_deletion(self, gene_list : Iterable = None, method : str = 'fba',
                             solution : cobra.core.Solution = None, processes : int = None, **kwargs):
        return cobra.flux_analysis.single_gene_deletion(self, **get_method_kwargs(locals()))

    def double_gene_deletion(self, gene_list1 : Iterable = None, gene_list2 : Iterable = None, method : str = 'fba',
                             solution : cobra.core.Solution = None, processes : int = None, **kwargs):
        return cobra.flux_analysis.double_gene_deletion(self, **get_method_kwargs(locals()))

    def find_essential_genes(self, threshold : float = None, processes : int = None):
        return cobra.flux_analysis.find_essential_genes(self, **get_method_kwargs(locals()))

    def single_reaction_deletion(self, reaction_list : Iterable = None, method : str = 'fba',
                                 solution : cobra.core.Solution = None, processes : int = None, **kwargs):
        return cobra.flux_analysis.single_reaction_deletion(self, **get_method_kwargs(locals()))

    def double_reaction_deletion(self, reaction_list1 : Iterable = None, reaction_list2 : Iterable = None,
                                 method : str = 'fba', solution : cobra.core.Solution = None,
                                 processes : int = None, **kwargs):
        return cobra.flux_analysis.double_reaction_deletion(self, **get_method_kwargs(locals()))

    def find_essential_reactions(self, threshold : float = None, processes : int = None):
        return cobra.flux_analysis.find_essential_reactions(self, **get_method_kwargs(locals()))

    def __repr__(self):
        return f"""\n{self.__class__.__name__} at {hex(id(self))}\n\n\
                          id: '{self.id}'
                        name: '{self.name}'

                   objective: {self.objective_str}
                   direction: {self.objective.direction}

                       genes: {len(self.genes)}
                   reactions: {len(self.reactions)}
                 metabolites: {len(self.metabolites)}
                compartments: {len(self.compartments)}
        """

    def neighbors(self, metabolite : Union[str, cobra.Metabolite], n=1,
                  ignore : Optional[List[str]] = None, element=None):
        ## TODO: consider bounds, reactants, products
        # TODO: return network, not neighbors
        """This quickly explodes, as the network grows exponentially"""
        if isinstance(metabolite, str):
            metabolite = self.metabolites.get_by_id(metabolite)

        if ignore is None:
            ignore = []

        if element:
            if not element in  metabolite.elements:
                raise ValueError(f"{metabolite} does not contain {element}\n{metabolite.id}: {metabolite.elements}")

        # recurse
        r, m = [], []
        def get_connections(metabolite, level=0):
            if level == n:
                return

            for reaction in metabolite.reactions:
                if reaction in r:
                    continue
                r.append(reaction)

                for metabolite in reaction.metabolites:
                    if metabolite.id in ignore:
                        continue
                    elif element and not element in metabolite.elements:
                        continue
                    else:
                        if metabolite in m:
                            continue
                        m.append(metabolite)

                    get_connections(metabolite, level=level+1)

        get_connections(metabolite)

        return m



