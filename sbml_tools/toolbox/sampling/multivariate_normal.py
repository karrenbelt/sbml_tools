
import numpy as np


def multivariate_normal_sampler(mean, covariance, n_samples=1):
    # faster than numpy.random.multivariate_normal, but might run into numerical issues
    L = np.linalg.cholesky(covariance)
    Z = np.random.normal(size=(n_samples, covariance.shape[0]))
    return Z.dot(L) + mean
