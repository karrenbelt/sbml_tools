
import emcee
import numpy as np
import pandas as pd
from scipy.stats import norm, multivariate_normal
import matplotlib.pyplot as plt

# To compute the truncated density function pdf_t from the entire density function pdf, do the following:
#
# Let [a, b] be the truncation interval; (x axis)
# Let A := cdf(a) and B := cdf(b)
# Then pdf_t(x) := pdf(x) / (B - A) if x in [a, b] and 0 elsewhere.
# In cases where a = -infinity (resp. b = +infinity), take A := 0 (resp. B := 1).

# area under the curve should also equal 1.0, therefore we need to rescale after truncating it
seed = 42
np.random.seed(seed)


def test_univariate_rejection_sampling():
    n_samples = 100_000
    amin, amax = -1, 2
    samples = np.zeros((0,))    # empty for now
    while samples.shape[0] < n_samples:
        s = np.random.normal(0, 1, size=(n_samples,))
        accepted = s[(s >= amin) & (s <= amax)]
        samples = np.concatenate((samples, accepted), axis=0)
    samples = samples[:n_samples]

    fig = plt.hist(samples, bins=50, density=True)
    t = np.linspace(-2, 3, 500)
    plt.plot(t, norm.pdf(t)/(norm.cdf(amax) - norm.cdf(amin)), 'r')
    plt.show()


n_samples = 100_000
mean = np.array([0.3, 2])
cov = np.array([[2, 1.1], [1.1, 2]])
bounds = np.array([(-1.0, 2.4)] * mean.shape[0])


def test_multivariate_rejection_sampling():
    samples = np.zeros((0, 2))
    while samples.shape[0] < n_samples:
        s = np.random.multivariate_normal(mean, cov, size=(n_samples,))
        accepted = s[(np.min(s - bounds[:, 0], axis=1) >= 0) & (np.max(s - bounds[:, 1], axis=1) <= 0)]
        samples = np.concatenate((samples, accepted), axis=0)
    samples = samples[:n_samples, :]

    fig, axs = plt.subplots(1, 2, figsize=(14,6))
    #multi_norm = multivariate_normal(mean, cov)
    for i in range(len(mean)):
        #t = np.linspace(lb[i]-1, ub[i]+1, 500)
        axs[i].hist(samples[:, i], bins=50, density=True)
        #multi_norm = multivariate_normal(mean[i], cov[i][0])
        #axs[i].plot(t, multi_norm.pdf(t) / (multi_norm.cdf(ub[i]) - multi_norm.cdf(lb[i])), 'r')
    plt.show()


def emcee_multivariate():
    n_walkers = 250
    n_dim = mean.shape[0] # no idea
    n_burnin = 1000
    n_steps = 1000

    def lnprob_trunc_norm(x, mu, icov, bounds):
        if np.any(x < bounds[:,0]) or np.any(x > bounds[:,1]):
            return -np.inf
        diff = x - mu
        return - 0.5 * np.dot(diff, np.dot(icov, diff))

    icov = np.linalg.inv(cov)

    # TODO: check why this doens't work
    p0 = emcee.utils.sample_ball(mean, np.sqrt(np.diag(cov)), size=n_walkers)
    # p0 = np.random.multivariate_normal(mean, cov, size=n_walkers)

    #p0 = np.random.rand(n_dim * n_walkers).reshape((n_walkers, n_dim)) * -10
    sampler = emcee.EnsembleSampler(n_walkers, n_dim, lnprob_trunc_norm, args=[mean, icov, bounds])
    # burn-in
    pos, prob, state = sampler.run_mcmc(p0, n_burnin)
    # production run
    sampler.reset()
    sampler.run_mcmc(pos, n_steps)

    for i in range(n_dim):
        plt.figure()
        plt.hist(sampler.flatchain[:,i], 100, color="k", histtype="step")
        plt.title(f"Dimension {i}")
        plt.show()
    print(sampler.acceptance_fraction.mean())



# 2. gibbs sampling
"""
Suppose we have two parameters p1 and p2 and some data y. Our goal is to find the posterior distribution of
p(p1,p2||x). To do this we need to find the conditional distributions p(p1||p2,x) and p(p2||p1,x). Then, Gibbs updates
are: pick some p2(i), sample p1(i+1) ~ p(p1||p2,x), sample p2(i+1) ~ p(p2||p1,x), and keep incrementing i.

1. Write down the posterior conditional density in log-form
2. Throw away all terms that don’t depend on the current sampling variable
3. Pretend this is the density for your variable of interest and all other variables are fixed.
4. That’s your conditional sampling density
"""
#