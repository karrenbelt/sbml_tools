# https://calgo.acm.org

import numpy as np
from scipy.stats import norm

# To compute the truncated density function pdf_t from the entire density function pdf, do the following:
#
# Let [a, b] be the truncation interval; (x axis)
# Let A := cdf(a) and B := cdf(b)
# Then pdf_t(x) := pdf(x) / (B - A) if x in [a, b] and 0 elsewhere.
# In cases where a = -infinity (resp. b = +infinity), take A := 0 (resp. B := 1).


def log_normal_truncated_ab_sample(mu, sigma, a, b):
    assert sigma >= 0.0
    assert b >= a
    lncdf_a = log_normal_cdf(a, mu, sigma)
    lncdf_b = log_normal_cdf(b, mu, sigma)
    cdf  = np.random.uniform(lncdf_a, lncdf_b, size=1)
    x = log_normal_cdf_inv(cdf, mu, sigma)
    return x


def log_normal_cdf(x, mu, sigma):
    if x <= 0.0:
        cdf = 0.0
    else:
        cdf = norm.cdf(np.log(x), loc=mu, scale=sigma)
    return cdf


def log_normal_cdf_inv(cdf, mu, sigma):
    assert cdf > 0.0 and cdf < 1.0
    logx = norm.ppf(cdf, loc=mu, scale=sigma)
    x = np.exp(logx)
    return x

# TESTING


def log_normal_truncated_ab_sample_test():
    nsample = 1000
    seed = 42
    np.random.seed(seed)

    mu = 0.5
    sigma = 3.0
    a = np.exp(mu)
    b = np.exp(mu + 2.0 * sigma)

    pdf_mean = log_normal_truncated_ab_mean(mu, sigma, a, b)
    pdf_variance = log_normal_truncated_ab_variance(mu, sigma, a, b)

    print('PDF parameter mu =             %14g' % mu)
    print('PDF parameter sigma =          %14g' % sigma)
    print('PDF parameter a =              %14g' % a)
    print('PDF parameter b =              %14g' % b)
    print('PDF mean =                     %14g' % pdf_mean)
    print('PDF variance =                 %14g' % pdf_variance)

    x = np.zeros(nsample)
    for i in range(nsample):
        x[i] = log_normal_truncated_ab_sample(mu, sigma, a, b)

    mean = np.mean(x)
    variance = np.var(x, ddof=1)
    xmax = np.max(x)
    xmin = np.min(x)

    print()
    print('sample size =     %14d' % (nsample))
    print('sample mean =     %14g' % (mean))
    print('sample variance = %14g' % (variance))
    print('sample maximum =  %14g' % (xmax))
    print('sample minimum =  %14g' % (xmin))
    return


def log_normal_truncated_ab_mean(mu, sigma, a, b):
    a0 = (np.log(a) - mu) / sigma
    b0 = (np.log(b) - mu) / sigma
    c1 = norm.cdf(sigma - a0)
    c2 = norm.cdf(sigma - b0)
    c3 = norm.cdf(a0)
    c4 = norm.cdf(b0)
    ln_mean = np.exp(mu + 0.5 * sigma ** 2)
    mean = ln_mean * (c1 - c2) / (c4 - c3)
    return mean


def log_normal_truncated_ab_variance(mu, sigma, a, b):
    mean = log_normal_truncated_ab_mean(mu, sigma, a, b)
    a0 = (np.log(a) - mu) / sigma
    b0 = (np.log(b) - mu) / sigma
    c1 = norm.cdf(2.0 * sigma - a0)
    c2 = norm.cdf(2.0 * sigma - b0)
    c3 = norm.cdf(a0)
    c4 = norm.cdf(b0)
    ln_xsquared = np.exp(2.0 * mu + 2.0 * sigma ** 2)
    lntab_xsquared = ln_xsquared * (c1 - c2) / (c4 - c3)
    variance = lntab_xsquared - mean ** 2
    return variance