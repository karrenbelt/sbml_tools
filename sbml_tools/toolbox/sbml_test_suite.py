import os
import re
from pathlib import Path
from typing import Union, List
import collections
import pandas as pd
from cached_property import cached_property

from sbml_tools import settings, config, utils
from sbml_tools.wrappers import SBMLDocument
from sbml_tools.toolbox.sbml_io import read_sbml

# This script facilitates interaction with the sbml-test-suite, which is a submodule of this repository

# regex
SETTINGS_REGEX = re.compile(rf'([a-z]+):\s*(.*)')
INT_REGEX = re.compile(r'^\d+$')
FLOAT_REGEX = re.compile(r'^[-+]?[0-9]*\.?[0-9]+([eE][-+]?[0-9]+)?$')
COMMA_REGEX = re.compile(r"^\s+|\s*,\s*|\s+$")  # used to split, optional spaces are stripped


def cast_string(v: str):  # only used for settings file
    return [int(s) if INT_REGEX.match(s) else float(s) if FLOAT_REGEX.match(s) else s
            for s in COMMA_REGEX.split(v) if s]


def get_snippet(contents, start, end):  # parsing .m file
    i = contents.index(start)
    return ' '.join(contents[i + len(start):i + contents[i:].index(end)].strip().split())


def parse_description(file_path):
    contents = Path(file_path).read_text()
    tags = ('synopsis:', 'componentTags:', 'testTags:', 'testType:', 'levels:', 'generatedBy:', '\n')
    d = {utils.snake_case(tags[i].strip(':')): get_snippet(contents, tags[i], tags[i + 1]) for i in
         range(len(tags) - 1)}
    d['packages_present'] = get_snippet(contents, 'packagesPresent:', '\n') if 'packagesPresent:' in contents else ''

    for k, v in d.items():
        if k == 'synopsis':
            continue
        v = list(map(utils.snake_case, COMMA_REGEX.split(v)))
        if k in ('test_type', 'generated_by'):
            assert len(v) == 1, f'more than one item: {v}'
            v = v[0]
        d[k] = v
    return d


class SemanticTestCaseError(Exception):
    pass


class SemanticTestCase:

    def __init__(self, number: Union[int, str]):
        self.number = f'{number:05d}' if isinstance(number, int) else number
        self.model_dir = os.path.join(settings.TEST_CASES_SEMANTIC_DIR, self.number)
        if not os.path.isdir(self.model_dir):
            raise NotADirectoryError(f'model directory does not exist:\n{self.model_dir}')

    @cached_property
    def description(self) -> dict:
        return parse_description(os.path.join(self.model_dir, f'{self.number}-model.m'))

    @property
    def synopsis(self) -> dict:
        return self.description.get('synopsis')

    @property
    def component_tags(self) -> list:
        return self.description.get('component_tags')

    @property
    def test_tags(self) -> list:
        return self.description.get('test_tags')

    @property
    def levels(self):
        return self.description.get('levels')

    @property
    def test_type(self) -> list:
        return self.description.get('test_type')

    @property
    def packages_present(self) -> list:
        return self.description.get('packages_present')

    @cached_property
    def results(self) -> pd.DataFrame:
        return pd.read_csv(os.path.join(self.model_dir, f'{self.number}-results.csv'))

    @cached_property
    def settings(self) -> dict:
        # keys = ['start', 'duration', 'steps', 'variables', 'absolute', 'relative', 'amount', 'concentration']
        file_path = os.path.join(self.model_dir, f'{self.number}-settings.txt')
        contents = Path(file_path).read_text()
        key_value_pairs = map(lambda x: SETTINGS_REGEX.match(x).groups(), filter(len, contents.split('\n')))
        return {k: cast_string(v) for k, v in key_value_pairs}

    def load_document(self, level: int = config.DEFAULT_SBML_LEVEL, version: int = config.DEFAULT_SBML_VERSION):
        file_name = f'{self.number}-sbml-l{level}v{version}.xml'
        file_path = os.path.join(self.model_dir, file_name)
        if not os.path.isfile(file_path):
            raise FileNotFoundError(f'no model for L{level}V{version}, available:\n{self.levels}')
        return SBMLDocument(read_sbml(os.path.join(self.model_dir, file_name)))

    def __repr__(self):
        return f'<{self.__class__.__name__}: {self.number}'


def _load_properties(test_cases):
    # this way we have to iterate the objects only once
    component_tags, test_tags, test_types, packages = [set() for _ in range(4)]
    for case in test_cases:
        component_tags.update({tag for tag in case.component_tags})
        test_tags.update({tag for tag in case.test_tags})
        test_types.add(case.test_type)
        packages.update({tag for tag in case.packages_present})
    return component_tags, test_tags, test_types, packages


def tuplify(x):
    return (x,) if isinstance(x, str) else x


class SemanticTestCases:

    def __init__(self):
        self._cases = list(map(SemanticTestCase, filter(str.isdigit, sorted(os.listdir(settings.TEST_CASES_SEMANTIC_DIR)))))
        self._component_tags, self._test_tags, self._test_types, self._packages = _load_properties(self)

    @property
    def available_component_tags(self):  # a for b in c for a in b
        return self._component_tags

    @property
    def available_test_tags(self):
        return self._test_tags

    @property
    def available_test_types(self):
        return self._test_types

    @property
    def available_packages(self):
        return self._packages

    @utils.memoize
    def _select(self, attr, *tags, how):
        if how not in (all, any):
            raise SemanticTestCaseError(f'how must be one of: {all, any}, got: {how}')
        if how == any:
            return [case for case in self if set(tags).intersection(tuplify(getattr(case, attr)))]
        return [case for case in self if set(tags).issubset(tuplify(getattr(case, attr)))]

    def select_by_component_tags(self, *component_tags: str, how=all):
        return self._select('component_tags', *component_tags, how=how)

    def select_by_test_tags(self, *test_tags: str, how=all):
        return self._select('test_tags', *test_tags, how=how)

    def select_by_test_types(self, *test_types: str, how=all):
        return self._select('test_type', *test_types, how=how)

    def select_by_packages(self, *packages: str, how=all):
        return self._select('packages_present', *packages, how=how)

    def select_by_level(self, level, version):
        return [case for case in self if f'{level}.{version}' in case.levels]

    def select_by(self, component_tags=None, test_tags=None, test_types=None, packages=None,
                  level=None, version=None, how=all):

        cases = set() if how is any else set(self._cases)
        operator = set.intersection if how is all else set.union
        if component_tags:
            cases = operator(cases, self.select_by_component_tags(*tuplify(component_tags), how=how))
        if test_tags:
            cases = operator(cases, self.select_by_test_tags(*tuplify(test_tags), how=how))
        if test_types:
            cases = operator(cases, self.select_by_test_types(*tuplify(test_types), how=how))
        if packages:
            cases = operator(cases, self.select_by_packages(*tuplify(packages), how=how))
        if level or version:
            cases = operator(cases, self.select_by_level(level, version))

        return cases

    def __iter__(self):
        for case in self._cases:
            yield case

    def __getitem__(self, key):
        return self._cases[key]

    def __len__(self):
        return len(self._cases)

    def __repr__(self):
        return f'<{self.__class__.__name__}>'


def test_semantic_test_cases():
    SEMANTIC_TEST_CASES = SemanticTestCases()
    SEMANTIC_TEST_CASES.available_component_tags
    SEMANTIC_TEST_CASES.available_test_tags
    SEMANTIC_TEST_CASES.available_test_types
    SEMANTIC_TEST_CASES.available_packages

    assert not SEMANTIC_TEST_CASES.select_by_component_tags('concentration')
    SEMANTIC_TEST_CASES.select_by_component_tags('rate_rule', 'event_with_delay', 'stoichiometry_math')

    SEMANTIC_TEST_CASES.select_by_test_tags('concentration')
    SEMANTIC_TEST_CASES.select_by_test_tags('amount', 'non_unity_stoichiometry')
    SEMANTIC_TEST_CASES.select_by_test_tags('amount', 'non_unity_stoichiometry', how=any)

    SEMANTIC_TEST_CASES.select_by_test_types('flux_balance_steady_state')
    SEMANTIC_TEST_CASES.select_by_test_types('time_course')
    SEMANTIC_TEST_CASES.select_by_packages('fbc')

    SEMANTIC_TEST_CASES.select_by_level(3, 2)

    assert not SEMANTIC_TEST_CASES.select_by(component_tags='concentration')
    component_tags = ['rate_rule', 'event_with_delay', 'stoichiometry_math']
    assert (SEMANTIC_TEST_CASES.select_by(component_tags=component_tags)
            == set(SEMANTIC_TEST_CASES.select_by_component_tags(*component_tags)))

    how = all
    test_tags = ['non_constant_parameter', ]
    selected_by = SEMANTIC_TEST_CASES.select_by(component_tags=component_tags, test_tags=test_tags, how=how)
    intersection = set(SEMANTIC_TEST_CASES.select_by_component_tags(*component_tags, how=how)).intersection(
        SEMANTIC_TEST_CASES.select_by_test_tags(*test_tags, how=how))
    assert selected_by == intersection

    how = any
    selected_by = SEMANTIC_TEST_CASES.select_by(component_tags=component_tags, test_tags=test_tags, how=how)
    unioned = set(SEMANTIC_TEST_CASES.select_by_component_tags(*component_tags, how=how)).union(
        SEMANTIC_TEST_CASES.select_by_test_tags(*test_tags, how=how))
    assert selected_by == unioned

    how = all
    selected_by = SEMANTIC_TEST_CASES.select_by(component_tags=component_tags, test_tags=test_tags, how=how,
                                                level=2, version=3, test_types='time_course')


# code below here needs to be moved
def test_description_parsing():
    for directory in os.listdir(settings.TEST_CASES_SEMANTIC_DIR):
        model_dir = os.path.join(settings.TEST_CASES_SEMANTIC_DIR, directory)
        if not os.path.isdir(model_dir):
            continue
        test_case = SemanticTestCase(directory)
        print(test_case.description)
        test_case.test_type
        test_case.component_tags
        test_case.test_tags
        test_case.packages_present

        print(test_case.settings)


def semantics_files_overview():
    """
    parse all description and settings files, return a dict with all unique values found for each
    """
    cases = dict()
    unique_settings = collections.defaultdict(set)
    unique_descriptors = collections.defaultdict(set)
    for directory in os.listdir(settings.TEST_CASES_SEMANTIC_DIR):
        model_dir = os.path.join(settings.TEST_CASES_SEMANTIC_DIR, directory)
        if not os.path.isdir(model_dir):
            continue

        description_file_path = os.path.join(model_dir, f'{directory}-model.m')
        for k, v in parse_description(description_file_path).items():
            unique_descriptors[k].update(set(COMMA_REGEX.split(v)))

        settings_file_path = os.path.join(model_dir, f'{directory}-settings.txt')
        contents = Path(settings_file_path).read_text()
        SETTINGS_REGEX = re.compile(rf'([a-z]+):\s*(.*)')
        pairs = map(lambda x: SETTINGS_REGEX.match(x).groups(), filter(len, contents.split('\n')))
        amount, concentration = False, False
        for k, v in pairs:
            if v:
                unique_settings[k].add(tuple(cast_string(v)))
            if k == 'amount' and v:
                amount = True
            if k == 'concentration' and v:
                concentration = True

        if amount and concentration:
            print(directory)  # never occurs

    # collapse tuples if all contain only one value
    for k, v in unique_settings.items():
        if all([x == 1 for x in map(len, v)]):
            unique_settings[k] = {x[0] for x in v}
