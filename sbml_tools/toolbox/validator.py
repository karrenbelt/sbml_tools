import os
import argparse
import subprocess
import warnings
from typing import Union, Iterable, Optional

from lxml import etree
import libsbml
from sbml_tools.toolbox.sbml_io import read_sbml
from sbml_tools.utils import setup_logger
from sbml_tools import settings

# libsbml.SBMLValidator()  # only just discovered this exists

logger = setup_logger(settings.LOG_DIR, __name__)


def check(value):
    """ translate libsbml error codes to messages; used in model construction """
    if value is None:
        raise ValueError('LibSBML returned a null value')
    elif type(value) is int:
        if value == libsbml.LIBSBML_OPERATION_SUCCESS:
            return
        else:
            err_msg = f"LibSBML returned error code {value}: \n" \
                      + f"{libsbml.OperationReturnValue_toString(value).strip()}"
            raise RuntimeError(err_msg)
    return value


def are_equal(element1 : libsbml.SBase, element2 : libsbml.SBase) -> bool:
    """assess whether two objects are equal"""
    def elements_equal(e1, e2):
        "test if etree's are the equivalent (attribute order should not matter)"
        if e1.tag != e2.tag: return False # str
        if e1.text != e2.text: return False # str
        if e1.tail != e2.tail: return False # str
        if e1.attrib != e2.attrib: return False # dict
        if len(e1) != len(e2): return False # int
        return all(elements_equal(c1, c2) for c1, c2 in zip(e1, e2))

    e1 = etree.fromstring(element1.toXMLNode().toXMLString(), etree.HTMLParser())
    e2 = etree.fromstring(element2.toXMLNode().toXMLString(), etree.HTMLParser())

    return elements_equal(e1, e2)


class ValidationError(Exception):
    def __init__(self, message):
        self.message = message

    def __str__(self):
        return str(self.message)


class ValidatorChecks:

    def __init__(self):
        # Check consistency of measurement factories associated with quantities
        self.g = libsbml.LIBSBML_CAT_GENERAL_CONSISTENCY
        # Check correctness and consistency of identifiers used for model entities
        self.i = libsbml.LIBSBML_CAT_IDENTIFIER_CONSISTENCY
        # Check syntax of MathML mathematical expressions
        self.u = libsbml.LIBSBML_CAT_UNITS_CONSISTENCY
        # Check validity of SBO identifiers (if any) used in the model
        self.m = libsbml.LIBSBML_CAT_MATHML_CONSISTENCY
        # Check validity of SBO identifiers (if any) used in the model
        self.s = libsbml.LIBSBML_CAT_SBO_CONSISTENCY
        # Perform static analysis of whether the model is overdetermined
        self.o = libsbml.LIBSBML_CAT_OVERDETERMINED_MODEL
        # Perform all other general SBML consistency checks
        self.p = libsbml.LIBSBML_CAT_MODELING_PRACTICE

CHECK_CODES = ValidatorChecks()


def validate_sbml(sbml: Union[libsbml.SBMLDocument, str], offcheck: Optional[Iterable] = 'u',
                  check_warnings: bool = True, offline: bool = True):
    """
    sbml: a SBML document, a file path, or a SBML string

    offcheck:
        u  -->  Disable checking the consistency of measurement factories associated with quantities

        i  -->  Disable checking the correctness and consistency of identifiers used for model entities

        m  -->  Disable checking the syntax of MathML mathematical expressions

        s  -->  Disable checking the validity of SBO identifiers (if any) used in the model

        o  -->  Disable static analysis of whether the model is overdetermined

        p  -->  Disable additional checks for recommended good modeling practices

        g  -->  Disable all other general SBML consistency checks

    check_warnings:
        whether to check and report of warnings
    """
    if offline:
        validate_sbml_offline(sbml, offcheck, check_warnings)
    else:
        validate_sbml_online(sbml, offcheck, check_warnings)


def validate_sbml_offline(sbml: Union[libsbml.SBMLDocument, str], offcheck: Optional[Iterable] = 'u',
                          check_warnings: bool = True):

    doc = read_sbml(sbml)
    check_internal_consistency(doc, check_warnings)
    check_consistency(doc, offcheck, check_warnings)


def check_internal_consistency(doc, check_warnings : bool = True):
    """ check fundamental syntactic and structural errors that violate the XML schema. """
    doc.checkInternalConsistency()
    assess_errors(doc, check_warnings)


def check_consistency(doc, offcheck: Iterable = 'u', check_warnings: bool = True) -> None:

    checks = {'u', 'i', 'm', 's', 'o', 'p', 'g'}
    for check in offcheck:
        checks.remove(check)

    for check, value in vars(CHECK_CODES).items():
        if check in checks:
            doc.setConsistencyChecks(value, True)
        else:
            doc.setConsistencyChecks(value, False)

    doc.checkConsistency()
    assess_errors(doc, check_warnings)


def assess_errors(doc, check_warnings: bool = True) -> None:

    n_errors = 0
    n_warnings = 0
    error_messages = ''
    warning_messages = ''

    for i in range(doc.getNumErrors()):
        error = doc.getError(i)

        if error.getSeverity() in (libsbml.LIBSBML_SEV_ERROR, libsbml.LIBSBML_SEV_FATAL):
            n_errors += 1
            error_messages += translate_error(error)
        elif check_warnings:
            n_warnings += 1
            warning_messages += translate_error(error)

    if n_errors:
        raise ValidationError(f"not a valid SBML model, found {n_errors} errors:\n\n{error_messages}")

    if n_warnings:
        logger.warning(f"valid SBML, but {n_warnings} warnings found:\n\n{warning_messages}")


def translate_error(error) -> str:

    severity = error.getSeverityAsString()
    package = error.getPackage()
    category = error.getCategoryAsString()
    message = error.getMessage()
    line = error.getLine()

    return f"{severity}: {category} ({package if package else 'core'}, Line {line})\n{message}\n"


def clear_log(doc):
    # not used currently: if done prior to consistency checking simple reading errors go unscathed
    """ log messages keep accumulating if re-validating the same document """
    log = doc.getErrorLog()
    log.clearLog()


def validate_sbml_online(sbml: str, offcheck: Optional[Iterable] = 'u', check_warnings: bool = True):

    assert os.path.isfile(sbml), f"file not found: {sbml}"

    command = f"curl -F file=@{sbml} -F output=xml -F offcheck={','.join(offcheck)} http://sbml.org/validator/"
    p = subprocess.Popen(command, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = (bytes.decode("utf-8") for bytes in p.communicate())

    result = stdout.split('\n')[11].strip()
    if result == '<no-errors/>':
        return
    elif result == '<warning/>':
        if not check_warnings:
            return
        n_warnings = stdout.count("severity='Warning'")
        warnings.warn(f"valid SBML, but {n_warnings} warnings found:\n\n{stdout}")
    elif result == '<errors/>':
        raise ValidationError(f'This document is not valid SBML!\n\n{stdout}')
    else:
        raise IOError(f'unexpected result: {result}\n\n{stderr}\n{stdout}')


def main(files, offcheck, check_warnings, offline):

    for file in files:
        assert os.path.isfile(file), f"file not found: {file}"

    for file in files:
        validate_sbml(file, offcheck, check_warnings, offline)


if __name__ == '__main__':
    """Validate SBML files using the SBML validator"""
    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument('files', metavar='<files>', type=str, nargs='+', help='one or more file paths')
    parser.add_argument('-f', '--outfile', type=str, help='filename to write output to')
    # parser.add_argument('-o', '--output', type=str, nargs='?', default='xml', help='text or xml, default: xml')
    parser.add_argument('-v', '--verbose', help='print to console', action='store_true')
    parser.add_argument('--offcheck', type=str, nargs='?', default='u',
                        help="""
    NOTE: must be comma separated: u,i,m,s,o,p,g.
    Default = u

    u  -->  Disable checking the consistency of measurement factories associated
        with quantities (SBML L2V3 rules 105nn)

    i  -->  Disable checking the correctness and consistency of identifiers
            used for model entities (SBML L2V3 rules 103nn)

    m  -->  Disable checking the syntax of MathML mathematical expressions
            (SBML L2V3 rules 102nn)

    s  -->  Disable checking the validity of SBO identifiers (if any) used in
            the model (SBML L2V3 rules 107nn)

    o  -->  Disable static analysis of whether the model is overdetermined

    p  -->  Disable additional checks for recommended good modeling practices

    g  -->  Disable all other general SBML consistency checks
            (SBML L2v3 rules 2nnnn)
    """)
    main(**vars(parser.parse_args()))