import numpy as np
import pandas as pd
from scipy import interpolate
from scipy import optimize
# from scipy.interpolate import interp1d, Rbf
import operator
import re
import inspect
from typing import Union, Iterable, Callable
import matplotlib.pyplot as plt
import libsbml
import roadrunner
from sbml_tools.wrappers import SBMLDocument, ASTNode
from itertools import zip_longest


# TODO: generalize:
#  1. x_new should not be given to __init__
#  2. __init__ should take a dataframe, and an SBML model
#  3. astnode should be integrated into the model as assignment rule or rate rule
#  4. assignment rule or rate rule should be assignable to compartment, species or parameter


def func1(x):
    return np.sin(x) + np.sin(3 * x)


def func2(x):
    return x * np.sin(x) + x * np.cos(2 * x)


def func3(x):
    a = np.sin(x); b = np.cos(x)
    return a*(np.sin(x+1)) + b


def func4(x):
    a, b = np.sin(x), np.cos(x)
    return a*(np.sin(x+1)) + b


MODULE_PREFIX_PATTERN = re.compile(r'(\w+\s*\.)(.*?)')


def test_module_prefix_pattern():
    MODULE_PREFIX_PATTERN.findall('np.sum(x)')
    MODULE_PREFIX_PATTERN.findall('np.sub.sum(x)')
    for s in ('np.sum(x)', 'np.sub.sum(x)', 'np.sin(x) + np.cos(y)', 'np  .  sin (x)',):
        print(MODULE_PREFIX_PATTERN.sub('', s))


def test_replacement():
    pattern = re.compile(r'\ba\b')
    for s in ('a+b', 'a*sin(b)', 'b*sin(a)', 'aa+n'):
        print(pattern.sub('something', s))


def func_to_astnode(func: Callable) -> ASTNode:
    """
    Experimental function to convert simple python functions to ASTNode.

    TODO:
    Doing this properly requires parsing with ast:
    - ast.parse(inspect.getsource(func))
    - some class that defines visit_FunctionDef:
      class FuncParser(ast.NodeVisitor):
          def visit_FunctionDef(self, node):
              # do stuff
              print(node.name)
              self.generic_visit(node)
    - instantiating and calling visit: FuncParser().visit(ast.parse(inspect.getsource(func)))
    """

    func_str = inspect.getsource(func)
    formula = ''
    replacement_patterns = {}
    for line in re.split('\n|;', func_str):
        line = line.strip().split('#')[0]
        # def and return
        if line.startswith('def '):
            line = line.split(':', 1)[1]
        elif line.startswith('return '):
            line = line.split('return ')[1]
        # deal with assignments; need to make a pattern to replace
        line = MODULE_PREFIX_PATTERN.sub('', line)
        if '=' in line:
            variables, assignments = map(str.strip, line.split('='))
            variables = variables[1:-1] if variables[0] == '(' and variables[-1] == ')' else variables
            variables = list(map(str.strip, variables.split(',')))
            assignments = assignments[1:-1] if assignments[0] == '(' and assignments[-1] == ')' else assignments
            assignments = list(map(str.strip, assignments.split(',')))
            for variable, assignment in zip(variables, assignments):
                pattern = re.compile(rf'\b{variable}\b')
                replacement_patterns[pattern] = assignment
        else:
            # final expression
            for pattern, replacement in replacement_patterns.items():
                line = pattern.sub(replacement, line)
            formula += line
    return ASTNode.from_string(formula)


def test_func_to_ast_node():
    for func in (func1, func2, func3, func4):
        ast_node = func_to_astnode(func)
        print(ast_node)


class Polynomial:

    def __init__(self, *coefficients, variable_name: str = 'x'):
        self.coeff = coefficients
        self.var_name = variable_name

    def __call__(self, x, *coefficients):
        # being able to pass coefficients is useful for fitting
        if not coefficients:
            coefficients = self.coeff

        res = 0
        for coeff in coefficients:
            res = res * x + coeff
        return res

    @property
    def degree(self) -> int:
        return len(self.coeff)

    def __add__(self, other) -> 'Polynomial':
        return Polynomial(*[sum(t) for t in zip_longest(self.coeff[::-1], other.coeff[::-1], fillvalue=0)])

    def __sub__(self, other) -> 'Polynomial':
        return Polynomial(*[operator.sub(*t) for t in zip_longest(self.coeff[::-1], other.coeff[::-1], fillvalue=0)])

    def derivative(self) -> 'Polynomial':
        return Polynomial(*[self.coeff[i] * (self.degree - i - 1) for i in range(self.degree - 1)])

    def __repr__(self):
        return f"Polynomial{self.coeff}"

    def __str__(self):
        return ' + '.join([f"{self.coeff[i]}*{self.var_name}^{self.degree - i - 1}" for i in range(self.degree)])


class DataIntegrator:
    """
    class for integrating experimental measurement data into an SBML model, by
    1. interpolation using piece-wise linear formula
        linear: exact
        cubic spline: exact
        radial basis function: linear approximation

    2. curve fitting:
        polynomial: exact
        arbitrary function: linear approximation

    """
    def __init__(self, x, y, x_new):
        self.x = x
        self.y = y
        self.x_new = x_new

    # interpolation
    def linear_interpolation(self) -> libsbml.ASTNode:
        """piecewise linear interpolation function"""
        items = []
        for i in range(self.x.shape[0] - 1):
            m = (self.y[i+1] - self.y[i]) / (self.x[i+1] - self.x[i])
            formula = f'{self.y[i]} + {m} * (time - {self.x[i]})'
            condition = f'time >= {self.x[i]} && time < {self.x[i+1]}'
            items.append(f'{formula}, {condition}')
        items.append('{}, time >= {}'.format(self.y[i], self.x[i]))  # last value after last time
        items.append('0.0')  # otherwise
        formula = 'piecewise({})'.format(', '.join(items))
        return ASTNode.from_string(formula)

    def cubic_spline_interpolation(self) -> libsbml.ASTNode:
        """piecewise-linear cubic spline function in polynomial form"""
        coeffs = interpolate.CubicSpline(self.x, self.y, bc_type='natural').c.T
        terms = []
        for i in range(self.x.shape[0] - 1):
            d, c, b, a = coeffs[i]
            formula = f'{d}*(time-{self.x[i]})^3 + {c}*(time-{self.x[i]})^2 + {b}*(time-{self.x[i]}) + {a}'
            condition = 'time >= {x1} && time <= {x2}'.format(x1=self.x[i], x2=self.x[i+1])
            terms.append(f'{formula}, {condition}')
        terms.append('0.0')  # otherwise
        formula = f"piecewise({', '.join(terms)})"
        return ASTNode.from_string(formula)

    def rbf(self, function='gaussian') -> ASTNode:
        """a piece-wise linear approximation of the radial basis function interpolation"""
        y_new = interpolate.Rbf(self.x, self.y, function=function)(self.x_new)
        terms = []
        for i in range(self.x_new.shape[0] - 1):
            m = (y_new[i + 1] - y_new[i]) / (self.x_new[i + 1] - self.x_new[i])
            formula = f'{y_new[i]} + {m} * (time - {self.x_new[i]})'
            condition = f'time >= {self.x_new[i]} && time < {self.x_new[i + 1]}'
            terms.append(f'{formula}, {condition}')
        terms.append('{}, time >= {}'.format(y_new[i], self.x_new[i]))  # last value after last time
        terms.append('0.0')  # otherwise
        formula = 'piecewise({})'.format(', '.join(terms))
        return ASTNode.from_string(formula)

    # curve fitting
    def polynomial_fit(self, degree: int = None, p0: Iterable[Union[float, int]] = None,
                       sigma=None, bounds=(-np.inf, np.inf)) -> ASTNode:
        """an n-th order polynomial function fitted to the data"""
        if p0 is None:
            p0 = [1] * degree
        if degree is None:
            degree = len(p0)
        assert len(p0) == degree, f'degree {degree} does not match size of initial guess {p0}'

        polynomial = Polynomial(*p0)
        popt, pcov = optimize.curve_fit(polynomial, self.x, self.y, p0, sigma, bounds=bounds)
        formula = str(Polynomial(*popt, variable_name='time'))
        return ASTNode.from_string(formula)

    def fit_function(self, func, p0=None, sigma=None, bounds=(-np.inf, np.inf)):
        """
        experimental feature for 1d data, first argument must be 'x' (or 'time') and will be replaced by 'time'
        as the function parses the function code string using regex
        one should validate oneself if the astnode is as desired,
        e.g. np.sum(a) will become sum(a), but sum is interpreted as a used-defined function, not as a sum
        """
        popt, pcov = optimize.curve_fit(func, self.x, self.y, p0, sigma, bounds=bounds)
        ast_node_string = func_to_astnode(func).string  # first see if at all this is valid
        for i, p in enumerate(inspect.signature(func).parameters):
            if i == 0:
                ast_node_string = re.sub(rf'\bx\b', 'time', ast_node_string)
            ast_node_string = re.sub(rf'\b{p}\b', str(popt[i-1]), ast_node_string)
        return ASTNode.from_string(ast_node_string)

    # find the B-spline representation of 1-D curve.
    # returns a tuple containing the vector of knots, the B-spline coefficients, and the degree of the spline.
    # tck = interpolate.splrep(self.x, self.y)
    # # turn it into a piecewise polynomial in terms of coefficients and breakpoints
    # return interpolate.PPoly.from_spline(tck).c.T

    # t = np.linspace(0, x_end, 10)
    # tck = interpolate.splrep(x, y, t=t[1:-1])
    # pp = interpolate.PPoly.from_spline(tck)
    # # pp.c UnivariateSpline


def get_roadrunner_ysim(x_0: int, x_end: int, points: int, ast_node: ASTNode):
    doc = SBMLDocument()
    doc.create_model()
    doc.model.create_compartment(id='c', constant=True)
    doc.model.create_species(id='s1', compartment='c',
                             has_only_substance_units=True, boundary_condition=False, constant=False)
    doc.model.create_assignment_rule(variable='s1', math=ast_node)
    rr = roadrunner.RoadRunner(doc.wrapped.toSBML())
    y_sim = rr.simulate(start=x_0, end=x_end, points=points)['[s1]']
    return y_sim


def plot(x, y, x_new, y_new, y_sim, y_err=None, title=None):
    if y_err is not None:
        plt.errorbar(x, y, yerr=y_err, fmt='o', markersize=7, label='raw')
    else:
        plt.plot(x, y, 'o', markersize=7, label='raw')

    plt.plot(x_new, y_new, '-', linewidth=1, label='scipy')
    plt.plot(x_new, y_sim, ':', linewidth=3, label='sbml')
    plt.legend()
    if title:
        plt.title(title)
    plt.show()


def test():
    # rr = roadrunner.RoadRunner(sbml)
    # rr.integrator.absolute_tolerance
    # rr.integrator.relative_tolerance

    np.random.seed(42)
    x_0 = 0
    x_end = 4*np.pi
    points = 100

    x = np.linspace(x_0, x_end, 20)
    y = np.sin(x)

    x_new = np.linspace(x_0, x_end, points)
    integrator = DataIntegrator(x, y, x_new)

    # test1: linear
    y_new = interpolate.interp1d(x, y, kind='linear')(x_new)
    y_sim = get_roadrunner_ysim(x_0, x_end, points, integrator.linear_interpolation())
    assert np.allclose(y_new, y_sim, rtol=1e-5, atol=1e-8)
    plot(x, y, x_new, y_new, y_sim, title='linear interpolation of sin(x) data')

    # test2: cubic spline
    y_new = interpolate.interp1d(x, y, kind='cubic')(x_new)
    y_sim = get_roadrunner_ysim(x_0, x_end, points, integrator.cubic_spline_interpolation())
    assert np.allclose(y_new, y_sim, rtol=1e-5, atol=1e-2)
    plot(x, y, x_new, y_new, y_sim, title='cubic spline interpolation of sin(x) data')

    # test3: 5th-order polynomial - curve fitting
    x_end = 4
    points = 1000
    coefficients = (-0.9, 3.3, 0.5, 1, 0.4)
    polynomial = Polynomial(*coefficients)

    x = np.linspace(x_0, x_end, 20)
    y = polynomial(x)
    x_new = np.linspace(x_0, x_end, points)

    integrator = DataIntegrator(x, y, x_new)
    y_new = polynomial(x_new)
    ast_node = integrator.polynomial_fit(degree=len(coefficients))
    y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
    plot(x, y, x_new, y_new, y_sim, title=f'fit to {polynomial.degree}th degree polynomial data')

    # testing more polynomials: general polynomials - curve fitting
    for coefficients in [
            (2, ), (-2, ),
            (2, 6), (2, -3), (-2, 3), (-2, -2),
            (4, 3, 4), (2, -6, 4), (-3, 3, 3),
            (-4, 3, -2, 1),
            (0.5, -0.7, 0.5, -6, 0.1)
            ]:
        polynomial = Polynomial(*coefficients)
        y = polynomial(x)
        integrator.y = y
        y_new = polynomial(x_new)
        ast_node = integrator.polynomial_fit(degree=len(coefficients))
        y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
        if not np.allclose(y_new, y_sim, rtol=1e-5, atol=1e-8):
            plot(x, y, x_new, y_new, y_sim, title=f'fit to {polynomial.degree}th degree polynomial data')
            raise ValueError(f'polynomials are not equal')

    # test4: noisy data, fitting weighted function
    np.random.seed(42)
    x_0 = 0
    x_end = 10
    points = 1000
    x = np.linspace(x_0, x_end, 20)
    coefficients = (-0.2, 2.3, -0.2, 3)
    polynomial = Polynomial(*coefficients)
    y = polynomial(x)

    y_err = y * np.random.normal(size=x.shape) * 0.3
    y += y_err

    x_new = np.linspace(x_0, x_end, points)
    integrator = DataIntegrator(x, y, x_new)
    y_new = polynomial(x_new)

    ast_node = integrator.polynomial_fit(degree=len(coefficients))
    y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
    title = f'fit to {polynomial.degree}th degree polynomial noisy data'
    plot(x, y, x_new, y_new, y_sim, y_err=y_err, title=title)

    ast_node = integrator.polynomial_fit(degree=len(coefficients), sigma=y_err)
    y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
    title = f'fit to {polynomial.degree}th degree polynomial noisy data, weighed by error'
    plot(x, y, x_new, y_new, y_sim, y_err=y_err, title=title)

    # test5: with an without noise - this can be fit wrongly (e.g. different phase)
    np.random.seed(42)
    x_0 = 0
    x_end = 10
    points = 1000

    def underdamped_oscillator(x, a, gamma, omega, alpha):
        return np.exp(-gamma * x) * a * np.cos(omega*x - alpha)

    x = np.linspace(x_0, x_end, 20)
    parameters = dict(a=2, gamma=0.4, omega=3, alpha=2)
    y = underdamped_oscillator(x, **parameters)
    x_new = np.linspace(x_0, x_end, points)
    y_new = underdamped_oscillator(x_new, **parameters)  # true

    integrator = DataIntegrator(x, y, x_new)
    ast_node = integrator.fit_function(underdamped_oscillator)
    y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
    plot(x, y, x_new, y_new, y_sim, title='fit of underdamped oscillator')

    # with noise - fitting works correctly but the fit is not perfect due to noise and limited data
    y_err = np.abs(y * np.random.normal(size=x.shape) * 0.3)
    integrator.y += y_err
    ast_node = integrator.fit_function(underdamped_oscillator, sigma=y_err)
    y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
    title = f'fit to underdamped oscillator noisy data, weighed by error'
    plot(x, y, x_new, y_new, y_sim, y_err=y_err, title=title)

    # test6: rbf interpolation
    for func in (func1, func2):
        np.random.seed(42)
        x_0 = 0
        x_end = 10
        points = 1000
        x = np.linspace(x_0, x_end, 20)
        y = func(x)
        x_new = np.linspace(x_0, x_end, points)

        integrator = DataIntegrator(x, y, x_new)
        y_new = interpolate.Rbf(x, y, function='gaussian')(x_new)
        ast_node = integrator.rbf(function='gaussian')
        y_sim = get_roadrunner_ysim(x_0, x_end, points, ast_node)
        plot(x, y, x_new, y_new, y_sim, title=f'piece-wise linear approximation of rbf integration')

