import os
from typing import Union

import libsbml


def read_sbml(sbml: Union[str, libsbml.SBMLDocument]) -> libsbml.SBMLDocument:

    if isinstance(sbml, libsbml.SBMLDocument):
        return sbml

    reader = libsbml.SBMLReader()
    if os.path.isfile(sbml):
        doc = reader.readSBMLFromFile(sbml)
    else:
        doc = reader.readSBMLFromString(sbml)

    if doc.getNumErrors() > 0:
        print("There were errors while reading, better to fix those first")
        doc.printErrors()
        raise IOError(f"not a valid SBML document")

    return doc


def write_sbml(doc: libsbml.SBMLDocument, filepath: str, overwrite: bool = False) -> None:
    if os.path.isfile(filepath) and not overwrite:
        raise ValueError(f"filepath exists: {filepath}\nset overwrite to True if you wish to overwrite the file")
    writer = libsbml.SBMLWriter()
    writer.writeSBMLToFile(doc, filepath)
    print(f"SBML file written to: {filepath}")
