
from typing import Union, Iterable, Dict, List
import re
import libsbml
import pandas as pd

from sbml_tools import utils, settings
# from sbml_tools.wrappers import SBMLDocument
from sbml_tools.wrappers import utils
from sbml_tools.toolbox.sbml_io import read_sbml

from sbml_tools.utils import get_bigg_metabolite_identifier_mapping, get_bigg_reaction_identifier_mapping, setup_logger

logger = setup_logger(settings.LOG_DIR, __name__)

# notes from emails with Elad and Wolf:
# Elad: reaction table (with EC numbers), and the script will just return a table of all the relevant kinetic parameters
# (along with the metabolite IDs, e.g. CHEBI). If there are multiple values for the same kcat, Km, Ki,
# then we can keep all of them.

# Wolf: The input file would contain the table "Reaction" (and, if necessary, additionally the table "Compound").
# The output file would contain the table "Parameter".
# other relevant columns: !Condition", "!pH", "!Temperature", and "!Organism"


BiGG_reactions = get_bigg_reaction_identifier_mapping()
BiGG_metabolites = get_bigg_metabolite_identifier_mapping()
# TODO: test this on the universal BiGG model


def get_annotated_identifiers(items: Iterable[libsbml.SBase]):
    """retrieve annotated identifiers"""
    uris = dict()
    for obj in items:
        if not obj.cv_terms:
            continue
        for cv_term in obj.cv_terms:
            if not cv_term.getBiologicalQualifierType() == libsbml.BQB_IS:
                continue  # only retrieve identifiers
            if cv_term.getNumNestedCVTerms():  # not expecting any nested identifier annotations
                raise NotImplementedError(f"nested cv terms not implemented")
            uris[obj.id] = [cv_term.getResourceURI(i) for i in range(cv_term.getNumResources())]
    return uris


class IdentifierAnnotator:
    """
    annotate model species and reactions identifiers, using BiGG identifiers
    """
    def __init__(self, sbml: Union[str, libsbml.SBMLDocument]):
        self.doc = read_sbml(sbml)  # Document(read_sbml(sbml))

    @property
    def species(self) -> libsbml.ListOfSpecies:
        return self.doc.model.species

    @property
    def reactions(self) -> libsbml.ListOfReactions:
        return self.doc.model.reactions

    @property
    def annotated_species_identifiers(self) -> Dict[str, list]:
        return get_annotated_identifiers(self.species)

    @property
    def annotated_reaction_identifiers(self) -> Dict[str, list]:
        return get_annotated_identifiers(self.reactions)

    @property
    def annotated_elements(self) -> List[libsbml.SBase]:
        return [x for x in self.doc.model.all_elements if x.getAnnotation()]

    def remove_all_annotations(self):
        for sbase in self.doc.all_elements:
            utils.check(sbase.setAnnotation(None))

    def _annotate_cv_term(self, sbase: libsbml.SBase, namespace_url: str, accession: str):
        """merely ensures namespace and pattern validity, might still be invalid. Only biological qualifiers."""
        utils.ensure_meta_id(sbase)

        resource = f'http://identifiers.org/{namespace_url}/{accession}'
        if not utils.is_valid_miriam(resource):
            print(f'not valid MIRIAM: {resource}')
            # raise ValueError(f'not valid MIRIAM: {resource}')

        cv = libsbml.CVTerm()
        utils.check(cv.setQualifierType(libsbml.BIOLOGICAL_QUALIFIER))
        utils.check(cv.setBiologicalQualifierType(libsbml.BQB_IS))
        utils.check(cv.addResource(resource))
        utils.check(sbase.addCVTerm(cv))

    def _find_id(self, data: pd.DataFrame, query: str, case_sensitive: bool = True):
        sid = utils.clipper(query)
        pattern = rf'^{sid}$'
        logical = data.index.str.contains(pattern, regex=True, case=case_sensitive)
        ids = data.index[logical].values
        if not ids.any():
            return
        elif len(ids) > 1:
            raise ValueError(f'found multiple matches: {ids}')
        return ids[0]

    def annotate_species_through_BiGG(self, case_sensitive: bool = True):
        # there are 8 case-sensitive indices (otherwise non-unique) in BiGG_metabolites
        for s in self.species:
            sid = self._find_id(BiGG_metabolites, s.id, case_sensitive)
            if sid is None:
                print(f'cannot annotate {s}')
                continue
            self._annotate_cv_term(s, 'bigg.metabolite', sid)
            annotations = BiGG_metabolites.loc[sid].dropna()
            for namespace, accessions in annotations.items():
                for accession in accessions:
                    self._annotate_cv_term(s, namespace, accession)

    def annotate_reactions_through_BiGG(self, case_sensitive=True):
        # there are no case-sensitive indices (that otherwise would be non-unique) in BiGG_reactions
        for r in self.reactions:
            rid = self._find_id(BiGG_reactions, r.id, case_sensitive)
            if rid is None:
                print(f'cannot annotate {r}')
                continue
            self._annotate_cv_term(r, 'bigg.reaction', rid)
            annotations = BiGG_reactions.loc[rid].dropna()
            for namespace, accessions in annotations.items():
                for accession in accessions:
                    self._annotate_cv_term(r, namespace, accession)


def test():

    # this uses BiGG identifiers, which is why I chose it as the base case
    annotator = IdentifierAnnotator(settings.ECOLI_CORE_MODEL)
    species_annotation = annotator.annotated_species_identifiers
    reaction_annotation = annotator.annotated_reaction_identifiers

    print(len(annotator.annotated_elements))
    annotator.remove_all_annotations()
    print(len(annotator.annotated_elements))
    annotator.annotate_species_through_BiGG()
    print(len(annotator.annotated_elements))
    annotator.annotate_reactions_through_BiGG()
    print(len(annotator.annotated_elements))

    species_reannotation = annotator.annotated_species_identifiers
    reaction_reannotation = annotator.annotated_reaction_identifiers

    # assert species_annotation == species_reannotation
    # assert reaction_annotation == reaction_reannotation


