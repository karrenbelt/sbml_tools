import os

import pandas as pd
from sbtab import SBtab
from pbalancing.balancer import ParameterBalancing
from pbalancing import misc
from bidict import bidict
from sbml_tools import settings, utils
from sbml_tools.toolbox.sbml_io import read_sbml
from pint import Quantity


# TODO:
#  converter that sets enzymes, if they are species, as parameters, which is required for parameter balancing


logger = utils.setup_logger(settings.LOG_DIR, __name__)

SBTAB_MAP = bidict(
    id='ID',
    mean='Mean',
    std='Std',
    organism='Organism',
    quantity_type='QuantityType',
    reaction='Reaction:SBML:reaction:id',
    species='Compound:SBML:species:id',
    mode='Mode',
    unit='Unit',
    unconstrained_gmean='UnconstrainedGeometricMean',
    unconstrained_gstd='UnconstrainedGeometricStd',
    unconstrained_mean='UnconstrainedMean',
    unconstrained_std='UnconstrainedStd'
    )

PB_PRIOR = utils.get_pb_prior()
SYMBOL_MAP = bidict(PB_PRIOR['quantity_type'].to_dict())

# these are typical E. coli values, matching those that I use by default in equilibrator
DEFAULT_PH = Quantity(7.4, 'dimensionless')
DEFAULT_T = Quantity(37.0, 'degC')
DEFAULT_CELL_VOLUME = Quantity(1, 'micrometer**3')  # .to('litre') # sanity check


class Options:

    def __init__(self,
                 config: bool = True,
                 use_pseudo_values: bool = True,
                 ph: Quantity = DEFAULT_PH,
                 temperature: Quantity = DEFAULT_T,
                 cell_volume: Quantity = DEFAULT_CELL_VOLUME,
                 overwrite_kinetics: bool = False,
                 parametrisation: str = 'hal',
                 enzyme_prefactor: bool = True,
                 default_inhibition: str = 'complete',
                 default_activation: str = 'complete',
                 model_name: str = 'outputname',
                 boundary_values: str = 'ignore'):
        """
        :param config:
        :param use_pseudo_values: usage of pseudo values as substitute for missing parameter values to improve balancing
        :param temperature: temperature  that  allows  parameter  balancing  for  correction  of  parameter values
               that do not correspond to the systems target temperature
        :param ph: H  value  that  allows  parameter  balancing  for  correction  of  parameter  values
               that  do  not correspond to the systems target pH value
        :param cell_volume: volume of the cell
        :param overwrite_kinetics: should existing kinetic laws in the SBML file be overwritten?
        :param parametrisation: 'hal', 'cat' or 'weg' different parametrisation types for the convenience kinetics.
        :param enzyme_prefactor: should a prefactor for the enzyme concentration be set in the rate law?
        :param default_inhibition: type of enzyme inhibition
        :param default_activation: type of enzyme activation
        :param model_name: name that is used for the result files
        :param boundary_values: ignore boundary values (temporary feature)
        """
        kwargs = {k: v for k, v in locals().items() if not k == 'self'}
        self.__dict__['ph'] = kwargs.pop('ph').to('dimensionless').magnitude
        self.__dict__['temperature'] = kwargs.pop('temperature').to('degK').magnitude
        self.__dict__['cell_volume'] = kwargs.pop('cell_volume').to('micrometer**3').magnitude
        self.__dict__.update(kwargs)


class ModelParameterBalancer:
    """simplified version of the original parameter balancing wrapper"""
    def __init__(self, sbml, data=None, options=None):
        self.doc = read_sbml(sbml)
        if self.doc.model is None:
            raise ValueError("document has no model, bailing")

    @property
    def pseudos(self):
        return (PB_PRIOR.loc[['keq', 'kcatf', 'kcatr',  'mu', 'a', 'vmaxf', 'vmaxr'],
                             ['quantity_type', 'prior_median', 'prior_std', 'prior_geometric_std']]
                .set_index('quantity_type'))

    @property
    def priors(self):
        return (PB_PRIOR.loc[['mu0', 'kv', 'km', 'ka', 'ki', 'c', 'u', 'ph', 'dmu0', 'thetaf',
                              'thetar', 'tauf', 'taur', 'kmprod', 'kcatratio'],
                             ['quantity_type', 'prior_median', 'prior_std', 'prior_geometric_std']]\
                .set_index('quantity_type'))

    @property
    def bounds(self):
        return PB_PRIOR.set_index('quantity_type')[['lower_bound', 'upper_bound']]

    @property
    def exchange_reactions(self):
        return [reaction for reaction in self.doc.model.reactions if not reaction.reactants or not reaction.products]

    @property
    def biomass_reactions(self):
        return [reaction for reaction in self.doc.model.reactions if 'biomass' in reaction.id.lower()]

    def balance(self, data: pd.DataFrame = None, options: Options = None):
        """
        required columns: 'reaction', 'species', 'symbol', 'mean', 'std'
        optional columns: 'id', 'unit'

        # intermediate output
            sbtab_final: table with the balancing results
            mean_post:   mean posteriori
            q_post:      posterior mean vector
            c_string:    string representation of c_xpost
                         where c_xpost = numpy.dot((numpy.dot(self.Q, self.C_post)), self.Q.transpose())
            c_post:      posterior covariance matrix
            Q:           dependency matrix
            shannons:    shannon entropy of the prior and the posterior covariance matrices
            log:         logged warnings that one SHOULD inspect

        output columns: ['symbol', 'reaction', 'species', 'mode', 'unit', 'unconstrained_gmean',
                         'unconstrained_gstd', 'unconstrained_mean', 'unconstrained_std']
        """
        wrapper_log = []  # log messages from this wrapper and prepend to pbalancer log
        if options is None:
            options = vars(Options())
        else:
            options = vars(options)

        # the next part is added in the original wrapper
        options.update({
            'standard chemical potential': True,
            'catalytic rate constant geometric mean': True,
            'Michaelis constant': True,
            'activation constant': True,
            'inhibitory constant': True,
            'concentration': True,
            'concentration of enzyme': True,
            'equilibrium constant': True,
            'substrate catalytic rate constant': True,
            'product catalytic rate constant': True,
            'forward maximal velocity': True,
            'reverse maximal velocity': True,
            'chemical potential': True,
            'reaction affinity': True,
            })

        if 'unit' not in data.columns:
            wrapper_log.append('no unit column found, assuming default factories based on symbols defined in the data')
            units = PB_PRIOR['unit'][PB_PRIOR.index.isin(data.symbol.unique())].reset_index()
            data = pd.merge(data, units, on='symbol', how='outer')
        if 'quantity_type' not in data.columns:
            quality_type = PB_PRIOR['quantity_type'][PB_PRIOR.index.isin(data.symbol.unique())].reset_index()
            data = pd.merge(data, quality_type, on='symbol', how='outer')

        # remove exchange reactions if present - these are not balanced and not appropriate for balancing
        doc = self.doc.clone()  # deepcopy
        for reaction in self.exchange_reactions:
            wrapper_log.append(f'removing exchange reaction before balancing {reaction}')
            doc.model.removeReaction(reaction.id)

        # remove biomass reactions if present - these are not balanced and not appropriate for balancing
        for reaction in self.biomass_reactions:
            wrapper_log.append(f'removing biomass reaction before balancing {reaction}')
            doc.model.removeReaction(reaction.id)

        pb = ParameterBalancing(doc.model)
        pb.doc = doc  # doc or die, better keep this in scope or risk a SIGDEV ERROR
        # adopting naming convention in parameter balancing for clarity
        pseudo_std = pd.concat([self.pseudos['prior_std'].dropna(), self.pseudos['prior_geometric_std'].dropna()])
        pseudos = pd.concat([self.pseudos['prior_median'], pseudo_std], axis=1).T.to_dict(orient='list')
        prior_std = pd.concat([self.priors['prior_std'].dropna(), self.priors['prior_geometric_std'].dropna()])
        priors = pd.concat([self.priors['prior_median'], prior_std], axis=1).T.to_dict(orient='list')
        pmin = self.bounds['lower_bound'].to_dict()
        pmax = self.bounds['upper_bound'].to_dict()

        if data is None:
            sbtab = pb.make_empty_sbtab(pmin, pmax, options)
        else:
            df = (data.replace(SYMBOL_MAP).rename(columns=dict(symbol='quantity_type')).rename(columns=SBTAB_MAP))
            sbtab_data = SBtab.SBtabTable.from_data_frame(df, table_id='data_table_id.csv', table_type='Quantity')
            sbtab = pb.make_sbtab(sbtab_data, 'data.tsv', 'All organisms', options['cell_volume'], pmin, pmax, options)

        if options['use_pseudo_values']:
            sbtab_new = pb.fill_sbtab(sbtab, pseudos, priors)
        else:
            sbtab_new = pb.fill_sbtab(sbtab)

        # perform balancing. The returned information should allow one to sample the posterior directly
        result = pb.make_balancing(sbtab_new, sbtab, pmin, pmax, options)
        (sbtab_final, mean_post, q_post, c_string, c_post, Q, shannons, log) = result

        log = "\n".join(wrapper_log) + log
        if log:
            logger.info(log)
            print(log)

        df = (sbtab_final.to_data_frame().replace(SYMBOL_MAP.inverse)
              .rename(columns=SBTAB_MAP.inverse).rename(columns=dict(quantity_type='symbol')))
        del doc
        return df


def test():
    self = ModelParameterBalancer(settings.ECOLI_CORE_MODEL)
    data = pd.read_csv(os.path.join(settings.DATA_DIR, 'coli_data_for_pb.csv'), index_col=0)
    df = self.balance(data.drop('id', axis=1))


