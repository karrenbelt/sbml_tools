
from typing import Union
from sbml_tools.wrappers import ASTNode, Model, Compartment, Species, Reaction, LocalParameter, Parameter, utils
from sbml_tools.toolbox.factories import UnitDefinitionFactory, ParameterFactory
from sbml_tools.toolbox.kinetics.sbo_utils import NAME_TO_SBO_TERM, SBO
from sbml_tools import config

# TODO
## idea is to formulate pure ASTNodes, and interpret them afterwards for parameter and unit creation
## advantage: more modular
## this should encapsulte all kinetics from the respective folder


class KineticLawFactory:
    """
    create kinetic laws:
    - in full complexity
    - abstracted as function definitions using lambda expression at model level

    """
    def __init__(self, model: Model):
        self.model = model
        self.parameter_factory = ParameterFactory(model)

    def mass_action_kinetics(self):
        return  # ASTNode.from_string()
