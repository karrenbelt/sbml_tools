import re
import numpy as np
from sbml_tools.wrappers import Unit


# TODO:
#  rescaling (e.g. 3e6 micrometre to 3 metre)


PREDEFINED_KINDS = [
    'ampere',  # base unit of currency
    'becquerel',  # unit of radiation: counts / second
    'candela',  # base unit of luminosity
    'coulomb',  # unit of charge: ampere * second
    'farad',  # unit of capacitance: coulomb / volt
    'gram',  # base unit of mass
    'gray',  # unit of radiation: joule / kilogram
    'henry',  # unit of inductance: weber / ampere
    'hertz',  # unit of frequency: 1 / second
    'joule',  # unit of energy: newton * meter
    'katal',  # unit of activity: mole / second
    'kelvin',  # base unit of temperature
    'kilogram',  # unit of mass (actual SI unit)
    'litre',  # unit of volume: decimeter ** 3
    'lumen',  # unit of luminous flux: candela * steradian
    'lux',  # unit of illuminance: lumen / meter ** 2
    'metre',  # base unit of length
    'mole',  # base unit of substance
    'newton',  # unit of force: kilogram * meter / second ** 2
    'ohm',  # unit of resistance: volt / ampere
    'pascal',  # unit of pressure: newton / meter ** 2
    'radian',  # base unit of angle
    'second',  # base unit of time
    'siemens',  # unit of conductance: ampere / volt
    'sievert',  # unit of radiation: joule / kilogram
    'steradian',  # unit of angle: radian ** 2
    'tesla',  # unit of magnetic flux: weber / meter ** 2
    'volt',  # unit of electric potential: joule / coulomb
    'watt',  # unit of power: joule / second
    'weber',  # unit of magnetic flux: volt * second
    # special cases:
    'avogadro',  # number of avogadro * dimensionless
    'dimensionless',  # dimensionless - meant for when units cancel out
    'item',  # dimensionless - meant for "N items" when mole is not appropriate
    ]

CUSTOM_KINDS = ['minute', 'hour', 'day', 'week', 'celsius', 'fahrenheit', 'angstrom', 'calorie']


class MetricPrefix:
    y = yocto = 1e-24
    z = zepto = 1e-21
    a = atto = 1e-18
    f = femto = 1e-15
    p = pico = 1e-12
    n = nano = 1e-09
    u = µ = micro = 1e-06
    m = milli = 1e-03
    c = centi = 1e-2
    d = deci = 1e-1
    da = deca = 1e1
    h = hecto = 1e2
    k = kilo = 1e3
    M = mega = 1e6
    G = giga = 1e9
    T = tera = 1e12
    P = peta = 1e15
    E = exa = 1e+18
    Z = zetta = 1e+21
    Y = yotta = 1e+24


exp_prefixes = dict(
    squared=2,
    cubic=3,
    quatric=4,
    quintic=5,
    sextic=6,
    heptic=7,
    octic=8,
    nonic=9,
    decic=10,
    )


def compile_unit_pattern():
    metric_prefix = r'|'.join([k for k in vars(MetricPrefix) if not k.startswith('_')])
    unit_kind = r'|'.join(PREDEFINED_KINDS + CUSTOM_KINDS)
    exp_prefix = r'|'.join(exp_prefixes.keys())
    pattern = re.compile(rf'(per_)?({exp_prefix})?_?({metric_prefix})?_?({unit_kind})_?([0-9]+)?')
    return pattern


UNIT_PATTERN = compile_unit_pattern()


def tokenize_unit_identifier(unit_identifier):

    kinds_found = re.findall(r'|'.join(PREDEFINED_KINDS + CUSTOM_KINDS), unit_identifier)
    if len(kinds_found) != 1:
        raise ValueError(f'a unit consists of exactly one kind, found: {kinds_found}')

    match = UNIT_PATTERN.match(unit_identifier)
    if not match:
        raise ValueError(f'unit identifier {unit_identifier} not recognized')
    inverse, exp_prefix, metric_prefix, kind, exp_suffix = match.groups()
    if exp_prefix and exp_suffix:
        raise ValueError(f'both exponent prefix {exp_prefix} and suffix {exp_suffix} found')

    exponent = exp_prefixes[exp_prefix] if exp_prefix else int(exp_suffix) if exp_suffix else 1
    scale = int(np.log10(getattr(MetricPrefix, metric_prefix))) if metric_prefix else 0
    # multiplier = int(float(number)) if number and float(number).is_integer() else float(number) if number else 1.0
    multiplier = 1.0

    if inverse:
        exponent *= -1

    return kind, exponent, scale, multiplier


class UnitFactory:
    # (multiplier * 10 ^ scale * kind) ^ exponent
    # units are always part of a unit definition, and as such we do not associated an identifier with them
    # here we create solely one-dimensional units

    def __init__(self, level: int, version: int):
        self.level = level
        self.version = version

    @property
    def implemented_unit_kinds(self):
        return PREDEFINED_KINDS + CUSTOM_KINDS

    def _create_unit(self, kind, exponent, scale, multiplier):
        level = self.level
        version = self.version
        return Unit(level=level, version=version,
                    kind=kind, exponent=exponent, scale=scale, multiplier=multiplier)

    def create_unit(self, identifier):

        kind, exponent, scale, multiplier = tokenize_unit_identifier(identifier)

        if kind in PREDEFINED_KINDS:
            if kind == 'dimensionless':
                exponent = 0
            return self._create_unit(kind, exponent=exponent, scale=scale, multiplier=multiplier)

        # time
        elif kind == 'minute':
            return self._create_unit('second', exponent, scale, 60 * multiplier)
        elif kind == 'hour':
            return self._create_unit('second', exponent, scale, 60 * 60 * multiplier)
        elif kind == 'day':
            return self._create_unit('second', exponent, scale, 24 * 60 * 60 * multiplier)
        elif kind == 'week':
            return self._create_unit('second', exponent, scale, 7 * 24 * 60 * 60 * multiplier)

        # temperature
        elif kind == 'celsius':
            return self._create_unit('kelvin', exponent, scale, multiplier + 273.15)
        elif kind == 'fahrenheit':
            return self._create_unit('kelvin', exponent, scale,  5/9 * (multiplier + 459.67))

        # length
        elif kind == 'angstrom':
            return self._create_unit('metre', exponent * -10, scale, multiplier)

        # energy (force * length)
        elif kind == 'calorie':
            return self._create_unit('joule', exponent, scale, 4.184 * multiplier)

        else:
            raise ValueError(f'unit not implemented: {identifier}')


