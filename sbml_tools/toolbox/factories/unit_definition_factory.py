
import re
from sbml_tools.wrappers import Unit, UnitDefinition
from sbml_tools.toolbox.factories.unit_factory import UnitFactory, PREDEFINED_KINDS, CUSTOM_KINDS


# TODO:
#  1. dimensionless units


def compile_unit_definition_pattern():
    # 1. we need to detect is if there are multiple units present
    # 2. their relationship is multiplicative
    pattern = re.compile(rf'_(?=per_)|_times_')  # _times_ is not used; remove
    return pattern


UNIT_DEFINITION_PATTERN = compile_unit_definition_pattern()


def tokenize_unit_definition_identifier(unit_definition_identifier):
    units = UNIT_DEFINITION_PATTERN.split(unit_definition_identifier)
    return units


class UnitDefinitionFactory(UnitFactory):
    # unit_definition = unit_1 * unit_2 * ... * unit_n
    # NOTES:
    # 1. don't use multiplier to set the value of a unit, it is not meant for that purpose
    # 2. floating point conversion may introduce inaccuracies

    def __init__(self, level: int, version: int):

        super().__init__(level, version)

    def _create(self, identifier, units, name=None, sbo_term=None):
        return UnitDefinition(level=self.level, version=self.version,
                              id=identifier, units=units, name=name, sbo_term=sbo_term)

    def __call__(self, identifier):
        kwargs = dict(identifier=identifier)  # strip numeric prefix if present
        unit_identifiers = tokenize_unit_definition_identifier(identifier)

        units = []
        for unit_identifier in unit_identifiers:
            # if unit_identifier == 'molar':
            #     units.extend = [self.create_unit('mole'), self.create_unit('litre')]

            unit = self.create_unit(unit_identifier)
            units.append(unit)
        kwargs.update(units=units)

        return self._create(**kwargs)

