
from sbml_tools import config
from sbml_tools.wrappers import SBMLDocument


config.ENFORCE_UNITS = False


def setup_model():
    doc = SBMLDocument()
    model = doc.create_model(
        # substance_units='mole',
        # time_units='second',
        # volume_units='litre',
        # area_units='metre2',
        # length_units='metre',
        # extent_units='mole',
        )
    model.create_compartment(id='c', constant=True)


    species_identifiers = ['S1', 'B', 'C', 'D', 'E']
    model.create_species(id='S1', compartment='c', has_only_substance_units=True,
                         boundary_condition=False, constant=False)
    # create parameters
    # doc.model.create_parameter(id='kcatf', constant=False, value=1.0)
    # create reactions
    doc.model.create_reaction(id='rxn', reversible=True, reactants='A + B', products='C + D', modifiers='E')
    reaction = doc.model.get('rxn')