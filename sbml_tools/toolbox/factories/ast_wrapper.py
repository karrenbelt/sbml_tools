import re
import operator
import collections
from typing import List, Union

import numpy as np
import libsbml
import sympy
import sympy.stats
from sympy.printing.mathml import MathMLPrinter

# TODO this is the first version with a lot of monkey-patching - outdated

def _print_Symbol(self, sym):
    """This removes the replacement of greek characters, and conversion of _ and ^ to sub and superscript"""
    ci = self.dom.createElement(self.mathml_tag(sym))
    ci.appendChild(self.dom.createTextNode(sym.name))
    return ci


def _print_float(self, e):
    """printer only has int defined and not float so we add it."""
    x = self.dom.createElement(self.mathml_tag(e))
    x.appendChild(self.dom.createTextNode(str(e)))
    return x


def mathml_tag(self, e):
    """Returns the MathML tag for an expression."""
    translate = {
        'Add': 'plus',
        'Mul': 'times',
        'Derivative': 'diff',
        'Number': 'cn',
        'int': 'cn',
        'Pow': 'power',
        'Symbol': 'ci',
        'MatrixSymbol': 'ci',
        'RandomSymbol': 'ci',
        'Integral': 'int',
        'Sum': 'sum',
        'sin': 'sin',
        'cos': 'cos',
        'tan': 'tan',
        'cot': 'cot',
        'asin': 'arcsin',
        'asinh': 'arcsinh',
        'acos': 'arccos',
        'acosh': 'arccosh',
        'atan': 'arctan',
        'atanh': 'arctanh',
        'acot': 'arccot',
        'atan2': 'arctan',
        'log': 'ln',
        'Equality': 'eq',
        'Unequality': 'neq',
        'GreaterThan': 'geq',
        'LessThan': 'leq',
        'StrictGreaterThan': 'gt',
        'StrictLessThan': 'lt',
    }
    ### TODO: this is what I added
    translate.update({'float': 'cn',
                      'acoth': 'arccoth',
                      'acsc': 'arccsc',
                      'asec': 'arcsec',
                      'acsch': 'arccsch',
                      'asech': 'arcsech',
                      })

    for cls in e.__class__.__mro__:
        n = cls.__name__
        if n in translate:
            return translate[n]
    # Not found in the MRO set
    n = e.__class__.__name__
    return n.lower()

# method overwriting
MathMLPrinter.mathml_tag = mathml_tag
MathMLPrinter._print_Symbol = _print_Symbol
MathMLPrinter._print_float = _print_float


ASTS = {
    libsbml.AST_CONSTANT_E: 'AST_CONSTANT_E',
    libsbml.AST_CONSTANT_FALSE: 'AST_CONSTANT_FALSE',
    libsbml.AST_CONSTANT_PI: 'AST_CONSTANT_PI',
    libsbml.AST_CONSTANT_TRUE: 'AST_CONSTANT_TRUE',
    libsbml.AST_CSYMBOL_FUNCTION: 'AST_CSYMBOL_FUNCTION',
    libsbml.AST_DISTRIB_FUNCTION_BERNOULLI: 'AST_DISTRIB_FUNCTION_BERNOULLI',
    libsbml.AST_DISTRIB_FUNCTION_BINOMIAL: 'AST_DISTRIB_FUNCTION_BINOMIAL',
    libsbml.AST_DISTRIB_FUNCTION_CAUCHY: 'AST_DISTRIB_FUNCTION_CAUCHY',
    libsbml.AST_DISTRIB_FUNCTION_CHISQUARE: 'AST_DISTRIB_FUNCTION_CHISQUARE',
    libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL: 'AST_DISTRIB_FUNCTION_EXPONENTIAL',
    libsbml.AST_DISTRIB_FUNCTION_GAMMA: 'AST_DISTRIB_FUNCTION_GAMMA',
    libsbml.AST_DISTRIB_FUNCTION_LAPLACE: 'AST_DISTRIB_FUNCTION_LAPLACE',
    libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL: 'AST_DISTRIB_FUNCTION_LOGNORMAL',
    libsbml.AST_DISTRIB_FUNCTION_NORMAL: 'AST_DISTRIB_FUNCTION_NORMAL',
    libsbml.AST_DISTRIB_FUNCTION_POISSON: 'AST_DISTRIB_FUNCTION_POISSON',
    libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH: 'AST_DISTRIB_FUNCTION_RAYLEIGH',
    libsbml.AST_DISTRIB_FUNCTION_UNIFORM: 'AST_DISTRIB_FUNCTION_UNIFORM',
    libsbml.AST_DIVIDE: 'AST_DIVIDE',
    libsbml.AST_END_OF_CORE: 'AST_END_OF_CORE',
    libsbml.AST_FUNCTION: 'AST_FUNCTION',
    libsbml.AST_FUNCTION_ABS: 'AST_FUNCTION_ABS',
    libsbml.AST_FUNCTION_ARCCOS: 'AST_FUNCTION_ARCCOS',
    libsbml.AST_FUNCTION_ARCCOSH: 'AST_FUNCTION_ARCCOSH',
    libsbml.AST_FUNCTION_ARCCOT: 'AST_FUNCTION_ARCCOT',
    libsbml.AST_FUNCTION_ARCCOTH: 'AST_FUNCTION_ARCCOTH',
    libsbml.AST_FUNCTION_ARCCSC: 'AST_FUNCTION_ARCCSC',
    libsbml.AST_FUNCTION_ARCCSCH: 'AST_FUNCTION_ARCCSCH',
    libsbml.AST_FUNCTION_ARCSEC: 'AST_FUNCTION_ARCSEC',
    libsbml.AST_FUNCTION_ARCSECH: 'AST_FUNCTION_ARCSECH',
    libsbml.AST_FUNCTION_ARCSIN: 'AST_FUNCTION_ARCSIN',
    libsbml.AST_FUNCTION_ARCSINH: 'AST_FUNCTION_ARCSINH',
    libsbml.AST_FUNCTION_ARCTAN: 'AST_FUNCTION_ARCTAN',
    libsbml.AST_FUNCTION_ARCTANH: 'AST_FUNCTION_ARCTANH',
    libsbml.AST_FUNCTION_CEILING: 'AST_FUNCTION_CEILING',
    libsbml.AST_FUNCTION_COS: 'AST_FUNCTION_COS',
    libsbml.AST_FUNCTION_COSH: 'AST_FUNCTION_COSH',
    libsbml.AST_FUNCTION_COT: 'AST_FUNCTION_COT',
    libsbml.AST_FUNCTION_COTH: 'AST_FUNCTION_COTH',
    libsbml.AST_FUNCTION_CSC: 'AST_FUNCTION_CSC',
    libsbml.AST_FUNCTION_CSCH: 'AST_FUNCTION_CSCH',
    libsbml.AST_FUNCTION_DELAY: 'AST_FUNCTION_DELAY',
    libsbml.AST_FUNCTION_EXP: 'AST_FUNCTION_EXP',
    libsbml.AST_FUNCTION_FACTORIAL: 'AST_FUNCTION_FACTORIAL',
    libsbml.AST_FUNCTION_FLOOR: 'AST_FUNCTION_FLOOR',
    libsbml.AST_FUNCTION_LN: 'AST_FUNCTION_LN',
    libsbml.AST_FUNCTION_LOG: 'AST_FUNCTION_LOG',
    libsbml.AST_FUNCTION_MAX: 'AST_FUNCTION_MAX',
    libsbml.AST_FUNCTION_MIN: 'AST_FUNCTION_MIN',
    libsbml.AST_FUNCTION_PIECEWISE: 'AST_FUNCTION_PIECEWISE',
    libsbml.AST_FUNCTION_POWER: 'AST_FUNCTION_POWER',
    libsbml.AST_FUNCTION_QUOTIENT: 'AST_FUNCTION_QUOTIENT',
    libsbml.AST_FUNCTION_RATE_OF: 'AST_FUNCTION_RATE_OF',
    libsbml.AST_FUNCTION_REM: 'AST_FUNCTION_REM',
    libsbml.AST_FUNCTION_ROOT: 'AST_FUNCTION_ROOT',
    libsbml.AST_FUNCTION_SEC: 'AST_FUNCTION_SEC',
    libsbml.AST_FUNCTION_SECH: 'AST_FUNCTION_SECH',
    libsbml.AST_FUNCTION_SIN: 'AST_FUNCTION_SIN',
    libsbml.AST_FUNCTION_SINH: 'AST_FUNCTION_SINH',
    libsbml.AST_FUNCTION_TAN: 'AST_FUNCTION_TAN',
    libsbml.AST_FUNCTION_TANH: 'AST_FUNCTION_TANH',
    libsbml.AST_INTEGER: 'AST_INTEGER',
    libsbml.AST_LAMBDA: 'AST_LAMBDA',
    libsbml.AST_LINEAR_ALGEBRA_DETERMINANT: 'AST_LINEAR_ALGEBRA_DETERMINANT',
    libsbml.AST_LINEAR_ALGEBRA_MATRIX: 'AST_LINEAR_ALGEBRA_MATRIX',
    libsbml.AST_LINEAR_ALGEBRA_MATRIXROW: 'AST_LINEAR_ALGEBRA_MATRIXROW',
    libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT: 'AST_LINEAR_ALGEBRA_OUTER_PRODUCT',
    libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT: 'AST_LINEAR_ALGEBRA_SCALAR_PRODUCT',
    libsbml.AST_LINEAR_ALGEBRA_SELECTOR: 'AST_LINEAR_ALGEBRA_SELECTOR',
    libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE: 'AST_LINEAR_ALGEBRA_TRANSPOSE',
    libsbml.AST_LINEAR_ALGEBRA_VECTOR: 'AST_LINEAR_ALGEBRA_VECTOR',
    libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT: 'AST_LINEAR_ALGEBRA_VECTOR_PRODUCT',
    libsbml.AST_LOGICAL_AND: 'AST_LOGICAL_AND',
    libsbml.AST_LOGICAL_EXISTS: 'AST_LOGICAL_EXISTS',
    libsbml.AST_LOGICAL_FORALL: 'AST_LOGICAL_FORALL',
    libsbml.AST_LOGICAL_IMPLIES: 'AST_LOGICAL_IMPLIES',
    libsbml.AST_LOGICAL_NOT: 'AST_LOGICAL_NOT',
    libsbml.AST_LOGICAL_OR: 'AST_LOGICAL_OR',
    libsbml.AST_LOGICAL_XOR: 'AST_LOGICAL_XOR',
    libsbml.AST_MINUS: 'AST_MINUS',
    libsbml.AST_NAME: 'AST_NAME',
    libsbml.AST_NAME_AVOGADRO: 'AST_NAME_AVOGADRO',
    libsbml.AST_NAME_TIME: 'AST_NAME_TIME',
    libsbml.AST_PLUS: 'AST_PLUS',
    libsbml.AST_POWER: 'AST_POWER',
    libsbml.AST_RATIONAL: 'AST_RATIONAL',
    libsbml.AST_REAL: 'AST_REAL',
    libsbml.AST_REAL_E: 'AST_REAL_E',
    libsbml.AST_RELATIONAL_EQ: 'AST_RELATIONAL_EQ',
    libsbml.AST_RELATIONAL_GEQ: 'AST_RELATIONAL_GEQ',
    libsbml.AST_RELATIONAL_GT: 'AST_RELATIONAL_GT',
    libsbml.AST_RELATIONAL_LEQ: 'AST_RELATIONAL_LEQ',
    libsbml.AST_RELATIONAL_LT: 'AST_RELATIONAL_LT',
    libsbml.AST_RELATIONAL_NEQ: 'AST_RELATIONAL_NEQ',
    libsbml.AST_SERIES_PRODUCT: 'AST_SERIES_PRODUCT',
    libsbml.AST_SERIES_SUM: 'AST_SERIES_SUM',
    libsbml.AST_STATISTICS_MEAN: 'AST_STATISTICS_MEAN',
    libsbml.AST_STATISTICS_MEDIAN: 'AST_STATISTICS_MEDIAN',
    libsbml.AST_STATISTICS_MODE: 'AST_STATISTICS_MODE',
    libsbml.AST_STATISTICS_MOMENT: 'AST_STATISTICS_MOMENT',
    libsbml.AST_STATISTICS_SDEV: 'AST_STATISTICS_SDEV',
    libsbml.AST_STATISTICS_VARIANCE: 'AST_STATISTICS_VARIANCE',
    libsbml.AST_TIMES: 'AST_TIMES',
    libsbml.AST_UNKNOWN: 'AST_UNKNOWN',
    }

# Fox and Hill (2006) https://arxiv.org/pdf/physics/0612087.pdf
# AVOGADRO = sympy.Integer('602214141070409084099072')

DISTRIBUTIONS = {
    # libsbml.AST_DISTRIB_FUNCTION_BERNOULLI: sympy.stats.Bernoulli,
    # libsbml.AST_DISTRIB_FUNCTION_BINOMIAL: sympy.stats.Binomial,
    # libsbml.AST_DISTRIB_FUNCTION_CAUCHY: sympy.stats.Cauchy,
    # libsbml.AST_DISTRIB_FUNCTION_CHISQUARE: sympy.stats.ChiSquared,
    # libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL: sympy.stats.Exponential,
    # libsbml.AST_DISTRIB_FUNCTION_GAMMA: sympy.stats.Gamma,
    # libsbml.AST_DISTRIB_FUNCTION_LAPLACE: sympy.stats.Laplace,
    # libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL: sympy.stats.LogNormal,
    # libsbml.AST_DISTRIB_FUNCTION_NORMAL: sympy.stats.Normal,
    # libsbml.AST_DISTRIB_FUNCTION_POISSON: sympy.stats.Poisson,
    # libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH: sympy.stats.Rayleigh,
    # libsbml.AST_DISTRIB_FUNCTION_UNIFORM: sympy.stats.Uniform,
    }

CONSTANTS = {
    libsbml.AST_CONSTANT_E: sympy.E,
    # libsbml.AST_CONSTANT_FALSE: sympy.false,
    libsbml.AST_CONSTANT_PI: sympy.pi,
    # libsbml.AST_CONSTANT_TRUE: sympy.true,
}

NUMERICAL = {
    libsbml.AST_INTEGER,
    libsbml.AST_RATIONAL,
    libsbml.AST_REAL, # also nan, infinity
    libsbml.AST_REAL_E,
}

FUNCTIONS = {
    # libsbml.AST_CSYMBOL_FUNCTION, # not implemented
    # libsbml.AST_FUNCTION, # sympy error
    # libsbml.AST_LAMBDA, # sympy error
    }

CSYMBOLS = {
    libsbml.AST_NAME_TIME: sympy.Symbol('time'), # doesn't have a name attribute
    libsbml.AST_NAME_AVOGADRO: sympy.Symbol('avogadro'),
    }

CFUNCTIONS = {
    # libsbml.AST_FUNCTION_DELAY: sympy.Function('delay'), # not implemented
    # libsbml.AST_FUNCTION_RATE_OF: sympy.Function('rate_of'), # not implemented
    }

TRIGONOMETRIC_OPERATORS = {
    libsbml.AST_FUNCTION_ARCCSCH: sympy.acsch,
    libsbml.AST_FUNCTION_ARCSECH: sympy.asech,
    libsbml.AST_FUNCTION_ARCCOS: sympy.acos,
    libsbml.AST_FUNCTION_ARCCOSH: sympy.acosh,
    libsbml.AST_FUNCTION_ARCCOT: sympy.acot,
    libsbml.AST_FUNCTION_ARCCOTH: sympy.acoth,
    libsbml.AST_FUNCTION_ARCCSC: sympy.acsc,
    libsbml.AST_FUNCTION_ARCSEC: sympy.asec,
    libsbml.AST_FUNCTION_ARCSIN: sympy.asin,
    libsbml.AST_FUNCTION_ARCSINH: sympy.asinh,
    libsbml.AST_FUNCTION_ARCTAN: sympy.atan,
    libsbml.AST_FUNCTION_ARCTANH: sympy.atanh,
    libsbml.AST_FUNCTION_COS: sympy.cos,
    libsbml.AST_FUNCTION_COSH: sympy.cosh,
    libsbml.AST_FUNCTION_COT: sympy.cot,
    libsbml.AST_FUNCTION_COTH: sympy.coth,
    libsbml.AST_FUNCTION_CSC: sympy.csc,
    libsbml.AST_FUNCTION_CSCH: sympy.csch,
    libsbml.AST_FUNCTION_SEC: sympy.sec,
    libsbml.AST_FUNCTION_SECH: sympy.sech,
    libsbml.AST_FUNCTION_SIN: sympy.sin,
    libsbml.AST_FUNCTION_SINH: sympy.sinh,
    libsbml.AST_FUNCTION_TAN: sympy.tan,
    libsbml.AST_FUNCTION_TANH: sympy.tanh,
    }

ARITHMETIC_OPERATORS = {
    libsbml.AST_PLUS: sympy.Add,  # for non-unary types?
    libsbml.AST_MINUS: operator.sub,  # sympy.Add with all secondary arguments negative
    libsbml.AST_POWER: sympy.Pow,  # operator.pow,
    libsbml.AST_FUNCTION_POWER: sympy.Pow,  # operator.pow,
    libsbml.AST_DIVIDE: operator.truediv,  # sympy.Mul with all secondary arguments inverted
    libsbml.AST_TIMES: sympy.Mul,  # operator.mul,

    libsbml.AST_FUNCTION_CEILING: sympy.ceiling,
    libsbml.AST_FUNCTION_ABS: sympy.Abs,
    libsbml.AST_FUNCTION_ROOT: sympy.root,  # reversed arguments with sympy?
    libsbml.AST_FUNCTION_FACTORIAL: sympy.factorial,
    libsbml.AST_FUNCTION_FLOOR: sympy.floor,
    libsbml.AST_FUNCTION_EXP: sympy.exp,
    libsbml.AST_FUNCTION_LN: sympy.ln,
    # libsbml.AST_FUNCTION_LOG: sympy.log, # exceptional case, need to get the base
    libsbml.AST_FUNCTION_MAX: sympy.Max,
    libsbml.AST_FUNCTION_MIN: sympy.Min,
    # libsbml.AST_FUNCTION_QUOTIENT: '', # not implemented
    # libsbml.AST_FUNCTION_REM: '', # not implemented
    # libsbml.AST_FUNCTION_PIECEWISE: sympy.Piecewise, # not implemented
    }

LOGICAL_OPERATORS = {
    libsbml.AST_LOGICAL_AND: sympy.And,
    # libsbml.AST_LOGICAL_EXISTS: '',
    # libsbml.AST_LOGICAL_FORALL: '',
    # libsbml.AST_LOGICAL_IMPLIES: sympy.Implies,
    # libsbml.AST_LOGICAL_NOT: sympy.Not,
    libsbml.AST_LOGICAL_OR: sympy.Or,
    # libsbml.AST_LOGICAL_XOR: sympy.Xor,
    }

LINEAR_ALGEBRA = {
    # libsbml.AST_LINEAR_ALGEBRA_DETERMINANT,
    # libsbml.AST_LINEAR_ALGEBRA_MATRIX,
    # libsbml.AST_LINEAR_ALGEBRA_MATRIXROW,
    # libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT,
    # libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT,
    # libsbml.AST_LINEAR_ALGEBRA_SELECTOR,
    # libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE,
    # libsbml.AST_LINEAR_ALGEBRA_VECTOR,
    # libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT,
    }

RELATIONAL_OPERATORS = {
    libsbml.AST_RELATIONAL_EQ: sympy.Eq,
    libsbml.AST_RELATIONAL_GEQ: sympy.Ge,
    libsbml.AST_RELATIONAL_GT: sympy.Gt,
    libsbml.AST_RELATIONAL_LEQ: sympy.Le,
    libsbml.AST_RELATIONAL_LT: sympy.Lt,
    libsbml.AST_RELATIONAL_NEQ: sympy.Ne,
    }

UNKNOWN = {
    # libsbml.AST_END_OF_CORE,
    # libsbml.AST_UNKNOWN,
    }

SERIES = {
    # libsbml.AST_SERIES_PRODUCT,
    # libsbml.AST_SERIES_SUM
    }

STATISTICS = {
#     libsbml.AST_STATISTICS_MEAN,
#     libsbml.AST_STATISTICS_MEDIAN,
#     libsbml.AST_STATISTICS_MODE,
#     libsbml.AST_STATISTICS_MOMENT,
#     libsbml.AST_STATISTICS_SDEV,
#     libsbml.AST_STATISTICS_VARIANCE,
    }

VARIABLES = {libsbml.AST_NAME: sympy.Symbol}

ALL_OPERATORS = {**TRIGONOMETRIC_OPERATORS, **ARITHMETIC_OPERATORS, **LOGICAL_OPERATORS, **RELATIONAL_OPERATORS}

IMPLEMENTED = {*DISTRIBUTIONS, *CONSTANTS, *NUMERICAL, *FUNCTIONS, *CSYMBOLS, *CFUNCTIONS,
               *TRIGONOMETRIC_OPERATORS, *ARITHMETIC_OPERATORS, *LOGICAL_OPERATORS, *RELATIONAL_OPERATORS,
               *LINEAR_ALGEBRA, *UNKNOWN, *SERIES, *STATISTICS, *VARIABLES,}

NOT_IMPLEMENTED = {
    # distributions
    libsbml.AST_DISTRIB_FUNCTION_BERNOULLI,
    libsbml.AST_DISTRIB_FUNCTION_BINOMIAL,
    libsbml.AST_DISTRIB_FUNCTION_CAUCHY,
    libsbml.AST_DISTRIB_FUNCTION_CHISQUARE,
    libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL,
    libsbml.AST_DISTRIB_FUNCTION_GAMMA,
    libsbml.AST_DISTRIB_FUNCTION_LAPLACE,
    libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL,
    libsbml.AST_DISTRIB_FUNCTION_NORMAL,
    libsbml.AST_DISTRIB_FUNCTION_POISSON,
    libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH,
    libsbml.AST_DISTRIB_FUNCTION_UNIFORM,

    # CONSTANTS
    libsbml.AST_CONSTANT_FALSE,  #  sympy.false not iterable
    libsbml.AST_CONSTANT_TRUE,  # sympy.true not iterable

    # FUNCTIONS
    libsbml.AST_CSYMBOL_FUNCTION,
    libsbml.AST_FUNCTION,
    libsbml.AST_LAMBDA,  # not iterable

    # CSYMBOL FUNCTIONS
    libsbml.AST_FUNCTION_DELAY,
    libsbml.AST_FUNCTION_RATE_OF,

    libsbml.AST_FUNCTION_LOG,

    # ARITHMETIC OPERATORS
    libsbml.AST_FUNCTION_REM,
    libsbml.AST_FUNCTION_QUOTIENT,
    libsbml.AST_FUNCTION_PIECEWISE,

    # LOGICAL OPERATORS
    libsbml.AST_LOGICAL_EXISTS,
    libsbml.AST_LOGICAL_FORALL,
    libsbml.AST_LOGICAL_IMPLIES,
    libsbml.AST_LOGICAL_XOR, # sympy.Xor not iterable
    libsbml.AST_LOGICAL_NOT, # sympy.Not not iterable

    # LINEAR ALGEBRA
    libsbml.AST_LINEAR_ALGEBRA_DETERMINANT,
    libsbml.AST_LINEAR_ALGEBRA_MATRIX,
    libsbml.AST_LINEAR_ALGEBRA_MATRIXROW,
    libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT,
    libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT,
    libsbml.AST_LINEAR_ALGEBRA_SELECTOR,
    libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE,
    libsbml.AST_LINEAR_ALGEBRA_VECTOR,
    libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT,

    # UNKNOWN
    libsbml.AST_UNKNOWN,
    libsbml.AST_END_OF_CORE,

    # SERIES
    libsbml.AST_SERIES_PRODUCT,
    libsbml.AST_SERIES_SUM,

    # STATISTICS
    libsbml.AST_STATISTICS_MEAN,
    libsbml.AST_STATISTICS_MEDIAN,
    libsbml.AST_STATISTICS_MODE,
    libsbml.AST_STATISTICS_MOMENT,
    libsbml.AST_STATISTICS_SDEV,
    libsbml.AST_STATISTICS_VARIANCE,
    }

CSYMBOL_MATHML = {"<ci>time</ci>" : '<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/time"> time </csymbol>',
                  "<ci>avogadro</ci>":'<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/avogadro"> avogadro </csymbol>',
                  #"<ci>delay</ci>": '<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/delay"> delay </csymbol>',
                  #"<ci>rate_of</ci>":'<csymbol encoding="text" definitionURL="http://www.sbml.org/sbml/symbols/rateOf"> rate_of </csymbol>',
                  }

### special cases, duplicate
UNARY_OPERATORS = {libsbml.AST_PLUS: operator.pos,
                   libsbml.AST_MINUS: operator.neg,
    }


def are_equal_ast_nodes(a1 : libsbml.ASTNode, a2 : libsbml.ASTNode) -> bool:
    if type(a1) is not type(a2):
        return False
    if a1.getType() != a2.getType():
        return False
    if a1.getName() != a2.getName():
        return False
    if a1.getUnits() != a2.getUnits():
        return False
    if a1.getValue() != a2.getValue():
        if not np.isnan(a1.getValue()) == np.isnan(a2.getValue()):
            return False
    if a1.getNumChildren() != a2.getNumChildren():
        return False
    for i in range(a1.getNumChildren()):
        if not are_equal_ast_nodes(a1.getChild(i), a2.getChild(i)):
            return False
    return True

def check_ast_node(ast_node : libsbml.ASTNode) -> libsbml.ASTNode:
    if ast_node is None or not ast_node.isWellFormedASTNode():
        raise ValueError(f"Invalid libsml.ASTNode")
    return ast_node

def math_to_sympy(ast_node : libsbml.ASTNode):
    return ASTNodeWrapper(ast_node).to_sympy()

def sympy_to_math(sympy_expr : sympy.Expr):
    return ASTNodeWrapper.from_sympy(sympy.sympify(sympy_expr)).node


class ASTNodeWrapper:
    """Class for wrapping ASTNode of libsbml and converting it to Sympy and back."""

    def __init__(self, ast_node : libsbml.ASTNode):
        self.node = check_ast_node(ast_node)

    @property
    def type(self):
        return self.node.getType()

    @property
    def name(self):
        name = self.node.getName()
        if not name:
            raise ValueError(f"expected a name to translate into a sympy variable {self}")
        return name

    @property
    def value(self):
        value = None
        if self.type == libsbml.AST_INTEGER:
            value = self.node.getInteger()
        elif self.type in NUMERICAL:
            value = self.node.getReal()
        if value is None:
            raise ValueError(f"Not a numerical value for ASTNode type '{self.type}'.")
        return value

    @classmethod
    def from_sympy(cls, sympy_expression : sympy.Expr):
        """Convert a sympy expression back to ASTNode through MathML."""

        mathml = sympy.mathml(sympy_expression) # to_mathml(sympy_expression) ## customized mathml = sympy.mathml(sympy_expression)
        math_open = "<math xmlns='http://www.w3.org/1998/Math/MathML'>"
        math_close = "</math>"
        mathml = ''.join((math_open, mathml, math_close)).encode('ascii', 'xmlcharrefreplace').decode('utf-8')

        for k,v in CSYMBOL_MATHML.items():
            mathml = re.sub(k, v, mathml)

        ast_node = libsbml.readMathMLFromString(mathml)
        if ast_node is None or not ast_node.isWellFormedASTNode():
            raise ValueError(f"Invalid libsml.ASTNode from {mathml}")

        return cls(ast_node)

    def __iter__(self):
        for i in range(self.node.getNumChildren()):
            yield ASTNodeWrapper(self.node.getChild(i))

    def to_sympy(self, cast_to_int=True):

        if self.type in NOT_IMPLEMENTED:
            raise NotImplementedError(f"{ASTS[self.type]} not implemented.")

        elif self.type in CSYMBOLS:  # TODO: time
            return CSYMBOLS[self.type]

        elif self.type in CONSTANTS:
            return CONSTANTS[self.type]
        elif self.type in VARIABLES:
            return VARIABLES[self.type](self.name)  # generates the symbol

        elif self.type in NUMERICAL:
            return self.value

        elif self.type in ALL_OPERATORS:
            children = [child.to_sympy() for child in self]

            if len(children) == 1 and self.type in UNARY_OPERATORS:
                return UNARY_OPERATORS[self.type](*children)

            if self.type == libsbml.AST_FUNCTION_ROOT:
                children = reversed(children)
            return ALL_OPERATORS[self.type](*children)

        elif self.type in ALL_OPERATORS or self.type in FUNCTIONS:
            children = [child.to_sympy() for child in self]
            if self.type == libsbml.AST_LAMBDA:
                return sympy.Lambda(children[:-1], children[-1])
            elif self.type == libsbml.AST_FUNCTION:
                return sympy.Function(self.name)(*children)
            else: # libsbml.AST_CSYMBOL_FUNCTION
                raise ValueError(f"Unsupported ASTNode type: {self.type}")
        else:
            raise ValueError(f"ASTNode type '{self.type}' not recognized")

    def __str__(self):
        return f"ASTNodeWrapper({libsbml.formulaToL3String(self.node)})"

    def __repr__(self):
        return str(self)

    def __eq__(self, other):
        if type(self) is not type(other):
            return False
        return are_equal_ast_nodes(self.node, other.node)


pre = '<?xml version="1.0" encoding="UTF-8"?><math xmlns="http://www.w3.org/1998/Math/MathML">'
suf = '</math>'

# mathml_str = libsbml.writeMathMLToString(libsbml.parseL3Formula('Lambda(x, x + 1)')).replace('\n','')
# mathml_str = pre + '<lambda><bvar><ci>x</ci></bvar><apply><plus/><ci>x</ci><cn>1</cn></apply></lambda>' + suf
# print(libsbml.formulaToL3String(libsbml.readMathMLFromString(mathml_str)))
