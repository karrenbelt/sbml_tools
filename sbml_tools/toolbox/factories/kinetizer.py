import fractions
import functools
import collections
from typing import Optional, List

import libsbml
import numpy as np
from sympy import Symbol, Mul, Pow, Add, sqrt

from sbml_tools.toolbox.factories.ast_wrapper import sympy_to_math, math_to_sympy
from sbml_tools import check
from sbml_tools.utils import NAME_TO_SBO_TERM, SBO_MAPPING, get_pb_prior, clipper, flatten, try_cast_to_int


# TODO: first implementation, to be replaced


# and sbo term names, sbo terms currently serve no additional purpose
IMPLEMENTED_RATE_LAWS = dict(
     irreversible_mass_action='mass action rate law for irreversible reactions',
     reversible_mass_action='mass action rate law for reversible reactions',
     reversible_mass_action_haldane_substituted='mass action rate law for reversible reactions',
     irreversible_michaelis_menten='Henri-Michaelis-Menten rate law',  # Briggs-Haldane rate law
     reversible_michaelis_menten='Henri-Michaelis-Menten rate law',
     reversible_michaelis_menten_haldane_substituted='Henri-Michaelis-Menten rate law',
     hanekom='enzymatic rate law',
     convenience='modular rate law',
     convenience_haldane_substituted='modular rate law',
     common_modular='common modular rate law',
     common_modular_haldane_substituted='common modular rate law',
     direct_binding_modular='direct binding modular rate law',
     direct_binding_modular_haldane_substituted='direct binding modular rate law',
     simultaneous_binding_modular='simultaneous binding modular rate law',
     simultaneous_binding_modular_haldane_substituted='simultaneous binding modular rate law',
     power_law_modular='power-law modular rate law',
     power_law_modular_haldane_substituted='power-law modular rate law',
     force_dependent_modular='force-dependent modular rate law',
     force_dependent_modular_haldane_substituted='force-dependent modular rate law',
     hofmeyr='polymerization',
    )

IRREVERSIBLE_RATE_LAWS = {'irreversible_mass_action',
                          'irreversible_michaelis_menten',
                          'hofmeyr',
                          }

PB_PRIOR = get_pb_prior()

# SBO_ANNOTATION = {v: k for k, v in get_sbo_table().name.items()}

SpeciesReference = collections.namedtuple('SpeciesRef', 'species stoichiometry constant')
RegulatoryInteraction = collections.namedtuple('RegulatoryInteraction', 'reaction regulator regulation_type inhibition')
# LocalParameter = collections.namedtuple('Parameter', 'id value factories')


class Kinetizer:
    """
    Assign kinetic rate laws to enzymatically catalyzed biochemical reactions. .

    Species
    - Types: c, u
    - The 'species' attribute of the SpeciesReference must refer to a species existing in the model object

        Enzymes (ModifierReference or Parameter)
    - Type: u
    - Enzymes: are added as reaction modifiers per default, and should be present as species in the model

    Parameters:
    - Types: kcatf, kcatr, km, ki, ka, w, b, keq
      in case of metabolites-specific reaction parameters (km, ki, ka, w and b) the metabolite id will be suffixed
    - Creation / assignment:
      1. locally, on the kinetic law of the reaction.
      2. Alternatively, they can be set on as model parameters. We will prefix the reaction identifier,
         as per convention when promoting local parameters to global model parameters
    - SBO terms: parameter SBO term are assigned, which can be used to sample a generic prior

    Rate laws

    Regulatory interactions
    - creates the modifier reference if not present

    checking molecularity (stoichiometry) as many rate laws assume these to be integers
    """
    reserved_symbols = ['v', 'kcatf', 'kcatr', 'u', 'c', 'km', 'ki', 'ka', 'w', 'b', 'keq', 'vmaxf', 'vmaxr']

    def __init__(self, reaction: libsbml.Reaction, exclude_from_kinetics: Optional[list] = None):
        self._reaction = reaction
        self.exclude = exclude_from_kinetics if exclude_from_kinetics is not None else []

        all_integers = self.has_valid_molecularities()
        if not all_integers and 'biomass' not in self.id.lower():
            self.make_integer_stoichiometries()

        if any([species in self.reserved_symbols for species in self.species_ids]):
            raise ValueError('reserved symbols found as species identifiers')

    def has_valid_molecularities(self) -> bool:
        converted_species = self.reactants + self.products
        if not converted_species:
            return True
        is_constant, is_integer = zip(*[(s_ref.constant, s_ref.stoichiometry.is_integer())
                                        for s_ref in converted_species])
        if not all(is_constant):
            raise NotImplementedError(f"non-constant stoichiometries found for {self.id}")
        return all(is_integer)

    def make_integer_stoichiometries(self):
        stoichiometric_coefficients = [s.stoichiometry for s in list(self.reactants) + list(self.products)]
        denominators = [fractions.Fraction(x).limit_denominator().denominator for x in stoichiometric_coefficients]
        least_common_multiple = functools.reduce(lambda a, b: a * b // np.gcd(a, b), denominators)
        for s_ref in list(self._reaction.reactants) + list(self._reaction.products):
            check(s_ref.setStoichiometry(s_ref.getStoichiometry() * least_common_multiple))
        print(f"multiplied stoichiometries in {self.id} with {least_common_multiple}")

    @property
    def id(self):
        return self._reaction.id

    @property
    def model(self):
        return self._reaction.getModel()

    @property
    def reactants(self):
        return [s_ref for s_ref in self._reaction.reactants if s_ref.species not in self.exclude]

    @property
    def products(self):
        return [s_ref for s_ref in self._reaction.products if s_ref.species not in self.exclude]

    @property
    def modifiers(self):
        return [m_ref for m_ref in self._reaction.modifiers]

    @property
    def local_parameters(self):
        if self.kinetic_law:
            return list(self.kinetic_law.local_parameters)

    @property
    def local_parameter_ids(self):
        if self.kinetic_law:
            return [p.id for p in self.kinetic_law.local_parameters]

    @property
    def n_reactants(self):
        return int(sum([s.stoichiometry for s in self.reactants]))

    @property
    def n_products(self):
        return int(sum([s.stoichiometry for s in self.products]))

    @property
    def n_reactant_species(self):
        return self._reaction.getNumReactants()

    @property
    def n_product_species(self):
        return self._reaction.getNumProducts()

    @property
    def n_modifiers(self):
        return self._reaction.getNumModifiers()

    @property
    def n_local_parameters(self):
        if self.kinetic_law:
            return self.kinetic_law.getNumLocalParameters()
        return 0

    @property
    def reactant_ids(self):
        return [ref.species for ref in self.reactants]

    @property
    def product_ids(self):
        return [ref.species for ref in self.products]

    @property
    def modifier_ids(self):
        return [ref.species for ref in self.modifiers]

    @property
    def species_ids(self):
        return [ref.species for ref in self.reactants + self.products + self.modifiers]

    @property
    def kinetic_law(self):
        return self._reaction.getKineticLaw()

    @property
    def expr(self):
        if self.kinetic_law and self.kinetic_law.isSetMath():
            return math_to_sympy(self.kinetic_law.math)

    def remove_local_parameters(self):
        for _ in range(self.n_local_parameters):  # if they exist, a law must exist
            self._reaction.getKineticLaw().removeLocalParameter(0)

    def remove_modifiers(self):
        for _ in range(self.n_modifiers):
            self._reaction.removeModifier(0)

    def _prepend_id(self, parameter_ids):
        return [f'{self.id}_{s}' for s in parameter_ids]

    def _add_parameters(self, rate_law_parameters, local_parameters: bool = True):
        # rate_law_parameters = [str(s) for s in sympy_expr.free_symbols if str(s) not in self.species_ids]
        parameter_types = [pid.split('_')[0] for pid in rate_law_parameters]
        sbo_terms = [NAME_TO_SBO_TERM[SBO_MAPPING[p_type]] for p_type in parameter_types]

        if local_parameters:  # NOTE: local parameter has no attribute 'constant'
            self.remove_local_parameters()
            for i, pid in enumerate(rate_law_parameters):
                local_parameter = self.kinetic_law.createLocalParameter()
                check(local_parameter.setId(pid))
                check(local_parameter.setSBOTerm(sbo_terms[i]))

        else:  # prepend the reaction name
            model = self.model
            if model is None:
                raise ValueError(f'cannot set model parameters since there is no model associated with {self.id}')

            model_parameters = self._prepend_id(rate_law_parameters)
            already_in_model = {p.getId(): p for p in model.getListOfAllElements() if isinstance(p, libsbml.Parameter)}
            for i, pid in enumerate(model_parameters):
                if pid in already_in_model:
                    parameter = already_in_model[pid]
                else:
                    parameter = model.createParameter()
                    check(parameter.setId(pid))
                    check(parameter.setConstant(True))  # a required attribute
                check(parameter.setSBOTerm(sbo_terms[i]))

    def assign_kinetic_law(self, rate_law: str, local_parameters: bool = True, enzyme_as_modifier: bool = True,
                           regulation: Optional[List[RegulatoryInteraction]] = None):
        """
        Assigns / overwrites the kinetic rate law and modifiers, and assigns SBO terms
        enzyme_as_modifier: add as modifier (should reference an exisitng species in the model), or as parameter

        if local_parameters:
            remove old and create new local parameters (short math notation used),
            the local parameters can be promoted to global ones by libsbml conversion
        else:
            prepend the reaction identifier to the parameter identifiers and create them globally
        """
        if not self.kinetic_law:
            self._reaction.createKineticLaw()

        if regulation is None:
            regulation = []

        if rate_law not in IMPLEMENTED_RATE_LAWS:
            raise NotImplementedError(f"no {rate_law} rate law implemented")
        if rate_law != 'hofmeyr' and not self.has_valid_molecularities():  # checks for integer stoichiometries only
            raise ValueError(f'non-integer stoichiometries found {self.id}: {self.reaction_formula}')

        if rate_law in IRREVERSIBLE_RATE_LAWS:
            check(self._reaction.setReversible(False))
        else:
            check(self._reaction.setReversible(True))

        sympy_expr = getattr(self, rate_law)  # NOTE: SBO terms are insufficiently developed to differentiate all laws

        try:  # create / set the kinetic law and parameters, if not we remove the kinetic law to keep a valid model
            self.remove_modifiers()
            rate_law_parameters = [str(s) for s in sympy_expr.free_symbols if str(s) not in self.species_ids]
            if enzyme_as_modifier:
                enzyme = self._prepend_id(str(self.enzyme))[0]
                modifier = self._reaction.createModifier()
                check(modifier.setSpecies(enzyme))
                check(modifier.setSBOTerm(NAME_TO_SBO_TERM[SBO_MAPPING['u']]))
                sympy_expr = sympy_expr.subs({'u': enzyme})
                rate_law_parameters.remove(str(self.enzyme))
            self._add_parameters(rate_law_parameters, local_parameters=local_parameters)

            if not local_parameters:
                sympy_expr = sympy_expr.subs(dict(zip(rate_law_parameters, self._prepend_id(rate_law_parameters))))
            check(self.kinetic_law.setMath(sympy_to_math(sympy_expr)))
            check(self.kinetic_law.setSBOTerm(NAME_TO_SBO_TERM[IMPLEMENTED_RATE_LAWS[rate_law]]))

            for species_reference in self.reactants:
                check(species_reference.setSBOTerm(NAME_TO_SBO_TERM['reactant']))
            for species_reference in self.products:
                check(species_reference.setSBOTerm(NAME_TO_SBO_TERM['product']))

            # this is done last using a separate method, allowing to add regulation at a later stage if so desired
            if regulation:
                self.assign_regulation(regulation, local_parameters=local_parameters)

        except Exception as e:  # set back to None to keep a valid SBML Model
            self._reaction.setKineticLaw(None)
            raise e

    def assign_regulation(self, regulation: List[RegulatoryInteraction], local_parameters=True):
        """
        Assigns / overwrites regulatory terms to the existing rate law.
        Currently only allosteric control (r_reg) is implemented: simple, partial and complete

        v = E * r_reg * T / (D + d_reg)
        r_reg represents allosteric inhibition / activation, which can be either simple, partial or complete
        d_reg represents specific regulation, which is competitive inhibition / activation (thus non-allosteric)
        """
        if self.expr is None:
            raise ValueError('cannot add regulation without having set a rate law first')

        if not all([regulatory_interaction.reaction == self.id for regulatory_interaction in regulation]):
            raise ValueError("reaction id does not match")

        allosteric_terms = []
        for regulatory_interaction in regulation:
            if regulatory_interaction.regulation_type == 'specific':
                raise NotImplementedError('non-allosteric regulation (d_reg) is not implemented')

            regulator = regulatory_interaction.regulator
            if regulator not in self.modifier_ids:
                modifier = self._reaction.createModifier()
                check(modifier.setSpecies(regulator))
                check(modifier.setSBOTerm(NAME_TO_SBO_TERM['allosteric control']))

            ki = Symbol(f'ki_{regulator}')  # inhibitory constant
            ka = Symbol(f'ka_{regulator}')  # activation constant
            w = Symbol(f'w_{regulator}')  # regulation number
            b = Symbol(f'b_{regulator}')  # basal rate of activator / inhibitor
            c = Symbol(regulator)

            sympy_expr = {
                ('simple', 1): (ki / (ki + c)) ** w,
                ('simple', 0): (c / (ka + c)) ** w,
                ('specific', 1): (c / ki) ** w,
                ('specific', 0): (ka / c) ** w,
                ('partial', 1): (b + (1 - b) / (1 + c / ki)) ** w,
                ('partial', 0): (b + (1 - b) * (c / ka) / (1 + c / ka)) ** w,
                ('complete', 1): (1 / (1 + c / ki)) ** w,
                ('complete', 0): ((c / ka) / (1 + c / ka)) ** w,
                }[regulatory_interaction.regulation_type, regulatory_interaction.inhibition]

            regulatory_parameters = [str(s) for s in sympy_expr.free_symbols if str(s) != regulator]
            self._add_parameters(regulatory_parameters, local_parameters=local_parameters)

            if not local_parameters:
                sympy_expr = sympy_expr.subs(dict(zip(regulatory_parameters, self._prepend_id(regulatory_parameters))))
            allosteric_terms.append(sympy_expr)

        check(self.kinetic_law.setMath(sympy_to_math(Mul(*[self.expr, *allosteric_terms]))))

    @property
    def mass_action_reactants(self):
        return Mul(*[Pow(Symbol(s_ref.species), try_cast_to_int(s_ref.stoichiometry)) for s_ref in self.reactants])

    @property
    def mass_action_products(self):
        return Mul(*[Pow(Symbol(s_ref.species), try_cast_to_int(s_ref.stoichiometry)) for s_ref in self.products])

    @property
    def mass_action_ratio(self):
        return self.mass_action_products / self.mass_action_reactants

    @property
    def flux(self):
        return Symbol('v')

    @property
    def enzyme(self):
        return Symbol('u')

    @property
    def keq(self):
        return Symbol('keq')

    @property
    def kcatf(self):
        return Symbol('kcatf')

    @property
    def kcatr(self):
        return Symbol('kcatr')

    @property
    def rho(self):
        return self.mass_action_ratio / self.keq

    # Utility functions
    @property
    def _haldane(self):
        return 1 - self.mass_action_ratio / self.keq

    def _prime(self, s_ref):  # normalized reactant concentration: S / km_S
        return Symbol(s_ref.species) / Symbol(f'km_{clipper(s_ref.species)}')

    @property
    def _explicit_numerator(self):  # TODO: assert int
        return (self.kcatf * Mul(*[self._prime(s_ref) ** int(s_ref.stoichiometry) for s_ref in self.reactants])
                - self.kcatr * Mul(*[self._prime(s_ref) ** int(s_ref.stoichiometry) for s_ref in self.products]))

    @property
    def _haldane_numerator(self):  # TODO: verify, assert int stoich
        return (self.kcatf * Mul(*[self._prime(s_ref) ** int(s_ref.stoichiometry)
                                   for s_ref in self.reactants]) * self._haldane)

    # Mass-Action rate laws
    def _mass_action_kinetics(self, form):
        return {'irreversible': self.enzyme * self.kcatf * self.mass_action_reactants,
                'explicit': self.enzyme * (self.kcatf * self.mass_action_reactants
                                           - self.kcatr * self.mass_action_products),
                'haldane': self.enzyme * self.kcatf * self.mass_action_reactants * self._haldane,
                }[form]

    @property
    def irreversible_mass_action(self):
        return self._mass_action_kinetics('irreversible')

    @property
    def reversible_mass_action(self):
        return self._mass_action_kinetics('explicit')

    @property
    def reversible_mass_action_haldane_substituted(self):
        return self._mass_action_kinetics('haldane')

    # Single substrate Michaelis Menten
    def _michaelis_menten_kinetics(self, form):
        if not self.n_reactants == 1:
            raise AssertionError(f"More than one substrate {self.id}: ({self.reactants})")
        if form != 'irreversible' and not (self.n_reactants == self.n_products == 1):
            raise AssertionError(f"More than one product {self.id}: ({self.products})")
        reactant_s_ref, product_s_ref = self.reactants[0], self.products[0]
        return {'irreversible': self.enzyme * self.kcatf * Symbol(reactant_s_ref.species) / (
                        Symbol(reactant_s_ref.species) + Symbol('km_' + reactant_s_ref.species)),
                'explicit': self.enzyme * self._explicit_numerator / (
                        1 + self._prime(reactant_s_ref) + self._prime(product_s_ref)),
                'haldane': self.enzyme * self._haldane_numerator / (
                        1 + self._prime(reactant_s_ref) + self._prime(product_s_ref)),
                }[form]

    @property
    def irreversible_michaelis_menten(self):  # also known as Briggs-Haldane
        return self._michaelis_menten_kinetics('irreversible')

    @property
    def reversible_michaelis_menten(self):
        return self._michaelis_menten_kinetics('explicit')

    @property
    def reversible_michaelis_menten_haldane_substituted(self):
        return self._michaelis_menten_kinetics('haldane')

    # Hanekom rate laws (random order rate laws)
    def _hanekom_kinetics(self, form=''):  # TODO: explicit form
        if not (self.n_reactants == self.n_products) and not (self.n_reactants, self.n_products) in [(1, 2), (2, 1)]:
            raise AssertionError(f"{self.id}: n_reactants ({self.reactants}) must equal n_products ({self.products})" +
                                 f", or be uni-bi, or bi-uni")
        reactant_primes = flatten([[self._prime(s_ref)] * int(s_ref.stoichiometry) for s_ref in self.reactants])
        product_primes = flatten([[self._prime(s_ref)] * int(s_ref.stoichiometry) for s_ref in self.products])

        if self.n_reactants - self.n_products == 1:  # special case: bi - uni
            return (self.enzyme * self.kcatf * Mul(*reactant_primes) * self._haldane
                    / Add(1, Mul(*reactant_primes), product_primes[0]))
        elif self.n_reactants - self.n_products == -1:  # special case: uni - bi
            return (self.enzyme * self.kcatf * reactant_primes[0] * self._haldane
                    / Add(1, reactant_primes[0], Mul(*product_primes)))
        else:  # the general form when the number of reactants equals the number of products
            return (self.enzyme * (self.kcatf * Mul(*reactant_primes) * self._haldane)
                    / Mul(*[Add(1, reactant_primes[i], product_primes[i]) for i in range(self.n_reactants)]))

    @property
    def hanekom(self):  # (e.g. random order bi bi)
        return self._hanekom_kinetics()

    # Modular rate laws (arbitrary stoichiometries)
    @property
    def _convenience_denominator(self):
        return (Mul(*[Add(1, *[(self._prime(s_ref) ** i) for i in range(1, int(s_ref.stoichiometry) + 1)])
                      for s_ref in self.reactants])
                + Mul(*[Add(1, *[(self._prime(s_ref) ** i) for i in range(1, int(s_ref.stoichiometry) + 1)])
                        for s_ref in self.products]) - 1)

    @property  # same as convenience, with a slight difference for molecularities not equal to 1
    def _common_denominator(self):
        return (Mul(*[(1 + self._prime(s_ref)) ** int(s_ref.stoichiometry) for s_ref in self.reactants])
                + Mul(*[(1 + self._prime(s_ref)) ** int(s_ref.stoichiometry) for s_ref in self.products]) - 1)

    @property  # same as common modular, but only contains reactant / product terms of the highest order
    def _direct_denominator(self):
        return (Mul(*[Add(1, *[self._prime(s_ref) ** int(s_ref.stoichiometry)]) for s_ref in self.reactants])
                + Mul(*[Add(1, *[self._prime(s_ref) ** int(s_ref.stoichiometry)]) for s_ref in self.products]) - 1)

    @property
    def _simultaneous_denominator(self):
        return Mul(
            *[(1 + self._prime(s_ref)) ** int(s_ref.stoichiometry) for s_ref in [*self.reactants, *self.products]])

    @property
    def _force_denominator(self):
        return sqrt(
            Mul(*[self._prime(s_ref) ** int(s_ref.stoichiometry) for s_ref in [*self.reactants, *self.products]]))

    def modular(self, numerator_form='explicit', denominator_form='convenience'):

        """Convenience kinetics and Modular rate laws (Liebermeister 2006 & 2010).

        Generalized forms of reversible Michaelis-Menten that apply to any reaction stoichiometry.

        v = E_r * R_reg * T / (D + D_reg)

        Convenience kinetics: Derived from a random order enzyme mechanism (Liebermeister 2006)

        Modular rate laws: 5 flavors (Liebermeister 2010)

        - common: Same as convenience, where each term in the denominator represents one binding state of the enzyme.
            It has a slight difference with respect to convenience kinetics, in that non-unitary molecularities
            yield a different denominator (e.g. 1 + C' + C'^2 instead of 1 + 2C' + C'^2). This is because in the
            common modular form the first C can bind to either of the two binding sites, whereas in convenience
            kinetics the order is fixed.

        - direct binding: In contrast to common modular, the denominator just contains substrate and product terms of
            the highest order, so the reaction rate is higher, especially at low concentrations where the
            lower-order terms dominate.

        - simultaneous binding: In contrast to common modular, contains additional denominator terms, so the reaction
            rate is lower, especially at high concentrations.

        - power-law: Equivalent to mass action kinetics.

        - force-dependent: In contrast to the other modular rate laws, the FM rate law lacks the biochemically required
            denominator summand 1 and is not supported by a biochemical mechanism. Even worse, as one of the substrate
            or product concentrations goes to zero, the reaction affinity, and accordingly the rate, becomes infinite.

        Both convenience and modular rate laws come with either an explicit or haldane substituted denominator
        """
        return (self.enzyme * {'explicit': self._explicit_numerator, 'haldane': self._haldane_numerator}[numerator_form]
                / {'convenience': self._convenience_denominator,
                   'common': self._common_denominator,
                   'direct_binding': self._direct_denominator,
                   'simultaneous_binding': self._simultaneous_denominator,
                   'power_law': 1,
                   'force_dependent': self._force_denominator}[denominator_form])

    @property
    def convenience(self):
        return self.modular('explicit', 'convenience')

    @property
    def convenience_haldane_substituted(self):
        return self.modular('haldane', 'convenience')

    @property
    def common_modular(self):
        return self.modular('explicit', 'common')

    @property
    def common_modular_haldane_substituted(self):
        return self.modular('haldane', 'common')

    @property
    def direct_binding_modular(self):
        return self.modular('explicit', 'direct_binding')

    @property
    def direct_binding_modular_haldane_substituted(self):
        return self.modular('haldane', 'direct_binding')

    @property
    def simultaneous_binding_modular(self):
        return self.modular('explicit', 'simultaneous_binding')

    @property
    def simultaneous_binding_modular_haldane_substituted(self):
        return self.modular('haldane', 'simultaneous_binding')

    @property
    def power_law_modular(self):
        return self.modular('explicit', 'power_law')

    @property
    def power_law_modular_haldane_substituted(self):
        return self.modular('haldane', 'power_law')

    @property
    def force_dependent_modular(self):
        return self.modular('explicit', 'force_dependent')

    @property
    def force_dependent_modular_haldane_substituted(self):
        return self.modular('haldane', 'force_dependent')

    # Template-directed polymerization
    @property
    def hofmeyr(self):
        """We make the following assumptions and simplifications:
           - The enzyme concentration for this template driven reaction is constant.
             This assumption holds as long as template << enzyme.
           - All catalytic constants for the subunits are equal, we can lump them into a single kcatf.
           - dissociation half-reactions >> catalytic steps
           - enzyme >> K0
           - the sequence of the initial dimer is irrelevant, only sequence composition matters
        """
        return self.enzyme * self.kcatf / (1 + Mul(*[self._prime(s_ref) for s_ref in self.reactants]) ** -1)

    def hill(self):
        raise NotImplementedError()

    @property
    def species_excluded(self):
        if not self.expr:
            return list(set(self.species_ids))
        return set(self.species_ids).difference(map(str, self.expr.free_symbols))

    @property
    def reaction_formula(self):

        def get_substring(s_references):
            l = [(str(try_cast_to_int(s_ref.stoichiometry)), s_ref.species) for s_ref in s_references]
            return ' + '.join([pair[1] if pair[0] == '1' else ' '.join(pair) for pair in l])

        arrow = ['-->', '<=>'][self._reaction.reversible]
        return f"{get_substring(self.reactants)} {arrow} {get_substring(self.products)}"

    def __repr__(self):
        return f"""\n{self.__class__.__name__} at {hex(id(self))}\n\n\
                          id: '{self.id}'
                        name: '{self._reaction.name}'
                   
                   reactants: {list(self.reactant_ids)}
                    products: {list(self.product_ids)}
                   modifiers: {list(self.modifier_ids)}
             not in rate law: {list(self.species_excluded)}

            reaction formula: {self.reaction_formula}
            kinetic rate law: {self.expr} 
            local parameters: {self.local_parameter_ids}
        """


def test():
    from sbml_tools import SbmlBuilder
    builder = SbmlBuilder()
    builder.add_compartment('cytosol', size=1.0)

    builder.add_species('A', compartment='cytosol', initial_concentration=1.0)
    builder.add_species('B', compartment='cytosol', initial_concentration=1.0)
    builder.add_species('C', compartment='cytosol', initial_concentration=1.0)
    builder.add_species('D', compartment='cytosol', initial_concentration=1.0)
    #builder.add_species('E', compartment='cytosol', initial_concentration=1.0)

    builder.add_species('X', compartment='cytosol', initial_concentration=1.0)
    builder.add_species('Y', compartment='cytosol', initial_concentration=1.0)

    builder.add_reaction('R_rxn',
                         reactants=[SpeciesReference('A', 1.0, True), SpeciesReference('B', 1.5, True)],
                         products=[SpeciesReference('B', 1.0, True), SpeciesReference('D', 2.0, True)])
                                  # SpeciesReference('E', 1.0, True)])

    reaction = builder.reactions[0]
    kinetizer = Kinetizer(reaction)
    kinetizer.assign_kinetic_law(rate_law='convenience_haldane_substituted', local_parameters=True)
    regulation = [RegulatoryInteraction(reaction.id, 'X', regulation_type='complete', inhibition=True),
                  RegulatoryInteraction(reaction.id, 'Y', regulation_type='simple', inhibition=False)]
    kinetizer.assign_regulation(regulation, local_parameters=True)

    print(kinetizer.reactants)
    print(kinetizer.products)
    print(kinetizer.modifiers)
    print(kinetizer.mass_action_ratio)
    print(kinetizer.irreversible_mass_action)
    print(kinetizer.reversible_mass_action)

    kinetizer = Kinetizer(reaction)
    print(kinetizer.modular(numerator_form='explicit', denominator_form='convenience'))
    print(kinetizer.modular(numerator_form='haldane', denominator_form='convenience'))
    print(kinetizer.modular(numerator_form='explicit', denominator_form='common'))
    print(kinetizer.modular(numerator_form='explicit', denominator_form='direct_binding'))
    print(kinetizer.modular(numerator_form='explicit', denominator_form='power_law'))
    print(kinetizer.modular(numerator_form='explicit', denominator_form='force_dependent'))
    print(kinetizer.hofmeyr)

    import sympy
    sols = sympy.solve(kinetizer.expr - kinetizer.flux, kinetizer.kcatf)
    print(len(sols))

    all_species = list(reaction.reactants) + list(reaction.products)
    [s.stoichiometry for s in all_species]
