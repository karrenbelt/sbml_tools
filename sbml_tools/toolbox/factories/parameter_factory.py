from typing import Union
import pandas as pd
from sbml_tools.wrappers import SBMLDocument, Model, Compartment, Species, Reaction, LocalParameter, Parameter, utils
from sbml_tools.wrappers import SimpleSpeciesReference
from sbml_tools.toolbox.factories import UnitDefinitionFactory
from sbml_tools import config


class ParameterFactoryError(Exception):
    pass


PARAMETER_DEFINITION_DEFAULTS = pd.DataFrame(dict(
    # symbol = [name, sbo_term, constant, value, units, model_entity]
    # time='time',
    R=['ideal gas constant', None, True, 8.31446261815324, 'joule_per_kelvin_per_mole', 'model'],
    T=['thermodynamic temperature', 147, True, None, 'kelvin', 'model'],
    pH=['pH', None, True, None, 'dimensionless', 'compartment'],
    I=['ionic strength', None, True, None, None, 'compartment'],
    mu0=['standard chemical potential', 463, True, None, None, 'species'],
    mu=['chemical potential', None, True, None, None, 'species'],
    dmu0=['standard gibbs energy of reaction', None, True, None, None, 'reaction'],
    keq=['equilibrium constant', 281, True, None, 'dimensionless', 'reaction'],
    kf=['forward rate constant', 153, True, None, None, 'reaction'],
    kr=['reverse rate constant', 156, True, None, None, 'reaction'],
    kcatf=['substrate catalytic rate constant', 321, True, None, None, 'reaction'],
    kcatr=['product catalytic rate constant', 322, True, None, None, 'reaction'],
    vmaxf=['forward maximal velocity', 324, True, None, None, 'reaction'],
    vmaxr=['reverse maximal velocity', 325, True, None, None, 'reaction'],
    # a='reaction affinity',
    # kv='geometric mean rate constant',
    # kmprod='Michaelis constant for product',
    km=['michaelis constant', 27, True, None, None, 'reaction'],
    ka=['activation constant', 363, True, None, None, 'reaction'],
    ki=['inhibitory constant', 261, True, None, None, 'reaction'],
    ks=['half saturation constant', 194, True, None, None, 'reaction'],  # pseudo-dissociation constant
    b=['basal rate constant', 485, True, None, None, 'reaction'],
    w=['regulation number', 190, True, None, None, 'reaction'],  # hill coefficient
    ), index=['name', 'sbo_term', 'constant', 'value', 'units', 'model_entity'])


class ParameterFactory:
    """
    local parameters:
    - live in a separate XML namespace per reaction
    - are associated with a kinetic law, and only within that kinetic law are required to be unique
    - take precedence over global objects that have the same name (parameters, species, compartments, etc.)
    - are local to reactions and therefore cannot be changed by initial assignments

    global parameters:
    - must be unique at the global level
    - prefix the reaction identifier, as per convention when promoting local parameters to global model parameters
    - have an additional attribute "constant" that defaults to True and can be changed later.

    Parameter objects are merely created here, and not associated with the model or reaction.
    """

    # we want to have access to the model to obtain species / compartment units
    # 1. every parameter has a specific symbol associated
    # 2. if compartment or species are involved, these are suffixed
    # 3. if a reaction is involved, this will be prefixed unless they are created as local parameters
    def __init__(self, model: Model, create_local_parameters: bool = False):
        self.model = model
        self.unit_definition_factory = UnitDefinitionFactory(model.level, model.version)
        self.create_local_parameters = create_local_parameters
        self.parameter_defaults = PARAMETER_DEFINITION_DEFAULTS.copy()

    @property
    def create_local_parameters(self) -> bool:
        return self._create_local_parameters

    @create_local_parameters.setter
    def create_local_parameters(self, boolean: bool):
        if self.model.level < 3 and boolean:
            raise ValueError('cannot create local parameters in SBML level < 3')
        self._create_local_parameters = boolean

    def _infer_units(self, symbol, entity, species):
        # if we are left to infer the units, they must be present at the level of the model
        if symbol in ('vmaxf', 'vmaxr'):
            substance_units = self.model.substance_units  # TODO: infer reaction substance units from participants
            assert substance_units and self.model.time_units, f'cannot infer substance or time units for {symbol}'
            units = f'per_{substance_units}_per_{self.model.time_units}'
        elif symbol in ('kf', 'kr', 'kcatf', 'kcatr'):
            assert self.model.time_units, f'cannot infer time units from model for {symbol}'
            units = f'per_{self.model.time_units}'
        elif symbol == 'I':  # if not 3d compartment, shouldn't have this parameter either
            assert entity.spatial_dimensions == 3, f'compartment dimensions must be 3 to set ionic strength'
            volume_units = entity.units if entity.units else self.model.volume_units
            assert self.model.substance_units, f'cannot infer substance units for {symbol}'
            units = f'{self.model.substance_units}_per_{volume_units}'
        elif symbol in ('km', 'ka', 'ki', 'ks'):
            units = species.substance_units if species.substance_units else self.model.substance_units
            assert units, f'cannot infer units for {symbol}'
        elif symbol == 'b':
            substance_units = species.substance_units if species.substance_units else self.model.substance_units
            assert substance_units and self.model.time_units, f'cannot infer substance or time units for {symbol}'
            units = f'{substance_units}_per_{self.model.time_units}'
        else:  # mu0, mu, dmu0,
            raise ValueError(f"cannot infer units from model units for: '{symbol}'")
        return units

    def _get_unit_definition(self, symbol: str, entity, units: str, species):
        # if we are to use default units, this has higher precedence than inferring them from model units
        # however, usage of default units defaults to False; we still want the developer to explicitly allow this
        # if config.USE_DEFAULT_UNITS:
        #     units = DEFAULT_UNITS.get(symbol)

        if not units:  # first infer from entity (compartment, species), or model
            units = self._infer_units(symbol, entity, species)

        # get or construct the unit definition
        unit_definition = self.model.unit_definitions.get(units)
        if not unit_definition:
            unit_definition = self.unit_definition_factory(units)
        # we check if unit_definition is of the correct type
        # self._check_unit_type(symbol, unit_definition)
        return unit_definition

    def _get_or_create(self, symbol: str,
                       entity: Union[Model, Compartment, Species, Reaction, str] = None,
                       **kwargs):

        if isinstance(entity, str):
            entity = self.model.get(entity)

        kwargs = {**PARAMETER_DEFINITION_DEFAULTS[symbol].to_dict(), **kwargs}
        # kwargs.update(kwargs)

        species = kwargs.pop('species', None)  # km, ki, ka, ks, w, b
        if isinstance(species, str):
            species = self.model.species.get(species)
        elif isinstance(species, SimpleSpeciesReference):
            species = species.get_species()

        model_entity = kwargs.pop('model_entity')
        if not isinstance(entity, dict(
                model=Model,
                compartment=Compartment,
                species=Species,
                reaction=Reaction)[model_entity]):
            raise ValueError(f"entity '{entity}' does not match expected entity type: {model_entity}")

        #  1. add prefix reaction identifier and/or suffix entity identifier if needed
        identifier = symbol
        if isinstance(entity, (Compartment, Species)):  # always global
            identifier = f'{identifier}_{entity.id}'
        if isinstance(entity, Reaction):
            if not self.create_local_parameters:
                identifier = f'{entity.id}_{identifier}'  # global
            if species:
                identifier = f"{identifier}_{species.id}"  # km, ki, ka, ks, w, b

        # 2. get the parameter if the ID exists, ensure the definition matches, and return it
        parameter = self.model.parameters.get(identifier)
        if parameter:  # TODO: retrieve local parameter if exists
            differences = {attr: (value, kwargs[attr]) for attr, value in parameter.as_kwargs.items()
                           if attr in kwargs and kwargs[attr] not in (value, None)}
            if differences:
                raise ParameterFactoryError(f'parameter already exists, but the passed keywords '
                                            f'do not match the existing definition:\n{differences}')
            return parameter

        # 4. generate the unit definition
        unit_definition = self._get_unit_definition(symbol, entity,
                                                    kwargs.get('units'), species)
        kwargs['units'] = unit_definition.id

        # 5. create the parameter
        if isinstance(entity, Reaction) and self.create_local_parameters:
            if not entity.kinetic_law:
                entity.create_kinetic_law()
            constant = kwargs.pop('constant')
            if constant not in (True, None):
                raise ParameterFactoryError(f'local parameters are always constant, received: constant={constant}')
            parameter = entity.kinetic_law.create_local_parameter(id=identifier, **kwargs)
        else:
            parameter = self.model.create_parameter(id=identifier, **kwargs)

        # 6. add the unit definition to the model in case it did not exist
        if unit_definition not in list(self.model.unit_definitions):
            self.model.add_unit_definition(unit_definition)

        return parameter

    def __call__(self, symbol, **kwargs):
        return dict(
            R=self.ideal_gas_constant,
            T=self.thermodynamic_temperature,
            pH=self.ph,
            I=self.ionic_strength,
            mu0=self.standard_chemical_potential,
            mu=self.chemical_potential,
            dmu0=self.standard_gibbs_energy_of_reaction,
            keq=self.equilibrium_constant,
            kf=self.forward_rate_constant,
            kr=self.reverse_rate_constant,
            kcatf=self.substrate_catalytic_rate_constant,
            kcatr=self.product_catalytic_rate_constant,
            vmaxf=self.forward_maximal_velocity,
            vmaxr=self.reverse_maximal_velocity,
            km=self.michaelis_constant,
            ka=self.activation_constant,
            ki=self.inhibitory_constant,
            ks=self.half_saturation_constant,
            b=self.basel_rate_constant,
            w=self.regulation_number,
            )[symbol](**kwargs)

    # model-level parameters
    def ideal_gas_constant(self):
        return self._get_or_create(symbol='R', entity=self.model)

    def thermodynamic_temperature(self, **kwargs):
        assert kwargs.get('value', 0) >= 0, f'thermodynamic temperature cannot be below absolute zero'
        return self._get_or_create(symbol='T', entity=self.model, **kwargs)

    # compartment-level parameters
    def ph(self, compartment: Compartment, **kwargs):
        return self._get_or_create(symbol='pH', entity=compartment, **kwargs)

    def ionic_strength(self, compartment: Compartment, **kwargs):
        return self._get_or_create(symbol='I', entity=compartment, **kwargs)

    # Thermodynamic species-level parameters
    def standard_chemical_potential(self, species: Species, **kwargs):
        return self._get_or_create(symbol='mu0', entity=species, **kwargs)

    def chemical_potential(self, species: Species, **kwargs):
        return self._get_or_create(symbol='mu', entity=species, **kwargs)

    # Thermodynamic reaction-level parameters
    def standard_gibbs_energy_of_reaction(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='dmu0', entity=reaction, **kwargs)

    def equilibrium_constant(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='keq', entity=reaction, **kwargs)

    # Kinetic reaction-level parameters (these parameters can be local)
    def forward_rate_constant(self, reaction: Reaction, **kwargs):  # association constant
        return self._get_or_create(symbol='kf', entity=reaction, **kwargs)

    def reverse_rate_constant(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='kr', entity=reaction, **kwargs)

    def substrate_catalytic_rate_constant(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='kcatf', entity=reaction, **kwargs)

    def product_catalytic_rate_constant(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='kcatr', entity=reaction, **kwargs)

    def forward_maximal_velocity(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='vmaxf', entity=reaction, **kwargs)

    def reverse_maximal_velocity(self, reaction: Reaction, **kwargs):
        return self._get_or_create(symbol='vmaxr', entity=reaction, **kwargs)

    # def reaction_affinity(self, reaction: Reaction, value: float = None, units: str = None):
    #     return self._create(identifier=f'a_{reaction.id}', name='reaction affinity',
    #                         sbo_term=None, constant=True, value=value, units='kilojoule_per_mole')

    # Kinetic reaction-species-level parameters
    def _check_species_references(self, reaction: Reaction, species: Union[Species, SimpleSpeciesReference]):
        identifier = species.species if isinstance(species, SimpleSpeciesReference) else species.id
        if identifier not in [sref.species for sref in reaction.reactants + reaction.products]:
            raise ParameterFactoryError(f'species {species} not listed as reactant / product for {reaction}')

    def _check_modifier_references(self, reaction, species: Union[Species, SimpleSpeciesReference]):
        identifier = species.species if isinstance(species, SimpleSpeciesReference) else species.id
        if identifier not in [mref.species for mref in reaction.modifiers]:
            raise ParameterFactoryError(f'species {species} not listed as modifier for {reaction}')

    def michaelis_constant(self, reaction: Reaction, species: Union[Species, SimpleSpeciesReference], **kwargs):
        self._check_species_references(reaction, species)
        return self._get_or_create(symbol='km', entity=reaction, species=species, **kwargs)

    def activation_constant(self, reaction: Reaction, species: Species, **kwargs):
        self._check_modifier_references(reaction, species)
        return self._get_or_create(symbol='ka', entity=reaction, species=species, **kwargs)

    def inhibitory_constant(self, reaction: Reaction, species: Species, **kwargs):
        self._check_modifier_references(reaction, species)
        return self._get_or_create(symbol='ki', entity=reaction, species=species, **kwargs)

    def half_saturation_constant(self, reaction: Reaction, species: Species, **kwargs):
        self._check_species_references(reaction, species)
        return self._get_or_create(symbol='ks', entity=reaction, species=species, **kwargs)

    def basel_rate_constant(self, reaction: Reaction, species: Species, **kwargs):
        """basal rate of activator / inhibitor, used in partial activation / inhibition"""
        self._check_modifier_references(reaction, species)
        return self._get_or_create(symbol='b', entity=reaction, species=species, **kwargs)

    def regulation_number(self, reaction: Reaction, species: Species, **kwargs):
        """
        a number reflecting cooperativity of the binding process, where a value of 1.0 means no cooperativity
        and values > 1.0 representing positive cooperativity (similar to the hill coefficient).
        It does not have a mechanistic interpretation, as it does not reflect a physically possible reaction scheme.
        """
        self._check_modifier_references(reaction, species)
        return self._get_or_create(symbol='w', entity=reaction, species=species, **kwargs)

    # elif identifier == 'ks':
    #     kwargs.update(dict(name='half saturation constant', sbo_term=194))
    # elif identifier == 'm':
    #     kwargs.update(dict(name='number of binding sites', sbo_term=189))


def test():
    from sbml_tools.toolbox.test_model import setup_test_model

    doc = setup_test_model()
    factory = ParameterFactory(doc.model)

    # global
    R = factory.ideal_gas_constant()
    assert doc.model.unit_definitions[R.units]
    T = factory.thermodynamic_temperature(value=300, constant=False)
    assert doc.model.unit_definitions[T.units]

    # compartment-specific
    compartment = doc.model.compartments['c']
    assert compartment.units == 'litre'
    pH_cytosol = factory.ph(compartment=compartment)
    assert doc.model.unit_definitions[pH_cytosol.units]
    # I_cytosol = factory.ionic_strength(compartment=compartment)  # error as should
    # I_cytosol = factory.ionic_strength(compartment=compartment, units='millimole_per_litre')  # works

    assert not doc.model.unit_definitions['mole']
    doc.model.substance_units = 'mole'  # note: different from what I set on species
    assert doc.model.unit_definitions['mole']

    I_cytosol = factory.ionic_strength(compartment=compartment)  # compartment has volume units, model substance units
    assert doc.model.unit_definitions.get(I_cytosol.units).id == 'mole_per_litre'

    # species-specific
    species = doc.model.species['A']
    mu0_A = factory.standard_chemical_potential(species=species, units='kilojoule_per_mole')
    assert mu0_A == doc.model.parameters['mu0_A']
    mu_A = factory.chemical_potential(species=species, units='kilojoule_per_mole')
    assert mu_A == doc.model.parameters['mu_A']

    # reaction-specific
    reaction = doc.model.reactions['rxn']
    dmu0_rxn = factory.standard_gibbs_energy_of_reaction(reaction=reaction, units='kilojoule_per_mole')
    assert dmu0_rxn == doc.model.parameters['rxn_dmu0']
    rxn_keq = factory.equilibrium_constant(reaction)
    assert rxn_keq == doc.model.parameters['rxn_keq']

    # kinetic reaction parameters
    # no units given, cannot be inferred from model because not set
    # kf_rxn = factory.forward_rate_constant(reaction)  # error as should; cannot infer time units

    # set model units, else need to be explicitly passed
    doc.model.time_units = 'second'
    rxn_kf = factory.forward_rate_constant(reaction)
    assert rxn_kf.units == 'per_second'
    rxn_kr = factory.reverse_rate_constant(reaction)
    assert rxn_kr.units == 'per_second'

    rxn_kcatf = factory.substrate_catalytic_rate_constant(reaction)
    assert rxn_kcatf.units == 'per_second'
    rxn_kcatr = factory.product_catalytic_rate_constant(reaction)
    assert rxn_kcatr.units == 'per_second'

    # TODO: infer units from species involved instead of model when available
    rxn_vmaxf = factory.forward_maximal_velocity(reaction)
    assert rxn_vmaxf.units == 'per_mole_per_second'
    rxn_vmaxr = factory.reverse_maximal_velocity(reaction)
    assert rxn_vmaxr.units == 'per_mole_per_second'

    # reaction-species parameters
    rxn_km_A = factory.michaelis_constant(reaction, species)
    assert rxn_km_A.units == species.substance_units
    rxn_ka_A = factory.activation_constant(reaction, species)
    assert rxn_ka_A.units == species.substance_units
    rxn_ki_A = factory.inhibitory_constant(reaction, species)
    assert rxn_ki_A.units == species.substance_units

    rxn_b_A = factory.basel_rate_constant(reaction, species)
    assert rxn_b_A.units == 'millimole_per_second'  # species units and model time units

    rxn_w_A = factory.regulation_number(reaction, species)
    assert rxn_w_A.units == 'dimensionless'

    # now creating local reaction parameters
    factory.create_local_parameters = True
    kf = factory.forward_rate_constant(reaction)
    assert reaction.local_parameters['kf'] == kf
    kr = factory.reverse_rate_constant(reaction)
    assert reaction.local_parameters['kr'] == kr
