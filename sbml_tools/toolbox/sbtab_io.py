import os
from typing import Union
import libsbml
from sbtab import sbml2sbtab, sbtab2sbml, SBtab, validatorSBtab
from sbml_tools import settings
from sbml_tools.toolbox import sbml_io
from sbml_tools.toolbox.validator import validate_sbml


def read_sbtab(sbtab: Union[str, SBtab.SBtabDocument]) -> SBtab.SBtabDocument:

    if isinstance(sbtab, SBtab.SBtabDocument):
        return sbtab

    if os.path.isfile(sbtab):
        file_name = os.path.split(sbtab)[-1]
        with open(sbtab, 'r') as file:
            sbtab = SBtab.SBtabDocument(file_name, file.read(), file_name)
    else:
        sbtab = SBtab.SBtabDocument('', sbtab, '')

    return sbtab


def validate_sbtab(sbtab: SBtab.SBtabDocument):
    sbtab_validator = validatorSBtab.ValidateDocument(sbtab)
    warnings = sbtab_validator.validate_document()
    for warning in warnings:
        print(warning)


class Converter:

    @staticmethod
    def to_sbml(sbtab: Union[str, SBtab.SBtabDocument], level: int = 3, version: int = 2) -> libsbml.SBMLDocument:
        sbtab = read_sbtab(sbtab)
        converter = sbtab2sbml.SBtabDocument(sbtab)
        sbml, warnings = converter.convert_to_sbml(''.join(map(str, [level, version])))
        for warning in warnings:
            print(warning)
        return sbml_io.read_sbml(sbml)

    @staticmethod
    def to_sbtab(sbml: Union[str, libsbml.SBMLDocument]) -> SBtab.SBtabDocument:
        if isinstance(sbml, str) and os.path.isfile(sbml):
            file_name = os.path.split(sbml)[-1]
        else:
            file_name = 'dummy.xml'
        doc = sbml_io.read_sbml(sbml)
        converter = sbml2sbtab.SBMLDocument(doc.model, file_name)
        sbtab, warnings = converter.convert_to_sbtab()
        for warning in warnings:
            print(warning)
        return sbtab


def test():

    SBTAB_FNAME = os.path.join(settings.SBTAB_DIR, 'input.tsv')
    sbtab = read_sbtab(SBTAB_FNAME)
    doc = Converter.to_sbml(sbtab)

    validate_sbml(doc, check_warnings=False)

    sbtab2 = Converter.to_sbtab(doc)
    validate_sbtab(sbtab2)
