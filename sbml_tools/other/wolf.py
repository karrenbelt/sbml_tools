
import os
import re
from bidict import bidict
from sbml_tools import settings
from sbml_tools.toolbox.brenda import Parser
from sbml_tools.toolbox.sbtab_io import Converter, read_sbtab
from sbml_tools.toolbox.annotator import IdentifierAnnotator
from sbml_tools.toolbox.ncbi.taxonomy import Taxonomy
from sbml_tools.wrappers.annotations import MIRIAM, URL_TO_MIRIAM_IDX

# loading this large file, together with the large parser (BRENDA) yields heap space issues
# taxonomy = Taxonomy()


SBTAB_MAP = bidict(
    id='ID',
    identifiers='Identifiers',
    option='Option',
    value='Value',
    comment='Comment',
    reaction_formula='ReactionFormula',
    ec_code='Identifiers:ec-code',
    # bigg='Identifiers:',
    # rhea='Identifiers:',
    # metanetx='Identifiers:',
    # biocyc='Identifiers:',
    )


class Configuration:

    def __init__(self, provenance: str, drop_mutants: bool = False):
        self.provenance = provenance
        self.drop_mutants = drop_mutants


def test():  # from sbtab --> dataframes --> sbtab
    # configuration is optional
    # species and reactions are a must

    parser = Parser.read_from_cache()  # TODO: doesn't work without running parser.py

    SBTAB_IN_FNAME = os.path.join(settings.SBTAB_DIR, 'input.tsv')
    sbtab_doc = read_sbtab(SBTAB_IN_FNAME)

    dfs = dict()
    for sbtab in sbtab_doc.sbtabs:
        dfs[sbtab.table_id.lower()] = sbtab.to_data_frame().rename(columns=SBTAB_MAP.inverse)

    if 'configuration' in dfs:
        configuration = Configuration(**dfs['configuration'].set_index('option')['value'].to_dict())
    else:
        configuration = Configuration(provenance='BRENDA')

    species = dfs['compound'].set_index('id').to_dict(orient='index')
    reactions = dfs['reaction'].set_index('id').to_dict(orient='index')

    # make a pattern to match all species identifiers from strings
    REACTION_FORMULA_PATTERN = re.compile(r"(?=\b("+'|'.join(species)+r")\b)")
    ARROW_PATTERN = re.compile(r"\s*(?:<=>|-->|<--)\s*")  #

    proteins = dict([(k, []) for k in reactions])  # dict.fromkeys(reactions, [])  # <-- creates aliases
    for rxn_id, info in reactions.items():

        reaction_formula = info['reaction_formula']  # must exist
        reactants, products = list(map(REACTION_FORMULA_PATTERN.findall, ARROW_PATTERN.split(reaction_formula)))

        ec_code = info.get('ec_code')
        ec_number = parser.ec_data.get(ec_code)
        if ec_number is None:
            continue

        for protein in ec_number.proteins.values():
            if protein.species == 'Escherichia coli':
                proteins[rxn_id].append(protein)
                1/0
                # TODO: some comments are not parsed properly
                ## - only comment refering to the protein id should be there
                ## - some references are not there that don't link properly
                ## e.g. '2.7.1.11', protein_id 6, KM value has ref 102, but species doesn't


def test2():   # from sbtab --> sbml --> dataframes --> sbtab
    # rename to BiGG
    SBTAB_IN_FNAME = os.path.join(settings.SBTAB_DIR, 'input.tsv')
    doc = Converter.to_sbml(SBTAB_IN_FNAME)
    fbp = doc.model.species.get('fbp')
    fbp.id = 'fdp'

    # some warnings are thrown
    annotator = IdentifierAnnotator(doc)
    annotator.annotate_reactions_through_BiGG(case_sensitive=False)
    annotator.annotate_species_through_BiGG()

    parser = Parser.read_from_cache()  # TODO: doesn't work without running parser.py

    for rxn_id, annotated_identifiers in annotator.annotated_reaction_identifiers.items():
        ec_number_id = None
        for anno in annotated_identifiers:
            url, identifier = anno.rsplit('/', 1)
            miriam_ns = MIRIAM.get(URL_TO_MIRIAM_IDX.get(url+'/'), {})
            if miriam_ns is None:
                continue  # unknown identifier namespace
            pattern = miriam_ns.get('pattern')
            match = re.match(pattern, identifier)
            if not match:
                raise ValueError(f"invalid identfier pattern for {miriam_ns['name']}")

            # if 'ec-code' in anno:
            #     ec_number_id =
        if not ec_number_id:
            print('cannot annotate without EC number')
            continue
        ec_number = parser.ec_data[ec_number_id]



    SBTAB_OUT_FNAME = os.path.join(settings.SBTAB_DIR, 'output.tsv')