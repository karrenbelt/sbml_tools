import os
import collections
import numpy as np
import pandas as pd
import cobra
# from pandas import ExcelWriter, ExcelFile
from sbml_tools import settings, utils
from sbml_tools.wrappers import SBMLDocument
from sbml_tools.toolbox.sbml_io import read_sbml

"""
extracting central carbon metabolism enzymes for Peter's proteomics method
"""

# this model has subsystem assignment, but not species annotations
filepath = settings.IJO1366_subsystems
subsystem_doc = SBMLDocument(read_sbml(filepath))

# this model has species annotation in the form of inchi keys
doc = SBMLDocument(read_sbml(settings.IJO1366))


CENTRAL_CARBON_METABOLISM = [
    'Glycolysis/Gluconeogenesis',
    'Pentose Phosphate Pathway',
    'Citric Acid Cycle',
    'Oxidative Phosphorylation',
    'Glyoxylate Metabolism',
    'Anaplerotic Reactions',
    'Methylglyoxal Metabolism',
    'Pyruvate Metabolism',
    ]


data = []
plugin = subsystem_doc.model.packages[1]
for i in range(plugin.getNumGroups()):
    # pathway = dict()
    group = plugin.getGroup(i)

    for member in group.members:
        reaction = doc.model.reactions.get(member.id_ref)
        reaction_plugin = reaction.wrapped.getPlugin(0)
        gpa = reaction_plugin.getGeneProductAssociation()
        if gpa:
            genes = [x.getGeneProduct()[2:] for x in gpa.getListOfAllElements() if x.__class__.__name__ == 'GeneProductRef']
            data.append([group.name, reaction.id, reaction.name, genes])

df = pd.DataFrame(data, columns=['subsystem', 'reaction_id', 'reaction_name', 'genes'])
# df = df[df['subsystem'].isin(CENTRAL_CARBON_METABOLISM)]

df.to_csv('genes_ecoli.csv', index=False)




import cobra

#
# model = cobra.io.read_sbml_model(settings.IJO1366_subsystems)
#
# for reaction in model.reactions:
#     if reaction.subsystem:
#         1/0
