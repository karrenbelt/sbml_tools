
import os
from pprint import pprint
import itertools

import escher
from sbml_tools import settings, utils
from sbml_tools.toolbox.cobra_wrapper import CobraWrapper

LYSINE_FIGURE_DIR = os.path.join(settings.FIGURE_DIR, 'lysine')
if not os.path.isdir(LYSINE_FIGURE_DIR):
    os.mkdir(LYSINE_FIGURE_DIR)

# references
#
# 1. Widespread bacterial lysine degradation proceeding via glutarate and L-2-hydroxyglutarate
#    Lysine degradation in E.coli
#    DOI: https://doi.org/10.1038/s41467-018-07563-6
#
# 2. The lysP gene encodes the lysine-specific permease:
#    Lysine import overexpressed in anaerobic acidic conditions
#    DOI: 10.1128/jb.174.10.3242-3249.1992
#
# 3. Identification of Genes Required for Growth of Escherichia coli MG1655 at Moderately Low pH
#    Lysine decarboxylation as a mechanism to cope with pH stress
#    DOI: 10.3389/fmicb.2016.01672

#
# L-2-hydroxyglutarate == (S)-2-hydroxyglutarate
# I believe this is the following in iML1515:
# AHGDx (EC 1.1.99.2)
# (S)-2-hydroxyglutarate + acceptor ⇌ 2-oxoglutarate + reduced acceptor
#


# TODO: use Memote to compare the models: https://github.com/opencobra/memote

iML1515 = CobraWrapper(settings.IML1515) # contains
model = iML1515.copy()

### renaming groups, which are pathways, since these don't need to be pathways
### introducing the convention that pathways ids should start with
# for group in model.groups:
#     group.id = 'pathway'

sbo_table = utils.get_sbo_table()
sbo_annotation = utils.get_sbo_annotation()

bigg_amino_acid_exchange_ids = [
    'EX_ala__D_e',
    'EX_arg__L_e',
    'EX_asn__L_e',
    'EX_asp__L_e',
    'EX_cys__L_e',
    'EX_gln__L_e',
    'EX_glu__L_e',
    'EX_gly_e',
    'EX_his__L_e',
    'EX_ile__L_e',
    'EX_leu__L_e',
    'EX_lys__L_e',
    'EX_met__L_e',
    'EX_phe__L_e',
    'EX_pro__L_e',
    'EX_ser__L_e',
    'EX_thr__L_e',
    'EX_trp__L_e',
    'EX_tyr__L_e',
    'EX_val__L_e',
    ]

cofactors = [
    "h_c", "h_e",
    "h2o_c", "h2o_e"
    "o2_c", "o2_e",
    "nh4_c", "nh4_e",
    "pi_c", "ppi_c", "pppi_c",
    "k_c",
    "nad_c", "nadh_c",
    "nadp_c", "nadph_c",
    "fad_c", "fadh2_c",
    "n2o_c",
    "amp_c", "adp_c", "atp_c",
    "coa_c", "accoa_c",
    "ACP_c"
    "q2h2"
    ]

### Initial concentrations of amino acids in mM
aa_x0 = {
    'l-Histidine': 1.102732,
    'l-Isoleucine': 1.830739,
    'l-Leucine': 7.628078,
    'l-Methionine': 0.838639,
    'l-Valine': 5.97887,
    'l-Arginine': 4.135277,
    'l-Cysteine': 4.131557,
    'l-Glutamic acid': 4.080157,
    'l-Phenylalanine': 2.423083,
    'l-Proline': 6.083606,
    'l-Asparagine': 3.786344,
    'l-Aspartic acid': 0.375834,
    'l-Glutamine': 4.107644,
    'l-Serine': 4.759974,
    'l-Threonine': 4.199625,
    'l-Alanine': 4.491976,
    'Glycine': 3.998293,
    'l-Lysine': 3.422184,
    'l-Tryptophan': 0.97996
    }

oxygen_exchange_id = 'EX_o2_e'

# define media compositions - m9 without carbon is the starting medium of iJO1366?
M9 = model.medium # may or may not include glycose at -10 already, depending on the model

# S2hglut_c
l2hg = model.metabolites.get_by_id('S2hglut_c')



##################################
### Lysine degradation pathway ### as per https://doi.org/10.1038/s41467-018-07563-6
##################################
### gene knockouts and uniprot information,
# ldcC: b0186, ldcH, ABE-0000633
#       reaction (P52095): H+ + L-lysine = cadaverine + CO2
#
# cadA: b4131, ldcI, ABE-0013526
#       reaction (P0A9H3): H+ + L_lysine = cadaverine + CO2
#       Plays a role in pH homeostasis by consuming protons and neutralizing the acidic by-products of carbohydrate fermentation.
#
# csiD: b2659, ygaT, glaH, P76621, EG13523, ABE-0008754
#       reaction (P76621): 2-oxoglutarate + glutarate + O2 = (S)-2-hydroxyglutarate + CO2 + succinate
#       Acts as an alpha-ketoglutarate-dependent dioxygenase catalyzing hydroxylation of glutarate (GA)
#       to L-2-hydroxyglutarate (L2HG) in the stationary phase of E.coli. Functions in a L-lysine degradation pathway
#       that proceeds via cadaverine, glutarate and L-2-hydroxyglutarate. Other dicarboxylic acids
#       (oxalate, malonate, succinate, adipate, and pimelate) are not substrates for this enzyme.
#
# lhgo: b2660, ygaF, ABE-0008756
#       reaction (P37339): (S)-2-hydroxyglutarate + a quinone = 2-oxoglutarate + a quinol
#       Catalyzes the dehydrogenation of L-2-hydroxyglutarate (L2HG) to alpha-ketoglutarate and couples to the
#       respiratory chain by feeding electrons from the reaction into the membrane quinone pool.
#       Functions in a L-lysine degradation pathway that proceeds via cadaverine, glutarate and L-2-hydroxyglutarate.

pathway_genes = {}
pathway_gene_ids = ('ldcC', 'cadA', 'patA', 'gabP', 'gabT', 'gabD', 'csiD', 'lhgO')
for gene in pathway_gene_ids:
    result = model.get_gene_by_annotation(gene)
    if result:
        if len(result) > 1:
            raise ValueError(f'multiple instances found:\n{result}')
        pathway_genes[gene] = result[0]
    else:
        pathway_genes[gene] = None
        print(f'no gene found for {gene}')


pathway_genes = list(filter(None, pathway_genes.values()))
reactions_of_interest = list(itertools.chain(*[g.reactions for g in pathway_genes]))


### add exchange reaction for l2hg
model.add_or_update_metabolite(metabolite='l2hg_e')
EX_l2hg =  model.add_or_update_reaction(reaction='EX_l2hg', products = {'l2hg_e': 1.0},
                               lower_bound = -1000,  upper_bound = 1000, )
l2hgt = model.add_or_update_reaction(reaction='l2hgt', reactants = {'l2hg_e': 1.0}, products = {'l2hg_c': 1.0},
                                     lower_bound = -1000,  upper_bound = 1000, )

### scan some rich media
rich = {**{m: 10 for m in bigg_amino_acid_exchange_ids}, **model.medium}
rich_without_02 = rich.copy()
rich_without_02.update({'EX_o2_e': 0, 'EX_l2hg':10})

with model as m:
    m.medium = rich_without_02 # {**m.medium, **{'EX_o2_e': 0}}
    fluxes = m.optimize().fluxes
    # [[r.id for r in reactions_of_interest]]
    print(fluxes['EX_l2hg'])

### remove oxygen consumption from the pathway
SHGO = model.get_by_id('SHGO')
SHGO_original_metabolites = SHGO.metabolites
no_ox = {met:coeff for met, coeff in SHGO.metabolites.items() if not met.id == 'o2_c'}
SHGO._metabolites = no_ox

### need to have ran model.optimize first
model.shadow_prices_medium_components()


### generate dict of associated reactions for inspection
### this is relevant, as we want to know if other pathways are affected
pprint({gene.id : {r.id: r.reaction for r in gene.reactions} for gene in pathway_genes})

## assess biomass yield on single gene knockout strains
ko_growth = model.single_gene_deletion(gene_list=pathway_genes)

### we actually want to know if fluxes are different, here we define the conditions we want to screen
supplement1 = {'aerobic': {'EX_o2_e': 1000.0},
               'anaerobic': {'EX_o2_e': 0.0}}

supplement2 = {
    'glucose': {'EX_glc__D_e': 10.0},
    'glucose_lysine': {'EX_glc__D_e': 10.0, 'EX_lys__L_e': 10},
    'glucose_cas': {m: 10 for m in bigg_amino_acid_exchange_ids},
    'glucose_cas_no_lysine': {m: 10 for m in set(bigg_amino_acid_exchange_ids).difference(['EX_lys__L_e'])},
    'glucose_cas_lysine': {**{m: 10 for m in bigg_amino_acid_exchange_ids}, **{'EX_lys__L_e': 100}},
    }

### beware, supplements overwrite the base_medium (M9), and also each other (last one overwrites former if duplicates)
supplements = [supplement1, supplement2]

### overview of how the M9 is supplemented, useful for inspection
supplement_table = model.nutrient_combinations_table(supplements) # + [{'M9': M9}]

### perform FBA on a combination of M9 supplementation and different gene knockouts
screen = model.screen(base_medium=M9, supplements=supplements, gene_knock_outs=pathway_genes)
differences, groups = model.group_screen(screen)

## we would like to see if lysine uptake is increased (negative values are uptake for exchange reactions)
print(differences.loc['EX_lys__L_e'])

model.iterative_subsystem_flux_enrichment(differences['group1'] - differences['group3'],
                                     filepath=os.path.join(LYSINE_FIGURE_DIR, 'test_enrichment.pdf'),
                                     show=True)


builder = escher.Builder(
    map_name = 'iJO1366.Central metabolism',
    model = model,
    reaction_data = differences['group1'] - differences['group3'],
    metabolite_data = None,
    )

builder.save_html(os.path.join(LYSINE_FIGURE_DIR, f'test_flux.html'))

### network drawing - this works for small models only
# model = CobraWrapper(settings.CORE_MODEL)
# exclude = cofactors + model.exchange_ids + ['h_c', 'h_e'] + model.biomass_ids + model.exchange_ids
# graph = model.metabolic_network_graph(exclude=exclude)
