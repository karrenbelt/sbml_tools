
import os
import pandas as pd
import matplotlib.pyplot as plt
from sbml_tools import settings
from sbml_tools.toolbox.cobra_wrapper import CobraWrapper
from sbml_tools.toolbox.brenda.owl import get_chebi


CHEBI = get_chebi()

LIPIDS_FIGURE_DIR = os.path.join(settings.FIGURE_DIR, 'lipids')
if not os.path.isdir(LIPIDS_FIGURE_DIR):
    os.mkdir(LIPIDS_FIGURE_DIR)


LIPID_GENES_FNAME = os.path.join(settings.DATA_DIR, 'Jeucken2019_celrep_6253_mmc2.csv')
if not os.path.isfile(LIPID_GENES_FNAME):
    reference = """
    1. Jeucken A, Molenaar MR, van de Lest CHA, Jansen JWA, Helms JB, Brouwers JF. A
       Comprehensive Functional Characterization of Escherichia coli Lipid Genes. Cell
       Rep. 2019 Apr 30;27(5):1597-1606.e2. doi: 10.1016/j.celrep.2019.04.018. PubMed
       PMID: 31042483.
    """
    raise FileNotFoundError(f'supplementary table S1 file not found from:\n{reference}')

# white spaces!
table_s1 = pd.read_csv(LIPID_GENES_FNAME).applymap(lambda x: str.strip(x) if isinstance(x, str) else x)
lipid_genes = table_s1.set_index('kegg entry')['gene']

# parsed by Philipp from the sdf file  (https://lipidmaps.org/data/structure/download.php)
LIPID_MAPS_FNAME = os.path.join(settings.DATA_DIR, 'LM_20191002_PW.csv')
lipid_maps = pd.read_csv(LIPID_MAPS_FNAME)

# Brennsteiner Vincenth, Philipp Warmer
# NOTES:
# - which reactions are lipid annotated and which are not
# - identify enzymatic “holes” in the network
# - how many of the reaction without enzymes are lipid associated
# - is there enrichment?

# session 2
# - how many can be found in the model
# - from these metabolite get reactions, from reactions get genes
# - is there over representation in the lipid category, is there in the subcategory
# - difference unknown overall vs lipid specific
# membrane associated vs the rest - more unknowns?


lipid_maps['SYSTEMATIC_NAME']
lipid_categories = lipid_maps['CATEGORY'].unique()
lipid_subcategories = lipid_maps['MAIN_CLASS'].unique()



iML1515 = CobraWrapper(settings.IML1515)
model = iML1515.copy()

# select the relevant groups
group_table = model.group_table
membrane_lipid_metabolism = model.get_by_id('g12')
cell_envelope_biosynthesis = model.get_by_id('g14')
glycerophospholipid_metabolism = model.get_by_id('g30')
transport_inner_membrane = model.get_by_id('g33')
transport_outer_membrane = model.get_by_id('g34')

unknown_globally = len(model.reactions_without_genes) / len(model.reactions)

gene_absence_enrichment = model.subsystem_enrichment(model.reactions_without_genes)
# gene_absence_enrichment[['name', 'group_size', 'successes']]
gene_absence_enrichment['fraction'] = gene_absence_enrichment['successes'] / gene_absence_enrichment['group_size']
gene_absence_enrichment.set_index('name')['fraction'].sort_values(ascending=False).plot.bar()
plt.show()

unknown_phospho = gene_absence_enrichment['successes']['g30'] / gene_absence_enrichment['group_size']['g30']




# reactions = [reaction for reaction in glycerophospholipid_metabolism.members if not reaction.genes]
s = set()
for reaction in glycerophospholipid_metabolism.members:
    for gene in reaction.genes:
        s.add(gene)

# genes queried by id return fewer than those queried through annotation, which is problematic
genes_by_id = []
genes_not_in_model = []
for gene_id in lipid_genes.index:
    gene = model.get_by_id(gene_id)
    if gene is None:
        genes_not_in_model.append(gene_id)
    else:
        genes_by_id.append(gene)


genes_found_by_id = pd.Series({gene.id: gene.annotation['refseq_name'] for gene in genes_by_id})
# lipid_genes.to_frame().merge(genes_found_by_id.to_frame(), left_index=True, right_index=True)

# lipid_genes[genes_not_in_model]
df = table_s1.set_index('kegg entry').loc[genes_not_in_model]['protfunction']


some_lipids = model.metabolite_table.name[model.metabolite_table.name.str.contains('n-C')]

lipid_reaction_without_genes = set()
for lipid in some_lipids.index:
    metabolite = model.metabolites.get_by_id(lipid)
    for reaction in metabolite.reactions:
        if not reaction.gene_reaction_rule:
            lipid_reaction_without_genes.add(reaction)