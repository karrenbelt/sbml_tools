import os
import collections
import numpy as np
import pandas as pd
import cobra
# from pandas import ExcelWriter, ExcelFile
from sbml_tools import settings, utils
from sbml_tools.wrappers import SBMLDocument
from sbml_tools.toolbox.sbml_io import read_sbml

# read in a copy of the google sheet as excel, append identifiers
filepath = os.path.join(settings.DATA_DIR, 'monster_mix.xlsx')
monster_mix = pd.read_excel(filepath).dropna(how='all').dropna(how='all', axis=1)
excel_reader = pd.ExcelFile(filepath)  # this is needed to write into the existing file
excel_writer = pd.ExcelWriter(filepath)


# TODO: inchi keys we only need the first part (e.g. xxxx-yyyy-z -> xxxx)

# grouping of groups
CENTRAL_CARBON_METABOLISM = [
    'Glycolysis/Gluconeogenesis',
    'Pentose Phosphate Pathway',
    'Citric Acid Cycle',
    'Oxidative Phosphorylation',
    'Glyoxylate Metabolism',
    'Anaplerotic Reactions',
    'Methylglyoxal Metabolism',
    'Pyruvate Metabolism',
    ]

AMINO_ACIDS = [
    'Arginine and Proline Metabolism',
    'Tyrosine, Tryptophan, and Phenylalanine Metabolism',
    'Valine, Leucine, and Isoleucine Metabolism',
    'Cysteine Metabolism',
    'Methionine Metabolism',
    'Alanine and Aspartate Metabolism',
    'Threonine and Lysine Metabolism',
    'Histidine Metabolism',
    'Glycine and Serine Metabolism',
    'Glutamate Metabolism',
    ]

MEMBRANE = [
    'Transport, Inner Membrane',
    'Transport, Outer Membrane Porin',
    'Transport, Outer Membrane',
    'Cell Envelope Biosynthesis',
    'Membrane Lipid Metabolism',
    ]


OTHER = [
    'Nucleotide Salvage Pathway',
    'Glycerophospholipid Metabolism',
    'Alternate Carbon Metabolism',
    'Cofactor and Prosthetic Group Biosynthesis',
    'Murein Recycling',
    'Nitrogen Metabolism',
    'Lipopolysaccharide Biosynthesis / Recycling',
    'Unassigned',
    'Purine and Pyrimidine Biosynthesis',
    'Inorganic Ion Transport and Metabolism',
    'tRNA Charging',
    'Biomass and maintenance functions',
    'Folate Metabolism',
    'Murein Biosynthesis',
    ]


# NOTE: other identifiers can be obtained from the mapping file (but not inchi)
# available: 'reactome', 'chebi', 'seed.compound', 'kegg.compound', 'hmdb', 'biocyc',
#            'metanetx.chemical', 'lipidmaps', 'kegg.drug'
INCHI_PREFIX = 'http://identifiers.org/inchi_key/'
BIGG_PREFIX = 'http://identifiers.org/bigg.metabolite/'

# this mapping file does NOT include inchi keys, these are obtained from the model
mapping = utils.get_bigg_metabolite_identifier_mapping()

# this model has subsystem assignment, but not species annotations
filepath = settings.IJO1366_subsystems
subsystem_doc = SBMLDocument(read_sbml(filepath))

# this model has species annotation in the form of inchi keys
doc = SBMLDocument(read_sbml(settings.IJO1366))


def get_species_annotation():
    """
    model contains inchi keys, the mapping does not, hence we need this
    """
    d = collections.defaultdict(dict)
    for s in doc.model.species:
        d[s.id]['name'] = s.name
        for cv_term in s.cv_terms:
            for resource in cv_term.resources:
                if resource.startswith(INCHI_PREFIX):
                    d[s.id]['inchi_key'] = resource.split(INCHI_PREFIX)[-1]
                if resource.startswith(BIGG_PREFIX):
                    d[s.id]['bigg'] = resource.split(BIGG_PREFIX)[-1]

        if 'inchi_key' not in d[s.id]:
            d[s.id]['inchi_key'] = None

    df = pd.DataFrame.from_dict(d, orient='index')
    # df = df.apply(lambda x: x.astype(str).str.lower())
    # df.index = df.index.str.lower()
    return df


def all_model_group_names():
    plugin = subsystem_doc.model.packages[1]
    group_names = [plugin.getGroup(i).name for i in range(plugin.getNumGroups())]
    return group_names



species_annotation = get_species_annotation()


def get_inchi_per_reaction_per_subsystem():
    groups = dict()
    plugin = subsystem_doc.model.packages[1]
    for i in range(plugin.getNumGroups()):
        pathway = dict()
        group = plugin.getGroup(i)

        for member in group.members:
            reaction = doc.model.reactions.get(member.id_ref)
            species = [doc.model.species.get(s_ref.species) for s_ref in reaction.reactants + reaction.products]

            d = dict()
            for s in species:
                inchi_key = None
                for cv_term in s.cv_terms:
                    for resource in cv_term.resources:
                        if resource.startswith(INCHI_PREFIX):
                            inchi_key = resource.split(INCHI_PREFIX)[-1]
                bigg = s.id[2:-2]
                d[bigg] = inchi_key

            pathway[reaction.id] = [reaction.formula, d]
        groups[group.name] = pathway
    return groups


def get_inchi_per_subsytem(subsystems):
    groups = get_inchi_per_reaction_per_subsystem()

    pathway_inchis = dict()
    for name in subsystems:
        pathway_inchis[name] = dict()
        for r_id, (formula, inchi_key) in groups[name].items():
            pathway_inchis[name].update(inchi_key)
    return pd.DataFrame(pathway_inchis)


# inchi_keys_per_subsystem = get_inchi_per_subsytem(CENTRAL_CARBON_METABOLISM)

# this is the from the old list, need to find the bigg ids and inchi keys
# metabolite_names = monster_mix['Name']

# manual mapping of bigg annotation to compounds (77 missing)
MANUAL_MAPPING = {
    'Aspartate': 'asp__L',
    'L-Glutamic acid': 'glu__L',
    'Tyrosine': 'tyr__L',
    'Phenylalanine': 'phe__L',
    'Tryptophane': 'trp__L',
    'Arginine': 'arg__L',
    'Histidine': 'his__L',
    'Lysine': 'lys__L',
    'Glutamine': 'gln__L',
    'Asparagine': 'asn__L',
    'Serine': 'ser__L',
    'Threonine': 'thr__L',
    'Cysteine': 'cys__L',
    'Alanine': 'ala__L',
    'Isoleucine': 'ile__L',
    'Leucine': 'leu__L',
    'Methionine': 'met__L',
    'Proline': 'pro__L',
    'Valine': 'val__L',
    'Adenosine diphosphate': 'adp',
    'Guanosine diphosphate': 'gdp',
    '2-Oxoglutarate / alphaKetoGlutarate': 'akg',
    'Glucuronate': 'glcur',
    'Glycerate': 'glyc__R',  # glyc__S exists in BiGG, but not in the iJO1366
    'Lactate': 'lac__L',
    'cis-Aconitase': 'acon_C',
    'methyl-Citrate': '2mcit',
    'iso-Citrate': 'icit',
    'N-acetyl-glucosamine': 'acgam',
    'Ribose': 'rib__D',
    'Xylose': 'xyl__D',
    'Mannose': 'man',  # D-Mannose
    'Galactose': 'gal',  # D-Galactose
    'Glucose': 'glc__D',
    'Myo inositol': 'inost',  # Myo-Inositol
    'Mannitol': 'mnl',  # D-Mannitol
    'Fructose': 'fru',  # D-Fructose
    'Maltose': 'malt',  # Maltose C12H22O11
    'Sucrose': 'sucr',  # Sucrose C12H22O11
    'Fructose 1,6-biphosphate': 'fdp',  # D-Fructose 1,6-bisphosphate
    'Dihydroxyacetonephosphate': 'dhap',
    '3-Phosphoglycerate': '3pg',
    '2-Phosphoglycerate': '2pg',
    'Erythrose 4-phosphate': 'e4p',
    'Ribose-1-phosphate': 'r1p',
    'Ribose 5-phosphate': 'r5p',
    'Xylulose 5-phosphate': 'xu5p__D',
    'Ribulose 5-phosphate': 'ru5p__D',
    '6-phosphogluconate': '6pgc',
    'Fructose 1-phosphat': 'f1p',
    'Fructose 6-phosphate': 'f6p',
    'Glucose 1-phosphate': 'g1p',
    'Glucose 6-phosphate': 'g6p',
    'Glucosamine-6-phosphate': 'gam6p',
    'Pantothenate': 'pnto__R',
    '5-phospho-d-ribose 1-diphosphate': 'prpp',  # 5-Phospho-alpha-D-ribose 1-diphosphate
    'homocysteine': 'hcys__L',
    'glutathione red': 'gthrd',
    'glutathione ox': 'gthox',
    'galactose-1-phosphate': 'gal1p',
    'nad+': 'nad',
    'nadp+': 'nadp',
    'UDPG': 'udpg',  # UDPglucose
    '2-dehydro-3-deoxy-D-gluconate 6-phosphate': '2ddg6p',
    'acetyl phosphate': 'actp',
    'Adenosine monophosphate': 'amp',  # AMP C10H12N5O7P
    'Guanosine monophosphate': 'gmp',  # GMP C10H12N5O8P
    'Adenosine triphosphate': 'atp',  # ATP C10H12N5O13P3
    'Guanosine triphosphate*': 'gtp',  # GTP C10H12N5O14P3
    'cyclo-Adenosin monophosphate': 'camp',  # CAMP C10H11N5O6P  (3,5-camp)
    'cyclo-Guanosine monophosphate': '35cgmp',  #3',5'-Cyclic GMP, also exists:  23cgmp	2',3'-Cyclic GMP
    'Glycerol-phosphate': 'glyc3p',  # closest: 'glyc1p', 'glyc2p', 'glyc3p', -> should be glyc3p
    'CMP': 'cmp',
    'CDP': 'cdp',
    'CTP': 'ctp',
    'UMP': 'ump',
    'UDP': 'udp',
    'UTP': 'utp',
    'IMP': 'imp',
    '5-phospho-D-ribose 1-diphosphate': 'prpp',  # 5-Phospho-alpha-D-ribose 1-diphosphate
    'FAD': 'fad',
    'cystathione': 'cyst__L',
    }


missing = {  # TODO: add the bigg identifiers to the list, even if not in iJO1366
    'Hydroxy-proline': '',  # not found in BiGG
    'Cystine': 'cystine__L',  # L-cystine, found on BiGG but not in the model
    'alpha-amino-butyricacid': '',  # closest: sobuta	Isobutyric acid
    'gamma-amino-butyricacid': '',  # can we even distinguish alpha and gamma?
    '2,3 BisPhosphoGlycerate': '23dpg',  # is this even in e. coli?  should be 13dpg?
    '2-phospho-D-glycerate': '2pg',  # duplicate? chris: yes
    'UDP-GlcNac': '',  # not found on BiGG
    }


l = list()
for name in metabolite_names:
    if name in missing:
        l.append([name, None, None])
        continue
    elif name in species_annotation.index:  # never happens
        # print('index:', name)
        hits = species_annotation.loc[name]
    elif name in species_annotation['name'].values:
        # print('name:', name)
        hits = species_annotation[species_annotation['name'] == name]
    elif name in species_annotation['bigg'].values:
        # print('bigg:', name)
        hits = species_annotation[species_annotation['bigg'] == name]
    elif name in MANUAL_MAPPING:
        hits = species_annotation[species_annotation['bigg'] == MANUAL_MAPPING[name]]
        if hits.empty:
            raise ValueError(f'no hits in the model annotation for: {name}')
    else:
        if isinstance(name, float) and np.isnan(name):
            continue
        raise ValueError(f'not mappable: {name}')

    assert all(x == hits['bigg'].values[0] for x in hits['bigg'].values)
    assert all(x == hits['inchi_key'].values[0] for x in hits['inchi_key'].values)
    l.append([name, hits.iloc[0]['bigg'], hits.iloc[0]['inchi_key']])
    # species = doc.model.species.get(name.lower())


df = pd.DataFrame(l, columns=['name', 'bigg', 'inchi_key']).set_index('name')
# filepath = 'bigg_and_inchi.csv'
# df.to_csv(filepath)


# from ecocyc:
inchi_from_ecocyc = {
    'Hydroxy-proline': 'PMMYEEVYMWASQN-DMTCNVIQSA-N',  # trans-4-hydroxy-L-proline
    'Cystine': 'LEVWYRKDKASIDU-IMJSIDKUSA-N',  # L-cystine
    '2,3 BisPhosphoGlycerate': 'XOHUEYCVLUUEJJ-UWTATZPHSA-I',  # 2,3-diphospho-D-glycerate TODO should be 13dpg?
    # 'UDP-GlcNac': '',
    }


# do it iteratively to keep the order
l = []
for i, data in monster_mix.iterrows():
    name = data['Name']
    if name not in df.index:
        l.append([name, None, None, None, None])
    else:
        bigg, inchi_key = df.loc[name]
        kegg = mapping.loc[bigg, 'kegg.compound'] if bigg else None
        metanetx = mapping.loc[bigg, 'metanetx.chemical'][0] if bigg else None
        l.append([name, bigg, inchi_key, kegg, metanetx])

df = pd.DataFrame(l, columns=['name', 'bigg', 'inchi_key', 'kegg', 'metanetx'])

# write in existing excel file  # TODO: bad idea, lose all formatting
#
# to_update = {"planning MM2020": combined}
#
# for sheet in excel_reader.sheet_names:
#     sheet_df = excel_reader.parse(sheet)
#     sheet_df = monster_mix.set_index('Name').combine_first(combined).reset_index()
#     sheet_df['inchi_key'] = sheet_df['inchi_key'].fillna('').str.upper().replace({'': np.nan})
#     sheet_df.to_excel(excel_writer, sheet, index=False)
#
# excel_writer.save()

# df.to_csv('monster_mix_annotation.csv')  # TODO: I copy-pasted from this into the google sheet


# now to analyze potentially interesting candidates that are missing
# current = df['inchi_key'].dropna().values

d = dict()
inchi_keys_per_subsystem['in_monster_mix'] = False
for bigg, data in inchi_keys_per_subsystem.iterrows():
    # for name, inchi_key in inchi_keys.items():
    # bigg = name[2:-2]  # strip M_ and _c / _e / _p
    if bigg in df['bigg'].values:
        if isinstance(inchi_key, float) and not np.isnan(inchi_key):
            assert df[df['bigg'] == bigg]['inchi_key'].values[0] == inchi_key
        inchi_keys_per_subsystem.loc[bigg, 'in_monster_mix'] = True


# inchi_keys_per_subsystem.to_csv('inchi_keys_per_subsystem.csv')


def print_subsystem_reactions(subsystem):
    plugin = subsystem_doc.model.packages[1]
    groups = [plugin.getGroup(i) for i in range(plugin.getNumGroups())]
    for g in groups:
        if g.name == subsystem:
            break
    else:
        raise ValueError(f'subsystem not found: {subsystem}')

    for rxn_id in [m.id_ref for m in g.members]:
        reaction = doc.model.reactions.get(rxn_id)
        print(rxn_id, reaction.formula)
