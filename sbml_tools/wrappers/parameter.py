import libsbml
from typing import Tuple, Union, Optional, List

import numpy as np

from sbml_tools import settings
from sbml_tools.toolbox import read_sbml, check
from sbml_tools.wrappers import SBase, Unit, ListOf, utils


class ParameterError(Exception):
    pass


class Parameter(SBase):
    """
    constant == bool : parameters can describe both constant and variable values
    " A value of “ false ” for constant only indicates that a parameter can change value, not that it must. "
        - libsbml L3V2
    """
    required = ['id', 'constant']
    _eq_attrs = ['value', 'units', 'constant']

    def __init__(self, parameter: libsbml.Parameter = None, **kwargs):
        super().__init__(libsbml.Parameter, parameter, **kwargs)

    @property
    def value(self) -> float:
        if self.wrapped.isSetValue():
            return self.wrapped.getValue()

    @value.setter
    def value(self, value: Union[float, int]):
        check(self.wrapped.setValue(value))

    @property
    def units(self) -> str:
        if self.wrapped.isSetUnits():
            return self.wrapped.getUnits()

    @units.setter
    def units(self, units: str):
        check(self.wrapped.setUnits(units))

    @property
    def constant(self) -> Optional[bool]:
        if self.wrapped.isSetConstant():
            return self.wrapped.getConstant()

    @constant.setter
    def constant(self, constant: bool):
        check(self.wrapped.setConstant(constant))

    @utils.model_property
    def reactions(self):
        return [reaction for reaction in self.model.reactions if self.id in reaction.kinetic_law_symbols]

    @utils.model_property
    def kinetic_laws(self):
        return [reaction.kinetic_law for reaction in self.reactions if reaction.kinetic_law]


class ListOfParameters(ListOf):

    _contained_type = Parameter

    def __init__(self, list_of_parameters: libsbml.ListOfParameters = None):
        super().__init__(libsbml.ListOfParameters, list_of_parameters)

    @property
    def values(self) -> List[float]:
        return [parameter.value for parameter in self]

    @property
    def units(self) -> List[str]:
        return [parameter.units for parameter in self]
