
from typing import Tuple
import libsbml

from sbml_tools import settings
from sbml_tools.wrappers import SBase
from sbml_tools.wrappers.utils import wrap, method_kwargs
from sbml_tools.wrappers import Model, Species
from sbml_tools.toolbox import read_sbml, check
from sbml_tools.toolbox.tables import Tables
from sbml_tools.toolbox.validator import validate_sbml


# TODO:
#  1. validator
#  2. packages...

# TODO: http://sbml.org/Software/libSBML/5.18.0/docs/python-api/classlibsbml_1_1_s_b_m_l_converter.html
## http://sbml.org/Software/libSBML/5.18.0/docs/python-api/classlibsbml_1_1_s_base_plugin.html
## http://sbml.org/Software/libSBML/5.18.0/docs/python-api/classlibsbml_1_1_s_b_m_l_extension.html
## http://sbml.org/Software/libSBML/5.18.0/docs/python-api/classlibsbml_1_1_s_b_m_l_validator.html

# TODO: http://sbml.org/Software/libSBML/5.18.0/docs/python-api/libsbml-python-example-files.html
## http://sbml.org/Software/libSBML/5.18.0/docs/python-api/add_custom_validator_8py-example.html
## http://sbml.org/Software/libSBML/5.18.0/docs/python-api/print_math_8py-example.html

# NOTE: use libsbml conversion Properties, not converter, the latter yields SIGSEGV
# conversionProperties can take bs options and will not throw an error


class SBMLDocument(SBase):

    required = ['level', 'version']
    _eq_attrs = ['level', 'version', 'model']

    def __init__(self, document: libsbml.SBMLDocument = None, **kwargs):
        super().__init__(libsbml.SBMLDocument, document, **kwargs)

    @property
    def level_and_version(self) -> Tuple[int, int]:
        return self.level, self.version

    @level_and_version.setter
    def level_and_version(self, level_and_version: Tuple[int, int]):
        self.convert_level_and_version(*level_and_version)

    @property
    def all_elements(self) -> list:
        return list(map(wrap, self.wrapped.getListOfAllElements()))

    def create_model(self, id=None, name=None, **kwargs):
        self.model = Model(level=self.level, version=self.version, **method_kwargs(locals()))
        return self.model

    @property
    def model(self):
        return wrap(self.wrapped.getModel())

    @model.setter
    def model(self, model: Model):
        check(self.wrapped.setModel(model.wrapped))

    # model conversion
    def convert_level_and_version(self, level, version):  # TODO: rename
        prop = libsbml.ConversionProperties(libsbml.SBMLNamespaces(level, version))
        # prop.addOption('strict', True), prop.addOption('ignorePackages', True)
        prop.addOption('setLevelAndVersion', True)
        check(self.wrapped.convert(prop))

    def convert_fbc_to_cobra(self):
        props = libsbml.ConversionProperties()
        props.addOption("convert fbc to cobra", True, "Convert FBC model to Cobra model")
        check(self.wrapped.convert(props))

    def convert_cobra_to_fbc(self):
        props = libsbml.ConversionProperties()
        props.addOption("convert cobra", True, "Convert Cobra model to FBC model")
        check(self.wrapped.convert(props))

    def flatten_model(self, external_model_definition_filepath='.'):
        """flatten model: remove hierarchically model composition"""
        props = libsbml.ConversionProperties()
        props.addOption("flatten comp", True, "Convert Cobra model to FBC model")
        props.addOption('abortIfUnflattenable', 'all')  # 'all', 'requiredOnly', 'none'
        props.addOption('stripUnflattenablePackages', True)
        props.addOption('basePath', external_model_definition_filepath)
        props.addOption('leavePorts', False)
        props.addOption('listModelDefinitions', False)
        props.addOption('performValidation', True)
        check(self.wrapped.convert(props))

    def promote_local_parameters(self):
        """
        Turn local reaction parameters into global ones.
        Adds the reaction id as a parameters id prefix. If that already exists, then also adds a number as suffix
        """
        convProps = libsbml.ConversionProperties()
        convProps.addOption("promoteLocalParameters", True, "Promotes all Local Parameters to Global ones")
        check(self.wrapped.convert(convProps))

    # checkL1Compatibility,
    # checkL2v1Compatibility,
    # checkL2v2Compatibility,
    # checkL2v3Compatibility,
    # checkL2v4Compatibility,
    # checkL2v5Compatibility,
    # checkL3v1Compatibility,
    # checkL3v2Compatibility,

    def to_string(self):
        # doc.toSBML() and doc.toXMLNode().toXMLString() don't add the xml declaration and document encoding
        return libsbml.writeSBMLToString(self.wrapped)

    def validate(self):  # TODO: this is insufficient, need to add validator
        self.wrapped.checkConsistency()
        self.wrapped.checkInternalConsistency()
        return check(self.wrapped.validateSBML())

    @property
    def errors(self):  # get errors, not int
        return [self.wrapped.getError(i) for i in range(self.wrapped.getNumErrors())]

    @property
    def error_log(self) -> str:
        return self.wrapped.getErrorLog().toString()

    # def read_sbml(self, sbml) -> libsbml.SBMLDocument:
    #     return read_sbml(sbml)
    #
    # def write_sbml(self, filepath: str, validate: bool = True, overwrite=False) -> None:
    #     if validate: self.validate()
    #     write_sbml(self.doc, filepath, overwrite)

    def to_tables(self):
        return Tables(self.wrapped).convert()

    def print_tables(self):
        Tables(self.wrapped).print()

    def __repr__(self):
        return f"""{self.__class__.__name__}
           level: {self.level}
         version: {self.version}
        model id: {self.model.id if self.model else None}
        packages: {self.package_ids}"""


def test():
    # fbc conversion
    doc = SBMLDocument(read_sbml(settings.ECOLI_CORE_MODEL))
    assert 'fbc' in doc.package_ids
    doc.convert_fbc_to_cobra()
    assert 'fbc' not in doc.package_ids
    doc.convert_cobra_to_fbc()
    assert 'fbc' in doc.package_ids

    # NOTE: doc.doc.doc (same with model); e.g. doc.model.species.model (as per libsbml standard)
    doc = SBMLDocument()
    doc.create_model(id='model')
    assert doc.model.id == 'model'

    model = Model(id='new_model')
    doc.model = model
    assert doc.model.id == 'new_model'

    s = Species(id='s1', compartment='c', has_only_substance_units=False, constant=False, boundary_condition=False)
    doc.model.add_species(s)

    doc.model.create_species(id='s2', compartment='c', has_only_substance_units=False,
                             constant=False, boundary_condition=False)


    # doc.convert_fbc_to_cobra()
    # doc.convert_cobra_to_fbc()

    # doc.convert_fbc_to_cobra()
    # doc.convert_cobra_to_fbc()
    #
    # #
    # assert doc.level_and_version == (3, 2)
    # doc.convert(2, 4)
    # assert doc.level_and_version == (2, 4)
    # sbml.convert(4, 1) # error as should

