
import re
import functools
from typing import Union, Optional, Iterable, List
import libsbml

from sbml_tools import config
from sbml_tools.wrappers import SBase, ListOf, ASTNode, utils, Species
from sbml_tools.wrappers import SpeciesReference, ListOfSpeciesReferences, SimpleSpeciesReference
from sbml_tools.wrappers import ModifierSpeciesReference, ListOfModifierSpeciesReferences
from sbml_tools.wrappers import KineticLaw, LocalParameter, Parameter


def set_species_references(reaction: 'Reaction', species_reference: SimpleSpeciesReference, set_as: str):
    """
    This is a helper function because in libsbml you cannot add species references, only species
    This is not proper because:
    1. the attributes 'stoichiometry' and 'constant' cannot be set like this,
       they default to 1.0 and True, and worse: .isSetStoichiometry .isSetConstant will also default to True
    2. it requires generation of species perse, which have attributes that are necessarily required on a Species
       but not present on a SpeciesReference, and worse: both contain the boolean attribute constant,
       yet hold different meanings, leading to potential confusion.
    """
    if not isinstance(species_reference, SimpleSpeciesReference):
        raise ValueError(f'expecting a (Modifier)SpeciesReference, '
                         f'instead got a {type(species_reference)}: {species_reference}')
    if not species_reference.has_required_attributes:
        raise ReactionError(f'required attributes not set on {species_reference}')
    # if reaction.model:
    #     if not reaction.model.species.get(species_reference.species):
    #         raise ValueError(f"species referenced '{species_reference.species}' not in the model")
    if isinstance(species_reference, SpeciesReference):
        if set_as == 'reactant':
            reference = reaction.wrapped.createReactant()
        elif set_as == 'product':
            reference = reaction.wrapped.createProduct()
        else:
            raise ReactionError(f'cannot set {species_reference} as {set_as}')
        utils.check(reference.setSpecies(species_reference.species))
        utils.check(reference.setConstant(species_reference.constant))
        utils.check(reference.setStoichiometry(species_reference.stoichiometry))
    elif set_as == 'modifier' and isinstance(species_reference, ModifierSpeciesReference):
        modifier_reference = reaction.wrapped.createModifier()
        utils.check(modifier_reference.setSpecies(species_reference.species))
    else:
        raise ReactionError(f'cannot set {species_reference} as {set_as}')


def stringify(stoich: float, species: str):
    return species if stoich == 1.0 else f'{int(stoich)} {species}' if stoich.is_integer() else f'{stoich} {species}'


class ReactionError(Exception):
    pass


class Reaction(SBase):
    """
    the reversible attribute identifies whether a reaction is reversible or not, without having to deduce this
    by inspecting the kinetic law (which otherwise might be impossible). It does not affect the interpretation
    of the kinetic law, but in case the rate becomes negative this violates the assertion when reversible == False.

    compartment also does not influence the interpretation, but may be used for other purposes
    (e.g. checking rate laws, visualization)

    Every species occurring in a math element of the kinetic law of a reaction
    should be declared in at least one of the following: reactants, products or modifiers

    A reaction should contain at least one reactant OR product
    """

    required = ['id', 'reversible']
    _eq_attrs = ['id', 'reversible', 'reactants', 'products', 'modifiers', 'kinetic_law', 'compartment']

    def __init__(self, reaction: libsbml.Reaction = None, **kwargs):
        super().__init__(libsbml.Reaction, reaction, **kwargs)

    @property
    def reversible(self) -> Optional[bool]:
        if self.wrapped.isSetReversible():
            return self.wrapped.getReversible()

    @reversible.setter
    def reversible(self, reversible: bool):
        utils.check(self.wrapped.setReversible(reversible))

    @property
    def compartment(self) -> str:
        return self.wrapped.getCompartment()

    @compartment.setter
    def compartment(self, compartment: str):
        utils.check(self.wrapped.setCompartment(compartment))

    def add_reactant(self, reactant: SpeciesReference):
        set_species_references(self, reactant, set_as='reactant')

    def add_product(self, product: SpeciesReference):
        set_species_references(self, product, set_as='product')

    def add_modifier(self, modifier: Union[ModifierSpeciesReference, str]):
        if isinstance(modifier, str):
            modifier = ModifierSpeciesReference(species=modifier)
        set_species_references(self, modifier, set_as='modifier')

    def remove_reactant(self, reactant: Union[str, int]):
        return self.wrapped.removeReactant(reactant)

    def remove_all_reactants(self):
        for _ in range(self.wrapped.getNumReactants()):
            self.remove_reactant(0)

    def remove_product(self, product: Union[str, int]):
        return self.wrapped.removeProduct(product)

    def remove_all_products(self):
        for _ in range(self.wrapped.getNumProducts()):
            self.remove_product(0)

    def remove_modifier(self, modifier: Union[str, int]):
        return self.wrapped.removeModifier(modifier)

    def remove_all_modifiers(self):
        for _ in range(self.wrapped.getNumModifiers()):
            self.remove_modifier(0)

    def create_reactant(self, species: str, stoichiometry: float, constant: bool, **kwargs):
        return self.add_reactant(SpeciesReference(species=species, stoichiometry=stoichiometry, constant=constant,
                                                  **kwargs))

    @property
    def reactants(self) -> ListOfSpeciesReferences:
        return ListOfSpeciesReferences(self.wrapped.getListOfReactants())

    @reactants.setter
    def reactants(self, reactants: Iterable[SpeciesReference]):
        if isinstance(reactants, str):
            partial = functools.partial(utils.string_to_species_reference, level=self.level, version=self.version)
            reactants = list(map(partial, reactants.split('+')))
        self.remove_all_reactants()
        for reactant in reactants:
            self.add_reactant(reactant)

    @property
    def reactant_species(self) -> List[Species]:
        return [ref.get_species() for ref in self.reactants]

    def create_product(self, species: str, stoichiometry: float, constant: bool, **kwargs):
        return self.add_product(SpeciesReference(species=species, stoichiometry=stoichiometry, constant=constant,
                                                 **kwargs))

    @property
    def products(self) -> ListOfSpeciesReferences:
        return ListOfSpeciesReferences(self.wrapped.getListOfProducts())

    @products.setter
    def products(self, products: Iterable[SpeciesReference]):
        if isinstance(products, str):
            partial = functools.partial(utils.string_to_species_reference, level=self.level, version=self.version)
            products = list(map(partial, products.split('+')))
        self.remove_all_products()
        for product in products:
            self.add_product(product)

    @property
    def product_species(self) -> List[Species]:
        return [ref.get_species() for ref in self.products]

    def create_modifier(self, species, **kwargs):
        return self.add_modifier(ModifierSpeciesReference(species=species, **kwargs))

    @property
    def modifiers(self) -> ListOfModifierSpeciesReferences:
        return ListOfModifierSpeciesReferences(self.wrapped.getListOfModifiers())

    @modifiers.setter
    def modifiers(self, modifiers: Union[Iterable[Union[ModifierSpeciesReference, str]], str]):
        self.remove_all_modifiers()
        if isinstance(modifiers, str):
            modifiers = re.findall(r'[^,\s]+', modifiers)
        for modifier in modifiers:
            self.add_modifier(modifier)

    @property
    def species_references(self):
        return list(self.reactants) + list(self.products) + list(self.modifiers)

    @property
    def modifier_species(self) -> List[Species]:
        return [ref.get_species() for ref in self.modifiers]

    @property
    def species(self) -> List[Species]:
        """return species instances of those references in the reaction"""
        return self.reactant_species + self.product_species + self.modifier_species

    @property
    def kinetic_law(self) -> Optional[KineticLaw]:
        if self.wrapped.isSetKineticLaw():
            return KineticLaw(self.wrapped.getKineticLaw())

    @kinetic_law.setter
    def kinetic_law(self, kinetic_law: Union[KineticLaw, ASTNode, str]):
        if isinstance(kinetic_law, (ASTNode, str)):
            kinetic_law = KineticLaw(level=self.level, version=self.version, math=kinetic_law)
        if not (self.level == 3 and self.version == 2) and kinetic_law.math is None:  # some version constraint
            raise ReactionError(f'must set math on the kinetic law first, which may even be an empty ASTNode')
        utils.check(self.wrapped.setKineticLaw(kinetic_law.wrapped))

    def create_kinetic_law(self, math: Union[ASTNode, str] = None, local_parameters: Iterable[LocalParameter] = None):
        kinetic_law = KineticLaw(level=self.level, version=self.version, math=math, local_parameters=local_parameters)
        utils.check(self.wrapped.setKineticLaw(kinetic_law.wrapped))
        return self.kinetic_law  # important: return self.kinetic_law, otherwise no references

    @property
    def local_parameters(self):  # pull them up a level for easy access
        if self.kinetic_law:
            return self.kinetic_law.local_parameters

    @local_parameters.setter
    def local_parameters(self, local_parameters: Iterable[LocalParameter]):
        if not self.kinetic_law:
            raise ReactionError(f'cannot set local parameters without instantiating a kinetic law first')
        self.kinetic_law.local_parameters = local_parameters

    @property
    def math(self) -> Optional[ASTNode]:
        if self.kinetic_law:
            return self.kinetic_law.math

    @property
    def kinetic_law_symbols(self) -> Optional[List[str]]:
        return self.kinetic_law.math.symbols if self.kinetic_law and self.kinetic_law.math else None

    @property
    def parameters(self) -> Optional[List[Parameter]]:
        return self.kinetic_law.parameters if self.kinetic_law else []

    @property
    def formula(self):  # using wrapped.reversible because it defaults to True
        def to_string(species_ref):
            species = species_ref.get_species()
            if species_ref.stoichiometry == 1.0:
                return f'{species.id}[{species.compartment}]'
            elif species_ref.stoichiometry.is_integer():
                return f'{int(species_ref.stoichiometry)} {species.id}[{species.compartment}]'
            return f'{species_ref.stoichiometry} {species.id}[{species.compartment}]'

        reactant_string = ' + '.join([to_string(s_ref) for s_ref in self.reactants])
        product_string = ' + '.join([to_string(s_ref) for s_ref in self.products])
        return f"{reactant_string} {['-->', '<=>'][self.wrapped.reversible]} {product_string}"

    def __str__(self):
        return f"""{self.id}: {self.formula}"""

    # def __repr__(self):
    #     return f"""\n{self.__class__.__name__} at {hex(id(self))}\n\n\
    #                       id: '{self.id}'
    #                     name: '{self.name}'
    #
    #                reactants: {[s_ref.species for s_ref in self.reactants]}
    #                 products: {[s_ref.species for s_ref in self.products]}
    #                modifiers: {[m_ref.species for m_ref in self.modifiers]}
    #
    #         reaction formula: {self.formula}
    #     """
    #             #  kinetic rate law: {self.kinetic_law.math.string}
    #             # local parameters: {[p.id for p in self.kinetic_law.local_parameters]}


class ListOfReactions(ListOf):
    _contained_type = Reaction

    def __init__(self, list_of_reactions: libsbml.ListOfReactions = None):
        super().__init__(libsbml.ListOfReactions, list_of_reactions)

    @property
    def kinetic_laws(self):  # leaving None to preserve order for now
        return [reaction.kinetic_law for reaction in self]

    @property
    def local_parameters(self) -> List[LocalParameter]:
        return [local_parameter for reaction in self for local_parameter in reaction.local_parameters]

    @property
    def species_references(self):
        return [species_reference for reaction in self for species_reference in reaction.species_references]
