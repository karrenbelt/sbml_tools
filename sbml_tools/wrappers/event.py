import libsbml
from typing import Tuple, Union, Optional, Iterable

import numpy as np

from sbml_tools.wrappers import SBase, ASTNode, ListOf, utils
from sbml_tools.wrappers import Trigger, Priority, Delay, EventAssignment, ListOfEventAssignments


class EventError(Exception):
    pass


class Event(SBase):
    """
    """
    required = ['use_values_from_trigger_time']
    _eq_attrs = ['use_values_from_trigger_time', 'trigger', 'priority', 'delay', 'event_assignments']

    def __init__(self, event: libsbml.Event = None, **kwargs):
        super().__init__(libsbml.Event, event, **kwargs)

    @property
    def use_values_from_trigger_time(self) -> bool:
        if self.wrapped.isSetUseValuesFromTriggerTime():
            return self.wrapped.getUseValuesFromTriggerTime()

    @use_values_from_trigger_time.setter
    def use_values_from_trigger_time(self, use_values_from_trigger_time: bool):
        utils.check(self.wrapped.setUseValuesFromTriggerTime(use_values_from_trigger_time))

    @property
    def trigger(self) -> Trigger:
        if self.wrapped.isSetTrigger():
            return Trigger(self.wrapped.getTrigger())

    @trigger.setter
    def trigger(self, trigger: Trigger):
        utils.check(self.wrapped.setTrigger(trigger.wrapped))

    @property
    def priority(self) -> Priority:
        if self.wrapped.isSetPriority():
            return Priority(self.wrapped.getPriority())

    @priority.setter
    def priority(self, priority: Union[Priority, str]):
        if isinstance(priority, str):
            priority = Priority(level=self.level, version=self.version, math=priority)
        utils.check(self.wrapped.setPriority(priority.wrapped))

    @property
    def delay(self) -> Delay:
        if self.wrapped.isSetDelay():
            return Delay(self.wrapped.getDelay())

    @delay.setter
    def delay(self, delay: Union[Delay, str]):
        if isinstance(delay, str):
            delay = Delay(level=self.level, version=self.version, math=delay)
        utils.check(self.wrapped.setDelay(delay.wrapped))

    @property
    def event_assignments(self) -> ListOfEventAssignments:
        return ListOfEventAssignments(self.wrapped.getListOfEventAssignments())

    def add_event_assignment(self, event_assignment: Union[EventAssignment, str]):
        if isinstance(event_assignment, str):
            event_assignment = utils.string_to_event_assignment(event_assignment, self.level, self.version)
        utils.check(self.wrapped.addEventAssignment(event_assignment.wrapped))

    @event_assignments.setter
    def event_assignments(self, event_assignments: Union[ListOfEventAssignments, EventAssignment, Iterable[str], str]):
        self.remove_event_assignments()
        if isinstance(event_assignments, str):
            event_assignments = utils.semi_colon_splitter(event_assignments)
        elif isinstance(event_assignments, EventAssignment):
            event_assignments = [event_assignments]
        for event_assignment in event_assignments:
            self.add_event_assignment(event_assignment)

    def remove_event_assignments(self):
        for i in range(len(self.event_assignments)):
            self.wrapped.removeEventAssignment(0)

    def __repr__(self):
        assignments = '\n'.join([f"\t\t\t\t{a.variable}: {a.math}" for a in self.event_assignments])
        return f"""{' '.join([self.__class__.__name__, self.id]).strip()}:
              trigger: {self.trigger.math if self.trigger else None}
           persistent: {self.trigger.persistent if self.trigger else None}
        initial_value: {self.trigger.initial_value if self.trigger else None}
             priority: {self.priority.math if self.priority else None}
                delay: {self.delay.math if self.delay else None}
          assignments: [\n{assignments}\n{' '*23}]"""


class ListOfEvents(ListOf):
    _contained_type = Event

    def __init__(self, list_of_events: libsbml.ListOfEvents = None):
        super().__init__(libsbml.ListOfEvents, list_of_events)


