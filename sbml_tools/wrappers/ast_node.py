from typing import Tuple, Optional, Union, List, Set
import libsbml

import ast
import math
import numbers
import operator
import fractions
from more_itertools import consume
import numpy as np
import sympy
import pandas as pd
from bidict import bidict

from sbml_tools import config
from sbml_tools.utils import MathematicalOperators
from sbml_tools.wrappers import Wrapper, utils


# TODO:
#  1. semantic annotations
#  2. __eq__ is loose (checks value, rather than integer, real, numerator, denominator, mantissa, exponent
#  3. setting ASTNode via **kwargs; value overwrites type
#  4. units
#  5. libsbml parser settings


OPERATOR_PRECEDENCE = pd.DataFrame(
    [('name', 'symbol reference', 'operand', 6, 'n/a'),
     ('(expression)', 'expression reference', 'operand', 6, 'n/a'),
     ('f(...)', 'function call', 'prefix', 6, 'left'),
     ('-', 'negation', 'unary', 5, 'right'),
     ('^', 'power', 'binary', 4, 'left'),
     ('*', 'multiplication', 'binary', 3, 'left'),
     ('/', 'division', 'binary', 3, 'left'),
     ('+', 'addition', 'binary', 2, 'left'),
     ('-', 'subtration', 'binary', 2, 'left'),
     (',', 'delimiter', 'binary', 1, 'left'),
     ], columns=('token', 'operation', 'class', 'precedence', 'associates'))


BINARY_OPERATOR_MAP = {
    "**": operator.pow,
    "*": operator.mul,
    "/": operator.truediv,
    "+": operator.add,
    "-": operator.sub,
}


ALL_AST_TYPES = bidict({
    libsbml.AST_CONSTANT_E: 'AST_CONSTANT_E',
    libsbml.AST_CONSTANT_FALSE: 'AST_CONSTANT_FALSE',
    libsbml.AST_CONSTANT_PI: 'AST_CONSTANT_PI',
    libsbml.AST_CONSTANT_TRUE: 'AST_CONSTANT_TRUE',
    libsbml.AST_CSYMBOL_FUNCTION: 'AST_CSYMBOL_FUNCTION',
    libsbml.AST_DISTRIB_FUNCTION_BERNOULLI: 'AST_DISTRIB_FUNCTION_BERNOULLI',
    libsbml.AST_DISTRIB_FUNCTION_BINOMIAL: 'AST_DISTRIB_FUNCTION_BINOMIAL',
    libsbml.AST_DISTRIB_FUNCTION_CAUCHY: 'AST_DISTRIB_FUNCTION_CAUCHY',
    libsbml.AST_DISTRIB_FUNCTION_CHISQUARE: 'AST_DISTRIB_FUNCTION_CHISQUARE',
    libsbml.AST_DISTRIB_FUNCTION_EXPONENTIAL: 'AST_DISTRIB_FUNCTION_EXPONENTIAL',
    libsbml.AST_DISTRIB_FUNCTION_GAMMA: 'AST_DISTRIB_FUNCTION_GAMMA',
    libsbml.AST_DISTRIB_FUNCTION_LAPLACE: 'AST_DISTRIB_FUNCTION_LAPLACE',
    libsbml.AST_DISTRIB_FUNCTION_LOGNORMAL: 'AST_DISTRIB_FUNCTION_LOGNORMAL',
    libsbml.AST_DISTRIB_FUNCTION_NORMAL: 'AST_DISTRIB_FUNCTION_NORMAL',
    libsbml.AST_DISTRIB_FUNCTION_POISSON: 'AST_DISTRIB_FUNCTION_POISSON',
    libsbml.AST_DISTRIB_FUNCTION_RAYLEIGH: 'AST_DISTRIB_FUNCTION_RAYLEIGH',
    libsbml.AST_DISTRIB_FUNCTION_UNIFORM: 'AST_DISTRIB_FUNCTION_UNIFORM',
    libsbml.AST_DIVIDE: 'AST_DIVIDE',
    libsbml.AST_END_OF_CORE: 'AST_END_OF_CORE',  # cannot set this type
    libsbml.AST_FUNCTION: 'AST_FUNCTION',
    libsbml.AST_FUNCTION_ABS: 'AST_FUNCTION_ABS',
    libsbml.AST_FUNCTION_ARCCOS: 'AST_FUNCTION_ARCCOS',
    libsbml.AST_FUNCTION_ARCCOSH: 'AST_FUNCTION_ARCCOSH',
    libsbml.AST_FUNCTION_ARCCOT: 'AST_FUNCTION_ARCCOT',
    libsbml.AST_FUNCTION_ARCCOTH: 'AST_FUNCTION_ARCCOTH',
    libsbml.AST_FUNCTION_ARCCSC: 'AST_FUNCTION_ARCCSC',
    libsbml.AST_FUNCTION_ARCCSCH: 'AST_FUNCTION_ARCCSCH',
    libsbml.AST_FUNCTION_ARCSEC: 'AST_FUNCTION_ARCSEC',
    libsbml.AST_FUNCTION_ARCSECH: 'AST_FUNCTION_ARCSECH',
    libsbml.AST_FUNCTION_ARCSIN: 'AST_FUNCTION_ARCSIN',
    libsbml.AST_FUNCTION_ARCSINH: 'AST_FUNCTION_ARCSINH',
    libsbml.AST_FUNCTION_ARCTAN: 'AST_FUNCTION_ARCTAN',
    libsbml.AST_FUNCTION_ARCTANH: 'AST_FUNCTION_ARCTANH',
    libsbml.AST_FUNCTION_CEILING: 'AST_FUNCTION_CEILING',
    libsbml.AST_FUNCTION_COS: 'AST_FUNCTION_COS',
    libsbml.AST_FUNCTION_COSH: 'AST_FUNCTION_COSH',
    libsbml.AST_FUNCTION_COT: 'AST_FUNCTION_COT',
    libsbml.AST_FUNCTION_COTH: 'AST_FUNCTION_COTH',
    libsbml.AST_FUNCTION_CSC: 'AST_FUNCTION_CSC',
    libsbml.AST_FUNCTION_CSCH: 'AST_FUNCTION_CSCH',
    libsbml.AST_FUNCTION_DELAY: 'AST_FUNCTION_DELAY',
    libsbml.AST_FUNCTION_EXP: 'AST_FUNCTION_EXP',
    libsbml.AST_FUNCTION_FACTORIAL: 'AST_FUNCTION_FACTORIAL',
    libsbml.AST_FUNCTION_FLOOR: 'AST_FUNCTION_FLOOR',
    libsbml.AST_FUNCTION_LN: 'AST_FUNCTION_LN',
    libsbml.AST_FUNCTION_LOG: 'AST_FUNCTION_LOG',
    libsbml.AST_FUNCTION_MAX: 'AST_FUNCTION_MAX',
    libsbml.AST_FUNCTION_MIN: 'AST_FUNCTION_MIN',
    libsbml.AST_FUNCTION_PIECEWISE: 'AST_FUNCTION_PIECEWISE',
    libsbml.AST_FUNCTION_POWER: 'AST_FUNCTION_POWER',
    libsbml.AST_FUNCTION_QUOTIENT: 'AST_FUNCTION_QUOTIENT',
    libsbml.AST_FUNCTION_RATE_OF: 'AST_FUNCTION_RATE_OF',
    libsbml.AST_FUNCTION_REM: 'AST_FUNCTION_REM',
    libsbml.AST_FUNCTION_ROOT: 'AST_FUNCTION_ROOT',
    libsbml.AST_FUNCTION_SEC: 'AST_FUNCTION_SEC',
    libsbml.AST_FUNCTION_SECH: 'AST_FUNCTION_SECH',
    libsbml.AST_FUNCTION_SIN: 'AST_FUNCTION_SIN',
    libsbml.AST_FUNCTION_SINH: 'AST_FUNCTION_SINH',
    libsbml.AST_FUNCTION_TAN: 'AST_FUNCTION_TAN',
    libsbml.AST_FUNCTION_TANH: 'AST_FUNCTION_TANH',
    libsbml.AST_INTEGER: 'AST_INTEGER',
    libsbml.AST_LAMBDA: 'AST_LAMBDA',
    libsbml.AST_LINEAR_ALGEBRA_DETERMINANT: 'AST_LINEAR_ALGEBRA_DETERMINANT',
    libsbml.AST_LINEAR_ALGEBRA_MATRIX: 'AST_LINEAR_ALGEBRA_MATRIX',
    libsbml.AST_LINEAR_ALGEBRA_MATRIXROW: 'AST_LINEAR_ALGEBRA_MATRIXROW',
    libsbml.AST_LINEAR_ALGEBRA_OUTER_PRODUCT: 'AST_LINEAR_ALGEBRA_OUTER_PRODUCT',
    libsbml.AST_LINEAR_ALGEBRA_SCALAR_PRODUCT: 'AST_LINEAR_ALGEBRA_SCALAR_PRODUCT',
    libsbml.AST_LINEAR_ALGEBRA_SELECTOR: 'AST_LINEAR_ALGEBRA_SELECTOR',
    libsbml.AST_LINEAR_ALGEBRA_TRANSPOSE: 'AST_LINEAR_ALGEBRA_TRANSPOSE',
    libsbml.AST_LINEAR_ALGEBRA_VECTOR: 'AST_LINEAR_ALGEBRA_VECTOR',
    libsbml.AST_LINEAR_ALGEBRA_VECTOR_PRODUCT: 'AST_LINEAR_ALGEBRA_VECTOR_PRODUCT',
    libsbml.AST_LOGICAL_AND: 'AST_LOGICAL_AND',
    libsbml.AST_LOGICAL_EXISTS: 'AST_LOGICAL_EXISTS',
    libsbml.AST_LOGICAL_FORALL: 'AST_LOGICAL_FORALL',
    libsbml.AST_LOGICAL_IMPLIES: 'AST_LOGICAL_IMPLIES',
    libsbml.AST_LOGICAL_NOT: 'AST_LOGICAL_NOT',
    libsbml.AST_LOGICAL_OR: 'AST_LOGICAL_OR',
    libsbml.AST_LOGICAL_XOR: 'AST_LOGICAL_XOR',
    libsbml.AST_MINUS: 'AST_MINUS',
    libsbml.AST_NAME: 'AST_NAME',
    libsbml.AST_NAME_AVOGADRO: 'AST_NAME_AVOGADRO',
    libsbml.AST_NAME_TIME: 'AST_NAME_TIME',
    libsbml.AST_PLUS: 'AST_PLUS',
    libsbml.AST_POWER: 'AST_POWER',
    libsbml.AST_RATIONAL: 'AST_RATIONAL',
    libsbml.AST_REAL: 'AST_REAL',
    libsbml.AST_REAL_E: 'AST_REAL_E',
    libsbml.AST_RELATIONAL_EQ: 'AST_RELATIONAL_EQ',
    libsbml.AST_RELATIONAL_GEQ: 'AST_RELATIONAL_GEQ',
    libsbml.AST_RELATIONAL_GT: 'AST_RELATIONAL_GT',
    libsbml.AST_RELATIONAL_LEQ: 'AST_RELATIONAL_LEQ',
    libsbml.AST_RELATIONAL_LT: 'AST_RELATIONAL_LT',
    libsbml.AST_RELATIONAL_NEQ: 'AST_RELATIONAL_NEQ',
    libsbml.AST_SERIES_PRODUCT: 'AST_SERIES_PRODUCT',
    libsbml.AST_SERIES_SUM: 'AST_SERIES_SUM',
    libsbml.AST_STATISTICS_MEAN: 'AST_STATISTICS_MEAN',
    libsbml.AST_STATISTICS_MEDIAN: 'AST_STATISTICS_MEDIAN',
    libsbml.AST_STATISTICS_MODE: 'AST_STATISTICS_MODE',
    libsbml.AST_STATISTICS_MOMENT: 'AST_STATISTICS_MOMENT',
    libsbml.AST_STATISTICS_SDEV: 'AST_STATISTICS_SDEV',
    libsbml.AST_STATISTICS_VARIANCE: 'AST_STATISTICS_VARIANCE',
    libsbml.AST_TIMES: 'AST_TIMES',
    libsbml.AST_UNKNOWN: 'AST_UNKNOWN',
    })


AST_TAGS = {
    'AVOGADRO': libsbml.ASTNode.isAvogadro,
    'BOOLEAN': libsbml.ASTNode.isBoolean,
    'BVAR': libsbml.ASTNode.isBvar,
    'C_SYMBOL_FUNCTION': libsbml.ASTNode.isCSymbolFunction,
    'CI_NUMBER': libsbml.ASTNode.isCiNumber,
    'CONSTANT': libsbml.ASTNode.isConstant,
    'CONSTANT_NUMBER': libsbml.ASTNode.isConstantNumber,
    'FUNCTION': libsbml.ASTNode.isFunction,
    'INFINITY': libsbml.ASTNode.isInfinity,
    'INTEGER': libsbml.ASTNode.isInteger,
    'LAMBDA': libsbml.ASTNode.isLambda,
    'LOG10': libsbml.ASTNode.isLog10,
    'LOGICAL': libsbml.ASTNode.isLogical,
    'NAN': libsbml.ASTNode.isNaN,
    'NAME': libsbml.ASTNode.isName,
    'NEG_INFINITY': libsbml.ASTNode.isNegInfinity,
    'NUMBER': libsbml.ASTNode.isNumber,
    'OPERATOR': libsbml.ASTNode.isOperator,
    'PIECEWISE': libsbml.ASTNode.isPiecewise,
    'QUALIFIER': libsbml.ASTNode.isQualifier,
    'RATIONAL': libsbml.ASTNode.isRational,
    'REAL': libsbml.ASTNode.isReal,
    'RELATIONAL': libsbml.ASTNode.isRelational,
    'SEMANTICS': libsbml.ASTNode.isSemantics,
    'SQRT': libsbml.ASTNode.isSqrt,
    'U_MINUS': libsbml.ASTNode.isUMinus,
    'U_PLUS': libsbml.ASTNode.isUPlus,
    'UNKNOWN': libsbml.ASTNode.isUnknown,
    'USER_FUNCTION': libsbml.ASTNode.isUserFunction
    }


def check_ast_node(ast_node: libsbml.ASTNode) -> libsbml.ASTNode:
    if ast_node is None or not ast_node.isWellFormedASTNode():
        message = libsbml.getLastParseL3Error()
        raise ValueError(f"Invalid libsml.ASTNode:\n{message}")
    return ast_node


def are_equal_ast_nodes(a1: libsbml.ASTNode, a2: libsbml.ASTNode) -> bool:
    # if type(a1) is not type(a2):
    #     return False
    if a1.getType() != a2.getType():
        return False
    if a1.getName() != a2.getName():
        return False
    if a1.getUnits() != a2.getUnits():
        return False
    if a1.getValue() != a2.getValue():
        if not np.isnan(a1.getValue()) == np.isnan(a2.getValue()):
            return False
    if a1.getNumChildren() != a2.getNumChildren():
        return False
    for i in range(a1.getNumChildren()):
        if not are_equal_ast_nodes(a1.getChild(i), a2.getChild(i)):
            return False
    return True


def _as_ast_node(item):  # used in setter of children
    if isinstance(item, ASTNode):
        return item
    if isinstance(item, (int, float, fractions.Fraction)):
        return ASTNode(value=item)
    elif isinstance(item, str):
        return ASTNode.from_string(item)
    else:
        raise ASTNodeError(f"cannot convert '{item}' to ASTNode")


class ASTNodeError(Exception):
    pass


class ASTNode(Wrapper):

    # TODO: unit evaluation
    required = []
    _eq_attrs = ['type', 'name', 'value', 'units', 'children']

    def __init__(self, ast_node: libsbml.ASTNode = None, **kwargs):
        if isinstance(ast_node, libsbml.ASTNode):  # and ast_node.__class__.__name__ == self.__class__.__name__:
            check_ast_node(ast_node)
        super().__init__(libsbml.ASTNode, ast_node, **kwargs)

    @property
    def ast_plugins(self):  # TODO: not ran into a test case yet, not sure if num plugins returns num_ast_plugins
        return [self.wrapped.getASTPlugin(i) for i in range(self.wrapped.getNumPlugins())]

    @property
    def type(self) -> int:
        return self.wrapped.getType()

    @type.setter
    def type(self, ast_type: Union[int, str]):
        if isinstance(ast_type, str):
            ast_type = ALL_AST_TYPES.inverse(ast_type.upper())
        utils.check(self.wrapped.setType(ast_type))

    @property
    def type_string(self) -> str:
        return ALL_AST_TYPES[self.type]

    @property
    def name(self) -> Optional[str]:
        return self.wrapped.getName()

    @name.setter
    def name(self, ast_name: str):
        if not ast_name.isidentifier():
            raise ASTNodeError(f"'{ast_name}' is not a valid identifier")
        utils.check(self.wrapped.setName(ast_name))

    @property
    def value(self) -> Optional[Union[fractions.Fraction, float, int]]:
        # the order here matters: if isRational then also isReal, not vice versa
        if self.wrapped.isRational():
            return fractions.Fraction(self.wrapped.getNumerator(), self.wrapped.getDenominator())
        if self.wrapped.isReal():  # also for AST_REAL_E
            return self.wrapped.getReal()
        if self.wrapped.isInteger():
            return self.wrapped.getInteger()

    @value.setter
    def value(self, value: Union[int, float, Tuple[int, int], Tuple[float, int], fractions.Fraction]):
        """depending on the input, the numerical node type will be: INTEGER / REAL / RATIONAL or REAL_E"""
        if isinstance(value, fractions.Fraction):
            value = value.numerator, value.denominator
        if isinstance(value, tuple):
            utils.check(self.wrapped.setValue(*value))
        else:
            utils.check(self.wrapped.setValue(value))

    @property
    def precedence(self):
        return self.wrapped.getPrecedence()

    @property
    def is_number(self) -> bool:
        return self.wrapped.isNumber()

    @property
    def is_nan(self) -> bool:
        return self.wrapped.isNaN()

    @property
    def units(self) -> str:  # TODO: setter, wrap units (Pint)
        if self.wrapped.isSetUnits():
            return self.wrapped.getUnits()

    @units.setter
    def units(self, units: str):
        if not self.wrapped.isNumber():
            raise ValueError(f'units can only be set on numbers, not on {self.type_string}')
        # utils.ensure_unit_definition(self, units)
        utils.check(self.wrapped.setUnits(units))

    @property
    def mathml_string(self) -> str:
        return libsbml.writeMathMLToString(self.wrapped)

    @mathml_string.setter
    def mathml_string(self, math_ml: str):
        self.wrapped = check_ast_node(libsbml.readMathMLFromString(math_ml))

    @property
    def string(self) -> str:  # returns infix notation
        return libsbml.formulaToL3StringWithSettings(self.wrapped, config.L3PS)

    @string.setter
    def string(self, math_string: str):
        self.wrapped = check_ast_node(libsbml.parseL3FormulaWithSettings(math_string, config.L3PS))

    @classmethod
    def from_string(cls, math_string: str):
        # internally self.wrapped.canonicalize() is used to turn names into types whenever possible
        return cls(libsbml.parseL3FormulaWithSettings(math_string, config.L3PS))

    @classmethod
    def from_mathml_string(cls, mathml_string: str):
        return cls(libsbml.readMathMLFromString(mathml_string))

    @property
    def is_well_formed(self) -> bool:
        # recursively checks if this node and all children have the correct number of arguments
        return self.wrapped.isWellFormedASTNode()

    @property
    def is_lambda(self) -> bool:
        return self.wrapped.isLambda()

    def validate(self) -> None:
        check_ast_node(self.wrapped)

    @property
    def children(self) -> List['ASTNode']:
        """iterate the direct children"""
        return [self.__class__(self.wrapped.getChild(i)) for i in range(self.wrapped.getNumChildren())]

    @children.setter
    def children(self, children):  # need to be careful here, because we can iterate a single ast_node
        if isinstance(children, type(self)):
            children = (children, )
        self.remove_children()
        consume(map(self.add_child, map(_as_ast_node, children)))

    def rename(self, old: str, new: str):
        """rename symbols inplace"""
        self.wrapped.renameSIdRefs(old, new)  # no check, returns None

    def replace(self, symbol: str, other: 'ASTNode'):  # replaceChild,
        """replace symbol by math element"""  # also does what replaceIDWithFunction is supposed to do
        self.wrapped.replaceArgument(symbol, other.wrapped)  # no check, returns None

    def swap_children(self, other):
        self.wrapped.swapChildren(other.wrapped)

    def add_child(self, other: 'ASTNode'):  # TODO: validate node remains valid
        utils.check(self.wrapped.addChild(other.wrapped.deepCopy()))

    def prepend_child(self, other: 'ASTNode'):
        utils.check(self.wrapped.prependChild(other.wrapped.deepCopy()))

    def insert_child(self, n: int, other: 'ASTNode'):
        utils.check(self.wrapped.insertChild(n, other.wrapped.deepCopy()))

    def remove_child(self, n: int) -> None:
        utils.check(utils.check(self.wrapped.removeChild(n)))

    def replace_child(self, n: int, other):
        n = len(self.children) + n if n < 0 else n
        utils.check(self.wrapped.replaceChild(n, other.wrapped))

    def remove_children(self) -> None:
        consume((self.wrapped.removeChild(0) for _ in range(self.wrapped.getNumChildren())))

    @property
    def semantic_annotations(self):
        return [self.wrapped.getSemanticsAnnotation(i) for i in self.wrapped.getNumSemanticsAnnotations()]

    # def add_semantic_annotation(self, annotation):
    #     check(self.wrapped.addSemanticAnnotation(annotation))
    #
    # @semantic_annotations.setter
    # def semantic_annotations(self, annotations):
    #     any(map(self.add_semantic_annotation, annotations))

    def reduce_to_binary(self) -> None:
        """e.g. and(x, y, z), reduces to: and(and(x, y), z) and replaces the current wrapped ASTNode"""
        self.wrapped.reduceToBinary()

    @property
    def names(self) -> Set[str]:
        """all names of ast nodes: symbols, functions, etc."""
        return set([node.name for node in self if node.name])

    @property
    def symbols(self) -> Set[str]:
        """only symbols"""
        return set([node.name for node in self if node.name and not node.children])

    @property
    def user_functions(self) -> Set[str]:
        """all functions that are not predefined in libsbml mathml"""
        return set([node.name for node in self if node.wrapped.isUserFunction()])

    def __int__(self) -> int:
        if not self.is_number:
            raise ASTNodeError('ASTNode is not a number')
        return int(self.value)

    def __float__(self) -> float:
        if not self.is_number:
            raise ASTNodeError('ASTNode is not a number')
        return float(self.value)

    def __str__(self) -> str:
        return self.string

    def __repr__(self):
        return f"<{self.__class__.__name__}: {self.string}>"

    def __iter__(self):
        """iterate the entire ASTNode tree"""
        yield self
        for i in range(self.wrapped.getNumChildren()):
            yield from self.__class__(self.wrapped.getChild(i))

    def __eq__(self, other) -> bool:
        # IMPORTANT: this is used for equality checking, e.g. comparing SBMLDocuments
        # this class is extended and partly overwritten in toolbox/math/ast_node to facilitate expression building
        # NOTE: value comparison doesn't check the type (e.g. 3 == 3.0 == 3e0, as per python convention)
        if not isinstance(other, type(self)):
            return NotImplemented  # Delegate comparison to the other instance's __eq__.
        if self.mathml_string == other.mathml_string:
            # fast and avoids internal renaming on the libsbml part (e.g. ceil(x) == ceiling(x))
            # or plus(x, y, z) == plus(plus(x, y), z), which are not the same in libsbml but are in mathml
            return True
        return are_equal_ast_nodes(self.wrapped, other.wrapped)

    def __ne__(self, other) -> bool:
        # != operator
        return not self == other

    # def substitute(self, symbol, other):
    #     """
    #     replace & lambda replace. Inplace operation
    #     function definition is always a lambda
    #     """
    #     for node in self.children[-1] if self.is_lambda else self:
    #         if node.name == symbol:
    #             if other.wrapped.isLambda():
    #                 children, other_math = other.children[:-1], other.children[-1].copy()
    #                 if len(node.children) is not len(children):
    #                     raise ASTNodeError(f'unequal number of arguments, cannot substitute expressions')
    #                 for node_child, other_child in zip(node.children, children):
    #                     other_math.replace(other_child.name, node_child.copy())
    #                     if self.is_lambda:  # we replace only the expression
    #                         self.replace_child(-1, other_math)
    #                     else:  # we change the current node inplace
    #                         for k, v in filter(lambda x: x[1], other_math.as_kwargs.items()):
    #                             setattr(node, k, v)
    #             else:
    #                 node.replace(symbol, other)


def test_replacement():
    from sbml_tools.wrappers.utils import check

    # replacing symbol
    node = ASTNode.from_string('x')
    other = ASTNode.from_string('y')
    symbol = 'x'
    node.replace(symbol, other)
    # replace(node, symbol, other)
    assert node == ASTNode.from_string('y')

    node = ASTNode.from_string('f(x)')
    other = ASTNode.from_string('y')
    symbol = 'x'
    node.replace(symbol, other)
    assert node == ASTNode.from_string('f(y)')

    node = ASTNode.from_string('f(x) + f(x)')
    other = ASTNode.from_string('y')
    symbol = 'x'
    node.replace(symbol, other)
    assert node == ASTNode.from_string('f(y) + f(y)')

    node = ASTNode.from_string('f(x + f(z + x))')
    other = ASTNode.from_string('y')
    symbol = 'x'
    node.replace(symbol, other)
    assert node == ASTNode.from_string('f(y + f(z + y))')

    node = ASTNode.from_string('f(x)')
    other = ASTNode.from_string('g(x)')
    symbol = 'x'
    node.replace(symbol, other)
    assert node == ASTNode.from_string('f(g(x))')

    # need to substitute
    node = ASTNode.from_string('f(y)')
    other = ASTNode.from_string('g(x)')
    symbol = 'f'
    node.replace(symbol, other)
    assert node == ASTNode.from_string('f(y)')  # no replacement made

    # inline function call
    node = ASTNode.from_string('f1(x, y)')  # user function
    assert node.user_functions == {'f1'}
    other = ASTNode.from_string('lambda(a, b, a * b)')  # function definition 'f1'
    symbol = 'f1'
    node.substitute(symbol, other)
    assert node == ASTNode.from_string('x * y')

    # recursive substitution
    node = ASTNode.from_string('f1(x, f1(y, z))')  # user function
    assert node.user_functions == {'f1'}
    other = ASTNode.from_string('lambda(a, b, a * b)')  # function definition 'f1'
    symbol = 'f1'
    node.substitute(symbol, other)
    assert node == ASTNode.from_string('x * (y * z)')

    # as per function_definition
    node = ASTNode.from_string('lambda(x, f3(x))')
    other = ASTNode.from_string('lambda(x, true)')
    symbol = 'f3'
    self = node
    node.substitute(symbol, other)
    assert node == ASTNode.from_string('lambda(x, true)')

