
from abc import ABC
from typing import Optional
from sbml_tools.wrappers import SBase, Species, utils


class SimpleSpeciesReference(SBase, ABC):
    """
    abstract class for SpeciesReference and ModifierSpeciesReference
    """
    @property
    def species(self) -> str:
        return self.wrapped.getSpecies()

    @species.setter
    def species(self, species: str):
        utils.check(self.wrapped.setSpecies(species))

    @utils.model_property
    def get_species(self) -> Species:
        return self.model.species[self.species]

    def __repr__(self):
        return f"""<{' '.join([self.__class__.__name__, self.species])}>"""
