
from typing import Union
import libsbml
from sbml_tools.wrappers import Rule, utils
from sbml_tools.wrappers import Compartment, Species, Parameter
from sbml_tools.wrappers.species_reference import SpeciesReference


class AssignmentRule(Rule):
    """
    left-hand side is a scalar:
        x = f(V), where f is some arbitrary function and V is a vector of symbols that does not include x

    Used to set values of variables:
        - species: sets amount of concentration (units must match species units)
          there may not be a species reference referencing the same object, unless boundary condition == True
        - species reference: sets stoichiometry of the reactant or product (units should be dimensionless)
        - compartment: sets size (units must match compartment units)
        - parameter: sets value (units must match parameter units)
    """
    required = ['variable']
    _eq_attrs = ['variable', 'math', 'id_attribute']

    def __init__(self, assignment_rule: libsbml.AssignmentRule = None, **kwargs):
        super().__init__(libsbml.AssignmentRule, assignment_rule, **kwargs)

    @property
    def variable(self) -> str:
        return self.wrapped.getVariable()

    @variable.setter
    def variable(self, variable: str):
        utils.check(self.wrapped.setVariable(variable))

    @utils.model_property
    def object(self) -> Union[Compartment, Species, SpeciesReference, Parameter]:
        return self.model.get(self.variable)

    def __repr__(self):
        return f"""<{self.__class__.__name__} {self.variable}>"""
