# Wrappers
This folder contains a series of wrappers for libSBML objects.
It thinly wraps, setting the original libSBML objects as the instances accessible via the .wrapped attribute. 
This yields a clean pythonic interface, where the other attributes all reference the underlying .wrapped object,
yet still granting access to the original attributes via .wrapped as well whenever needed.

TODO: 
- revise xml_base.py (annotations, notes, message)
- revise citation class
- finalize SBase class attributes 
  - Notes: XHTML interface is far from optimal, but not highly relevant to me (also affects Constraint Messages)
  - Annotation: meta_id is set if not present, which affects object comparison via ==, should yield warning?
- also many individual scripts have TODO's

## wrapper.py
Abstract base class for all wrappers. Any class deriving needs to set the following class attributes:
- required
- _eq_attrs
 
Initialization can be performed:
- using no arguments: create a new instance. Either through an existing object, in which case it will 
adopt the same level and version, or otherwise defaults to L3V2.
- using an existing libsbml object instance, and wrapping
- using a new set of keyword arguments (kwargs), in which case all required attributes must be set. 
Assignment through arguments (args) is impossible (explicit is better than implicit).

Additional features:
- allows for deep copying objects without 'TypeError: can't pickle SwigPyObject objects'
- equality checking using '==', see the \_\_eq__ method on this object
- returning differences between two instance attributes of the same type for easy comparison / debugging

## sbase.py
### SBase
Base class those that wrap libsbml.SBase instances. Attributes
- id, name, meta_id, sbo_term, notes, annotation, history, cv_terms
- level, version, document, model, packages
- TODO: serialization / deserialization 

### MathBase
extends SBase with math (ASTNode) getter and setter

## sbo_term.py 
Not a class in libsbml. 
This class validates whether a valid sbml SBO term is assigned. Also allows inspection of allowed terms,
as well as predecessors / successors and simple paths of all SBO terms through the directed acyclic graph.

## xml_base.py
uses lxml to facilitate XHTML element access / editing, used for sbase.annotation, sbase.notes and constraint.message

### XMLBase
- retrieve all tags, text, attributes, comments, entities
- xpath, findall_ functions (using xpath)
- namespace manipulation

### XHTML
extending XMLBase with some convenience functions:
- add / get: paragraph, ordered list, table, citation
- TODO: functions to remove / strip parts

## notes_term.py
wraps an libsbml.SBase class to access Notes, extends XHTML. 

## annotations.py

### CVTerm
Controlled vocabulary terms (MIRIAM identifiers)
- setting by int and str
- checks validity of: qualifier type & recourse identifiers
- iterate nested terms (depth-first pre-order tree traversal)
- equality checking nested terms

### Creator
part of model history
- attributes: given name, family name, email, organisation
- checks if email address is of a valid format

### Date
part of model history
- if created anew sets current time and date by default

### History
history of the model object
- creators, creation date, modifier dates

### Annotation
contains cv_terms and history
- if meta id not present, creates a unique and reproducible meta id

