
from typing import List

import libsbml
from sbml_tools.wrappers import Wrapper, Creator, Date, utils


class HistoryError(Exception):
    pass


class History(Wrapper):
    """
    NOTE: one cannot "set" modifier dates or creators, only add them, and not remove them
    """

    required = []
    _eq_attrs = ['creators', 'created_date', 'modified_dates']

    def __init__(self, history: libsbml.ModelHistory = None, **kwargs):
        super().__init__(libsbml.ModelHistory, history, **kwargs)

    @property
    def has_required_attributes(self) -> bool:
        return self.wrapped.hasRequiredAttributes()

    @property
    def creators(self) -> List[Creator]:
        # history.getListCreators()
        return list(map(Creator, self.wrapped.getListCreators()))

    @creators.setter
    def creators(self, creators: List[Creator]):
        for creator in creators:
            utils.check(self.wrapped.addCreator(creator.wrapped))

    @property
    def created_date(self) -> Date:
        return Date(self.wrapped.getCreatedDate())

    @created_date.setter
    def created_date(self, date: Date):
        utils.check(self.wrapped.setCreatedDate(date.wrapped))

    @property
    def modified_dates(self) -> List[Date]:
        return list(map(Date, self.wrapped.getListModifiedDates()))

    @modified_dates.setter
    def modified_dates(self, dates: List[Date]):
        for date in dates:
            utils.check(self.wrapped.setModifiedDate(date.wrapped))

    def validate(self):
        """required are creators, creation_date and modifier_dates"""
        if not self.has_required_attributes:
            raise HistoryError('not all required attributes are set')

    def __repr__(self):
        return f"""
        Creators       : {self.creators}
        Creation date  : {self.created_date}
        Modifier dates : {self.modified_dates} 
        """
