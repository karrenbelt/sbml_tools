from abc import ABC, abstractmethod
from typing import Tuple, Union, List, Iterable, Optional
import libsbml
import inspect
from sbml_tools import settings
from sbml_tools.toolbox import read_sbml, check
from sbml_tools.wrappers import SBase, utils

# TODO: overwrite all list methods:
#  clear, copy, count, insert, reverse, sort


class ListOf(SBase, ABC):  # TODO: stopped inheriting from list
    """
    abstract base class lists
    behaves somewhat like an OrderedDict, as all elements have unique identifiers.
    ensures that one cannot add another element with the same id as element already in the list
    """
    def __init__(self, list_of_class: libsbml.ListOf, list_of: Union[libsbml.ListOf, Iterable[libsbml.SBase]] = None):
        if isinstance(list_of, (libsbml.ListOf, type(None))):
            super().__init__(list_of_class, list_of)
        else:  # TODO: now required to initialize from an iterable
            super().__init__(list_of_class, None)
            self.extend(list_of)

    @property
    @abstractmethod
    def _contained_type(self):
        raise NotImplementedError(f'_contained_type not implemented')

    def get_members_attribute(self, attr: str) -> List:
        return list(map(lambda x: getattr(x, attr), self))

    @property
    def ids(self) -> List[str]:
        return [item.id for item in self]

    def append(self, item: SBase):
        # libsbml.SBMLTypeCode_toString(15, 'core')
        if not isinstance(item, self._contained_type):
            raise TypeError(f'item must be {self._contained_type}, got {type(item)}')
        elif item.id and self.wrapped.getElementBySId(item.id):
            raise ValueError(f'already in list: {item}')
        check(self.wrapped.append(item.wrapped))

    def extend(self, items: Iterable[SBase]):
        for item in items:
            self.append(item)

    def get(self, key: Union[str, int], default=None) -> Optional[SBase]:
        item = utils.wrap(self.wrapped.get(key))
        return item if item else default

    def pop(self, index=-1):
        return self.remove(index) if index >= 0 else self.remove(len(self) + index)

    def index(self, item: Union[str, int, SBase], start=0, stop=9223372036854775807):
        obj = item if isinstance(item, SBase) else self[item]
        return list(self).index(obj if obj else item, start, stop)

    def remove(self, item: Union[str, int]):
        if self[item]:  # return [utils.wrap(self.wrapped.remove(item)) for item in items if self[item]]
            return utils.wrap(self.wrapped.remove(item))

    def clear(self):
        self.wrapped.clear()

    def __getitem__(self, item: Union[int, slice, str]):  # can now index, slice, or use a key
        if isinstance(item, int):
            return utils.wrap(self.wrapped[item])
        elif isinstance(item, slice):  # to prevent error if slice is too large
            item = slice(item.start, min(len(self), item.stop) if isinstance(item.stop, int) else item.stop, item.step)
            return list(self.wrapped)[item]  # cast to list, otherwise item.step is ignored. Don't return ListOf
        elif isinstance(item, str):
            object = utils.wrap(self.wrapped.get(item))
            if not object:
                raise KeyError(f'{item} not in {self}')
            return object
        raise ValueError(f'unexpected item: {item}')

    def __getattr__(self, attr):
        obj = self.get(attr)
        if not obj:
            raise AttributeError(f'{attr}')
        return obj

    def __add__(self, other):
        list_of = self.copy()
        list_of.extend(other)
        return list_of

    def __len__(self):
        return self.wrapped.size()

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented  # Delegate comparison to the other instance's __eq__.
        return (all([getattr(self, attr) == getattr(other, attr, None) for attr in self._base_eq_attrs])
                and list(self) == list(other))

    def __iter__(self):  # make it behave like a python list
        yield from map(utils.wrap, self.wrapped)

    # def __str__(self):
    #     return '\n'.join(map(str, self))


def test():

    from sbml_tools.wrappers import Species

    class TestListOf(ListOf):

        _contained_type = Species

    s1 = Species()
    s1.id = 's1'
    s2 = Species()
    s2.id = 's2'
    s3 = Species()
    s3.id = 's3'

    list_of_species = TestListOf(libsbml.ListOfSpecies)

    list_of_species.append(s1)
    # list_of_species.append('a') # error as should
    # list_of_species.append(s1) # error as should

    list_of_species.extend([s2, s3])
    assert len(list_of_species) == 3

    assert list_of_species.get('s1') == s1
    # libsbml getter does not return the same object
    assert list_of_species.get('s1') is not s1

    assert list(list_of_species) == [s1, s2, s3]  # works as desired

    list_of_species2 = list_of_species.copy()
    assert list_of_species == list_of_species2
    assert list_of_species.get('s3') == list_of_species2.get('s3')
    assert not list_of_species.get('s3') is list_of_species2.get('s3')

    # beware the species variables floating around here do no longer correspond to those in the list though
    assert s3 is not list_of_species.get('s3') and s3 is not list_of_species2.get('s3')
    s3 = list_of_species.get('s3')
    s3.name = 'species1'
    assert not list_of_species == list_of_species2

    for s in list_of_species:
        print(s)
    print(type(s))