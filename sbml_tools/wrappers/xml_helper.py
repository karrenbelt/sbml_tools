
import sys
from typing import Optional, List, Union, Tuple, Iterable
from more_itertools import consume

from lxml import etree
from lxml.builder import ElementMaker
import pandas as pd

from sbml_tools.wrappers import utils
from sbml_tools.wrappers.xml_base import XMLBase

# TODO: this needs a rework

# namespaces
XHTML_NAMESPACE = "http://www.w3.org/1999/xhtml"

NSMAP = {'xhtml': XHTML_NAMESPACE}  # default mapping, no namespace
# E = ElementMaker(namespace=XHTML_NAMESPACE,  nsmap=NSMAP)
xhtml_element_maker = ElementMaker(nsmap=NSMAP)

# since it is part of an SBML document, a minimal template suffices
# XHTML_TEMPLATE = lambda: E


class XHTML(XMLBase):
    """
    This class is inherited by Notes (part of any SBase derived class) and Message (part of Constraint)
    It provides some helper functions for adding XHTML to these objects, and retrieving it.

    1. If accessed through a parent object, add_ functions can be used to set content directly on the parent object,
       for example: species.notes.add_paragraph('some text') - This is why we keep a reference to the parent around
    2. Otherwise the Notes of Message instance needs to be set on the object after creation

    NOTE: a _get method is not necessary as the object is wrapped in the getter of SBase and Constraint
    """

    @property
    def parent(self):
        raise NotImplementedError(f'one must overwrite this parent property')

    def _set(self, xhtml: str):
        raise NotImplementedError(f'one must overwrite this _setter method')

    # adding simple elements to notes
    def add_paragraph(self, s: str):
        self.root.append(xhtml_element_maker.p(s))
        self._set(self.as_string)

    def add_ordered_list(self, ordered_list: Union[list, tuple]):
        """adding an ordered list (python lists and tuples are ordered by default)"""
        self.root.append(xhtml_element_maker.ol(*map(xhtml_element_maker.li, ordered_list)))
        self._set(self.as_string)

    def add_unordered_list(self, unordered_list: set):
        """adding an un ordered list (python set)"""
        self.root.append(xhtml_element_maker.ul(*map(xhtml_element_maker.li, unordered_list)))
        self._set(self.as_string)

    def add_table(self, df: pd.DataFrame):
        node = etree.fromstring(df.to_html())
        node.set('xmlns', xhtml_element_maker._nsmap['xhtml'])
        self.root.append(node)
        self._set(self.as_string)

    def add(self, *data: Union[str, list, tuple, set, pd.DataFrame]):
        for info in data:
            if isinstance(info, str):
                self.add_paragraph(info)
            elif isinstance(info, set):
                self.add_unordered_list(info)
            elif isinstance(info, (list, tuple)):
                self.add_ordered_list(info)
            elif isinstance(info, pd.DataFrame):
                self.add_table(info)
            else:
                NotImplementedError(f'data structure not accepted: {type(data)}')

    # getting simple elements from notes
    @property
    def paragraphs(self) -> List[str]:
        return list(map(lambda x: x.text, self.findall_by_tag('p')))

    @paragraphs.setter
    def paragraphs(self, paragraphs: Union[Iterable[str], str]):
        self.remove_paragraphs()
        consume(map(self.add_paragraph, paragraphs if utils.is_non_str_sequence(paragraphs) else [paragraphs]))

    @property
    def ordered_lists(self) -> List[list]:
        """only retrieves ordered lists, not unordered list or <li> elements not in an ordered list"""
        return list(map(lambda x: list(filter(lambda x: x.strip(), x.itertext())), self.findall_by_tag('ol')))

    @ordered_lists.setter
    def ordered_lists(self, lists: Union[Iterable[list], list]):
        self.remove_ordered_lists()
        consume(map(self.add_ordered_list, lists if utils.is_non_str_sequence(lists[0]) else [lists]))

    @property
    def unordered_lists(self) -> List[set]:
        return list(map(lambda x: set(filter(lambda x: x.strip(), x.itertext())), self.findall_by_tag('ul')))

    @unordered_lists.setter
    def unordered_lists(self, sets: Union[Iterable[set], set]):
        self.remove_unordered_lists()
        consume(map(self.add_unordered_list, sets if utils.is_non_str_sequence(next(iter(sets))) else [sets]))

    @property
    def tables(self) -> List[pd.DataFrame]:
        return list(map(lambda x: pd.read_html(etree.tostring(x), index_col=0)[0].infer_objects(),
                        self.findall_by_tag('table')))

    @tables.setter
    def tables(self, tables: Union[List[list], list]):
        self.remove_tables()
        consume(map(self.add_table, [tables] if isinstance(tables, pd.DataFrame) else tables))

    @staticmethod
    def remove_element(element):
        element.getparent().remove(element)

    def remove_paragraphs(self):
        consume(map(self.remove_element, self.findall_by_tag('p')))

    def remove_ordered_lists(self):
        consume(map(self.remove_element, self.findall_by_tag('ol')))

    def remove_unordered_lists(self):
        consume(map(self.remove_element, self.findall_by_tag('ul')))

    def remove_tables(self):
        consume(map(self.remove_element, self.findall_by_tag('table')))

    # validation
    def _validate_xhtml(self, xhtml: Optional[Union[etree._Element, str]] = None):
        """
        this only works for HTML tags (e.g. Notes),
        not for other namespaces (rdf, bqmodel, bqbiol, etc. as in Annotations)
        """
        if not xhtml:
            xhtml = self.root
        if isinstance(xhtml, etree._Element):
            xhtml = etree.tostring(xhtml).decode()
        etree.fromstring(xhtml, etree.HTMLParser(recover=False))

    def validate(self):
        if self.doctype:
            raise ValueError('notes may not have an DOCTYPE declaration, such as: <!DOCTYPE html>')
        if self.xml_version:
            raise ValueError(
                f'notes may not have an XML declaration, such as: <?xml version="1.0" encoding="UTF-8"?>')
        try:
            self._validate_xhtml()
        except etree.XMLSyntaxError as e:
            message = e.msg + '\n\nInvalid XHTML\n' + \
                      'Validate on https://www.w3schools.com/html/html_validate.html for more info'
            raise type(e)(message, e.code, *e.position, filename=None).with_traceback(sys.exc_info()[2])
        except Exception as e:
            raise e
