
from abc import ABC
import libsbml
from sbml_tools.wrappers import Rule


class AlgebraicRule(Rule, ABC):
    """
    left-hand side is zero:
        0 = f(W), where f is some arbitrary function and W is a vector of symbols

    The model is not allowed to be overdetermined, nor contain algebraic loops.
        - entities in this construct must at least not have the attribute constant == True,
        - there also may not be a assignment or rate rule for it
        - for species, they may not be determined by reactions; they must either have boundary condition == False
          or else not be involved in any reaction (as a reactant / product)
        - reaction identifiers can be referenced, but not determined by algebraic rules
    """
    required = []
    _eq_attrs = ['math', 'id_attribute']

    def __init__(self, algebraic_rule: libsbml.AlgebraicRule = None, **kwargs):
        super().__init__(libsbml.AlgebraicRule, algebraic_rule, **kwargs)