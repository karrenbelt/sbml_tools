
from typing import Union, List
import libsbml
from sbml_tools.wrappers.utils import check, model_property, substitute_lambda
from sbml_tools.wrappers import ASTNode, ListOf, SBase
import networkx as nx


class FunctionDefinitionError(Exception):
    pass


class FunctionDefinition(SBase):
    """
    A function definition cannot reference parameters or other model quantities;
    meaning all symbols they use are passed as parameters to them explicitly.

    recursive and mutually-recursive functions are not permitted.
    the lambda element can only be used in function definitions, and not contain another lambda itself.
    function definitions can be used in other function definitions, as well as elsewhere

    " The optional math element is a container for MathML content that defines the function. The content of
    this element can only be a MathML lambda element or a MathML semantics element containing a lambda
    element. FunctionDefinition is the only place in SBML Level 3 Core where a lambda element can be used. "

    examples:
        1. FunctionDefinition(id='x_times_y', math='lambda(x, y, x * y)')
        if 1. exists, then this definition can be referenced in another function definition:
        2. FunctionDefinition(id='dependent_func', math='lambda(x_times_y, x, y, x_times_y(x, y) + x + y')

    # TODO: dependency graph to figure out if function adding a definition leads to cyclic dependencies
    """
    required = ['id']
    _eq_attrs = ['math']

    def __init__(self, function_definition: libsbml.FunctionDefinition = None, **kwargs):
        super().__init__(libsbml.FunctionDefinition, function_definition, **kwargs)

    @property
    def math(self) -> ASTNode:
        if self.wrapped.isSetMath():
            return ASTNode(self.wrapped.getMath())

    @math.setter
    def math(self, math: Union[ASTNode, str]):
        if isinstance(math, str):
            math = ASTNode.from_string(math)
        if not math.type == libsbml.AST_LAMBDA:
            raise FunctionDefinitionError(f'function definition must be a lambda function')
        check(self.wrapped.setMath(math.wrapped))


class ListOfFunctionDefinitions(ListOf):

    _contained_type = FunctionDefinition

    def __init__(self, list_of_function_definitions: libsbml.ListOfFunctionDefinitions = None):
        super().__init__(libsbml.ListOfFunctionDefinitions, list_of_function_definitions)

    @model_property
    def edges(self) -> List[str]:
        """ adjacency list of function definition referenced in others """
        edges = []
        for identifier in self.ids:
            for function_definition in self.model.function_definitions:
                if function_definition.math and identifier in function_definition.math.names:
                    edges.append((function_definition.id, identifier))
        return edges

    @property
    def graph(self):
        """ directed graph of function definition dependencies """
        return nx.DiGraph(self.edges)

    @property
    def is_directed_acyclic_graph(self):
        return nx.is_directed_acyclic_graph(self.graph)

    def substitute_dependencies(self):
        """ substitute all references to other function definitions """
        assert self.is_directed_acyclic_graph, f'function definitions contain circular references'
        for (parent, child) in nx.line_graph(self.graph):
            substitute_lambda(self[parent].math, child, self[child].math)
