
import libsbml
from sbml_tools.wrappers import MathBase


class Priority(MathBase):
    """
    When two events occur simultaneously, this is used to determine the order of event execution
    Priority math must evaluate to a dimensionless number, positive, negative or zero
    The higher the value, the higher the priority
    """
    required = []
    _eq_attrs = ['math']

    def __init__(self, priority: libsbml.Priority = None, **kwargs):
        super().__init__(libsbml.Priority, priority, **kwargs)

    def __repr__(self):
        return f"""{' '.join([self.__class__.__name__, self.id]).strip()}: {self.math}"""
