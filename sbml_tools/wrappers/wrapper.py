
from typing import Tuple, List
import inspect
import copy
import numbers
import cmath
from abc import ABC, abstractmethod
import libsbml

from sbml_tools.wrappers import utils
from sbml_tools import config
from sbml_tools.utils import FrozenClass

# TODO: revise wrapper __init__


defaults = config.Defaults()


class WrapperError(Exception):
    pass


class CopyError(Exception):
    pass


class Wrapper(ABC, FrozenClass):
    """
    Any class wrapping an libSBML class instance should inherit from this
    It allows for
    - wrapping existing sbml objects
    - creating them anew empty
    - creating them from kwargs, in which case required attributes are enforced to be set

    Note that is a level and version are not passed as kwargs, they will default to SBML_LEVEL and SBML_VERSION
    """

    _base_eq_attrs = []  # only defined on SBase

    def __init__(self, wrapped_class, wrapped=None, **kwargs):
        if not inspect.isclass(wrapped_class) or not wrapped_class.__module__ == 'libsbml':
            raise WrapperError(f'Wrapper must wrap a libsbml class, not {wrapped_class}')
        self._wrapped_class = wrapped_class

        if wrapped_class is libsbml.SBase:
            return  # raise WrapperError(f'cannot instantiate from libsbml.SBase')
        if wrapped and not isinstance(wrapped, wrapped_class):
            raise WrapperError(f'expecting {wrapped_class}, received {type(wrapped)}')
            # raise WrapperError(f'currently not allowing construction from args, use kwargs instead '
            #                    f'or alternatively pass an libsbml instance of {wrapped_class} ')
        if bool(wrapped) and bool(kwargs):
            raise WrapperError(f'cannot initialize using both a {self._wrapped_class} and **kwargs')

        if wrapped:
            self.wrapped = wrapped
        else:
            # if model -> model.create ? associated the model
            if issubclass(wrapped_class, libsbml.SBase):
                self.wrapped = wrapped_class(kwargs.pop('level', config.DEFAULT_SBML_LEVEL),
                                             kwargs.pop('version', config.DEFAULT_SBML_VERSION))
            else:
                self.wrapped = wrapped_class()  # e.g. Date, Creator, CVTerm
            # now self.wrapped exists, construct from kwargs by setting attributes
            if kwargs:
                for attr in self.required:
                    value = kwargs.pop(attr, None)
                    if value is None:  # take from defaults if these are set
                        try:
                            value = getattr(getattr(defaults, self.wrapped.__class__.__name__.lower()), attr, None)
                        except Exception as e:
                            raise WrapperError(f"required attribute '{attr}' not given")

                    setattr(self, attr, value)  # uses setters
                # setting certain attributes requires checking of others
                # therefore the order of these class attributes matters, and we cannot use a set operation
                eq_attrs = [attr for attr in self._eq_attrs if attr not in self._base_eq_attrs]
                for attr in self._base_eq_attrs + eq_attrs:
                    value = kwargs.pop(attr, None)
                    if value is not None:
                        setattr(self, attr, value)
                # now if there are still kwargs, these are not technically part of that specific class instance
                # yet there might be a setter, which we will try to invoke
                for attr, value in kwargs.copy().items():  # iterate a copy, pop the original
                    if hasattr(self, attr) and value is not None:
                        setattr(self, attr, kwargs.pop(attr))
                # whatever is left was not recognized
                if kwargs:
                    raise ValueError(f'leftover kwargs found for {self}: {kwargs}')

        # self.__previous = None
        if config.FREEZE_CLASSES_AFTER_INIT:
            self._freeze()  # no new attributes after this point.

    def _get_wrapped(self):
        return self._wrapped

    def _set_wrapped(self, value):
        if not isinstance(value, self._wrapped_class):
            raise WrapperError(f"wrapped must be an instance of {self._wrapped_class} not {type(value)}")
        self._wrapped = value

    wrapped = property(_get_wrapped, _set_wrapped)

    def copy(self):
        return copy.deepcopy(self)

    def __deepcopy__(self, memo):  # deal with SwigPyObjects
        result = self.__class__.__new__(self.__class__)
        memo[id(self)] = result
        for k, v in self.__dict__.items():
            if getattr(v, '__module__', None) == 'libsbml':
                if inspect.isclass(v):
                    setattr(result, k, v)
                elif hasattr(v, 'clone'):
                    setattr(result, k, v.clone())
                elif hasattr(v, 'deepCopy'):
                    setattr(result, k, v.deepCopy())
                else:
                    raise CopyError(f"don't know how to copy libsbml object {k} from {self}")
            else:
                setattr(result, k, copy.deepcopy(v, memo))
        return result

    @property
    def _eq_attrs(self):  # TODO: rename to 'other_attributes'?
        raise NotImplementedError("overwrite __eq__ or implement attribute class attribute '_eq_attrs '" +
                                  f"to be compared for {self.__class__.__name__}")

    @property
    def equality_attributes(self) -> List[str]:
        # list of attributes that are used in __eq__ and .difference
        return self._base_eq_attrs + self._eq_attrs

    @property
    def as_kwargs(self):
        return {k: getattr(self, k) for k in self.equality_attributes}

    @property
    def required(self):  # TODO: rename to required_attributes?
        raise NotImplementedError(f"set a list of required attributes as a class attribute " +
                                  f"for {self.__class__.__name__}")

    @property
    def parent(self):
        return utils.wrap(self.wrapped.getParentSBMLObject())

    def __eq__(self, other) -> bool:
        # TODO: use of cmath.close is perhaps not proper; make configurable
        #  NOTE for implementing hash: a == b → hash(a) == hash(b) (assumption should hold)
        if not isinstance(other, type(self)):
            return NotImplemented  # Delegate comparison to the other instance's __eq__.
        if getattr(self, 'packages', None) or getattr(other, 'packages', None):
            raise NotImplementedError('extensions not implemented')

        # not all objects have level and version (e.g. Date in annotations)
        # those that have it can only contain or be associated with those of the same level; we check only top level
        if not (getattr(self, 'level', None) == getattr(other, 'level', None)
                or getattr(self, 'version', None) == getattr(other, 'version', None)):
            return False

        check_attrs = self._base_eq_attrs + self._eq_attrs

        # not very legible: probably should refactor this after having added the number comparison
        for attr in check_attrs:
            self_attr = getattr(self, attr)
            other_attr = getattr(other, attr, None)
            if isinstance(self_attr, numbers.Number):
                if cmath.isnan(self_attr):
                    if not cmath.isnan(other_attr):
                        return False
                elif not cmath.isclose(self_attr, other_attr,  # TODO: this is probably not proper practice
                                       rel_tol=config.IS_CLOSE_RELATIVE_TOLERANCE,
                                       abs_tol=config.IS_CLOSE_ABSOLUTE_TOLERANCE):
                    return False
            else:
                if not self_attr == other_attr:
                    return False
        return True

        # return all([getattr(self, attr) is getattr(other, attr, None) or
        #             cmath.isclose(getattr(self, attr), getattr(other, attr, float('nan')))
        #             if isinstance(getattr(self, attr), numbers.Number) else
        #             getattr(self, attr) == getattr(other, attr, None)
        #             for attr in check_attrs])

    def difference(self, other):
        """function to easily spot differences between two objects"""
        if not isinstance(other, type(self)):
            raise TypeError(f'objects {other} and {self} are not of the same type')
        if getattr(self, 'packages', None) or getattr(other, 'packages', None):
            raise NotImplementedError('extensions not implemented')

        differences = {}
        if not getattr(self, 'level', None) == getattr(other, 'level', None):
            differences['level'] = [getattr(self, 'level', None), getattr(other, 'level', None)]
        if not getattr(self, 'version', None) == getattr(other, 'version', None):
            differences['version'] = [getattr(self, 'version', None), getattr(other, 'version', None)]

        check_attrs = self._base_eq_attrs + self._eq_attrs
        for attr in check_attrs:
            self_attr = getattr(self, attr)
            other_attr = getattr(other, attr, None)
            if self_attr is other_attr or self_attr == other_attr:
                continue
            if isinstance(self_attr, numbers.Number) and isinstance(other_attr, numbers.Number):
                if cmath.isnan(self_attr) and cmath.isnan(other_attr):
                    continue
            differences[attr] = [self_attr, other_attr]
        return differences

    def __ne__(self, other):
        return not self == other

    # def __hash__(self):  # putting this on sbase
    #     attributes = [getattr(self, attr) for attr in self._base_eq_attrs + self._eq_attrs]
    #     attributes = tuple(map(lambda x: tuple(x) if isinstance(x, list) else x, attributes))
    #     return hash(tuple(attributes))

    # def __enter__(self):
    #     self.__previous = self
    #     return self.copy()
    #
    # def __exit__(self, exc_type, exc_val, exc_tb):
    #     return self.__previous
