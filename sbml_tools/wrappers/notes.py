#############
### NOTES ###
#############
# both notes and annotations should ideally make use of lxml trees
from lxml import etree
import libsbml
from sbml_tools.wrappers import Wrapper, utils
from sbml_tools.wrappers.xml_helper import XHTML


class Notes(XHTML):
    """
    Although the xhtml namespace can be declared on the top level of the SBMLDocument and subsequently used a prefix
    for all elements in the notes, we opt to declare the xhtml namespace on the top level of the notes element itself.

    SBML rules for notes:
    Proper XHTML for the Notes attribute should NOT contain a xml or docstring declaration

    xhtml:
     - <html>, <head>, <title>, and <body> are mandatory
     - XHTML elements must be properly nested
     - XHTML elements must always be closed
     - XHTML elements must be in lowercase
     - XHTML documents must have one root element
     - Attribute names must be in lower case
     - Attribute values must be quoted
     - Attribute minimization is forbidden

    NOTE: This class does not directly modify the SBase
    """

    def __init__(self, parent: 'SBase' = None):

        self._parent = parent
        if parent and parent.wrapped.isSetNotes():
            self.root = etree.fromstring(parent.wrapped.getNotes().toXMLString())
        else:
            self.root = etree.fromstring('<notes/>')

    @property
    def parent(self):
        return self._parent

    def _set(self, xhtml_string: str):
        if self.parent:
            self.parent.wrapped.setNotes(xhtml_string)

    def remove(self):
        utils.check(self.parent.unsetNotes())

    # def __repr__(self):
    #     return f"<{' '.join([self.__class__.__name__, self.wrapped.getId(), self.wrapped.getName()]).strip()}>"

