import libsbml
from typing import Tuple, Union, Iterable, List
import functools
from more_itertools import consume
from sbml_tools import settings, config
from sbml_tools.toolbox import read_sbml, check
from sbml_tools.wrappers import SBase, ListOf, utils
from sbml_tools.wrappers.utils import check, check_required_attributes, wrap, method_kwargs
from sbml_tools.wrappers import (
    FunctionDefinition, ListOfFunctionDefinitions,
    Unit, ListOfUnits, UnitDefinition, ListOfUnitDefinitions,
    Compartment, ListOfCompartments,
    Species, ListOfSpecies,
    Parameter, ListOfParameters,
    InitialAssignment, ListOfInitialAssignments,
    Rule, AlgebraicRule, AssignmentRule, RateRule, ListOfRules,
    Constraint, ListOfConstraints,
    Reaction, ListOfReactions, KineticLaw, LocalParameter,
    SpeciesReference, ModifierSpeciesReference,
    Event, ListOfEvents, Trigger, Delay, Priority, EventAssignment,
    ASTNode,
    UnitDefinitionError,
    )


# TODO: consider being stricter on initialization (e.g. disallow empty constraint, rules without math, etc)


def all_unique(x):
    seen = set()
    return not any(i in seen or seen.add(i) for i in x)


def remove_objects(list_of: ListOf, *items: Iterable[Union[str, int]]):
    """
    we remove objects based on their index, starting from the end to preserve the order,
    as not all ListOf objects contain SBase elements that have an identifier
    """
    if not items:  # remove all
        indices = range(len(list_of) - 1, -1, -1)
    else:
        indices = sorted([list_of.index(item) for item in items], reverse=True)
        if not all_unique(indices):
            raise ValueError(f'duplicates indices found for removal')
    removed = [list_of.remove(i) for i in indices][::-1]
    return removed[0] if len(removed) == 1 else removed


class Model(SBase):

    required = []
    _eq_attrs = ['substance_units', 'time_units', 'volume_units', 'area_units',
                 'length_units', 'extent_units', 'conversion_factor',
                 'function_definitions', 'unit_definitions', 'compartments', 'species', 'parameters',
                 'initial_assignments', 'rules', 'constraints', 'reactions', 'events']

    def __init__(self, model: libsbml.Model = None, **kwargs):
        super().__init__(libsbml.Model, model, **kwargs)

    # model units
    @property
    def substance_units(self) -> str:
        if self.wrapped.isSetSubstanceUnits():
            return self.wrapped.getSubstanceUnits()

    @substance_units.setter
    def substance_units(self, substance_units: str):
        utils.ensure_unit_definition(self, substance_units, 'substance')
        check(self.wrapped.setSubstanceUnits(substance_units))

    @property
    def time_units(self) -> str:
        if self.wrapped.isSetTimeUnits():
            return self.wrapped.getTimeUnits()

    @time_units.setter
    def time_units(self, time_units: str):
        utils.ensure_unit_definition(self, time_units, 'time')
        check(self.wrapped.setTimeUnits(time_units))

    @property
    def volume_units(self) -> str:
        if self.wrapped.isSetVolumeUnits():
            return self.wrapped.getVolumeUnits()

    @volume_units.setter
    def volume_units(self, volume_units: str):
        utils.ensure_unit_definition(self, volume_units, 'volume')
        check(self.wrapped.setVolumeUnits(volume_units))

    @property
    def area_units(self) -> str:
        if self.wrapped.isSetAreaUnits():
            return self.wrapped.getAreaUnits()

    @area_units.setter
    def area_units(self, area_units: str):
        utils.ensure_unit_definition(self, area_units, 'area')
        check(self.wrapped.setAreaUnits(area_units))

    @property
    def length_units(self) -> str:
        if self.wrapped.isSetLengthUnits():
            return self.wrapped.getLengthUnits()

    @length_units.setter
    def length_units(self, length_units: str):
        utils.ensure_unit_definition(self, length_units, 'length')
        check(self.wrapped.setLengthUnits(length_units))

    @property
    def extent_units(self) -> str:
        if self.wrapped.isSetExtentUnits():
            return self.wrapped.getExtentUnits()

    @extent_units.setter
    def extent_units(self, extent_units: str):
        utils.ensure_unit_definition(self, extent_units, 'substance')
        check(self.wrapped.setExtentUnits(extent_units))

    @property
    def conversion_factor(self) -> str:
        return self.wrapped.getConversionFactor()

    @conversion_factor.setter
    def conversion_factor(self, conversion_factor: str):
        check(self.wrapped.setConversionFactor(conversion_factor))

    # all model elements
    @property
    def all_elements(self) -> list:
        return list(map(wrap, self.wrapped.getListOfAllElements()))

    # the 10 optional lists that make up the model
    @property
    def function_definitions(self):
        return ListOfFunctionDefinitions(self.wrapped.getListOfFunctionDefinitions())

    @function_definitions.setter
    def function_definitions(self, function_definitions: Iterable[FunctionDefinition]):
        consume(map(self.add_function_definition, function_definitions))

    @property
    def unit_definitions(self):
        return ListOfUnitDefinitions(self.wrapped.getListOfUnitDefinitions())

    @unit_definitions.setter
    def unit_definitions(self, unit_definitions: Iterable[UnitDefinition]):
        consume(map(self.add_unit_definition, unit_definitions))

    @property
    def unit_definition_ids(self):
        return list(map(lambda x: x.id, self.model.unit_definitions))

    @property
    def compartments(self):
        return ListOfCompartments(self.wrapped.getListOfCompartments())

    @compartments.setter
    def compartments(self, compartments: Iterable[Compartment]):
        consume(map(self.add_compartment, compartments))

    @property
    def compartment_ids(self):
        return list(map(lambda x: x.id, self.model.compartments))

    @property
    def species(self):
        return ListOfSpecies(self.wrapped.getListOfSpecies())

    @species.setter
    def species(self, species: Iterable[Species]):
        consume(map(self.add_species, species))

    @property
    def species_ids(self):
        return list(map(lambda x: x.id, self.model.species))

    @property
    def parameters(self):
        return ListOfParameters(self.wrapped.getListOfParameters())

    @parameters.setter
    def parameters(self, parameters: Iterable[Parameter]):
        consume(map(self.add_parameter, parameters))

    @property
    def parameter_ids(self):
        return list(map(lambda x: x.id, self.model.parameters))

    @property
    def initial_assignments(self):
        return ListOfInitialAssignments(self.wrapped.getListOfInitialAssignments())

    @initial_assignments.setter
    def initial_assignments(self, initial_assignments: Iterable[InitialAssignment]):
        consume(map(self.add_initial_assignment, initial_assignments))

    @property
    def rules(self):
        return ListOfRules(self.wrapped.getListOfRules())

    @rules.setter
    def rules(self, rules: Iterable[Rule]):
        consume(map(self.add_rule, rules))

    @property
    def constraints(self):
        return ListOfConstraints(self.wrapped.getListOfConstraints())

    @constraints.setter
    def constraints(self, constraints: Iterable[Constraint]):
        consume(map(self.add_constraint, constraints))

    @property
    def reactions(self):
        return ListOfReactions(self.wrapped.getListOfReactions())

    @reactions.setter
    def reactions(self, reactions: Iterable[Reaction]):
        consume(map(self.add_reaction, reactions))

    @property
    def reaction_ids(self):
        return list(map(lambda x: x.id, self.model.reactions))

    @property
    def events(self):
        return ListOfEvents(self.wrapped.getListOfEvents())

    @events.setter
    def events(self, events: Iterable[Event]):
        consume(map(self.add_event, events))

    @property
    def event_ids(self):
        return list(map(lambda x: x.id, self.model.events))

    # add get remove (requires object inference)
    def add(self, *objects):  # takes it all, but rather convoluted
        for obj in objects:
            if isinstance(obj, Rule):
                check(self.wrapped.addRule(check_required_attributes(obj).wrapped))
            else:
                check(getattr(self.wrapped, f'add{obj.__class__.__name__}')(check_required_attributes(obj).wrapped))

    def get(self, obj_id: str, default=None):
        """
        Does not get by id perse (e.g. not UnitSId). Some objects don't use an id attribute (e.g. rules)
        Return the objects for obj_id if obj_id is in the model, else default.
        Use model[obj_id] for dict-like access.
        """
        element = self.wrapped.getElementBySId(obj_id)
        return wrap(element) if element else default

    def get_unit_definition(self, unit_id: str):
        """ separate namespace: UnitSid instead of Sid """
        return wrap(self.wrapped.getUnitDefinition(unit_id))

    def get_rule_by_variable(self, variable: str):
        return wrap(self.wrapped.getRuleByVariable(variable))

    def get_initial_assignment_by_symbol(self, symbol: str):
        return wrap(self.wrapped.getInitialAssignmentBySymbol(symbol))

    def remove(self, *obj_ids: str):  # .removeFromParentAndDelete() == SIGSEGV error
        objects = map(self.get, obj_ids)
        objects = [obj.parent.remove(obj.id) for obj in objects]
        return objects[0] if len(objects) == 1 else objects

    # individual create, add / remover
    def create_function_definition(self, id: str, math: ASTNode = None, **kwargs) -> FunctionDefinition:
        self.add_function_definition(FunctionDefinition(level=self.level, version=self.version,
                                                        **method_kwargs(locals())))
        return self.function_definitions[-1]

    def add_function_definition(self, function_definition: FunctionDefinition) -> None:
        check(self.wrapped.addFunctionDefinition(check_required_attributes(function_definition).wrapped))

    def remove_function_definitions(self, *function_definitions: Union[str, int]) -> Union[FunctionDefinition, List[FunctionDefinition]]:
        return remove_objects(self.function_definitions, *function_definitions)

    def create_unit_definition(self, id: str, units: Union[Iterable[Unit], Iterable[dict]], **kwargs) -> UnitDefinition:
        self.add_unit_definition(UnitDefinition(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.unit_definitions[-1]

    def add_unit_definition(self, unit_definition: UnitDefinition) -> None:
        if not check_required_attributes(unit_definition).units:  # actually required,
            raise ValueError(f'unit definition must have units set')   # move to UnitDefinition?
        check(self.wrapped.addUnitDefinition(unit_definition.wrapped))

    def remove_unit_definitions(self, *unit_definitions: Union[str, int]) -> Union[UnitDefinition, List[UnitDefinition]]:
        return remove_objects(self.unit_definitions, *unit_definitions)

    def create_compartment(self, id: str, constant: bool, spatial_dimensions: float = None,
                           size: float = None, units: str = None, **kwargs) -> Compartment:
        self.add_compartment(Compartment(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.compartments[-1]

    def add_compartment(self, compartment: Compartment) -> None:
        check(self.wrapped.addCompartment(check_required_attributes(compartment).wrapped))
        utils.ensure_unit_definition(compartment, compartment.units)

    def remove_compartments(self, *compartments: Union[str, int]) -> Union[Compartment, List[Compartment]]:
        return remove_objects(self.compartments, *compartments)

    def create_species(self, id: str, compartment: str, has_only_substance_units: bool,
                       boundary_condition: bool, constant: bool,
                       substance_units: str = None, initial_amount: float = None,
                       initial_concentration: float = None, conversion_factor: str = None, **kwargs) -> Species:
        self.add_species(Species(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.species[-1]

    def add_species(self, species: Species) -> None:
        utils.ensure_compartment_exist(self, species)
        check(self.wrapped.addSpecies(check_required_attributes(species).wrapped))
        utils.ensure_unit_definition(self.get(species.id), species.substance_units)

    def remove_species(self, *species: Union[str, int]) -> Union[Species, List[Species]]:
        return remove_objects(self.species, *species)

    def create_parameter(self, id: str, constant: bool, value: float = None, units: str = None, **kwargs) -> Parameter:
        self.add_parameter(Parameter(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.parameters[-1]

    def add_parameter(self, parameter: Parameter) -> None:
        check(self.wrapped.addParameter(check_required_attributes(parameter).wrapped))

    def remove_parameters(self, *parameters: Union[str, int]) -> Union[Parameter, List[Parameter]]:
        return remove_objects(self.parameters, *parameters)

    def create_initial_assignment(self, symbol: str, math: ASTNode = None, **kwargs) -> InitialAssignment:
        self.add_initial_assignment(InitialAssignment(level=self.level, version=self.version,
                                                      **method_kwargs(locals())))
        return self.initial_assignments[-1]

    @utils.ensure_reference_exists(config.ENFORCE_REFERENCE_EXISTS)
    def add_initial_assignment(self, initial_assignment: InitialAssignment) -> None:
        check(self.wrapped.addInitialAssignment(check_required_attributes(initial_assignment).wrapped))

    def remove_initial_assignments(self, *initial_assignments: Union[str, int]) -> Union[InitialAssignment, List[InitialAssignment]]:
        """ must be done by symbol; there can only be one per symbol, id_attribute is not necessarily unique """
        return remove_objects(self.initial_assignments, *initial_assignments)

    def create_algebraic_rule(self, math: ASTNode = None, **kwargs) -> AlgebraicRule:
        self.add_rule(AlgebraicRule(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.rules[-1]

    def create_assignment_rule(self, variable: str, math: ASTNode = None, **kwargs) -> AssignmentRule:
        self.add_rule(AssignmentRule(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.rules[-1]

    def create_rate_rule(self, variable: str, math: ASTNode = None, **kwargs) -> RateRule:
        self.add_rule(RateRule(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.rules[-1]

    @utils.ensure_reference_exists(config.ENFORCE_REFERENCE_EXISTS)
    def add_rule(self, rule: Union[AlgebraicRule, AssignmentRule, RateRule]) -> None:
        check(self.wrapped.addRule(check_required_attributes(rule).wrapped))

    def remove_rules(self, *rules: Union[str, int]) -> Union[Rule, List[Rule]]:
        """ must be done by variable; there can only be one per variable, id_attribute is not necessarily unique """
        return remove_objects(self.rules, *rules)

    def create_constraint(self, math: ASTNode = None, message = None, **kwargs):
        self.add_constraint(Constraint(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.constraints[-1]

    def add_constraint(self, constraint: Constraint) -> None:
        check(self.wrapped.addConstraint(check_required_attributes(constraint).wrapped))

    def remove_constraints(self, *constraints: Union[str, int]) -> Union[Constraint, List[Constraint]]:
        return remove_objects(self.constraints, *constraints)

    def create_reaction(self, id: str, reversible: bool,
                        reactants: Iterable[SpeciesReference] = None,
                        products: Iterable[SpeciesReference] = None,
                        modifiers: Iterable[ModifierSpeciesReference] = None,
                        kinetic_law: KineticLaw = None, **kwargs):
        self.add_reaction(Reaction(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.reactions[-1]

    def add_reaction(self, reaction: Reaction) -> None:
        utils.ensure_species_exist(self, reaction)
        check(self.wrapped.addReaction(check_required_attributes(reaction).wrapped))

    def remove_reactions(self, *reactions: Union[str, int]) -> Union[Reaction, List[Reaction]]:
        return remove_objects(self.reactions, *reactions)

    def create_event(self, use_values_from_trigger_time: bool, trigger: Trigger = None,
                     priority: Priority = None, delay: Delay = None,
                     event_assignments: Iterable[EventAssignment] = None, **kwargs):
        self.add_event(Event(level=self.level, version=self.version, **method_kwargs(locals())))
        return self.events[-1]

    def add_event(self, event: Event) -> None:
        if not event.trigger:  # otherwise libsbml error: invalidly-constructed
            raise ValueError(f'event must have a trigger set')
        check(self.wrapped.addEvent(check_required_attributes(event).wrapped))

    def remove_events(self, *events: Iterable[Union[str, int]]) -> Union[Event, List[Event]]:
        return remove_objects(self.events, *events)

    @property
    def local_parameters(self) -> List[LocalParameter]:
        """takes precedence over global parameters whose identifier is identical"""
        return [local_parameter for reaction in self.reactions for local_parameter in reaction.local_parameters]

    # @property
    # def differential_equations(self):
    #     from sbml_tools.toolbox.math import ASTNode
    #     states = dict(zip(self.model.species_ids, [([], []) for _ in range(len(self.model.species))]))
    #     for reaction in self.model.reactions:
    #         for reactant in reaction.reactants:
    #             # term = reactant.stoichiometry * ASTNode(reaction.math.wrapped)
    #             states[reactant.species][0].append(reaction.math)
    #         for product in reaction.products:
    #             # term = product.stoichiometry * ASTNode(reaction.math.wrapped)
    #             states[product.species][1].append(reaction.math)
    #
    #     for state, (production, consumption) in states.items():
    #         production = list(filter(None, production))
    #         consumption = list(filter(None, production))
    #         math = production.pop() if production else consumption.pop() if consumption else None
    #         for flux, ast_type in [(production, libsbml.AST_PLUS), (consumption, libsbml.AST_MINUS)]:
    #             for node in flux:
    #                 math = ASTNode(type=ast_type, children=[math, node])
    #         states[state] = math
    #     return list(states.values())

    def __getitem__(self, key: str):
        obj = self.get(key)
        if not obj:
            raise KeyError(f'{key}')
        return obj

    def __getattr__(self, key: str):
        obj = self.get(key)
        if not obj:
            raise AttributeError(f'{key}')
        return obj

    def __repr__(self):
        return f"""\n{self.__class__.__name__} at {hex(id(self))}\n\n\
                          id: '{self.id}'
                        name: '{self.name}'
                    packages: {self.package_ids}
        function definitions: {self.function_definitions}
            unit definitions: {self.unit_definitions}
                compartments: {self.compartments}
                     species: {self.species}
                  parameters: {self.parameters}
         initial assignments: {self.initial_assignments}
                       rules: {self.rules}
                 constraints: {self.constraints}
                   reactions: {self.reactions}
                      events: {self.events}
        """
    #             local_parameters: <ListOfLocalParameters[{len(self.local_parameters)}]>

