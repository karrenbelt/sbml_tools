from typing import Union, Optional
import sys
import libsbml
import numpy as np

import pint
from bidict import bidict
from sbml_tools.toolbox.validator import check
from sbml_tools.wrappers import SBase, ListOf

import pint


# TODO:
#  1. move declaration of unit registry to __init__.py of this repository, also call set_application_registry(ureg)
#  2. DEFAULT_MODEL_UNITS, mapping of Celsius as a unit


unit_registry = pint.UnitRegistry()

# There are 3 definitions of units that are not SI (derived) units: item, dimensionless and avogadro
N_AVOGADRO = 6.02214179e23
# unit_registry.define(f'avogadro = {N_AVOGADRO} * dimensionless')   # unit_registry has N_A


UNIT_KINDS = bidict({
    libsbml.UNIT_KIND_AMPERE: 'ampere',
    libsbml.UNIT_KIND_AVOGADRO: 'avogadro',
    libsbml.UNIT_KIND_BECQUEREL: 'becquerel',
    libsbml.UNIT_KIND_CANDELA: 'candela',
    libsbml.UNIT_KIND_CELSIUS: 'celsius',
    libsbml.UNIT_KIND_COULOMB: 'coulomb',
    libsbml.UNIT_KIND_DIMENSIONLESS: 'dimensionless',
    libsbml.UNIT_KIND_FARAD: 'farad',
    libsbml.UNIT_KIND_GRAM: 'gram',
    libsbml.UNIT_KIND_GRAY: 'gray',
    libsbml.UNIT_KIND_HENRY: 'henry',
    libsbml.UNIT_KIND_HERTZ: 'hertz',
    libsbml.UNIT_KIND_ITEM: 'item',
    libsbml.UNIT_KIND_JOULE: 'joule',
    libsbml.UNIT_KIND_KATAL: 'katal',
    libsbml.UNIT_KIND_KELVIN: 'kelvin',
    libsbml.UNIT_KIND_KILOGRAM: 'kilogram',
    libsbml.UNIT_KIND_LITER: 'liter',
    libsbml.UNIT_KIND_LITRE: 'litre',
    libsbml.UNIT_KIND_LUMEN: 'lumen',
    libsbml.UNIT_KIND_LUX: 'lux',
    libsbml.UNIT_KIND_METER: 'meter',
    libsbml.UNIT_KIND_METRE: 'metre',
    libsbml.UNIT_KIND_MOLE: 'mole',
    libsbml.UNIT_KIND_NEWTON: 'newton',
    libsbml.UNIT_KIND_OHM: 'ohm',
    libsbml.UNIT_KIND_PASCAL: 'pascal',
    libsbml.UNIT_KIND_RADIAN: 'radian',
    libsbml.UNIT_KIND_SECOND: 'second',
    libsbml.UNIT_KIND_SIEMENS: 'siemens',
    libsbml.UNIT_KIND_SIEVERT: 'sievert',
    libsbml.UNIT_KIND_STERADIAN: 'steradian',
    libsbml.UNIT_KIND_TESLA: 'tesla',
    libsbml.UNIT_KIND_VOLT: 'volt',
    libsbml.UNIT_KIND_WATT: 'watt',
    libsbml.UNIT_KIND_WEBER: 'weber',
    libsbml.UNIT_KIND_INVALID: 'invalid',
    })


# not accepted by libsbml.Unit.setKind, requires remapping - these are outdated (not in SBML level 3)
BACKUP_MAP = {libsbml.UNIT_KIND_METER: libsbml.UNIT_KIND_METRE,
              libsbml.UNIT_KIND_LITER: libsbml.UNIT_KIND_LITRE,
              # Celsius: special case: requires conversion and has been removed in SBML L2V4
              # libsbml.UNIT_KIND_CELSIUS: libsbml.UNIT_KIND_KELVIN
              }

PINT_MAP = bidict(avogadro='avogadro_constant', item='count')


def cast_to_int(value: Union[float, int]):
    if isinstance(value, float):
        value = int(value) if value.is_integer() else value
        if isinstance(value, float):
            raise ValueError(f'value must be of type int, or a float that equals an integer: {value}')
    return value


def assert_within_range(value):
    assert -2 ** 31 <= value <= 2 ** 31 - 1, f'numeric range violation'


class UnitError(Exception):
    pass


class Unit(SBase):
    """
    unit = (multiplier * 10 ^ scale * kind) ^ exponent
    default to create the base units: exponent=1, scale=0, multiplier=1.0
    NOTE: - libsbml generates some weird initial values, but also requires all to be set still
            therefore we opt to return np.nan if not yet set
    """
    required = ['kind', 'exponent', 'scale', 'multiplier']
    _eq_attrs = required

    def __init__(self, unit: libsbml.Unit = None, **kwargs):
        super().__init__(libsbml.Unit, unit, **kwargs)
        self.check_unit_logic()

    @property
    def kind(self) -> str:
        return str(UNIT_KINDS[self.wrapped.getKind()])

    @kind.setter
    def kind(self, kind: Union[str, int]):
        if isinstance(kind, str):
            kind = UNIT_KINDS.inverse[kind]
        kind = BACKUP_MAP.get(kind, kind)
        check(self.wrapped.setKind(kind))

    @property
    def exponent(self) -> Optional[float]:
        if not self.wrapped.isSetExponent():
            return
        return self.wrapped.getExponent()

    @exponent.setter
    def exponent(self, exponent: Union[float, int]):
        # type casting here is important, 1.0 is set to 1, but 1.1 is set to 0 otherwise
        assert_within_range(exponent)
        check(self.wrapped.setExponent(cast_to_int(exponent)))

    @property
    def scale(self) -> Optional[int]:
        if not self.wrapped.isSetScale():
            return
        return self.wrapped.getScale()

    @scale.setter
    def scale(self, scale: Union[float, int]):
        assert_within_range(scale)
        check(self.wrapped.setScale(cast_to_int(scale)))

    @property
    def multiplier(self) -> Optional[float]:
        if not self.wrapped.isSetMultiplier():
            return
        return self.wrapped.getMultiplier()

    @multiplier.setter
    def multiplier(self, multiplier: Union[float, int]):
        assert -sys.float_info.max <= multiplier <= sys.float_info.max
        check(self.wrapped.setMultiplier(multiplier))

    def convert_to_SI(self):
        self.validate()
        unit_def = libsbml.Unit.convertToSI(self.wrapped)
        if not unit_def.getNumUnits() == 1:
            raise UnitError(f'unit not converted into 1 unit')
        self.wrapped = unit_def.getUnit(0)

    def equals(self, other: 'Unit') -> bool:
        if not isinstance(other, Unit):
            raise UnitError(f'other should be {self.__class__.__name__}, got {other.__class__.__name__}')
        udef_self = libsbml.Unit.convertToSI(self.wrapped)
        udef_other = libsbml.Unit.convertToSI(other.wrapped)
        return libsbml.UnitDefinition.areIdentical(udef_self, udef_other)

    @property
    def as_quantity(self) -> pint.Quantity:
        self.validate()
        kind = PINT_MAP.get(self.kind, self.kind)
        return (self.multiplier * 10 ** self.scale * unit_registry.parse_units(kind)) ** self.exponent

    @classmethod
    def from_quantity(cls, quantity: unit_registry.Quantity, scale=0):
        magnitude, units = quantity.to_tuple()
        if not units:  # exceptional case, magnitude is always 1, exponent 0
            kind, exponent = 'dimensionless', 0
            return cls(kind=kind, exponent=exponent, scale=0, multiplier=magnitude)
        if len(units) > 1:
            raise ValueError(f'expecting a single unit and exponent, found {units}')
        kind, exponent = units[0]
        kind = PINT_MAP.inverse.get(kind, kind)
        multiplier = (magnitude ** (1/exponent) / 10 ** scale)
        return cls(kind=kind, exponent=exponent, scale=scale, multiplier=multiplier)

    def check_unit_logic(self):
        if self.kind == 'dimensionless' and self.exponent != 0:
            raise UnitError(f'invalid exponent: {self.exponent}, should be zero since the unit is dimensionless')
        if self.exponent == 0 and self.kind != 'dimensionless':
            raise UnitError(f'invalid unit kind: {self.kind}, since exponent is zero this unit is dimensionless')
        if self.has_required_attributes:
            if self.multiplier * 10 ** self.scale == 0 and self.exponent < 0:
                raise UnitError(f'cannot raise zero to a negative power')

    def validate(self):
        if not self.has_required_attributes:
            raise UnitError('required attributes not set')
        self.check_unit_logic()

    def __str__(self):
        return f'({self.multiplier} * 10^{self.scale} * {self.kind})^{self.exponent}'

    def __repr__(self):
        return f'<Unit {self.kind}: exponent={self.exponent}, scale={self.scale}, multiplier={self.multiplier}>'


class ListOfUnits(ListOf):
    _contained_type = Unit

    def __init__(self, list_of_units: libsbml.ListOfUnits = None):
        super().__init__(libsbml.ListOfUnits, list_of_units)

