from typing import Union, List, Dict, Tuple, Iterable, Optional

import libsbml
from lxml import etree
from lxml.builder import ElementMaker

# from sbml_tools.wrappers import Wrapper, CVTerm, Creator, Date, History, utils
from sbml_tools.wrappers.xml_base import XMLBase


# namespaces
RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#"
DCTERMS_NAMESPACE = "http://purl.org/dc/terms/"
VCARD4_NAMESPACE = "http://www.w3.org/2006/vcard/ns#"
BQBIOL_NAMESPACE = "http://biomodels.net/biology-qualifiers/"
BQMODEL_NAMESPACE = "http://biomodels.net/model-qualifiers/"

NSMAP = dict(
    rdf=RDF_NAMESPACE,
    dcterms=DCTERMS_NAMESPACE,
    vcard4=VCARD4_NAMESPACE,
    bqmodel=BQMODEL_NAMESPACE,
    bqbiol=BQBIOL_NAMESPACE)

E = ElementMaker(namespace=RDF_NAMESPACE, nsmap=NSMAP)


def make_minimal_template():
    return E.RDF(E.Description(about=''))


class Annotation(XMLBase):
    """
    Annotations:
        - describing the creator of a model and its modification history
        - referring to controlled vocabulary terms and database identifiers that define and describe
          biological and biochemical entities
        - propriatary namespaces

    These can be accessed

    annotations add additional qualifying information and never change existing information
    this also holds for nested annotations with respect to parent annotations

    namespaces: application-specific annotation data must be entirely contained inside a top level element
        - there can only be one top level element using a given namespace
        - the ordering of top level elements is not significant

    NOTE:
        - meta id must be set to link annotations to SBase objects, both in the annotation and the SBase object,
        - if annotations are not MIRIAM compliant, this means that they are in a proprietary format
        - it should not be used to refer to SBO terms
        - duplicate CVTerms are not added and silently ignored
    """
    def __init__(self, annotation: libsbml.XMLNode = None):
        if not annotation:
            annotation = libsbml.RDFAnnotationParser_createAnnotation()
        self.root = etree.fromstring(annotation.toXMLString())

