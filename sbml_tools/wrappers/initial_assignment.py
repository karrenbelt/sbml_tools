
from typing import Optional, Union
import libsbml
from sbml_tools.wrappers import ListOf, MathBase
from sbml_tools.wrappers.utils import check, model_property
from sbml_tools.wrappers import Compartment, Species, Parameter
from sbml_tools.wrappers.species_reference import SpeciesReference


class InitialAssignmentError(Exception):
    pass


class InitialAssignment(MathBase):
    """
    Initial assignments overrule values set to objects reference by their 'symbol'.

    An assignment rule's 'symbol' can assign to:
        - compartment 'size'
        - species 'initial_amount' or 'initial_concentration',
        - species_reference 'stoichiometry'
        - parameter 'value', even for parameters that that have their 'constant' attribute set to True
        - Additionally. might reference object from packages.

    An initial assignment, unlike an assignment rule:
        - only applies up to and including the beginning of a simulation (e.g. t <= 0)
        - can set the value of a constant Parameter

    There can only be either an initial assignment OR an assignment rule for the same symbol in the model

    """
    required = ['symbol']
    _eq_attrs = ['symbol', 'math', 'id_attribute']
    _base_eq_attrs = ['id_attribute', 'name', 'meta_id', 'sbo_term', 'notes', 'annotation']

    def __init__(self, initial_assignment: libsbml.InitialAssignment = None, **kwargs):
        super().__init__(libsbml.InitialAssignment, initial_assignment, **kwargs)

    @property
    def id_attribute(self) -> str:
        return self.wrapped.getIdAttribute()

    @id_attribute.setter
    def id_attribute(self, id_attribute: str):
        check(self.wrapped.setIdAttribute(id_attribute))

    @property
    def symbol(self) -> str:
        if self.wrapped.isSetSymbol():
            return self.wrapped.getSymbol()

    @symbol.setter
    def symbol(self, symbol: str):  # TODO: check if exists
        check(self.wrapped.setSymbol(symbol))

    @model_property
    def object(self) -> Union[Compartment, Species, SpeciesReference, Parameter]:
        return self.model.get(self.symbol)


class ListOfInitialAssignments(ListOf):
    _contained_type = InitialAssignment

    def __init__(self, list_of_initial_assignments: libsbml.ListOfInitialAssignments = None):
        super().__init__(libsbml.ListOfInitialAssignments, list_of_initial_assignments)


def bug():
    # This is not the kind of behaviour that is expected from a getter; is it returning the original and keeping a copy?
    import libsbml
    assert libsbml.__version__ == '5.18.0'
    import string

    def unique(sequence):
        seen = set()
        return [x for x in sequence if not (x in seen or seen.add(x))]

    def simplify(li):
        d = dict(zip(unique(li), string.ascii_uppercase))
        return [d[el] for el in li]

    level = 3
    version = 2
    f_def = libsbml.FunctionDefinition(level, version)
    i_ass = libsbml.InitialAssignment(level, version)
    k_law = libsbml.KineticLaw(level, version)

    # same for libsbml.parseL3Formula('x'), thus not because it is empty or type is unknown
    a = libsbml.ASTNode()
    assert a is not None and a.isWellFormedASTNode()

    ast_node_ids = []
    for obj in [f_def, i_ass, k_law]:
        ids = []
        ids.append(id(a))  # 0. 'A'
        assert obj.setMath(a) == 0
        ids.append(id(a))  # 1. 'A'
        ids.append(id(obj.getMath()))  # 2. 'B'
        ids.append(id(obj.getMath()))  # 3. 'B'
        ids.append(id(obj.getMath()))  # 4. 'B'

        # assigning to variables
        b = obj.getMath()
        ids.append(id(b))  # 5. 'B'
        ids.append(id(obj.getMath()))  # 6. 'C'
        c = obj.getMath()
        ids.append(id(b))  # 7. 'B'
        ids.append(id(obj.getMath()))  # 8. 'D'

        ## NOTE: assigning to same variable as before: c
        c = obj.getMath()
        ids.append(id(obj.getMath()))  # 9. 'C'

        ast_node_ids.append(ids)

    # generate simpler representation
    simpler = list(map(simplify, ast_node_ids))
