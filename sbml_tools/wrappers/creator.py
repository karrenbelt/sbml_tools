
import re
import libsbml
from sbml_tools.wrappers import Wrapper, utils


def is_valid_email(email: str) -> bool:
    if re.search(r'^\w+([.-]?\w+)*@\w+([.-]?\w+)*(\.\w{2,3})+$', email):
        return True
    return False


class Creator(Wrapper):
    """
    Given name and Family name are required to be valid
    email address and organization are optional attributes
    """
    required = ['given_name', 'family_name']
    _eq_attrs = ['given_name', 'family_name', 'email', 'organization']

    def __init__(self, creator: libsbml.ModelCreator = None, **kwargs):
        super().__init__(libsbml.ModelCreator, creator, **kwargs)

    @property
    def family_name(self) -> str:
        return self.wrapped.getFamilyName()

    @family_name.setter
    def family_name(self, family_name: str):
        utils.check(self.wrapped.setFamilyName(family_name))

    @property
    def given_name(self) -> str:
        return self.wrapped.getGivenName()

    @given_name.setter
    def given_name(self, given_name: str):
        utils.check(self.wrapped.setGivenName(given_name))

    @property
    def email(self) -> str:
        return self.wrapped.getEmail()

    @email.setter
    def email(self, email: str):
        if not is_valid_email(email):
            raise ValueError(f'does not constitute a valid email address: {email}')
        utils.check(self.wrapped.setEmail(email))

    @property
    def organization(self) -> str:
        return self.wrapped.getOrganisation()

    @organization.setter
    def organization(self, organization: str):
        utils.check(self.wrapped.setOrganisation(organization))

    @property
    def has_required_attributes(self) -> bool:
        """given_name and family_name need to be set"""
        return self.wrapped.hasRequiredAttributes()

    def __repr__(self):
        return f"""<{self.__class__.__name__} {self.given_name} {self.family_name}>"""
