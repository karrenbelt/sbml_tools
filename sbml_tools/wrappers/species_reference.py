
import math
import libsbml
from sbml_tools import config
from sbml_tools.wrappers.list_of import ListOf
from sbml_tools.wrappers.simple_species_reference import SimpleSpeciesReference
from sbml_tools.wrappers import utils


class StoichiometryError(Exception):
    pass


class SpeciesReference(SimpleSpeciesReference):
    """
    part of Reaction
    """
    required = ['species', 'constant']
    _eq_attrs = ['species', 'stoichiometry', 'constant']

    def __init__(self, species_reference: libsbml.SpeciesReference = None, **kwargs):
        super().__init__(libsbml.SpeciesReference, species_reference, **kwargs)
        if config.AUTOMATED_SPECIES_REFERENCE_ID_GENERATION and not self.id:
            self.id = f'{self.species}_reference'

    @property
    def stoichiometry(self) -> float:
        if self.wrapped.isSetStoichiometry():
            return self.wrapped.getStoichiometry()

    @stoichiometry.setter
    def stoichiometry(self, stoichiometry: float):
        if not math.isnan(stoichiometry) and not stoichiometry > 0:
            raise StoichiometryError(f'stoichiometries must be strictly positive')
        utils.check(self.wrapped.setStoichiometry(stoichiometry))

    @property
    def constant(self) -> bool:
        if self.wrapped.isSetConstant():
            return self.wrapped.getConstant()

    @constant.setter
    def constant(self, constant: bool):
        utils.check(self.wrapped.setConstant(constant))


class ListOfSpeciesReferences(ListOf):

    _contained_type = SpeciesReference

    def __init__(self, list_of_species_references: libsbml.ListOfSpeciesReferences = None):
        super().__init__(libsbml.ListOfSpeciesReferences, list_of_species_references)
