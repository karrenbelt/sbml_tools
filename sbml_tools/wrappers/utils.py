from typing import Optional, Dict, List, Union, Set
import re
import uuid
import hashlib

import networkx as nx

import libsbml
from lxml import etree
from sbml_tools import settings, config


# TODO:
#  1. move up one directory (one of: utils.py, config.py, settings.py)
#  2. regex patterns


from collections.abc import Iterable


# def get_symbols_from_ast_node(ast_node) -> Set[str]:
#     return set([node.name for node in ast_node if not node.children and node.name])


def is_non_str_sequence(obj):
    return isinstance(obj, Iterable) and not isinstance(obj, (str, bytes, bytearray))


def method_kwargs(kwargs):
    """"""
    kwargs.update(kwargs.pop('kwargs', {}))
    return {k: v for k, v in kwargs.items() if not k == 'self'}


def check(value):
    """ translate libsbml error codes to messages; used in model construction """
    if value is None:
        raise ValueError('LibSBML returned a null value')
    elif type(value) is int:
        if value == libsbml.LIBSBML_OPERATION_SUCCESS:
            return
        else:
            err_msg = f"LibSBML returned error code {value}: \n" \
                      + f"{libsbml.OperationReturnValue_toString(value).strip()}"
            raise RuntimeError(err_msg)
    return value


def check_required_attributes(obj):  # would be nice to see what is not set
    if obj.has_required_attributes:
        return obj
    raise ValueError(f'{obj} does not have the required attributes set')


# def chunks(lst, n):
#     """Yield successive n-sized chunks from lst."""
#     for i in range(0, len(lst), n):
#         yield lst[i:i + n]

def model_property(method):
    @property
    def wrapper(self, *args, **kwargs):
        if not self.model:
            raise AttributeError(f'object is not associated with a model: {self}')
        return method(self, *args, **kwargs)
    return wrapper


def ensure_reference_exists(enforce: bool):
    """ a method decorator for model.add_initial_assignment and model.add_rule """
    def inner(method):
        def wrapper(self, *args, **kwargs):
            import inspect
            key = list(inspect.signature(method).parameters)[1]
            attr = dict(rule='variable', initial_assignment='symbol')[key]
            obj = args[0] if args else kwargs.get(key)
            reference = getattr(obj, attr, None)
            if reference and enforce:
                referenced = self.model.get(reference)
                # if not referenced:
                #     raise ValueError(f'{obj} does not reference any object in the model: {reference}')
                # if referenced.__class__.__name__ not in ('Compartment', 'Species', 'SpeciesReference', 'Parameter'):
                #     raise ValueError(f'wrong type of object referenced for {obj}: {referenced}\n'
                #                      f'can only reference: Compartment, Species, SpeciesReference or Parameter')
            method(self, *args, **kwargs)
        return wrapper
    return inner


def wrap(obj: Optional[libsbml.SBase]):
    """
    wraps the specific libsbml object with their proper wrapper
    imports must be nested to prevent circular references resulting in ImportErrors
    """
    from sbml_tools.wrappers import (
        FunctionDefinition, ListOfFunctionDefinitions,
        Unit, ListOfUnits, UnitDefinition, ListOfUnitDefinitions,
        Compartment, ListOfCompartments,
        Species, ListOfSpecies,
        Parameter, ListOfParameters,
        InitialAssignment, ListOfInitialAssignments,
        Rule, AlgebraicRule, AssignmentRule, RateRule, ListOfRules,
        Constraint, ListOfConstraints,
        Reaction, ListOfReactions,
        KineticLaw, LocalParameter, ListOfLocalParameters,
        SpeciesReference, ListOfSpeciesReferences,
        ModifierSpeciesReference, ListOfModifierSpeciesReferences,
        Event, Trigger, Priority, Delay, ListOfEvents,
        EventAssignment, ListOfEventAssignments,
        Model, SBMLDocument,
    )

    if obj is None:
        return

    wrapper = {
        libsbml.FunctionDefinition: FunctionDefinition,
        libsbml.ListOfFunctionDefinitions: ListOfFunctionDefinitions,
        libsbml.Unit: Unit,
        libsbml.ListOfUnits: ListOfUnits,
        libsbml.UnitDefinition: UnitDefinition,
        libsbml.ListOfUnitDefinitions: ListOfUnitDefinitions,
        libsbml.Compartment: Compartment,
        libsbml.ListOfCompartments: ListOfCompartments,
        libsbml.Species: Species,
        libsbml.ListOfSpecies: ListOfSpecies,
        libsbml.Parameter: Parameter,
        libsbml.ListOfParameters: ListOfParameters,
        libsbml.InitialAssignment: InitialAssignment,
        libsbml.ListOfInitialAssignments: ListOfInitialAssignments,
        # libsbml.Rule: Rule,  # cannot be instantiated, base class
        libsbml.AlgebraicRule: AlgebraicRule,
        libsbml.AssignmentRule: AssignmentRule,
        libsbml.RateRule: RateRule,
        libsbml.ListOfRules: ListOfRules,
        libsbml.Constraint: Constraint,
        libsbml.ListOfConstraints: ListOfConstraints,
        libsbml.Reaction: Reaction,
        libsbml.SpeciesReference: SpeciesReference,
        libsbml.ListOfSpeciesReferences: ListOfSpeciesReferences,
        libsbml.ModifierSpeciesReference: ModifierSpeciesReference,
        libsbml.KineticLaw: KineticLaw,
        libsbml.LocalParameter: LocalParameter,
        libsbml.ListOfLocalParameters: ListOfLocalParameters,
        # libsbml.ListOfModifierSpeciesReferences: ListOfModifierSpeciesReferences,  # == ListOfSpeciesReferences
        libsbml.ListOfReactions: ListOfReactions,
        libsbml.Event: Event,
        libsbml.ListOfEvents: ListOfEvents,
        libsbml.Trigger: Trigger,
        libsbml.Priority: Priority,
        libsbml.Delay: Delay,
        libsbml.EventAssignment: EventAssignment,
        libsbml.ListOfEventAssignments: ListOfEventAssignments,
        libsbml.Model: Model,
        libsbml.SBMLDocument: SBMLDocument,
    }.get(type(obj))

    if wrapper is None:
        raise ValueError(f'no wrapper for {obj}')

    return wrapper(obj)


def are_equal(element1: libsbml.SBase, element2: libsbml.SBase) -> bool:
    """assess whether two objects are exactly equal, using lxml etree (including, id, meta_id, etc)"""
    e1 = etree.fromstring(element1.toXMLNode().toXMLString(), etree.HTMLParser())
    e2 = etree.fromstring(element2.toXMLNode().toXMLString(), etree.HTMLParser())
    return elements_equal(e1, e2)


def xml_attributes_equal(a1: libsbml.XMLAttributes, a2: libsbml.XMLAttributes) -> bool:
    if not a1.getLength() == a2.getLength():
        return False
    d1 = {a1.getName(i): {a1.getPrefix(i), a1.getValue(i), a1.getURI(i)} for i in range(a1.getLength())}
    d2 = {a2.getName(i): {a2.getPrefix(i), a2.getValue(i), a2.getURI(i)} for i in range(a2.getLength())}
    return d1 == d2


def xml_nodes_equal(n1: libsbml.XMLNode, n2: libsbml.XMLNode) -> bool:
    """assess whether two libsbml.XMLNode's are equal."""
    if not n1.getName() == n2.getName():
        return False
    if not n1.getCharacters() == n2.getCharacters():
        return False
    # check namespace declarations
    if not n1.getNamespacesLength() == n2.getNamespacesLength():
        return False
    for i in range(n1.getNamespacesLength()):
        if not n1.getNamespacePrefix(i) == n2.getNamespacePrefix(i):
            return False
        if not n1.getNamespaceURI(i) == n2.getNamespaceURI(i):
            return False
    # check attributes (order does not matter for xml)
    if not xml_attributes_equal(n1.getAttributes(), n2.getAttributes()):
        return False
    # iterate children
    if not n1.getNumChildren() == n2.getNumChildren():
        return False
    # recurse
    children1 = [n1.getChild(i) for i in range(n1.getNumChildren())]
    children2 = [n2.getChild(i) for i in range(n2.getNumChildren())]
    return all(xml_nodes_equal(child1, child2) for child1, child2 in zip(children1, children2))


def get_sbo() -> Dict[int, dict]:
    """NOTE: SBO term 351 does not exist"""
    root = etree.parse(settings.SBO_FILE).getroot()
    data = dict()
    for term in root.findall('Term', namespaces=root.nsmap):
        data_term = dict()
        for child in term.getchildren():
            if child.nsmap == {None: 'http://www.biomodels.net/sbo'}:
                data_term[etree.QName(child).localname] = ' '.join((''.join(child.itertext())).split())
            elif child.nsmap == {None: 'http://www.w3.org/1998/Math/MathML'}:
                xml_string = etree.tostring(child).decode('utf-8')
                math = libsbml.formulaToL3String(libsbml.readMathMLFromString(xml_string))
                data_term[etree.QName(child).localname] = math
            else:
                raise ValueError(f'unexpected namespace found {child.nsmap}')
        data[int(data_term['id'].lstrip('SBO:'))] = data_term
    # we add -1, because this is the default value libsbml return if there is no SBO term set
    data[-1] = dict()
    return data


def get_duplicate_named_sbo_ids():  # there are 4 duplicate names
    SBO = get_sbo()
    rev_dict = {}
    for sbo_id, data in SBO.items():
        rev_dict.setdefault(data.get('name'), set()).add(sbo_id)
    result = filter(lambda x: len(x) > 1, rev_dict.values())
    for duplicate_named_ids in result:
        for sbo_id in duplicate_named_ids:
            print(sbo_id, SBO[sbo_id])


def construct_sbo_dag() -> nx.DiGraph:
    """construct directed acyclic graph (DAG) of SBO terms"""
    SBO = get_sbo()
    edges = []
    for k, v in SBO.items():
        if 'is_a' not in v:  # 0 and 598 (obsolete)
            continue

        if isinstance(v['is_a'], list):
            for elem in v['is_a']:
                edges.append((int(elem.lstrip('SBO:')), k))
        else:
            edges.append((int(v['is_a'].lstrip('SBO:')), k))

    return nx.DiGraph(edges)


def get_miriam() -> Dict[int, dict]:
    """
    all datatype nodes have id and pattern as attributes
    all have 'definition', 'name', 'namespace', 'resources', 'uris' as children
    other possible children include: 'annotation', 'comment', 'documentations', 'restrictions', 'synonyms', 'tags'
    """
    root = etree.parse(settings.MIRIAM_FILE).getroot()

    def strip_ns_from_tag(tag):
        return tag.split(f'{{{root.nsmap[None]}}}')[1]

    miriam = dict()
    for datatype in root.findall('datatype', namespaces=root.nsmap):
        data = {strip_ns_from_tag(x.tag): ' '.join(''.join(x.itertext()).split()) for x in datatype.getchildren()}
        data['id_string'] = datatype.attrib['id']
        data['pattern'] = datatype.attrib['pattern']
        miriam[int(datatype.attrib['id'].lstrip('MIR:'))] = data
    return miriam


MIRIAM = get_miriam()
# used to map url prefixes (e.g. 'http://identifiers.org/chebi/') to MIRIAM dict, used to validate
URL_TO_MIRIAM_IDX = {s: k for k, v in MIRIAM.items() for s in v['uris'].split(' ') if s.startswith('http')}


def is_valid_miriam(resource: str) -> bool:
    try:
        prefix, term = resource.rstrip('/').rsplit('/', 1)
    except ValueError:
        return False
    miriam_idx = URL_TO_MIRIAM_IDX.get(prefix + '/')
    if not miriam_idx:
        return False
    if not re.match(MIRIAM[miriam_idx]['pattern'], term):
        return False
    return True


def ensure_compartment_exist(model, species):
    from sbml_tools.wrappers.species import SpeciesError
    if config.ENFORCE_COMPARTMENT_BEFORE_SPECIES:
        if not model.compartments[species.compartment]:
            raise SpeciesError(f"species assigned to compartment that is not part of the model:{species.compartment}")


def ensure_species_exist(model, reaction):
    from sbml_tools.wrappers.reaction import ReactionError
    if config.ENFORCE_SPECIES_BEFORE_SPECIES_REFERENCES:
        for reference in list(reaction.reactants + reaction.products) + list(reaction.modifiers):
            if not model.species[reference.species]:
                raise ReactionError(f"reaction involves species that are not part of the model:\n{reference}")


def ensure_meta_id(sbase: libsbml.SBase):
    """
    Creates a unique meta id for the SBase object, in one of the following two ways:
    1. if the object is associated with a SBMLDocument,
       it will generate the XML string, and generate a unique hexadecimal md5 hash
       This meta id is reproducible if the document is the same.
    2. otherwise, generates a unique id that is not reproducible

    - Meta ids are required to associate meta data annotations using the Recourse Description Format (RDF).
    - They cannot start with a number, which is why 'meta_' is prepended
    - The meta_id is set on the SBase object immediately,
      which is necessary to ensure that the next id generated will also be unique.
    """
    if sbase.isSetMetaId() or not config.AUTOMATED_META_ID_GENERATION:
        return

    doc = sbase.getSBMLDocument()
    if doc:
        s = sbase.toXMLNode().toXMLString()
        meta_id = 'meta_' + hashlib.md5(s.encode('utf-8')).hexdigest()
        n = 0
        while doc.getElementByMetaId(meta_id):
            n += 1
            meta_id = 'meta_' + hashlib.md5((s + str(n)).encode('utf-8')).hexdigest()
    else:
        meta_id = 'meta_' + uuid.uuid4().hex

    check(sbase.setMetaId(meta_id))


def ensure_unit_definition(obj, unit_definition_identifier: Optional[str], variant_of: Optional[str] = None):
    """
    Automated unit definition generation for model, compartment, species, and parameters
    1. a unit definition creation from string input, catching some fraction of bad entries (e.g. typos).
       It is also restricts the identifier namespace in the sense that these can be interpreted meaningfully
    2. depending on the type of object, it checks the type of units is proper, either dimensionless or
       otherwise of a matching type for that particular object (substance, time, volume, area, length)
    3. the unit definition is added to the model, unless it already exists, in the latter case we ensure
       that the existing unit equals the newly generated one.
    4. in case an object is created through the model instance and units are not provided, we set the units
       according to those defined at the level of the model (which is already implicitly done)

    # TODO: ASTNode
    # TODO: check if identical unit already exists (e.g. squared_metre == metre2)
    """
    if not config.ENFORCE_UNITS:
        return

    from sbml_tools.wrappers import Model, Compartment, Species, Parameter, LocalParameter, UnitDefinitionError
    from sbml_tools.toolbox.factories.unit_definition_factory import UnitDefinitionFactory
    from sbml_tools.toolbox.factories.parameter_factory import ParameterFactory
    unit_definition_factory = UnitDefinitionFactory(obj.level, obj.version)

    model = obj if isinstance(obj, Model) else obj.model  # a Model without SBMLDocument has .model as None

    if isinstance(obj, Model):  # always needs to pass a unit_definition_identifier
        unit_definition = unit_definition_factory(unit_definition_identifier)
        if (not getattr(unit_definition, f'is_variant_of_{variant_of}')
                and not unit_definition.is_variant_of_dimensionless):
            raise UnitDefinitionError(f'expected a unit of {variant_of}, got:\n{unit_definition}')

    elif isinstance(obj, Compartment):
        # if obj.spatial_dimensions is None:
        #     raise UnitDefinitionError('spatial dimensions need to be set before units can be set')
        if not unit_definition_identifier:
            if not model or not obj.wrapped.isSetSpatialDimensions():
                return  # in this case we cannot infer the units through the model
            unit_definition_identifier = {
                0: 'dimensionless',
                1: model.length_units,
                2: model.area_units,
                3: model.volume_units,
                }[obj.spatial_dimensions]
        if not unit_definition_identifier:
            raise UnitDefinitionError('pass units or set them at the level of the model')
        unit_definition = unit_definition_factory(unit_definition_identifier)
        if not unit_definition.is_variant_of_dimensionless:
            if not {0: unit_definition.is_variant_of_dimensionless,
                    1: unit_definition.is_variant_of_length,
                    2: unit_definition.is_variant_of_area,
                    3: unit_definition.is_variant_of_volume,
                    }[obj.spatial_dimensions]:
                error_message = (f'unit {unit_definition} not suitable for this compartment \n'
                                 f'compartment dimensionality: {obj.spatial_dimensions}')
                raise UnitDefinitionError(error_message)

    elif isinstance(obj, Species):
        if not unit_definition_identifier:
            if not model:
                return  # in this case we cannot infer the units through the model
            unit_definition_identifier = model.substance_units
        if not unit_definition_identifier:
            raise UnitDefinitionError('pass substance units or set them at the level of the model')
        unit_definition = unit_definition_factory(unit_definition_identifier)
        if not unit_definition.is_variant_of_dimensionless and not unit_definition.is_variant_of_substance:
            raise UnitDefinitionError(f'expected a unit of substance, got:\n{unit_definition}')

    elif isinstance(obj, (Parameter, LocalParameter, ParameterFactory)):
        # no constraints placed on unit types -> parameter factory
        # if not unit_definition_identifier or not model:
        #     return  # in this case we cannot infer the units through the model
        unit_definition = unit_definition_factory(unit_definition_identifier)

    else:
        raise NotImplementedError(f'expected a Model, Compartment, Species, Parameter or LocalParameter, '
                                  f'or RateLawParameterFactory, but got {type(obj)}')

    if model and config.AUTOMATED_UNIT_DEFINITION_CREATION:
        existing_unit_definition = model.unit_definitions.get(unit_definition.id)
        if not existing_unit_definition:
            model.add_unit_definition(unit_definition)
        else:
            if not existing_unit_definition.equals(unit_definition):
                raise UnitDefinitionError(f'unit definition identifier already exists in the model, ',
                                          f'but the unit defined is different:\n'
                                          f'existing: {existing_unit_definition}\nnew: {unit_definition}')


def validate_sbo_term_assignment(sbase, sbo_term: int):
    from sbml_tools.wrappers.sbo_term import SBO, SBML_COMPONENT_SBO_BRANCHES, SBOTermError, SBOTerm
    sbo_term = SBOTerm(sbo_term)
    if sbo_term not in SBO:
        raise SBOTermError(f"SBOTerm '{sbo_term}' does not exist")
    correct_parent_branch = SBML_COMPONENT_SBO_BRANCHES.get(type(sbase.wrapped), 0)
    sbo_parent_branch = SBOTerm(libsbml.SBO_getParentBranch(sbo_term))
    if correct_parent_branch:
        if isinstance(correct_parent_branch, list) and sbo_parent_branch not in correct_parent_branch:
            expected = '\n'.join([f"'{SBOTerm(branch).name}': {SBOTerm(branch)})" for branch in correct_parent_branch])
            raise SBOTermError(f'SBOTerm not of the correct branch for {sbase}'
                               f"expected parent branches:\n'{expected}\n"
                               f"attempted assignment parent branch '{sbo_parent_branch.name}': {sbo_parent_branch}")
        elif isinstance(correct_parent_branch, int) and correct_parent_branch != sbo_parent_branch:
            correct_parent_branch = SBOTerm(correct_parent_branch)
            raise SBOTermError(f'SBOTerm not of the correct branch for {sbase}\n'
                               f"expected parent branch '{correct_parent_branch.name}': {correct_parent_branch}\n"
                               f"attempted assignment parent branch '{sbo_parent_branch.name}': {sbo_parent_branch}")


# string parsing
def semi_colon_splitter(s: str) -> List[str]:
    return re.split(r'\s*;\s*', s)


def compile_number_and_identifier_pattern():
    decint = r"(?:(?:[1-9](?:_?\d)*|0)(?![.eE]))"  # r"(?:[1-9](?:_?\d)*|0+(_?0)*)"
    digitpart = r"(?:\d(?:_?\d)*)"
    exponent = rf"(?:[eE][-+]?{digitpart})"
    fraction = rf"(?:\.{digitpart})"
    pointfloat = rf"(?:{digitpart}?{fraction}|{digitpart}\.)"
    exponentfloat = rf"(?:(?:{digitpart}|{pointfloat}){exponent})"
    floatnumber = rf"(?:{pointfloat}|{exponentfloat})"
    number = rf"(?:{decint}|{floatnumber})"
    species_id = r'[_a-zA-Z]\w*'
    number_and_identifier_pattern = re.compile(rf'^\s*({number})?\s*({species_id})?\s*$')
    return number_and_identifier_pattern


number_and_identifier_pattern = compile_number_and_identifier_pattern()


def parse_value_suffixed_identifier(value_identifier, value_strict=False, suffix_identifier_strict=False):
    match = number_and_identifier_pattern.match(value_identifier)
    if not match:
        raise ValueError(f"cannot parse: '{value_identifier}'")
    value, identifier = match.groups()
    if value_strict and not value:
        raise ValueError(f'could not parse value from: {value_identifier}')
    if suffix_identifier_strict and not identifier:
        raise ValueError(f'could not parse identifier from: {value_identifier}')
    return value, identifier


def parse_identifier_assignment(assignment: str):
    parts = re.split(r'\s*=\s*', assignment)
    if not len(parts) == 2:
        raise ValueError(f"there must be exactly one '=' sign in order to parse an assignment:\n{assignment}")
    return parts


def string_to_species_reference(reactant_or_product: str, level: int = None, version: int = None):
    """
    Parsing a string and returning the inferred species references
    Note that we assume the attribute 'constant' to be True
    """
    from sbml_tools.wrappers import SpeciesReference
    level = level if level else config.DEFAULT_SBML_LEVEL
    version = version if version else config.DEFAULT_SBML_VERSION
    stoichiometry, species = parse_value_suffixed_identifier(reactant_or_product, suffix_identifier_strict=True)
    if stoichiometry is None:
        stoichiometry = 1.0
    return SpeciesReference(level=level, version=version,
                            species=species, stoichiometry=float(stoichiometry), constant=True)


def string_to_parameter(parameter_string: str, local=False,
                        level: int = None, version: int = None,
                        constant: bool = True):
    """
    Parsing a string and returning the inferred assignment
    Note that we assume the attribute 'constant' to be True
    """
    from sbml_tools.wrappers import Parameter, LocalParameter
    if level is None:
        level = config.DEFAULT_SBML_LEVEL
    if version is None:
        version = config.DEFAULT_SBML_VERSION

    if '=' in parameter_string:
        assigned_identifier, assignment = parse_identifier_assignment(parameter_string)
        value, unit_identifier = parse_value_suffixed_identifier(assignment)
        if value is not None:
            value = float(value)
    else:
        assigned_identifier, value, unit_identifier = parameter_string, None, None

    if not local:
        return Parameter(level=level, version=version,
                         id=assigned_identifier, value=value, units=unit_identifier, constant=constant)
    return LocalParameter(level=level, version=version,
                          id=assigned_identifier, value=value, units=unit_identifier)


def string_to_event_assignment(event_assignment_string: str,
                               level: int = None, version: int = None):
    from sbml_tools.wrappers import EventAssignment
    if level is None:
        level = config.DEFAULT_SBML_LEVEL
    if version is None:
        version = config.DEFAULT_SBML_VERSION

    assigned_identifier, math = parse_identifier_assignment(event_assignment_string)
    return EventAssignment(level=level, version=version, variable=assigned_identifier, math=math)


# XML
def elements_equal(e1: etree._Element, e2: etree._Element):
    """ test if etree's are the equivalent (attribute order should not matter) """
    if e1.tag != e2.tag:
        return False  # str
    if e1.text != e2.text:
        return False  # str
    if e1.tail != e2.tail:
        return False  # str
    if e1.attrib != e2.attrib:
        return False  # dict
    if len(e1) != len(e2):
        return False  # int
    return all(elements_equal(c1, c2) for c1, c2 in zip(e1, e2))


def remove_xml_namespaces(e: Union[etree._ElementTree, etree._Element]) -> etree._Element:
    """ strip namespaces from etree tags (e.g. we obtain 'body' instead of '{http://www.w3.org/1999/xhtml}body') """
    if isinstance(e, etree._ElementTree):
        e = e.getroot()

    xslt = '''<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output method="xml" indent="no"/>

    <xsl:template match="/|comment()|processing-instruction()">
        <xsl:copy>
          <xsl:apply-templates/>
        </xsl:copy>
    </xsl:template>

    <xsl:template match="*">
        <xsl:element name="{local-name()}">
          <xsl:apply-templates select="@*|node()"/>
        </xsl:element>
    </xsl:template>

    <xsl:template match="@*">
        <xsl:attribute name="{local-name()}">
          <xsl:value-of select="."/>
        </xsl:attribute>
    </xsl:template>
    </xsl:stylesheet>
    '''

    xslt_doc = etree.fromstring(xslt)
    transform = etree.XSLT(xslt_doc)
    return transform(e).getroot()


def change_xml_namespaces(e, orig_namespace, new_namespace):
    """replace all instances of (nested) namespace occurrences in the tree"""
    xslt = """
    <xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
      <xsl:output indent="yes"/>
      <xsl:strip-space elements="*"/>

      <xsl:param name="orig_namespace"/>
      <xsl:param name="new_namespace"/>

      <xsl:template match="@*|node()">
        <xsl:copy>
          <xsl:apply-templates select="@*|node()"/>
        </xsl:copy>
      </xsl:template>

      <xsl:template match="*" priority="1">
        <xsl:choose>
          <xsl:when test="namespace-uri()=$orig_namespace">
            <xsl:element name="{name()}" namespace="{$new_namespace}">
              <xsl:apply-templates select="@*|node()"/>
            </xsl:element>
          </xsl:when>
          <xsl:otherwise>
            <xsl:copy>
              <xsl:apply-templates select="@*|node()"/>
            </xsl:copy>
          </xsl:otherwise>
        </xsl:choose>
      </xsl:template>

    </xsl:stylesheet>
    """

    xslt_doc = etree.fromstring(xslt)
    transform = etree.XSLT(xslt_doc)
    return transform(e, orig_namespace=f"'{orig_namespace}'", new_namespace=f"'{new_namespace}'").getroot()


def substitute_lambda(self: 'ASTNode', symbol: str, other: 'ASTNode'):
    """ substitute a referenced lambda expression (function definition math) """
    for node in self:
        if node.name == symbol:
            assert len(node.children) == len(other.children[:-1]), f'unequal number of lambda arguments'
            new_math = other.children[-1].copy()
            for a, b in zip(node.children, other.children[:-1]):
                new_math.replace(b.name, a)
            for k, v in filter(lambda x: x[1] is not None, new_math.as_kwargs.items()):
                setattr(node, k, v)

