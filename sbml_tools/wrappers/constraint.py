import libsbml
from typing import Tuple, Union, Optional

import numpy as np

from lxml import etree
from sbml_tools.wrappers import ListOf, MathBase, utils
from sbml_tools.wrappers.xml_helper import XHTML, xhtml_element_maker


class Message(XHTML):

    def __init__(self, parent: 'Constraint' = None):
        self._parent = parent
        if parent and parent.wrapped.isSetMessage():
            self.root = etree.fromstring(parent.wrapped.getMessage().toXMLString())
        else:
            self.root = etree.fromstring('<message/>')

    @property
    def parent(self):
        return self._parent

    def _set(self, xhtml_string: str):
        if self.parent:
            self.parent.wrapped.setMessage(xhtml_string)


class ConstraintError(Exception):
    pass


class Constraint(MathBase):
    """
    Constraints are first checked after initial assignments are performed at t == 0.
    A simulation is considered invalid from the moment that the constraint is violated.
    When a constraint is violated the math element of the constraint returns False.
    """
    required = []
    _eq_attrs = ['math', 'message']

    def __init__(self, constraint: libsbml.Constraint = None, **kwargs):
        super().__init__(libsbml.Constraint, constraint, **kwargs)

    @property
    def message(self) -> Message:
        if self.wrapped.isSetMessage():
            return Message(self)

    @message.setter
    def message(self, message: Union[Message, str]):
        if isinstance(message, str):
            message = Message.from_xhtml_string(message)
        utils.check(self.wrapped.setMessage(message.as_string))

    def create_message(self):
        self.message = Message()
        return self.message

    def remove_message(self):
        utils.check(self.wrapped.unsetMessage())


class ListOfConstraints(ListOf):

    _contained_type = Constraint

    def __init__(self, list_of_constraints: libsbml.ListOfConstraints = None):
        super().__init__(libsbml.ListOfConstraints, list_of_constraints)

