
from typing import Union
import libsbml
from sbml_tools.wrappers import MathBase, ListOf, utils


class EventAssignment(MathBase):
    """
    Note: when setting and id, the SBase class uses setIdAttribute instead, which is apparently required
    """
    required = ['variable']
    _eq_attrs = ['variable', 'math', 'id_attribute']
    _base_eq_attrs = ['id_attribute', 'name', 'meta_id', 'sbo_term', 'notes', 'annotation']

    def __init__(self, event_assignment: libsbml.EventAssignment = None, **kwargs):
        super().__init__(libsbml.EventAssignment, event_assignment, **kwargs)

    @property
    def id_attribute(self) -> str:
        return self.wrapped.getIdAttribute()

    @id_attribute.setter
    def id_attribute(self, id_attribute: str):
        utils.check(self.wrapped.setIdAttribute(id_attribute))

    @property
    def variable(self):
        if self.wrapped.isSetVariable():
            return self.wrapped.getVariable()

    @variable.setter
    def variable(self, variable: str):
        utils.check(self.wrapped.setVariable(variable))

    def remove(self, variable: Union[str, int]):
        return self.remove(variable)

    def __repr__(self):
        return f"""{' '.join([self.__class__.__name__, self.id]).strip()}: 
              variable: {self.variable} 
            assignment: {self.math}"""


class ListOfEventAssignments(ListOf):
    _contained_type = EventAssignment

    def __init__(self, list_of_event_assignment: libsbml.ListOfEventAssignments = None):
        super().__init__(libsbml.ListOfEventAssignments, list_of_event_assignment)
