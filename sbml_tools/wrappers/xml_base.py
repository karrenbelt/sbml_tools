
from abc import ABC

from typing import Optional, List, Union, Tuple
from lxml import etree
from sbml_tools.wrappers import utils


class XMLBase(ABC):
    """
    In this case we convert the libsbml XMLNode object into an lxml.etree. This object is easier to manipulate.
    Specifically because XSLT 1.0 and XPATH 1.0 are supported.
        - XSLT (eXtensible Stylesheet Language): a styling language for XML. XSLT stands for XSL Transformations.
        - XPATH: XPath can be used to navigate through elements and attributes in an XML document.
    """
    encoding = 'utf-8'

    def _get_root(self):
        return self.__root

    def _set_root(self, value):
        if not isinstance(value, etree._Element):
            raise TypeError("root must be set to an etree._Element")
        self.__root = value

    root = property(_get_root, _set_root)

    @property
    def name_space_map(self):
        return self.root.nsmap

    def strip_namespaces(self):
        return utils.remove_xml_namespaces(self.root)

    @property
    def all_tags(self) -> list:
        return [element.tag for element in self]

    @property
    def all_texts(self) -> list:
        """returns all texts and tails in the proper order (element text, subelement text and tail, element tail"""
        return list(self.root.itertext())

    @property
    def all_attributes(self) -> List[dict]:
        return [element.attrib for element in self]

    @property
    def all_comments(self):  # e.g. <!-- some text -->
        return list(self.root.iter(etree.Comment))

    @property
    def all_entities(self):  # e.g. &#234; - &#234;
        return list(self.root.iter(etree.Entity))

    # find elements of the tree
    def xpath(self, tag, namespace):
        """recursively find elements. This makes xpath work with namespaces prefixed"""
        return etree.ETXPath(f".//{{{namespace}}}{tag}")(self.root)

    def findall_by_namespace_prefix(self, namespace_prefix, tag='*'):
        return self.root.xpath(f'.//{namespace_prefix}:{tag}', namespaces=self.root.nsmap)

    def findall_by_tag(self, tag) -> list:
        return self.root.xpath(f"//*[local-name() = '{tag}']")

    def findall_by_attribute(self, attrib, value=None):
        if value:
            return self.root.findall(f".//*[@{attrib} = '{value}']")
        return self.root.findall(f".//*[@{attrib}]")  # find all elements with a specific attribute

    def findall_containing_text(self, search_term):
        return self.root.xpath(f'.//*[contains(text(), "{search_term}")]')

    def findall_starting_with(self, search_term):
        return self.root.xpath(f'.//*[starts-with(text(), "{search_term}")]')

    # printing and to_string
    @property
    def as_string(self):
        return etree.tostring(self.root, pretty_print=True, encoding=self.encoding).decode()

    def print_text(self, element: Optional[etree._Element] = None):
        """print plain text without tags and formatting"""
        if not element:
            element = etree.fromstring(self.as_string)
        print(''.join(element.itertext()).strip())

    # remove
    def strip_by_tag(self, tag):
        etree.strip_tags(self.root, tag)

    def remove_empty_tags(self):

        def recursively_empty(e):
            if e.text and e.text.strip():
                return False
            return all((recursively_empty(c) for c in e.iterchildren()))

        for elem in self:
            parent = elem.getparent()
            if recursively_empty(elem):
                parent.remove(elem)

    @property
    def max_depth(self) -> int:
        """maximum depth of the etree"""

        def depth(node):
            if node.getparent() is not None:
                return depth(node.getparent()) + 1
            else:
                return 0

        return depth(max(self.root.iter(), key=depth))

    # @property
    # def encoding(self):  # TODO: find out how to set this
    #     return self.root.getroottree().docinfo.encoding

    @property
    def parser(self):  # TODO: this should have the encoding set
        return self.root.getroottree().parser

    @property
    def doctype(self):
        return self.root.getroottree().docinfo.doctype

    @property
    def has_doctype_declaration(self) -> bool:
        return bool(self.doctype)

    @property
    def xml_version(self):
        return self.root.getroottree().docinfo.xml_version

    @property
    def has_xml_declaration(self):
        return f'?xml version="{self.xml_version}"' in self.as_string

    def validate_xml(self, xmlschema_doc: str):
        xmlschema = etree.XMLSchema(etree.fromstring(xmlschema_doc))
        if not xmlschema.validate(self.root.getroottree()):
            raise ValueError('XML violates schema')

    @classmethod
    def from_xhtml_string(cls, xhtml: str):
        new = cls()
        new.root = etree.fromstring(xhtml)
        return new

    def __iter__(self):
        """depth-first pre-order tree traversal"""
        return self.root.iter()

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return utils.elements_equal(self.root, other.root)

    def __repr__(self):
        return f"<{self.__class__.__name__}>"

    def __str__(self):
        return self.as_string

