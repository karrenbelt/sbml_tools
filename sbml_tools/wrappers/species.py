import libsbml
from typing import Tuple, Union, Optional
import cmath
import numpy as np
import pandas as pd

# from cached_property import cached_property

from sbml_tools.toolbox import check
from sbml_tools.wrappers import SBase, ListOf, UnitDefinitionError, utils, ASTNode


def get_species_logic_table():
    columns = ['constant', 'boundary_condition', 'can_have_assignment_or_rate_rule', 'can_be_reactant_or_product']
    table = pd.DataFrame(
        [[1, 1, 0, 1],
         [0, 1, 1, 1],
         [1, 0, 0, 0],
         [0, 0, 1, 1],
         ], dtype=bool, columns=columns).set_index(columns[:2])
    return table


SPECIES_LOGIC_TABLE = get_species_logic_table()


def what_can_change_species_amount(constant: bool, boundary_condition: bool):
    return {
        (True, True): '(never changes)',
        (False, True): 'rules and events',
        (True, False): '(never changes)',
        (False, False): 'reactions OR rules, and events',
    }[constant, boundary_condition]


def ensure_correct_species_logic(species, attribute, value):
    """
    used when setting initial_amount, initial concentration and has_only_substance_units
    # TODO: units
    """
    if value < 0:
        raise SpeciesError(f'{attribute} cannot be negative')

    if attribute == 'initial_amount':
        if species.has_only_substance_units is None:
            species.has_only_substance_units = True
        elif species.has_only_substance_units is False:
            raise SpeciesError(f'has_only_substance_units is False; needs to be True to set initial_amount')
    elif attribute == 'initial_concentration':
        if species.has_only_substance_units is None:
            species.has_only_substance_units = False
        elif species.has_only_substance_units is True:
            raise SpeciesError(f'has_only_substance_units is True; needs to be False to set initial_concentration')
    elif attribute == 'has_only_substance_units':
        if value is True:
            if species.initial_concentration is not None:
                raise SpeciesError(f'species has initial_concentration, cannot set has_only_substance_units as True\n'
                                   f'to switch unset the initial_concentration first')
        elif species.initial_amount is not None:
            raise SpeciesError(f'species has initial_amount, cannot set has_only_substance_units as False'
                               f'to switch unset the initial_amount first')
    else:
        ValueError(f'unknown species attribute {attribute}')


class SpeciesError(Exception):
    pass


class Species(SBase):
    """
    NOTES:
        1. if species.has_only_substance_units == False, rules and event assignments changed the concentration
        2. if constant == True and species.has_only_substance_units == False, then the concentration still changes
             when the size of the compartment changes; it is the amount that is kept constant

    has_only_substance_units is required, amount and concentration are optional
    e.g. amount or concentration could be set in an initial_assignment or assignment_rule

    conversion_factor must be an SId of a parameter in the model, with constant set to True
    this conversion factor is only used when calculating the effect of a reaction on the species, not in rules or
    other constructs that affect the species, and neither in mathematical expressions that reference the species
    """
    required = ['id', 'compartment', 'has_only_substance_units', 'boundary_condition', 'constant']
    _eq_attrs = ['compartment', 'initial_amount', 'initial_concentration', 'substance_units',
                 'has_only_substance_units', 'boundary_condition', 'constant', 'conversion_factor']

    def __init__(self, species: libsbml.Species = None, **kwargs):
        super().__init__(libsbml.Species, species, **kwargs)

    @property
    def compartment(self) -> str:
        if self.wrapped.isSetCompartment():
            return self.wrapped.getCompartment()

    @compartment.setter
    def compartment(self, compartment: str):
        check(self.wrapped.setCompartment(compartment))

    def get_compartment(self):
        return self.model.compartments.get(self.compartment)

    @property
    def initial_amount(self) -> float:
        """ count of the number of individual entities """
        if self.wrapped.isSetInitialAmount():
            return self.wrapped.getInitialAmount()

    @initial_amount.setter
    def initial_amount(self, initial_amount: Union[float, int]):
        ensure_correct_species_logic(self, 'initial_amount', initial_amount)
        check(self.wrapped.setInitialAmount(initial_amount))

    def unset_initial_amount(self):
        check(self.wrapped.unsetInitialAmount())

    @property
    def initial_concentration(self) -> float:
        """ amount divided by compartment size """
        if self.wrapped.isSetInitialConcentration():
            return self.wrapped.getInitialConcentration()

    @initial_concentration.setter
    def initial_concentration(self, initial_concentration: Union[float, int]):
        ensure_correct_species_logic(self, 'initial_concentration', initial_concentration)
        check(self.wrapped.setInitialConcentration(initial_concentration))

    def unset_initial_concentration(self):
        check(self.wrapped.unsetInitialConcentration())

    @property
    def substance_units(self) -> str:
        if self.wrapped.isSetSubstanceUnits():
            return self.wrapped.getSubstanceUnits()

    @substance_units.setter
    def substance_units(self, substance_units: str):
        utils.ensure_unit_definition(self, substance_units)
        check(self.wrapped.setSubstanceUnits(substance_units))

    @property
    def has_only_substance_units(self) -> Optional[bool]:
        """if True: species.amount is used; if False: species.concentration is used"""
        if self.wrapped.isSetHasOnlySubstanceUnits():
            return self.wrapped.getHasOnlySubstanceUnits()

    @has_only_substance_units.setter
    def has_only_substance_units(self, has_only_substance_units: bool):
        ensure_correct_species_logic(self, 'has_only_substance_units', has_only_substance_units)
        check(self.wrapped.setHasOnlySubstanceUnits(has_only_substance_units))

    @property
    def boundary_condition(self) -> Optional[bool]:
        if self.wrapped.isSetBoundaryCondition():
            return self.wrapped.getBoundaryCondition()

    @boundary_condition.setter
    def boundary_condition(self, boundary_condition: bool):
        check(self.wrapped.setBoundaryCondition(boundary_condition))

    @property
    def constant(self) -> Optional[bool]:
        """specifies whether a species' amount can potentially change"""
        if self.wrapped.isSetConstant():
            return self.wrapped.getConstant()

    @constant.setter
    def constant(self, constant: bool):
        check(self.wrapped.setConstant(constant))

    @property
    def conversion_factor(self) -> str:
        """Should return the id of a parameter. If set, takes precedence over the model.conversion_factor attribute"""
        if self.wrapped.isSetConversionFactor():
            return self.wrapped.getConversionFactor()

    @conversion_factor.setter
    def conversion_factor(self, conversion_factor: str):
        check(self.wrapped.setConversionFactor(conversion_factor))

    @property
    def logic_table(self):
        return SPECIES_LOGIC_TABLE

    @property
    def can_have_assignment_or_rate_rule(self) -> bool:
        return SPECIES_LOGIC_TABLE['can_have_assignment_or_rate_rule'][self.constant, self.boundary_condition]

    @property
    def can_be_reactant_or_product(self) -> bool:
        return SPECIES_LOGIC_TABLE['can_be_reactant_or_product'][self.constant, self.boundary_condition]

    @property
    def what_can_change_species_amount(self) -> str:
        return what_can_change_species_amount(self.constant, self.boundary_condition)

    @property
    def consuming_reactions(self):
        if self.model:
            return [reaction for reaction in self.model.reactions if self in reaction.reactant_species]

    @property
    def producing_reactions(self):
        if self.model:
            return [reaction for reaction in self.model.reactions if self in reaction.product_species]

    @property
    def modifies_reactions(self):
        if self.model:
            return [reaction for reaction in self.model.reactions if self in reaction.modifier_species]

    @property
    def reactions(self):
        if self.model:
            return [reaction for reaction in self.model.reactions if self in reaction.species]

    @property
    def formula(self) -> str:
        incoming = ' + '.join(map(lambda x: x.id, self.producing_reactions))
        outgoing = ' - '.join(map(lambda x: x.id, self.consuming_reactions))
        return f'{incoming} - {outgoing}' if outgoing else incoming

    @property
    def math(self) -> ASTNode:
        production = list(filter(None, [reaction.math for reaction in self.producing_reactions]))
        consumption = list(filter(None, [reaction.math for reaction in self.consuming_reactions]))
        math = production.pop() if production else consumption.pop() if consumption else None
        for rates, ast_type in [(production, libsbml.AST_PLUS), (consumption, libsbml.AST_MINUS)]:
            for node in rates:
                math = ASTNode(type=ast_type, children=[math, node])
        return math

    @property
    def kinetic_laws(self):
        if self.model:
            return [reaction.kinetic_law for reaction in self.model.reactions if reaction.kinetic_law and
                    self.id in reaction.kinetic_law_symbols]


class ListOfSpecies(ListOf):

    _contained_type = Species

    def __init__(self, list_of_species: libsbml.ListOfSpecies = None):
        super().__init__(libsbml.ListOfSpecies, list_of_species)
