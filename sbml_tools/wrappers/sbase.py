
from abc import ABC
from sbml_tools.toolbox.validator import check
from sbml_tools.toolbox import sbml_io
import libsbml
import inspect
from typing import Union, List, Dict, Tuple, Iterable, Optional
from typing import Generic, TypeVar

from sbml_tools import config
from sbml_tools.utils import NAME_TO_SBO_TERM
from sbml_tools.wrappers import Wrapper, Annotation, CVTerm, History, Notes, SBOTerm, ASTNode, utils
from sbml_tools.wrappers.packages import SBML_PACKAGES


# TODO:
#  1. serialization, deserialization of the object: xml, json, yaml, pickle
#  2. primitive data types: positiveInteger, ID (for meta_id), SId (all regular id's)


# class ID(str):
#
#     def __init__(self, s : str):
#         if not s.startswith():
#
#
# class positiveInteger(int):
#     def __new__(cls, value, *args, **kwargs):
#         if value < 0:
#             raise ValueError("positive types must not be less than zero")
#         return  super(cls, cls).__new__(cls, value)
#
#     def __add__(self, other):
#         res = super(positive, self).__add__(other)
#         return self.__class__(res)
#
#     def __sub__(self, other):
#         res = super(positive, self).__sub__(other)
#         return self.__class__(res)
#
#     def __mul__(self, other):
#         res = super(positive, self).__mul__(other)
#         return self.__class__(res)
#
#     def __truediv__(self, other):
#         res = super(positive, self).__truediv__(other)
#         return self.__class__(res)
#
#     def __str__(self):
#         return "%d" % int(self)
#
#     def __repr__(self):
#         return "positive(%d)" % int(self)
#
#
# def test():
#     a = positive(2)
#     b = 3
#     # a - b # error as should


class SBase(Wrapper):

    # plugins = []

    # only defined here, used in Wrapper
    # EventAssignment, InitialAssignment and Rules overwrite to include id_attribute instead of id
    _base_eq_attrs = ['id', 'name', 'meta_id', 'sbo_term', 'notes', 'annotation']

    def __init__(self, sbase_class: libsbml.SBase, sbase=None, **kwargs):
        """ allow init from instance and class, let Child or Wrapper set self.wrapped """
        super().__init__(sbase_class, sbase, **kwargs)

    # def __init_subclass__(cls, **kwargs):
    #     super().__init_subclass__(**kwargs)
    #     cls.plugins.append(cls)

    @property
    def doc(self):
        return utils.wrap(self.wrapped.getSBMLDocument())

    @property
    def model(self):
        return utils.wrap(self.wrapped.getModel())

    @property
    def id(self) -> str:
        if self.wrapped.isSetId():
            return self.wrapped.getId()

    @id.setter
    def id(self, identifier: str):
        self._assert_not_empty_list_of()
        if isinstance(self.wrapped, (libsbml.EventAssignment, libsbml.InitialAssignment,
                                     libsbml.Rule)):
            # don't know why, it's not SBML and appears libsbml specific
            raise ValueError(f'InitialAssignments, Rules and EventAssignments do not have an id '
                             f'instead set id_attribute')
        if not identifier.isidentifier():
            raise ValueError(f"'{identifier}' is not a valid identifier")
        if self.model:
            element = self.model.wrapped.getElementBySId(identifier)
            if element and not element == self.wrapped:
                raise ValueError(f'id already in model: {element}')
        check(self.wrapped.setId(identifier))

    @property
    def name(self) -> str:
        if self.wrapped.isSetName():
            return self.wrapped.getName()

    @name.setter
    def name(self, name: str):
        self._assert_not_empty_list_of()
        check(self.wrapped.setName(name))

    @property
    def meta_id(self) -> str:
        if self.wrapped.isSetMetaId():
            return self.wrapped.getMetaId()

    @meta_id.setter
    def meta_id(self, meta_id: str):
        self._assert_not_empty_list_of()
        if self.doc:
            element = self.model.getElementByMetaId(meta_id)
            if element and not element == self.wrapped:
                raise ValueError(f'meta_id already in model: {self.doc.getElementByMetaId(meta_id)}')
        check(self.wrapped.setMetaId(meta_id))

    @property
    def sbo_term(self) -> SBOTerm:
        if self.wrapped.isSetSBOTerm():
            return SBOTerm(self.wrapped.getSBOTerm())

    @sbo_term.setter
    def sbo_term(self, sbo_term: Union[SBOTerm, int, str]):
        self._assert_not_empty_list_of()
        if isinstance(sbo_term, str):
            sbo_term = NAME_TO_SBO_TERM[sbo_term]
        if config.VALIDATE_SBO_ASSIGNMENT:
            utils.validate_sbo_term_assignment(self, sbo_term)
        check(self.wrapped.setSBOTerm(sbo_term))

    def remove_sbo_term(self):
        check(self.wrapped.unsetSBOTerm())

    @property
    def notes(self) -> Notes:
        if self.wrapped.isSetNotes():
            return Notes(self)

    @notes.setter
    def notes(self, notes: [Notes, str]):
        self._assert_not_empty_list_of()
        if isinstance(notes, str):
            notes = Notes.from_xhtml_string(notes)
        check(self.wrapped.setNotes(notes.as_string))

    def create_notes(self):
        self.notes = Notes()
        return self.notes

    def remove_notes(self):
        check(self.notes.remove())

    # Annotation, also comprises CVTerms and History
    @property
    def annotation(self) -> Annotation:
        if self.wrapped.isSetAnnotation():
            return Annotation(self.wrapped.getAnnotation())

    @annotation.setter
    def annotation(self, annotation: Annotation):
        self._assert_not_empty_list_of()
        if annotation.findall_by_tag('creator'):
            print(f"test for setting creators via annotation element is failing, try setting via 'history' instead")
        check(self.wrapped.setAnnotation(annotation.as_string))

    @property
    def cv_terms(self) -> List[CVTerm]:
        if self.wrapped.getNumCVTerms():
            return list(map(CVTerm, self.wrapped.getCVTerms()))

    @cv_terms.setter
    def cv_terms(self, cv_terms: List[CVTerm]):
        if isinstance(cv_terms, CVTerm):  # important, otherwise we iterate nested terms
            cv_terms = [cv_terms]
        self.remove_cv_terms()
        for cv_term in cv_terms:
            self.add_cv_term(cv_term)

    def add_cv_term(self, cv_term: CVTerm):
        utils.ensure_meta_id(self.wrapped)
        utils.check(self.wrapped.addCVTerm(cv_term.wrapped))

    @property
    def history(self) -> History:
        if self.wrapped.isSetModelHistory():
            return History(self.wrapped.getModelHistory())

    @history.setter
    def history(self, history: History):
        utils.ensure_meta_id(self.wrapped)
        utils.check(self.wrapped.setModelHistory(history.wrapped))

    def remove_annotation(self):
        utils.check(self.wrapped.unsetAnnotation())

    def remove_cv_terms(self):
        utils.check(self.wrapped.unsetCVTerms())

    def remove_history(self):
        utils.check(self.wrapped.unsetModelHistory())

    @property
    def xmlns(self) -> str:
        return self.wrapped.getURI()

    @property
    def level(self) -> int:
        return self.wrapped.getLevel()

    @property
    def version(self) -> int:
        return self.wrapped.getVersion()

    @property
    def packages(self):
        return [self.wrapped.getPlugin(i) for i in range(self.wrapped.getNumPlugins())]

    @property
    def package_ids(self):
        return [plugin.getPackageName() for plugin in self.packages]

    def enable_package(self, package: str):
        if package not in SBML_PACKAGES:
            raise ValueError(f"package '{package}' not recognized, recognizable: {list(SBML_PACKAGES)}")
        check(self.wrapped.enablePackage(SBML_PACKAGES[package].getXmlnsL3V1V1(), package, True))

    def disable_package(self, package: str):
        if package not in SBML_PACKAGES:
            raise ValueError(f"package '{package}' not recognized, recognizable: {list(SBML_PACKAGES)}")
        check(self.wrapped.enablePackage(SBML_PACKAGES[package].getXmlnsL3V1V1(), package, False))

    @property
    def xml_string(self) -> str:  # TODO: move to serialize
        return self.wrapped.toXMLNode().toXMLString()

    @property
    def has_required_attributes(self) -> bool:
        return self.wrapped.hasRequiredAttributes()

    def __repr__(self):
        return self.wrapped.__repr__()

    def _class_specific_attributes(self):
        # for developmental purposes
        return [x for x in dir(self.wrapped) if x not in dir(libsbml.SBase)]

    def _assert_not_empty_list_of(self):
        """otherwise settings sbase attributes on empty lists will not work nor yield an error"""
        if isinstance(self, list) and not self:
            raise ValueError(f'cannot set attribute on empty {self}')

    def serialize(self, data_type='xml'):
        """these are more convenient formats for sharing / storing"""
        assert data_type in ('xml', 'json', 'yaml', 'pickle')
        if data_type == 'xml':
            return self.wrapped.toXMLNode().toXMLString()
        raise NotImplementedError(f'serialization to {data_type} not implemented')

    # @classmethod
    # def deserialize(cls, data, data_type='xml'):
    #     # should return an instance of it's class
    #     if data_type == 'xml':
    #         return libsbml.XMLNode().convertStringToXMLNode(data)
    #     raise NotImplementedError(f'serialization to {data_type} not implemented')

    def __hash__(self):
        return hash(self.serialize())


class MathBase(SBase):
    """extends SBase with math"""
    def __init__(self, sbase_class: libsbml.SBase, sbase=None, **kwargs):
        super().__init__(sbase_class, sbase, **kwargs)

    @property
    def math(self) -> ASTNode:
        if self.wrapped.isSetMath():
            return ASTNode(self.wrapped.getMath())

    @math.setter
    def math(self, math: Union[ASTNode, str]):
        if isinstance(math, str):
            math = ASTNode.from_string(math)
        check(self.wrapped.setMath(math.wrapped))
