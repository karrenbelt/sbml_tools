import libsbml
from packaging import version
# currently not implemented, but need to at least check for their presence when loading models
# out commented packages are in develop mental stages (experimental version 5.18.1)

# TODO: none of these have been implemented as wrappers


SBML_PACKAGES = dict(
    comp=libsbml.CompExtension,
    fbc=libsbml.FbcExtension,
    layout=libsbml.LayoutExtension,
    render=libsbml.RenderExtension,
    qual=libsbml.QualExtension,
    groups=libsbml.GroupsExtension,
    multi=libsbml.MultiExtension,
    l3v2extendedmath=libsbml.L3v2extendedmathExtension,
    )


if version.parse(libsbml.__version__) >= version.parse('5.18.1'):
    SBML_PACKAGES.update(
        dict(
            arrays=libsbml.ArraysExtension,
            distrib=libsbml.DistribExtension,
            dyn=libsbml.DynExtension,
            req=libsbml.ReqExtension,
            spatial=libsbml.SpatialExtension,
        )
    )
