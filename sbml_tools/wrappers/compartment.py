import libsbml
from typing import Tuple, Union, Optional, List

from sbml_tools import settings
from sbml_tools.toolbox import read_sbml, check
from sbml_tools.wrappers import SBase, ListOf, utils


class CompartmentError(Exception):
    pass


class Compartment(SBase):
    """
    libsbml returns certain default for attributes, but these are not truly set (e.g. inspect compartment.xml_string)
    """
    required = ['id', 'constant']
    _eq_attrs = ['spatial_dimensions', 'size', 'units', 'constant']

    def __init__(self, compartment: libsbml.Compartment = None, **kwargs):
        super().__init__(libsbml.Compartment, compartment, **kwargs)

    @property
    def spatial_dimensions(self) -> Optional[int]:
        if self.wrapped.isSetSpatialDimensions():
            return self.wrapped.getSpatialDimensions()

    @spatial_dimensions.setter
    def spatial_dimensions(self, spatial_dimensions: Union[float, int]):
        # if a non-integer float is provided, libsbml defaults to 0.
        # if a negative value is provided, it defaults to 2**32 minus the spatial dimensions passed
        if spatial_dimensions not in range(11):
            raise CompartmentError(f'the number of spatial dimensions must be either: 0, 1, 2 or 3, ... 10.')
        check(self.wrapped.setSpatialDimensions(spatial_dimensions))

    @property
    def size(self) -> Optional[float]:
        if self.wrapped.isSetSize():
            return self.wrapped.getSize()

    @size.setter
    def size(self, size: float):
        if size < 0:
            raise CompartmentError(f'compartment size cannot be negative')
        check(self.wrapped.setSize(size))

    @property
    def units(self) -> str:
        if self.wrapped.isSetUnits():
            return self.wrapped.getUnits()

    @units.setter
    def units(self, units: str):
        # now if one want to change the units, one might need to change the dimensionality first
        utils.ensure_unit_definition(self, units)  # TODO: make into decorator
        check(self.wrapped.setUnits(units))

    @property
    def constant(self) -> Optional[bool]:
        if self.wrapped.isSetConstant():
            return self.wrapped.getConstant()

    @constant.setter
    def constant(self, constant: bool):
        check(self.wrapped.setConstant(constant))

    @utils.model_property
    def species(self):
        return [species for species in self.model.species if species.compartment == self.id]

    @utils.model_property
    def kinetic_laws(self):
        return [reaction.kinetic_law for reaction in self.model.reactions if reaction.kinetic_law and
                self.id in reaction.kinetic_law_symbols]


class ListOfCompartments(ListOf):

    _contained_type = Compartment

    def __init__(self, list_of_compartments: libsbml.ListOfCompartments = None):
        super().__init__(libsbml.ListOfCompartments, list_of_compartments)

    @property
    def spatial_dimensions(self) -> List[int]:
        return self.get_members_attribute('spatial_dimensions')

    @property
    def sizes(self) -> List[float]:
        return self.get_members_attribute('size')

    @property
    def units(self) -> List[str]:
        return self.get_members_attribute('units')

