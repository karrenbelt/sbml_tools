
import libsbml
from sbml_tools.wrappers import MathBase


class Delay(MathBase):
    """
    This math formula is used to compute the time between the trigger and the execution of the event assignments
    Delay math must evaluate to a non-negative number
    """
    required = []
    _eq_attrs = ['math']

    def __init__(self, delay: libsbml.Delay = None, **kwargs):
        super().__init__(libsbml.Delay, delay, **kwargs)

    def __repr__(self):
        return f"""{' '.join([self.__class__.__name__, self.id]).strip()}: {self.math}"""
