
from typing import Union
import libsbml
from sbml_tools.wrappers import SBase, ListOf, utils


class LocalParameter(SBase):
    """
    part of Reaction

    " a local parameter whose identifier is identical to a global identifier defined in the model takes precedence
      over the value associated with the global identifier. " - libsbml L3V2 documentation
    """
    required = ['id']
    _eq_attrs = ['id', 'value', 'units']

    def __init__(self, local_parameter: libsbml.LocalParameter = None, **kwargs):
        super().__init__(libsbml.LocalParameter, local_parameter, **kwargs)

    @property
    def value(self) -> float:
        if self.wrapped.isSetValue():
            return self.wrapped.getValue()

    @value.setter
    def value(self, value: Union[float, int]):
        utils.check(self.wrapped.setValue(value))

    @property
    def units(self) -> str:
        if self.wrapped.isSetUnits():
            return self.wrapped.getUnits()

    @units.setter
    def units(self, units: str):
        utils.ensure_unit_definition(self, units)
        utils.check(self.wrapped.setUnits(units))


class ListOfLocalParameters(ListOf):

    _contained_type = LocalParameter

    def __init__(self, list_of_local_parameters: libsbml.ListOfLocalParameters = None):
        super().__init__(libsbml.ListOfLocalParameters, list_of_local_parameters)
