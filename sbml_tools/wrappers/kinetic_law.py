
import re
from typing import List, Union, Optional, Iterable, Set
import libsbml
from sbml_tools.wrappers import MathBase, LocalParameter, ListOfLocalParameters, utils


class KineticLaw(MathBase):
    """
    part of a Reaction object

    " a local parameter whose identifier is identical to a global identifier defined in the model takes precedence
      over the value associated with the global identifier. " - libsbml L3V2 documentation
    """
    required = []
    _eq_attrs = ['math', 'local_parameters']

    def __init__(self, kinetic_law: libsbml.KineticLaw = None, **kwargs):
        super().__init__(libsbml.KineticLaw, kinetic_law, **kwargs)

    @property
    def reaction(self):
        return self.wrapped.getParentSBMLObject()

    def create_local_parameter(self, id: str, value: float = None, units: str = None, **kwargs):
        local_parameter = LocalParameter(level=self.level, version=self.version,
                                         id=id, value=value, units=units, **kwargs)
        self.add_local_parameter(local_parameter)
        return local_parameter

    def add_local_parameter(self, local_parameter: Union[LocalParameter, str]):
        if isinstance(local_parameter, str):
            local_parameter = utils.string_to_parameter(local_parameter, local=True,
                                                        level=self.level, version=self.version)
        utils.check(self.wrapped.addLocalParameter(local_parameter.wrapped))

    def remove_local_parameter(self, local_parameter: Union[str, int]) -> Optional[LocalParameter]:
        """remove by id or index"""
        local_parameter = self.wrapped.removeLocalParameter(local_parameter)
        if local_parameter:
            return LocalParameter(local_parameter)

    @property
    def local_parameters(self) -> List[LocalParameter]:
        return ListOfLocalParameters(self.wrapped.getListOfLocalParameters())

    def remove_all_local_parameters(self):
        for _ in range(len(self.local_parameters)):
            self.remove_local_parameter(0)

    @local_parameters.setter
    def local_parameters(self, local_parameters: Union[Iterable[Union[LocalParameter, str]], str]):
        self.remove_all_local_parameters()
        if isinstance(local_parameters, str):
            local_parameters = utils.semi_colon_splitter(local_parameters)
        for local_parameter in local_parameters:
            self.add_local_parameter(local_parameter)

    @property
    def compartments(self):
        if self.model and self.math:
            return [parameter for parameter in self.model.compartments if parameter.id in self.math.symbols]

    @property
    def parameters(self):
        if self.model and self.math:
            return [parameter for parameter in self.model.parameters if parameter.id in self.math.symbols]

    @property
    def species(self):
        if self.model and self.math:
            return [species for species in self.model.species if species.id in self.math.symbols]

    def __repr__(self):
        return f"<{' '.join([self.__class__.__name__, self.reaction.id])}>"
