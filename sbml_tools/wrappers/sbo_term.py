###############
### SBOTerm ###
###############
# SBO - the sbo file is NOT tree-structured
# note that effectively SBO is a set of 7 'trees'
# some leafs are part of multiple branches; these are not proper trees thus but DAGs

import libsbml
import networkx as nx

from sbml_tools.wrappers import utils


SBO_TAGS = {
    'CONSERVATION_LAW': libsbml.SBO_isConservationLaw,
    'CONTINUOUS_FRAMEWORK': libsbml.SBO_isContinuousFramework,
    'DISCRETE_FRAMEWORK': libsbml.SBO_isDiscreteFramework,
    'ENTITY': libsbml.SBO_isEntity,
    'EVENT': libsbml.SBO_isEvent,
    'FUNCTIONAL_COMPARTMENT': libsbml.SBO_isFunctionalCompartment,
    'FUNCTIONAL_ENTITY': libsbml.SBO_isFunctionalEntity,
    'INTERACTION': libsbml.SBO_isInteraction,
    'KINETIC_CONSTANT': libsbml.SBO_isKineticConstant,
    'LOGICAL_FRAMEWORK': libsbml.SBO_isLogicalFramework,
    'MATERIAL_ENTITY': libsbml.SBO_isMaterialEntity,
    'MATHEMATICAL_EXPRESSION': libsbml.SBO_isMathematicalExpression,
    'METADATA_REPRESENTATION': libsbml.SBO_isMetadataRepresentation,
    'MODELLING_FRAMEWORK': libsbml.SBO_isModellingFramework,
    'MODIFIER': libsbml.SBO_isModifier,
    'OBSOLETE': libsbml.SBO_isObselete,  # type in libsbml code: obsolete
    'OCCURRING_ENTITY_REPRESENTATION': libsbml.SBO_isOccurringEntityRepresentation,
    'PARTICIPANT': libsbml.SBO_isParticipant,
    'PARTICIPANT_ROLE': libsbml.SBO_isParticipantRole,
    'PHYSICAL_ENTITY_REPRESENTATION': libsbml.SBO_isPhysicalEntityRepresentation,
    'PHYSICAL_PARTICIPANT': libsbml.SBO_isPhysicalParticipant,
    'PRODUCT': libsbml.SBO_isProduct,
    'QUANTITATIVE_PARAMETER': libsbml.SBO_isQuantitativeParameter,
    'QUANTITATIVE_SYSTEMS_DESCRIPTION_PARAMETER': libsbml.SBO_isQuantitativeSystemsDescriptionParameter,
    'RATE_LAW': libsbml.SBO_isRateLaw,
    'REACTANT': libsbml.SBO_isReactant,
    'STEADY_STATE_EXPRESSION': libsbml.SBO_isSteadyStateExpression,
    'SYSTEMS_DESCRIPTION_PARAMETER': libsbml.SBO_isSystemsDescriptionParameter, }

SBML_COMPONENT_SBO_BRANCHES = {
    libsbml.SBMLDocument: 4,  # modeling framework
    libsbml.Model: [231, 4],  # occurring entity representation / modeling framework
    libsbml.FunctionDefinition: 64,  # mathematical Expression
    libsbml.Compartment: 236,  # physical entity representation
    libsbml.Species: 236,  # physical entity representation
    libsbml.Reaction: 231,  # occurring entity representation
    libsbml.Parameter: 545,  # systems description parameter
    libsbml.SpeciesReference: 3,  # participant role
    libsbml.ModifierSpeciesReference: 3,  # participant role
    libsbml.KineticLaw: 64,  # mathematical Expression
    libsbml.LocalParameter: 545,  # systems description parameter
    libsbml.InitialAssignment: 64,  # mathematical Expression
    libsbml.AlgebraicRule: 64,  # mathematical Expression
    libsbml.AssignmentRule: 64,  # mathematical Expression
    libsbml.RateRule: 64,  # mathematical Expression
    libsbml.Constraint: 64,  # mathematical Expression
    libsbml.Event: 231,  # occurring entity representation
    libsbml.Trigger: 64,  # mathematical Expression
    libsbml.Priority: 64,  # mathematical Expression
    libsbml.Delay: 64,  # mathematical Expression
    libsbml.EventAssignment: 64,  # mathematical Expression
}


SBO = utils.get_sbo()
NAME_TO_SBO_TERM = {data.get('name'): sbo_id for sbo_id, data in SBO.items()}
SBO_DAG = utils.construct_sbo_dag()


class SBOTermError(Exception):
    pass


class SBOTerm(int):
    """
    This is just a fancy integer.

    although the SBO is often described as a tree, it is not a proper tree;
    as nodes can have multiple parents, a directed acyclic graph (DAG) appears more suitable

    Systems Biology Ontology (SBO) is composed of hierarchically arranged sets of controlled vocabularies
    that are commonly used in mathematical modelling. They represent a "is a" relationship (http://www.ebi.ac.uk/sbo).
    SBO is part of the Open Biological and Biomedical Ontology (OBO) candidate ontologies (http://www.obofoundry.org).

    It consists are seven orthogonal vocabularies, represented by a 'branch' of the ontology:
        entity
        interaction
        mathematical expression
        metadata representation
        modelling framework
        participant role
        quantitative parameter

    There are currently no specific SBO terms that correspond to SBML, UnitDefinition, Unit or ListOf__ classes

    NOTES:
        1. if the SBase object has no sbo_term, it will default to -1, which makes many methods not work properly
           to accommodate this behaviour of libsbml, we therefore do not validate sbo_terms by default

    """

    # SBO-derived properties
    @property
    def as_string(self) -> str:
        return SBO[self].get('id', '')

    @property
    def name(self):
        return SBO[self].get('name')

    @property
    def synonym(self):
        return SBO[self].get('synonym')

    @property
    def definition(self):  # only 51 is missing a definition
        return SBO[self].get('def')

    @property
    def math(self):
        math = SBO[self].get('math')
        if math:
            from sbml_tools.wrappers import ASTNode
            return ASTNode.from_string(math)

    @property
    def comment(self):
        return SBO[self].get('comment')

    @property
    def parent_branch(self):
        if self == -1 or self == 1000:
            return
        return SBOTerm(libsbml.SBO_getParentBranch(self))

    # graph-properties
    @property
    def direct_successors(self):
        return list(map(SBOTerm, SBO_DAG.successors(self)))

    @property
    def all_successors(self):
        return [SBOTerm(node) for node in nx.dfs_tree(SBO_DAG, self) if not node == self]

    @property
    def all_parent_branch_successors(self):
        return list(map(SBOTerm, nx.dfs_tree(SBO_DAG, self.parent_branch)))

    @property
    def direct_predecessors(self) -> list:
        return list(map(SBOTerm, SBO_DAG.predecessors(self)))

    @property
    def all_simple_paths(self):
        paths = nx.algorithms.all_simple_paths(SBO_DAG, self.parent_branch, self)
        return [list(map(SBOTerm, path)) for path in paths]

    @property
    def tags(self):
        return [k for k, f in SBO_TAGS.items() if f(self)]

    def __repr__(self):
        return f"""<{' '.join([self.__class__.__name__, self.as_string]).strip()}>"""

    def __str__(self):
        return self.as_string
