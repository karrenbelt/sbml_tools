
import libsbml
from sbml_tools.wrappers import SimpleSpeciesReference, ListOf


class ModifierSpeciesReference(SimpleSpeciesReference):
    """
    part of Reaction
    """
    required = ['species']
    _eq_attrs = ['species']

    def __init__(self, modifier_species_reference: libsbml.ModifierSpeciesReference = None, **kwargs):
        super().__init__(libsbml.ModifierSpeciesReference, modifier_species_reference, **kwargs)


class ListOfModifierSpeciesReferences(ListOf):  # also uses ListOfSpeciesReferences?

    _contained_type = ModifierSpeciesReference

    def __init__(self, list_of_modifier_species_references: libsbml.ListOfSpeciesReferences = None):
        super().__init__(libsbml.ListOfSpeciesReferences, list_of_modifier_species_references)
