import libsbml
from typing import Tuple, Union, List, Iterable

from sbml_tools.toolbox.sbml_io import read_sbml
from sbml_tools.toolbox.validator import check
from sbml_tools import settings
from sbml_tools.wrappers import SBase, ListOf, Unit, ListOfUnits, utils
# from sbml_tools.wrappers.unit import unit_registry
import operator
import functools
from pint import UnitRegistry

unit_registry = UnitRegistry()


class UnitDefinitionError(Exception):
    pass


class UnitDefinition(SBase):
    # unit_definition = unit_1 * unit_2 * ... * unit_n
    required = ['id']
    _eq_attrs = ['units']

    def __init__(self, unit_definition: libsbml.UnitDefinition = None, **kwargs):
        super().__init__(libsbml.UnitDefinition, unit_definition, **kwargs)

    @property
    def units(self) -> ListOfUnits:
        return ListOfUnits(self.wrapped.getListOfUnits())

    def add_unit(self, unit: Unit):
        check(self.wrapped.addUnit(utils.check_required_attributes(unit).wrapped))

    @units.setter
    def units(self, units: Iterable[Unit]):
        self.remove_units()
        for unit in units:
            self.add_unit(unit)

    @property
    def is_empty(self) -> bool:
        return not bool(self.units)

    def remove_units(self):
        for i in range(self.wrapped.getNumUnits()):
            check(self.wrapped.removeUnit(0))

    def simplify(self):  # inplace
        libsbml.UnitDefinition.simplify(self.wrapped)

    def convert_to_SI(self):  # also simplifies
        self.wrapped = self.wrapped.convertToSI(self.wrapped)

    @property
    def as_quantity(self):
        assert self.units, f'no units associated with unit_definition {self}'
        return functools.reduce(operator.mul, [unit.as_quantity for unit in self.units])

    @classmethod
    def from_quantity(cls, quantity: unit_registry.Quantity, identifier, level=3, version=2):
        magnitude, units = quantity.to_tuple()
        sbml_units = [Unit(kind=kind, exponent=exponent, scale=0, multiplier=magnitude) if i == 0 else
                      Unit(kind=kind, exponent=exponent, scale=0, multiplier=1.0)
                      for i, (kind, exponent) in enumerate(units)]
        return cls(level=level, version=version, id=identifier, name=str(quantity.units), units=sbml_units)

    def equals(self, other: 'UnitDefinition') -> bool:
        # process: remove scale (change to multiplier), convert to base SI, simplify, order alphabetically
        if not isinstance(other, UnitDefinition):
            raise UnitDefinitionError(f'other should be {self.__class__.__name__}, got {other.__class__.__name__}')
        udef_self = libsbml.UnitDefinition.convertToSI(self.wrapped)
        udef_other = libsbml.UnitDefinition.convertToSI(other.wrapped)
        return libsbml.UnitDefinition.areIdentical(udef_self, udef_other)

    @property
    def is_variant_of_area(self):
        return self.wrapped.isVariantOfArea()

    @property
    def is_variant_of_dimensionless(self):
        return self.wrapped.isVariantOfDimensionless()

    @property
    def is_variant_of_length(self):
        return self.wrapped.isVariantOfLength()

    @property
    def is_variant_of_mass(self):
        return self.wrapped.isVariantOfMass()

    @property
    def is_variant_of_substance(self):
        return self.wrapped.isVariantOfSubstance()

    @property
    def is_variant_of_substance_per_time(self):
        return self.wrapped.isVariantOfSubstancePerTime()

    @property
    def is_variant_of_time(self):
        return self.wrapped.isVariantOfTime()

    @property
    def is_variant_of_volume(self):
        return self.wrapped.isVariantOfVolume()

    def __repr__(self):
        # units = '\n\t'.join(map(repr, self.units))
        return f"<{' '.join([self.__class__.__name__, self.id if self.id else '']).strip()}>"
               # f"\n\t{units}"


class ListOfUnitDefinitions(ListOf):
    _contained_type = UnitDefinition

    def __init__(self, list_of_units_definitions: libsbml.ListOfUnitDefinitions = None):
        super().__init__(libsbml.ListOfUnitDefinitions, list_of_units_definitions)

