
import re
import dateutil
from datetime import datetime
import libsbml
from sbml_tools import config
from sbml_tools.wrappers import Wrapper, utils


def get_current_date_string() -> str:
    return re.sub('\.[0-9]+', '', datetime.now().astimezone().isoformat())


def is_valid_date_string(date: str) -> bool:
    try:
        dateutil.parser.parse(date)
        return True
    except:
        return False


class DateError(Exception):
    pass


class Date(Wrapper):
    """
    NOTE: initializing a datetime from libsbml set a default <Date 2000-01-01T00:00:00Z>
          Opted to overwrite this default with the current time at the moment of object instantiation
    """
    _eq_attrs = ['date_string']

    def __init__(self, date: libsbml.Date = None, **kwargs):
        super().__init__(libsbml.Date, date, **kwargs)
        if not date and not kwargs and config.AUTOMATED_DATE_INIT_TO_CURRENT:
            self.date_string = get_current_date_string()

    @property
    def year(self) -> int:
        return self.wrapped.getYear()

    @year.setter
    def year(self, year: int):
        utils.check(self.wrapped.setYear(year))

    @property
    def month(self) -> int:
        return self.wrapped.getMonth()

    @month.setter
    def month(self, month):
        utils.check(self.wrapped.setMonth(month))

    @property
    def day(self) -> int:
        return self.wrapped.getDay()

    @day.setter
    def day(self, day):
        utils.check(self.wrapped.setDay(day))

    @property
    def hour(self) -> int:
        return self.wrapped.getHour()

    @hour.setter
    def hour(self, hour):
        utils.check(self.wrapped.setHour(hour))

    @property
    def minute(self) -> int:
        return self.wrapped.getMinute()

    @minute.setter
    def minute(self, minute):
        utils.check(self.wrapped.setMinute(minute))

    @property
    def second(self) -> int:
        return self.wrapped.getSecond()

    @second.setter
    def second(self, second=int):
        utils.check(self.wrapped.setSecond(second))

    @property
    def hours_offset(self) -> int:
        return self.wrapped.getHoursOffset()

    @hours_offset.setter
    def hours_offset(self, hours_offset: int):
        if hours_offset < 0:
            self.wrapped.setSignOffset(0)
        else:
            self.wrapped.setSignOffset(1)
        utils.check(self.wrapped.setHoursOffset(abs(hours_offset)))

    @property
    def minutes_offset(self) -> int:
        return self.wrapped.getMinutesOffset()

    @minutes_offset.setter
    def minutes_offset(self, minutes_offset: int):
        utils.check(self.wrapped.setMinutesOffset(minutes_offset))

    @property
    def date_string(self) -> str:
        return self.wrapped.getDateAsString()

    @date_string.setter
    def date_string(self, date: str):
        if not is_valid_date_string(date):
            raise DateError(f'not a valid date string: {date}')
        utils.check(self.wrapped.setDateAsString(date))

    @classmethod
    def from_date_string(cls, date_string: str):
        date = libsbml.Date()
        utils.check(date.setDateAsString(date_string))
        return cls(date)

    @classmethod
    def from_current_time(cls):
        return cls.from_date_string(get_current_date_string())

    @property
    def as_datetime(self):
        return dateutil.parser.parse(self.date_string)

    def __gt__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.as_datetime > other.as_datetime

    def __ge__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.as_datetime >= other.as_datetime

    def __lt__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.as_datetime < other.as_datetime

    def __le__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented
        return self.as_datetime <= other.as_datetime

    def validate(self):
        if not self.wrapped.representsValidDate():
            raise DateError('not a valid date')

    def __repr__(self):
        return f"""<{self.__class__.__name__} {self.date_string}>"""
