
from typing import List

import libsbml
from sbml_tools.wrappers import ListOf, MathBase, utils

# 'isCompartmentVolume',
# 'isParameter',
# 'isSetUnits',
# 'isSetVariable',
# 'isSpeciesConcentration',


class Rule(MathBase):
    """
    abstract base class of AlgebraicRule, AssignmentRule, and RateRule
    """
    _base_eq_attrs = ['id_attribute', 'name', 'meta_id', 'sbo_term', 'notes', 'annotation']

    @property
    def id_attribute(self) -> str:
        return self.wrapped.getIdAttribute()

    @id_attribute.setter
    def id_attribute(self, id_attribute: str):
        utils.check(self.wrapped.setIdAttribute(id_attribute))

    @property
    def units(self) -> str:
        return self.wrapped.getUnits()

    @units.setter
    def units(self, units: str):  # only works if associated with a model?
        utils.check(self.wrapped.setUnits(units))

    def __repr__(self):
        return f"<{self.__class__.__name__}>"


class ListOfRules(ListOf):

    _contained_type = Rule

    def __init__(self, list_of_rules: libsbml.ListOfRules = None):
        super().__init__(libsbml.ListOfRules, list_of_rules)

    @property
    def variables(self) -> List[str]:
        return [rule.variable for rule in self if hasattr(rule, 'variable')]

