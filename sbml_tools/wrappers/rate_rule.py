
from typing import Union

import libsbml
from sbml_tools.wrappers import Rule, utils
from sbml_tools.wrappers import Compartment, Species, Parameter
from sbml_tools.wrappers.species_reference import SpeciesReference


class RateRule(Rule):
    """
    left-hand side is a rate-of-change:
        dx/dt = f(W), where f is some arbitrary function and W is a vector of symbols

    Used to set rates of variables:
        - species: sets rate of change of amount or concentration (units should be species unit / time)
          there may not be a species reference referencing the same object, unless boundary condition == True
        - species_reference: sets rate of change of stoichiometries of reactants / products (units: dimensionless / time)
        - compartment: sets rate of change of compartments size (units should be compartment unit / time)
        - parameter: sets rate of change of parameter value (units should be parameter unit / time)
    """
    required = ['variable']
    _eq_attrs = ['variable', 'math', 'id_attribute']

    def __init__(self, rate_rule: libsbml.RateRule = None, **kwargs):
        super().__init__(libsbml.RateRule, rate_rule, **kwargs)

    @property
    def variable(self) -> str:
        return self.wrapped.getVariable()

    @variable.setter
    def variable(self, variable: str):
        utils.check(self.wrapped.setVariable(variable))

    @utils.model_property
    def object(self) -> Union[Compartment, Species, SpeciesReference, Parameter]:
        return self.model.get(self.variable)

    def __repr__(self):
        return f"""<{self.__class__.__name__} {self.variable}>"""
