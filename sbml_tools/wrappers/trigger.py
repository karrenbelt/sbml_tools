
import libsbml
from sbml_tools.wrappers import MathBase, utils


class Trigger(MathBase):
    """
    Part of an Event.
    Trigger math must evaluate to a boolean
    Can be triggered more than once; whenever the math evaluation changes from False -> True

    initial value:
    - if set to True: the event can only trigger sometime after simulation started when the math switches False -> True
    - if set to False: the event may trigger at t == 0, if it's math at that point in time evaluates as True

    persistent:
    - if set to True: the event will be triggered, even if the math switches from True --> False before execution
    - if set to False: if the math evaluates from True --> False and not yet executed, it's aborted.
      This latter option is required for dealing with multiple simultaneous events, and Delays
    """
    required = ['initial_value', 'persistent']
    _eq_attrs = ['initial_value', 'persistent', 'math']

    def __init__(self, trigger: libsbml.Trigger = None, **kwargs):
        super().__init__(libsbml.Trigger, trigger, **kwargs)

    @property
    def initial_value(self) -> bool:
        if self.wrapped.isSetInitialValue():
            return self.wrapped.getInitialValue()

    @initial_value.setter
    def initial_value(self, initial_value: bool):
        utils.check(self.wrapped.setInitialValue(initial_value))

    @property
    def persistent(self) -> bool:
        if self.wrapped.isSetPersistent():
            return self.wrapped.getPersistent()

    @persistent.setter
    def persistent(self, persistent: bool):
        utils.check(self.wrapped.setPersistent(persistent))

    def __repr__(self):
        return f"""{' '.join([self.__class__.__name__, self.id]).strip()}:
               switch: {self.math}
           persistent: {self.persistent}
        initial_value: {self.initial_value}"""
