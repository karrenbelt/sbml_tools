
from typing import Union
import re
import libsbml
from bidict import bidict
from sbml_tools.wrappers import Wrapper, utils


QUALIFIER_TYPES = bidict(
    MODEL=libsbml.MODEL_QUALIFIER,
    BIOLOGICAL=libsbml.BIOLOGICAL_QUALIFIER,
    UNKNOWN=libsbml.UNKNOWN_QUALIFIER,
)

BQM = bidict(
    IS=libsbml.BQM_IS,
    IS_DESCRIBED_BY=libsbml.BQM_IS_DESCRIBED_BY,
    IS_DERIVED_FROM=libsbml.BQM_IS_DERIVED_FROM,
    IS_INSTANCE_OF=libsbml.BQM_IS_INSTANCE_OF,
    HAS_INSTANCE=libsbml.BQM_HAS_INSTANCE,
    UNKNOWN=libsbml.BQM_UNKNOWN,
)

BQB = bidict(
    IS=libsbml.BQB_IS,
    HAS_PART=libsbml.BQB_HAS_PART,
    IS_PART_OF=libsbml.BQB_IS_PART_OF,
    IS_VERSION_OF=libsbml.BQB_IS_VERSION_OF,
    HAS_VERSION=libsbml.BQB_HAS_VERSION,
    IS_HOMOLOG_TO=libsbml.BQB_IS_HOMOLOG_TO,
    IS_DESCRIBED_BY=libsbml.BQB_IS_DESCRIBED_BY,
    IS_ENCODED_BY=libsbml.BQB_IS_ENCODED_BY,
    OCCURS_IN=libsbml.BQB_OCCURS_IN,
    ENCODES=libsbml.BQB_ENCODES,
    HAS_PROPERTY=libsbml.BQB_HAS_PROPERTY,
    IS_PROPERTY_OF=libsbml.BQB_IS_PROPERTY_OF,
    HAS_TAXON=libsbml.BQB_HAS_TAXON,
    UNKNOWN=libsbml.BQB_UNKNOWN,
)


class CVTermError(Exception):
    pass


class CVTerm(Wrapper):
    """
    Uniform Resource Identifier (URI)
    Minimal Information Requested In the Annotation of biochemical Models (MIRIAM)
    This is a little bit more strict than libsbml is, in that one cannot set unknown values
    """
    required = []
    _eq_attrs = ['qualifier_type', 'model_qualifier', 'biological_qualifier', 'resources']

    def __init__(self, cv_term: libsbml.CVTerm = None, **kwargs):
        super().__init__(libsbml.CVTerm, cv_term, **kwargs)

    @property
    def qualifier_type(self) -> str:
        return QUALIFIER_TYPES.inverse[self.wrapped.getQualifierType()].lower()

    @qualifier_type.setter
    def qualifier_type(self, qualifier_type: Union[int, str]):
        # changing the qualifier type will unset set model / biological qualifier
        if isinstance(qualifier_type, str):
            qualifier_type = QUALIFIER_TYPES[qualifier_type.upper()]
        if not qualifier_type in QUALIFIER_TYPES.inverse:
            raise CVTermError(f'Invalid qualifier type: {qualifier_type}')
        if qualifier_type == QUALIFIER_TYPES['UNKNOWN']:
            raise CVTermError(f'Unknown qualifier type')
        utils.check(self.wrapped.setQualifierType(qualifier_type))

    @property
    def model_qualifier(self) -> str:
        return BQM.inverse[self.wrapped.getModelQualifierType()].lower()

    @model_qualifier.setter
    def model_qualifier(self, qualifier: Union[int, str]):
        if isinstance(qualifier, str):
            qualifier = BQM.get(qualifier.upper(), qualifier)
        if not self.qualifier_type == 'model':
            raise CVTermError(f'Qualifier type is: {self.qualifier_type}')
        if not qualifier in BQM.inverse:
            raise CVTermError(f'Invalid model qualifier: {qualifier}')
        if qualifier == BQM['UNKNOWN']:
            raise CVTermError(f'Unknown model qualifier')
        utils.check(self.wrapped.setModelQualifierType(qualifier))

    @property
    def biological_qualifier(self) -> str:
        return BQB.inverse[self.wrapped.getBiologicalQualifierType()].lower()

    @biological_qualifier.setter
    def biological_qualifier(self, qualifier: Union[int, str]):
        if isinstance(qualifier, str):
            qualifier = BQB.get(qualifier.upper(), qualifier)
        if not self.qualifier_type == 'biological':
            raise CVTermError(f'Qualifier type is: {self.qualifier_type}')
        if not qualifier in BQB.inverse:
            raise CVTermError(f'Invalid biological qualifier: {qualifier}')
        if qualifier == BQB['UNKNOWN']:
            raise CVTermError(f'Unknown biological qualifier')
        utils.check(self.wrapped.setBiologicalQualifierType(qualifier))

    @property
    def resources(self) -> list:
        return [self.wrapped.getResourceURI(i) for i in range(self.wrapped.getNumResources())]

    @resources.setter
    def resources(self, resources):
        for resource in resources:
            self.add_resource(resource)

    def add_resource(self, resource: str):
        if not utils.is_valid_miriam(resource):
            raise CVTermError('Invalid MIRIAM Identifier')
        utils.check(self.wrapped.addResource(resource))

    @property
    def nested_cv_terms(self) -> list:
        return list(map(CVTerm, [self.wrapped.getNestedCVTerm(i) for i in range(self.wrapped.getNumNestedCVTerms())]))

    def add_nested_cv_terms(self, cv_term: 'CVTerm'):
        cv_term.validate()
        utils.check(self.wrapped.addNestedCVTerm(cv_term.wrapped))

    def has_required_attributes(self) -> bool:
        """qualifier type, model / biological qualifier and at least one resource"""
        return self.wrapped.hasRequiredAttributes()

    def validate(self):
        def is_valid(cv_term):
            if not cv_term.has_required_attributes():
                return False
            if not all(map(utils.is_valid_miriam, cv_term.resources)):
                return False
            return all(map(is_valid, cv_term.nested_cv_terms))

        if not is_valid(self):
            raise CVTermError('invalid CVTerm attributes or resource identifiers')

    def __repr__(self):
        qualifier = self.model_qualifier if self.qualifier_type == 'model' else self.biological_qualifier
        return f"<{self.__class__.__name__} {self.qualifier_type}: {qualifier}>"

    def __iter__(self):
        """pre-order tree traversal"""
        yield self   # TODO: this just looks really stupid, why yield self?
        for cv_term in self.nested_cv_terms:
            yield from cv_term

    def __eq__(self, other):
        if not isinstance(other, type(self)):
            return NotImplemented

        def elements_equal(cv1, cv2):
            if not len(self.nested_cv_terms) == len(other.nested_cv_terms):
                return False
            for field in self._eq_attrs:
                if not getattr(cv1, field) == getattr(cv2, field):
                    return False
            return all(elements_equal(*children) for children in zip(cv1.nested_cv_terms, cv2.nested_cv_terms))

        return elements_equal(self, other)