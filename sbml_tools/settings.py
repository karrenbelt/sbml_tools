import os
import inspect
import platform

# Directories
SCRIPT_DIR     = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
BASE_DIR       = os.path.join(*os.path.split(SCRIPT_DIR)[0:-1])

DATA_DIR       = os.path.join(BASE_DIR, 'data')
CACHE_DIR      = os.path.join(BASE_DIR, 'cache')
MODEL_DIR      = os.path.join(BASE_DIR, 'models')
TEST_DIR       = os.path.join(BASE_DIR, 'tests')
LOG_DIR        = os.path.join(BASE_DIR, 'log')
FIGURE_DIR     = os.path.join(BASE_DIR, 'figures')

BIGG_DIR           = os.path.join(DATA_DIR, 'bigg')
NCBI_DIR           = os.path.join(DATA_DIR, 'ncbi')
BRENDA_DIR         = os.path.join(DATA_DIR, 'brenda')
ECOCYC_DIR         = os.path.join(DATA_DIR, 'ecocyc')
REGULONDB_DIR      = os.path.join(DATA_DIR, 'regulonDB')
IDENTIFIER_DIR     = os.path.join(DATA_DIR, 'identifiers')
PBALANCING_DIR     = os.path.join(DATA_DIR, 'parameter_balancing')
SBTAB_DIR          = os.path.join(DATA_DIR, 'sbtab')

# SBML test suite models
TEST_SUITE_SOURCE_DIR = os.path.join(BASE_DIR, 'sbml-test-suite/')
TEST_CASES_SEMANTIC_DIR = os.path.join(TEST_SUITE_SOURCE_DIR, 'cases/semantic')

# Data files
PB_PRIOR_FILE = os.path.join(PBALANCING_DIR, 'pb_prior.tsv')
PB_OPTIONS_FILE = os.path.join(PBALANCING_DIR, 'pb_options.tsv')

BIGG_METABOLITE_FILE    = os.path.join(BIGG_DIR, 'bigg_models_metabolites.txt')
BIGG_REACTION_FILE      = os.path.join(BIGG_DIR, 'bigg_models_reactions.txt')
BIGG_UNIVERSAL_MODEL     = os.path.join(BIGG_DIR, 'universal_model.json')

# Systems Biology Ontology
SBO_FILE = os.path.join(DATA_DIR, 'SBO_XML.xml')

# BRENDA enzyme database and CHEBI ontology files
BRENDA_DOWNLOAD = os.path.join(BRENDA_DIR, 'brenda_download.txt')
BTO_OWL = os.path.join(BRENDA_DIR, 'bto.owl')
CHEBI_OWL = os.path.join(BRENDA_DIR, 'chebi.owl')

# NCBI taxonomy
NCBI_TAXONOMY_NAMES = os.path.join(NCBI_DIR, 'taxdump', 'names.dmp')
NCBI_TAXONOMY_NODES = os.path.join(NCBI_DIR, 'taxdump', 'nodes.dmp')

# downloaded from https://www.ebi.ac.uk/miriam/main/export/
MIRIAM_FILE = os.path.join(IDENTIFIER_DIR, 'IdentifiersOrg-Registry.xml')

# note: these are files obtained from Elad Noor
BRENDA_TAXONOMY_FILE = os.path.join(BRENDA_DIR, 'taxonomy.csv')
ECOCYC_REG_FILE = os.path.join(ECOCYC_DIR, 'EcoCycRegulation.csv')  # ECOCYC
REGULONDB_TF_FILE = os.path.join(REGULONDB_DIR, 'TF_gene_interactions.txt')  # RegulonDB
META_ANALYSIS_FILE = os.path.join(DATA_DIR, 'meta_analysis.xls')  # from Elad Noor

# genbank file
GENBANK_BW25113 = os.path.join(DATA_DIR, 'sequence_BW25113.gb')

# Model files - test case models in SBML test suite

# FBA
# E_COLI_CORE_MODEL         = os.path.join(MODEL_DIR, 'e_coli_core.xml.gz')
# ECOLI_GSMM_MODEL          = os.path.join(MODEL_DIR, 'iJO1366.xml.gz')
IML1515 = os.path.join(MODEL_DIR, 'iML1515.xml')
IJO1366 = os.path.join(MODEL_DIR, 'iJO1366.xml')
IJO1366_subsystems = os.path.join(MODEL_DIR, 'iJO1366_mid.xml')  # from Tomek

# ODE
CHASSAGNOLE_MODEL   = os.path.join(MODEL_DIR, 'chassagnole.xml')
CHRISTODOULOU_MODEL = os.path.join(MODEL_DIR, 'christodoulou2018.xml')


# verify existence of directories and files defined above
for name, value in locals().copy().items():
    if name.endswith('_DIR') and not os.path.isdir(value):
        raise NotADirectoryError(f'not found:\n {name}: {value}')
    elif name.endswith('_FILE') and not os.path.isfile(value):
        raise FileNotFoundError(f'not found:\n {name}: {value}')

