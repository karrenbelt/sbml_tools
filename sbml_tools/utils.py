import os
import sys
import re
import json
import sympy
import pickle
import itertools
import functools
import operator
import collections
import logging
import time
import inspect
from logging.handlers import TimedRotatingFileHandler
import typing
from typing import get_type_hints
from typing import List, Union, Iterator
import hashlib
import xmltodict
import numpy as np
import pandas as pd
from uncertainties import ufloat
import matplotlib.pyplot as plt
from scipy.stats import truncnorm
from sbml_tools import settings
import cProfile, pstats, io
import datetime
from lxml import etree

reversible_arrow_finder = re.compile("<(-+|=+)>")
forward_arrow_finder = re.compile("(-+|=+)>")
reverse_arrow_finder = re.compile("<(-+|=+)")

replace_special_characters = dict((re.escape(s), '_') for s in (' ', ':', '/', '\\', '-'))
replace_pattern = re.compile("|".join(replace_special_characters.keys()))


def get_time_stamp():
    now = str(datetime.datetime.now()).split('.')[0]
    return replace_pattern.sub(lambda m: replace_special_characters[re.escape(m.group(0))], now)


def arrow_match(formula: str):
    return [f.search(formula) for f in (reversible_arrow_finder, forward_arrow_finder, reverse_arrow_finder)]


def single_character_splitter(s, split_on):
    ''' Splits only on single, not consecutive, occurrence of a character, using positive lookbehind / lookahead
    NOTE: No escape character built-in for split_on (e.g. escaping on /, one should supply also the escape: \/'''
    allowable_characters = 'A-Za-z0-9\(\)'
    return re.split(rf'(?<=[{allowable_characters}]){split_on}(?=[{allowable_characters}])', s)


def get_compartment_from_suffix(metabolite_identifier: str):
    parts = single_character_splitter(metabolite_identifier, '_')  # the assumed connecting element
    if len(parts) <= 1:
        raise ValueError(f"cannot split compartment identifier from metabolite suffix: {metabolite_identifier}")
    return parts[-1]


def replace(s: str, d: dict):
    rep = {re.escape(k): v for k, v in d.items()}
    pattern = re.compile("|".join(rep.keys()))
    return pattern.sub(lambda m: rep[re.escape(m.group(0))], s)


# NOTE: python internally compiles and caches regex expressions, so no need to compile first
def get_metabolite_ids_from_reaction_formula(reaction_formula: str):  # not used anymore, remove?
    '''strips all pure numeric words (stoichiometries) and arrows, returns a list of metabolite identifiers'''
    return re.sub("^\d+\s|\s\d+\s|\s\d+$|<(-+|=+)>|(-+|=+)>|<(-+|=+)|\s\+\s", ' ', reaction_formula).split()


def igetattr(obj, attr):
    for a in dir(obj):
        if a.lower() == attr.lower():
            return getattr(obj, a)


def get_method_kwargs(kwargs):  # bit experimental
    return {k: v for k, v in kwargs.items() if not (k.startswith('__') and k.endswith('__')) and not k == 'self'}


def exists(x):
    return x is not None


def snake_case(s: str):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', s)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()


def upper_camel_case(s: str):
    return ''.join([x.lower().capitalize() for x in s.split('_')])


def lower_camel_case(s: str):
    return ''.join([x.lower().capitalize() if not i == 0 else x.lower() for i, x in enumerate(s.split('_'))])


def get_truncnorm(mean=0, sd=1, lb=0, ub=10):  # call using .rvs()
    return truncnorm((lb - mean) / sd, (ub - mean) / sd, loc=mean, scale=sd)


def clipper(s: str):
    return re.sub(r'^([RMEP])_+|_([a-z])$', '', s)


def flatten(l: Union[List[list], Iterator[list]]) -> list:
    return [item for sublist in l for item in sublist]


def rec_flatten(l: Union[List[list], Iterator[list]]) -> list:
    return functools.reduce(operator.iadd, map(flatten, l), []) if isinstance(l, list) else [l]


def rec_dd():
    return collections.defaultdict(rec_dd)


def rec_dd_from_d(d):
    """turns a regular nested dict into a recursive default dict"""
    for k, v in d.items():
        if isinstance(v, dict):
            d[k] = rec_dd_from_d(v)

    new_d = rec_dd()
    for k, v in d.items():
        new_d[k] = v
    return new_d


def namedtuplify(mapping, name='NT'):  # thank you https://gist.github.com/hangtwenty/5960435
    """ Convert mappings to namedtuples recursively. """
    if isinstance(mapping, collections.Mapping):
        for key, value in list(mapping.items()):
            mapping[key] = namedtuplify(value)
        return namedtuple_wrapper(name, **mapping)
    elif isinstance(mapping, list):
        return [namedtuplify(item) for item in mapping]
    return mapping


def namedtuple_wrapper(name, **kwargs):
    wrap = collections.namedtuple(name, kwargs)
    return wrap(**kwargs)


def get_size(obj, seen=None):
    """Recursively finds size of objects"""
    size = sys.getsizeof(obj)
    if seen is None:
        seen = set()
    obj_id = id(obj)
    if obj_id in seen:
        return 0
    # Important mark as seen *before* entering recursion to gracefully handle self-referential objects
    seen.add(obj_id)
    if isinstance(obj, dict):
        size += sum([get_size(v, seen) for v in obj.values()])
        size += sum([get_size(k, seen) for k in obj.keys()])
    elif hasattr(obj, '__dict__'):
        size += get_size(obj.__dict__, seen)
    elif hasattr(obj, '__iter__') and not isinstance(obj, (str, bytes, bytearray)):
        size += sum([get_size(i, seen) for i in obj])
    return size


def split_every(n, iterator):
    """split an iterator into pieces of size n"""
    return itertools.takewhile(bool, (list(itertools.islice(iterator, n)) for _ in itertools.repeat(None)))


def md5_sum(s: str):
    return hashlib.md5(s.encode('utf-8')).hexdigest()


# Logger
def setup_logger(directory, script_name):
    formatter = logging.Formatter(fmt='%(asctime)s - %(module)s - %%(lineno)s - %(levelname)s - %(message)s')

    logging.captureWarnings(True)
    logger = logging.getLogger(__name__)
    logger.setLevel(logging.DEBUG)

    file_handler = TimedRotatingFileHandler(directory + '/' + script_name + '.log', backupCount=5)
    file_handler.setLevel(logging.DEBUG)
    file_handler.setFormatter(formatter)

    stream_handler = logging.StreamHandler()
    stream_handler.setFormatter(formatter)
    stream_handler.setLevel(logging.ERROR)

    logger.addHandler(file_handler)
    logger.addHandler(stream_handler)

    return logger


# Wrappers
def cache_file(file_path, use_cached=True):
    def decorator(func):
        def wrapped(*args, **kwargs):
            if os.path.exists(file_path) and use_cached:
                with open(file_path, 'rb') as cache_handle:
                    return pickle.load(cache_handle)

            res = func(*args, **kwargs)

            with open(file_path, 'wb') as cache_handle:
                pickle.dump(res, cache_handle, protocol=pickle.HIGHEST_PROTOCOL)
            return res

        return wrapped

    return decorator


def retry(times):
    def wrapper_fn(f):
        @functools.wraps(f)
        def new_wrapper(*args, **kwargs):
            for i in range(times):
                try:
                    return f(*args, **kwargs)
                except Exception as e:
                    message = e.args[0]
                    message += f"\nTried {times} times"
                    e.args = (message,)
                    raise e

        return new_wrapper

    return wrapper_fn


def timer(wrapped):
    @functools.wraps(wrapped)
    def _wrapper(*args, **kwargs):
        t = time.time()
        ret = wrapped(*args, **kwargs)
        print('runtime: {}'.format(time.time() - t))
        return ret

    return _wrapper


def memory(wrapped):
    @functools.wraps(wrapped)
    def _wrapper(*args, **kwargs):
        ret = wrapped(*args, **kwargs)
        size = sys.getsizeof(ret)
        print('memory usage: {}'.format(size))
        return ret

    return _wrapper


def memoize(func):
    cache = {}
    func_sig = inspect.signature(func)

    @functools.wraps(func)
    def cached(*args, **kwargs):
        bound = func_sig.bind(*args, **kwargs)
        key = frozenset(bound.arguments.items())

        if key not in cache:
            cache[key] = func(*args, **kwargs)
        return cache[key]

    return cached


def recursion_depth_counter(func):
    recursion_depth_counter.level = 0

    def wrapper(*args, **kwargs):
        print("In:", recursion_depth_counter.level)
        recursion_depth_counter.level += 1
        result = func(*args, **kwargs)
        recursion_depth_counter.level -= 1
        print("Out:", recursion_depth_counter.level)
        return result

    return wrapper


def n_times(n):
    assert n > 1, f'n must be strictly positive'

    def inner(func):

        def wrapper(*args, **kwargs):
            for _ in range(n):
                rv = func(*args, **kwargs)
            return rv

        return wrapper

    return inner


def profile(func):
    """A decorator that uses cProfile to profile a function"""

    def inner(*args, **kwargs):
        pr = cProfile.Profile()
        pr.enable()
        retval = func(*args, **kwargs)
        pr.disable()
        s = io.StringIO()
        sortby = 'cumulative'
        ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
        ps.print_stats()
        print(s.getvalue())
        return retval

    return inner


def for_all_methods(decorator, exclude=None):
    """
    decorate all methods of a specific class
    """
    if exclude is None:
        exclude = []

    def decorate(cls):
        for attr in cls.__dict__:
            if callable(getattr(cls, attr)) and attr not in exclude:
                setattr(cls, attr, decorator(getattr(cls, attr)))
        return cls

    return decorate


# uncertainty float utility functions
def ufloat_nanmean(ufloats):
    return _ufloat_nancore(ufloats, np.nanmean)


def ufloat_nansum(ufloats):
    return _ufloat_nancore(ufloats, np.nansum)


def ufloat_nanmin(ufloats):
    return _ufloat_nancore(ufloats, np.nanmin)


def _ufloat_nancore(ufloats, f):
    """deals with nan values in operations on uncertainty floats"""
    not_nan = [x for x in ufloats if (~np.isnan(x.s) & ~np.isnan(x.n))]
    if not_nan:
        return f(not_nan)
    else:
        not_nan_mean = [x for x in ufloats if ~np.isnan(x.n)]
        if not_nan_mean:
            return ufloat(f([x.n for x in not_nan_mean]), np.nan)
        else:
            return ufloat(np.nan, np.nan)


def merge_to_ufloat_df(mean_df, std_df):
    return mean_df.applymap(lambda x: ufloat(x, 0)) + std_df.applymap(lambda x: ufloat(0, x))


def split_ufloat_df(ufloat_df):
    # y, y_err = split_ufloat_df(ufloat_df)
    return (pd.DataFrame(np.array(x).reshape(ufloat_df.shape),
                         index=ufloat_df.index, columns=ufloat_df.columns)
            for x in zip(*[(v.n, v.s) for v in ufloat_df.values.ravel()]))


# Calculating means
def weighted_mean(values, weights) -> ufloat:
    average = np.average(values, weights=weights)
    variance = np.average((values - average) ** 2, weights=weights)
    return ufloat(average, np.sqrt(variance))


def geometric_mean(values: np.array) -> ufloat:
    gm = np.exp(np.mean(np.log(values)))
    gsd = np.exp(np.std(np.log(values)))
    return ufloat(gm, gsd)


def harmonic_mean(values: np.array) -> ufloat:
    hm = 1 / np.mean(1 / values)
    hsd = np.sqrt((np.mean(1 / values)) ** (-4) * np.var(1 / values) / len(values))
    return ufloat(hm, hsd)


def weighted_geometric_mean(values: np.array, weights: np.array) -> ufloat:
    weighted_log_mean = np.log(np.multiply(values, weights) / weights.mean())
    w_gm = np.exp(np.mean(weighted_log_mean))
    w_gsd = np.exp(np.std(weighted_log_mean))
    return ufloat(w_gm, w_gsd)


# Dictionary utilities
def invert_dict(d: dict):
    return {v: k for k, v in d.items()}


def invert_dict_nonunique(d: dict):
    newdict = collections.defaultdict(lambda: [])
    [newdict[v].append(k) for k, v in d.items()]
    return dict(newdict)


def invert_dol(d):  # invert dict of lists with unique hashable items
    return dict((v, k) for k in d for v in d[k])


def invert_dol_nonunique(d):  # invert dict of non-unique hashable items
    newdict = collections.defaultdict(lambda: [])
    for k in d:
        [newdict[v].append(k) for v in d[k]]
    return dict(newdict)


def flatten_dict(d, with_prefixed_keys: bool = True):  # functional-style
    def items():
        for key, value in d.items():
            if isinstance(value, dict):
                for subkey, subvalue in flatten_dict(value).items():
                    if with_prefixed_keys == True:
                        yield key + "." + subkey, subvalue
                    elif with_prefixed_keys == False:
                        yield subkey, subvalue
                    else:
                        raise ValueError('with_prefixed_keys must be True or False')
            else:
                yield key, value

    return dict(items())


# interactive plotting
class LineGraph:

    def __init__(self, figsize, subplots):
        # Disable toolbar - optional
        plt.rcParams['toolbar'] = 'None'
        self.fig, _ = plt.subplots(*subplots, figsize=figsize)
        # plt.xlabel('x-axis')
        # plt.ylabel('y-axis')
        # plt.suptitle(title, fontsize=12)

    def update_annot(self, line, annot, label):
        x, y = line.get_data()
        x = x[self.ind["ind"][0]]
        y = y[self.ind["ind"][0]]
        annot.xy = (x, y)
        annot.set_text(label)
        annot.get_bbox_patch().set_alpha(0.8)

    def hover(self, event, line, annot, label):
        vis = annot.get_visible()
        for ax in self.fig.axes:
            if event.inaxes == ax:
                cont, self.ind = line.contains(event)
                if cont:
                    self.update_annot(line, annot, label)
                    annot.set_visible(True)
                    self.fig.canvas.draw_idle()
                else:
                    if vis:
                        annot.set_visible(False)
                        self.fig.canvas.draw_idle()

    def plot_line(self, x, y, label, ax_idx):
        line, = self.fig.axes[ax_idx].plot(x, y, marker=None)
        annot = self.fig.axes[ax_idx].annotate("", xy=(0, 0), xytext=(-20, 20),
                                               textcoords="offset points",
                                               bbox=dict(boxstyle="round", fc="w"),
                                               arrowprops=dict(arrowstyle="->"))
        annot.set_visible(False)
        self.fig.canvas.mpl_connect("motion_notify_event",
                                    lambda event: self.hover(event, line, annot, label))

    def show_graph(self):
        plt.show()


def try_cast_to_int(number: float):
    """This is useful when working with sympy: https://code.google.com/archive/p/sympy/issues/1374#c1"""
    if number.is_integer():
        return int(number)
    return number


def eqs2matrix(eqs, syms, augment=False):
    """
    >> eqs
    [x + 2*y == 4, 2*c + y/2 == 0]
    >> eqs2matrix(eqs, (x, c))
    (Matrix([
    [1, 0],
    [0, 2]]), Matrix([
    [-2*y + 4],
    [    -y/2]]))
    >> eqs2matrix([2*c*(x+y)-4],(x, y))
    (Matrix([[2*c, 2*c]]), Matrix([[4]]))
    """
    s = sympy.Matrix([si.lhs - si.rhs if isinstance(si, sympy.Equality) else si for si in eqs])
    j = s.jacobian(syms)
    rhs = -(s - j * sympy.Matrix(syms))
    rhs.simplify()
    if augment:
        j.col_insert(0, rhs)
    else:
        j = (j, rhs)
    return j


def is_invertible(matrix):
    return matrix.shape[0] == matrix.shape[1] and np.linalg.matrix_rank(matrix) == matrix.shape[0]


def nullspace(A, atol=1e-13, rtol=0):
    """Compute an approximate basis for the nullspace of A.
    The algorithm used by this function is based on the singular value
    decomposition of `A`.
    Parameters
    ----------
    A : numpy.ndarray
        A should be at most 2-D.  A 1-D array with length k will be treated
        as a 2-D with shape (1, k)
    atol : float
        The absolute tolerance for a zero singular value.  Singular values
        smaller than `atol` are considered to be zero.
    rtol : float
        The relative tolerance.  Singular values less than rtol*smax are
        considered to be zero, where smax is the largest singular value.
    If both `atol` and `rtol` are positive, the combined tolerance is the
    maximum of the two; that is::
    tol = max(atol, rtol * smax)
    Singular values smaller than `tol` are considered to be zero.
    Returns
    -------
    numpy.ndarray
        If `A` is an array with shape (m, k), then `ns` will be an array
        with shape (k, n), where n is the estimated dimension of the
        nullspace of `A`.  The columns of `ns` are a basis for the
        nullspace; each element in numpy.dot(A, ns) will be approximately
        zero.
    Notes
    -----
    Taken from the numpy cookbook.
    """
    A = np.atleast_2d(A)
    u, s, vh = np.linalg.svd(A)
    tol = max(atol, rtol * s[0])
    nnz = (s >= tol).sum()
    ns = vh[nnz:].conj().T
    return ns


# data parsing
@cache_file(os.path.join(settings.CACHE_DIR, 'ncbi_taxonomy_id_to_scientific_name.p'))
def get_taxonomy_id_to_scientific_name():
    df = pd.read_csv('/Users/Zarathustra/ETH/repositories/sbml_tools/data/ncbi/taxdump/names.dmp',
                     index_col=0, error_bad_lines=False, sep='\t')
    d = df[df.synonym == 'scientific name']['all'].to_dict()
    return d


def get_miriam_table():
    """used for parsing MIRIAM identifiers from annotations"""
    with open(settings.MIRIAM_FILE, 'r+', encoding='utf-8') as f:
        d = xmltodict.parse(f.read(), process_namespaces=True)
    l = d['http://www.biomodels.net/MIRIAM/:miriam']['http://www.biomodels.net/MIRIAM/:datatype']
    # shared_keys = list(set.intersection(*map(set, [list(x.keys()) for x in l])))
    relevant_keys = ['@id', '@pattern', 'http://www.biomodels.net/MIRIAM/:namespace',
                     'http://www.biomodels.net/MIRIAM/:name', 'http://www.biomodels.net/MIRIAM/:definition']
    data = [[d[key] for key in relevant_keys] for d in l]
    columns = [re.sub('http://www.biomodels.net/MIRIAM/:|@', '', x) for x in relevant_keys]
    return pd.DataFrame(data, columns=columns)


def get_sbo_table():
    """table of systems biology ontology identifiers. Index aligns with identifiers (asserted)"""
    with open(settings.SBO_FILE, 'r', encoding='utf-8') as f:
        d = xmltodict.parse(f.read(), process_namespaces=True)
    df = pd.DataFrame(d['http://www.biomodels.net/sbo:sbo']['http://www.biomodels.net/sbo:Term'])
    df.columns = df.columns.str.split(':').str[-1]
    # unpacking single entry dict
    df['def'] = df['def'].apply(lambda x: list(x.values())[0] if isinstance(x, dict) else '')
    # NOTE: sbo term 351 does not exist
    SBO351 = {'id': 'SBO:0000351', 'name': '', 'comment': 'manual added, does not exist', 'def': ''}
    df = df.append(SBO351, ignore_index=True).sort_values('id').reset_index(drop=True)
    assert all(df.index == df['id'].str[-3:].astype(int)), f'index does not match SBO term ids'  # sanity check
    return df


NAME_TO_SBO_TERM = get_sbo_table().reset_index().set_index('name')['index'].to_dict()


def get_pb_prior():  # as per file with the original parameter balancing software of Lubitz (2018)
    # note that sbo annotation enzyme and metabolite refer to concentrations
    df = pd.read_csv(settings.PB_PRIOR_FILE, sep='\t', header=1)
    df.columns = [snake_case(x.strip('!')).strip() for x in df.columns]
    df.set_index('symbol', inplace=True)
    df.index = df.index.str.lower()
    # df['sbo_term_name'] = [SBO_MAPPING.get(i) for i in df.index]
    # df['sbo_term'] = [NAME_TO_SBO_TERM.get(sbo_term_name, -1) for sbo_term_name in df['sbo_term_name']]
    return df


@cache_file(os.path.join(settings.CACHE_DIR, 'bigg_metabolite_identifier_mapping.p'))
def get_bigg_metabolite_identifier_mapping():
    manual = {}
    manual.update({'q8h2_c': ['C00390']})
    manual.update({'q8_c': ['C00399']})

    '''returns a dataframe with lists of identifiers'''
    infile = settings.BIGG_METABOLITE_FILE
    df = pd.read_csv(infile, sep='\t', index_col=0).fillna('')
    d = dict()
    for i, row in df.iterrows():
        mapping = collections.defaultdict(list)
        data = df.loc[i, :]['database_links']
        if isinstance(data, pd.Series):
            data = ''  # catch double entry oddity in met maps (write log)
        identifiers = data.split('; ')
        for identifier in identifiers:
            database = identifier.split(':')[0]
            value = identifier.split('/')[-1]
            mapping[database].append(value)
        if i in manual:
            mapping['KEGG Compound'].extend(manual[i])
        d[i] = mapping
    df = pd.DataFrame.from_dict(d, orient='index').drop('', axis=1)

    # here we adopt the proper MIRIAM namespace identifier
    columns = {
        'BioCyc': 'biocyc',
        'CHEBI': 'chebi',
        'Human Metabolome Database': 'hmdb',
        'KEGG Compound': 'kegg.compound',
        'KEGG Drug': 'kegg.drug',
        'LipidMaps': 'lipidmaps',
        'MetaNetX (MNX) Chemical': 'metanetx.chemical',
        'Reactome': 'reactome',
        'SEED Compound': 'seed.compound',
    }
    df.rename(columns=columns, inplace=True)
    df.index = df.index.map(clipper)
    # df.applymap(lambda x: [] if not x==x else x) # replaces nan with []
    bigg_metabolites = df.loc[~df.index.duplicated(keep='first')]  # TODO: fillna with list, np.unique on columns
    return bigg_metabolites


@cache_file(os.path.join(settings.CACHE_DIR, 'bigg_reaction_idenfitier_mapping.p'))
def get_bigg_reaction_identifier_mapping():
    manual = {}
    # a few manual fixes for missing EC numbers in the model
    manual.update({'FHL': ['1.1.99.33']})
    manual.update({'GDMANE': ['1.1.1.271']})
    # adding more EC numbers for citrate synthase
    # in the model there is only 2.3.3.3, which is citrate (Re)-synthase
    # we add 2.3.3.16 - citrate synthase (unknown stereospecificity)
    # and    2.3.3.1 - citrate (Si)-synthase
    # almost all data in BRENDA is for the unspecific EC number, therefore
    # it will be first
    manual.update({'CS': ['2.3.3.1', '2.3.3.16']})

    # adding the real EC number for Pyruvate Dehydrogenase (PDH)
    # since BiGG only has the number for Dihydrolipoyllysine-residue
    # acetyltransferase which is a transient binding of the acyl
    # group to a modified lysine on the enzyme complex
    manual.update({'pdh': ['1.2.4.1']})

    df = pd.read_csv(settings.BIGG_REACTION_FILE, sep='\t', index_col=0)
    df = df[df.database_links.isna() == False]
    d = dict()

    for i, row in df.iterrows():
        mapping = collections.defaultdict(list)
        data = df.loc[i, :]['database_links']
        identifiers = data.split('; ')
        for identifier in identifiers:
            database = identifier.split(':')[0]
            value = identifier.split('/')[-1]
            mapping[database].append(value)
        if i in manual:
            mapping['EC Number'].extend(manual[i])
        d[i] = mapping

    df = pd.DataFrame.from_dict(d, orient='index')
    # here we adopt the proper MIRIAM namespace identifier
    columns = {
        'BioCyc': 'biocyc',
        'EC Number': 'ec-code',
        'KEGG Reaction': 'kegg.reaction',
        'MetaNetX (MNX) Equation': 'metanetx.reaction',
        'RHEA': 'rhea',
        'Reactome': 'reactome',
    }
    df.rename(columns=columns, inplace=True)
    df.index = df.index.map(clipper)
    bigg_reactions = df.loc[~df.index.duplicated(keep='first')]
    bigg_reactions.reactome = bigg_reactions.reactome.apply(lambda x: [s.split('_')[-1] for s in x] if x == x else x)
    return bigg_reactions


def get_universal_model():
    with open(settings.BIGG_UNIVERSAL_MODEL, 'r') as f:
        data = json.loads(f.read())
    return data


class OrderedDefaultDict(collections.OrderedDict):
    # Source: http://stackoverflow.com/a/6190500/562769
    def __init__(self, default_factory=None, *a, **kw):
        if (default_factory is not None and
                not isinstance(default_factory, collections.Callable)):
            raise TypeError('first argument must be callable')
        collections.OrderedDict.__init__(self, *a, **kw)
        self.default_factory = default_factory

    def __getitem__(self, key):
        try:
            return collections.OrderedDict.__getitem__(self, key)
        except KeyError:
            return self.__missing__(key)

    def __missing__(self, key):
        if self.default_factory is None:
            raise KeyError(key)
        self[key] = value = self.default_factory()
        return value

    def __reduce__(self):
        if self.default_factory is None:
            args = tuple()
        else:
            args = self.default_factory,
        return type(self), args, None, None, self.items()

    def copy(self):
        return self.__copy__()

    def __copy__(self):
        return type(self)(self.default_factory, self)

    def __deepcopy__(self, memo):
        import copy
        return type(self)(self.default_factory, copy.deepcopy(self.items()))

    def __repr__(self):
        return 'OrderedDefaultDict(%s, %s)' % (self.default_factory,
                                               collections.OrderedDict.__repr__(self))


# decorator to enforce attributes to be of a specific type - pycharm's autocomplete doesn't like it
def getter_setter_gen(name, type_):
    def getter(self):
        return getattr(self, "__" + name)

    def setter(self, value):
        if not isinstance(value, type_):
            raise TypeError("%s attribute must be set to an instance of %s" % (name, type_))
        setattr(self, "__" + name, value)

    return property(getter, setter)


def auto_attr_check(cls):
    """decorator to enforce attributes to be of a specific type"""
    new_dct = {}
    for key, value in cls.__dict__.items():
        if isinstance(value, type):
            value = getter_setter_gen(key, value)
        new_dct[key] = value
    # Creates a new class, using the modified dictionary as the class dict:
    return type(cls)(cls.__name__, cls.__bases__, new_dct)


# @auto_attr_check
# class Foo(object):
#     bar = int
#     baz = str
#     bam = float


class ReadOnlyDict(collections.Mapping):

    def __init__(self, data: dict):
        self._data = data

    def __getitem__(self, key):
        return self._data[key]

    def __len__(self):
        return len(self._data)

    def __iter__(self):
        return iter(self._data)


class FrozenClass:
    """
    a class inheriting this can call the ._freeze() method, and thereby ensure that no more attributes can be set.
    This is useful when using setters, to prevent setting an attribute whilst thinking that one is using a setter.
    """
    __is_frozen = False

    def __setattr__(self, attr, value):
        if self.__is_frozen and not hasattr(self, attr):
            raise TypeError(f"cannot set attribute {attr}, as it's not part of this instance of {type(self)}")
        object.__setattr__(self, attr, value)

    def _freeze(self):
        self.__is_frozen = True


class FrozenKeyDict(collections.Mapping):
    """
    Mapping which doesn't allow keys to be added/deleted.
    It does allow existing key/value pairs to be modified.
    """

    def __init__(self, *args, **kwargs):
        self._d = dict(*args, **kwargs)
        self._hash = None
        self._frozen = True

    def __iter__(self):
        return iter(self._d)

    def __len__(self):
        return len(self._d)

    def __getitem__(self, key):
        return self._d[key]

    def __setitem__(self, key, value):
        if self._frozen and key not in self._d:
            raise KeyError('must be one of %s' % list(self))
        self._d[key] = value

    def __delitem__(self, key):
        # modify to suit your needs ...
        raise KeyError('Removing keys not supported')

    def __hash__(self):
        # It would have been simpler and maybe more obvious to
        # use hash(tuple(sorted(self._d.items()))),
        # but this solution is O(n). I don't know what kind of
        # n we are going to run into, but sometimes it's hard to resist the
        # urge to optimize when it will gain improved algorithmic performance.
        if self._hash is None:
            hash_ = 0
            for pair in self.items():
                hash_ ^= hash(pair)
            self._hash = hash_
        return self._hash


class MathematicalOperators:
    """
    base class for mathematical operators
    TODO considerations:
     - make ABC
     - some special methods should probably raise a NotImplementedError instead of return NotImplemented
    """

    # relational operators
    def __lt__(self, other):
        # < operator
        return NotImplemented

    def __le__(self, other):
        # <= operator
        return NotImplemented

    def __eq__(self, other):
        # == operator
        return NotImplemented

    def __ne__(self, other):
        # != operator
        return NotImplemented

    def __gt__(self, other):
        # > operator
        return NotImplemented

    def __ge__(self, other):
        # >= operator
        return NotImplemented

    # logical operators
    def __and__(self, other):
        # & operator
        return NotImplemented

    def __rand__(self, other):
        return NotImplemented

    def __iand__(self, other):
        # &= operator
        return NotImplemented

    def __or__(self, other):
        # ^ operator
        return NotImplemented

    def __ror__(self, other):
        return NotImplemented

    def __ior__(self, other):
        return NotImplemented

    def __xor__(self, other):
        # | operator
        return NotImplemented

    def __rxor__(self, other):
        return NotImplemented

    def __ixor__(self, other):
        # |= operator
        return NotImplemented

    def __lshift__(self, other):
        # << operator
        return NotImplemented

    def __rlshift__(self, other):
        return NotImplemented

    def __ilshift__(self, other):
        # <<= operator
        return NotImplemented

    def __rshift__(self, other):
        # >> operator
        return NotImplemented

    def __rrshift__(self, other):
        return NotImplemented

    def __irshift__(self, other):
        # >>= operator
        return NotImplemented

    # unary arithmetic operators
    def __pos__(self):
        # unary + operator
        return NotImplemented

    def __neg__(self):
        # unary - operator
        return NotImplemented

    def __abs__(self):
        # abs() operator
        return NotImplemented

    def __invert__(self):
        # ~ operator
        return NotImplemented

    def __index__(self):
        # operator.index() operator
        return NotImplemented

    def __int__(self):
        # int() operator
        return NotImplemented

    def __float__(self):
        # float() operator
        return NotImplemented

    def __complex__(self):
        # complex() operator
        return NotImplemented

    def __round__(self):
        # round() operator
        return NotImplemented

    def __trunc__(self):
        # math.trunc() operator
        return NotImplemented

    def __ceil__(self):
        # math.ceil() operator
        return NotImplemented

    def __floor__(self, other):
        # math.floor() operator
        return NotImplemented

    # arithmetic operators
    def __add__(self, other):
        # + operator
        return NotImplemented

    def __radd__(self, other):
        return NotImplemented

    def __iadd__(self, other):
        # += operator
        return NotImplemented

    def __sub__(self, other):
        # - operator
        return NotImplemented

    def __rsub__(self, other):
        return NotImplemented

    def __isub__(self, other):
        # -= operator
        return NotImplemented

    def __mul__(self, other):
        # * operator
        return NotImplemented

    def __rmul__(self, other):
        return NotImplemented

    def __imul__(self, other):
        # *= operator
        return NotImplemented

    def __matmul__(self, other):
        # @ operator
        return NotImplemented

    def __rmatmul__(self, other):
        return NotImplemented

    def __imatmul__(self, other):
        # @= operator
        return NotImplemented

    def __truediv__(self, other):
        # / operator
        return NotImplemented

    def __rtruediv__(self, other):
        return NotImplemented

    def __itruediv__(self, other):
        # /= operator
        return NotImplemented

    def __floordiv__(self, other):
        # // operator
        return NotImplemented

    def __rfloordiv__(self, other):
        return NotImplemented

    def __ifloordiv__(self, other):
        # //= operator
        return NotImplemented

    def __mod__(self, other):
        return NotImplemented

    def __rmod__(self, other):
        return NotImplemented

    def __imod__(self, other):
        # %= operator
        return NotImplemented

    def __pow__(self, power, modulo=None):
        # ** operator
        return NotImplemented

    def __rpow__(self, other):
        # Note that ternary pow() will not try calling __rpow__() (the coercion rules would become too complicated)
        return NotImplemented

    def __ipow__(self, other):
        # **= operator
        return NotImplemented

    def __ifloordiv__(self, other):
        # //= operator
        return NotImplemented

    def __mod__(self, other):
        return NotImplemented

    def __rmod__(self, other):
        return NotImplemented

    def __imod__(self, other):
        # %= operator
        return NotImplemented

    def __divmod__(self, other):
        # divmod() operator
        return NotImplemented

    def __rdivmod__(self, other):
        return NotImplemented
